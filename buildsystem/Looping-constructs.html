<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Looping constructs</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Looping constructs">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Looping constructs">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Programming-in-M4sugar.html#Programming-in-M4sugar" rel="up" title="Programming in M4sugar">
<link href="Evaluation-Macros.html#Evaluation-Macros" rel="next" title="Evaluation Macros">
<link href="Conditional-constructs.html#Conditional-constructs" rel="prev" title="Conditional constructs">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Looping-constructs"></a>
<div class="header">
<p>
Next: <a href="Evaluation-Macros.html#Evaluation-Macros" accesskey="n" rel="next">Evaluation Macros</a>, Previous: <a href="Conditional-constructs.html#Conditional-constructs" accesskey="p" rel="prev">Conditional constructs</a>, Up: <a href="Programming-in-M4sugar.html#Programming-in-M4sugar" accesskey="u" rel="up">Programming in M4sugar</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Looping-constructs-1"></a>
<h4 class="subsection">15.6.5 Looping constructs</h4>

<p>The following macros are useful in implementing recursive algorithms in
M4, including loop operations.  An M4 list is formed by quoting a list
of quoted elements; generally the lists are comma-separated, although
<code>m4_foreach_w</code> is whitespace-separated.  For example, the list
&lsquo;<samp>[[a], [b,c]]</samp>&rsquo; contains two elements: &lsquo;<samp>[a]</samp>&rsquo; and &lsquo;<samp>[b,c]</samp>&rsquo;.
It is common to see lists with unquoted elements when those elements are
not likely to be macro names, as in &lsquo;<samp>[fputc_unlocked,
fgetc_unlocked]</samp>&rsquo;.
</p>
<p>Although not generally recommended, it is possible for quoted lists to
have side effects; all side effects are expanded only once, and prior to
visiting any list element.  On the other hand, the fact that unquoted
macros are expanded exactly once means that macros without side effects
can be used to generate lists.  For example,
</p>
<div class="example">
<pre class="example">m4_foreach([i], [[1], [2], [3]m4_errprintn([hi])], [i])
error&rarr;hi
&rArr;123
m4_define([list], [[1], [2], [3]])
&rArr;
m4_foreach([i], [list], [i])
&rArr;123
</pre></div>

<dl>
<dt><a name="index-m4_005fargn-1"></a>Macro: <strong>m4_argn</strong> <em>(<var>n</var>, <span class="roman">[</span><var>arg</var><span class="roman">]</span>&hellip;)</em></dt>
<dd><a name="index-m4_005fargn"></a>
<p>Extracts argument <var>n</var> (larger than 0) from the remaining arguments.
If there are too few arguments, the empty string is used.  For any
<var>n</var> besides 1, this is more efficient than the similar
&lsquo;<samp>m4_car(m4_shiftn([<var>n</var>], [], [<var>arg</var>&hellip;]))</samp>&rsquo;.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fcar-1"></a>Macro: <strong>m4_car</strong> <em>(<var>arg</var>&hellip;)</em></dt>
<dd><a name="index-m4_005fcar"></a>
<p>Expands to the quoted first <var>arg</var>.  Can be used with <code>m4_cdr</code>
to recursively iterate
through a list.  Generally, when using quoted lists of quoted elements,
<code>m4_car</code> should be called without any extra quotes.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fcdr-1"></a>Macro: <strong>m4_cdr</strong> <em>(<var>arg</var>&hellip;)</em></dt>
<dd><a name="index-m4_005fcdr"></a>
<p>Expands to a quoted list of all but the first <var>arg</var>, or the empty
string if there was only one argument.  Generally, when using quoted
lists of quoted elements, <code>m4_cdr</code> should be called without any
extra quotes.
</p>
<p>For example, this is a simple implementation of <code>m4_map</code>; note how
each iteration checks for the end of recursion, then merely applies the
first argument to the first element of the list, then repeats with the
rest of the list.  (The actual implementation in M4sugar is a bit more
involved, to gain some speed and share code with <code>m4_map_sep</code>, and
also to avoid expanding side effects in &lsquo;<samp>$2</samp>&rsquo; twice).
</p><div class="example">
<pre class="example">m4_define([m4_map], [m4_ifval([$2],
  [m4_apply([$1], m4_car($2))[]$0([$1], m4_cdr($2))])])dnl
m4_map([ m4_eval], [[[1]], [[1+1]], [[10],[16]]])
&rArr; 1 2 a
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-m4_005ffor-1"></a>Macro: <strong>m4_for</strong> <em>(<var>var</var>, <var>first</var>, <var>last</var>, <span class="roman">[</span><var>step</var><span class="roman">]</span>,   <var>expression</var>)</em></dt>
<dd><a name="index-m4_005ffor"></a>
<p>Loop over the numeric values between <var>first</var> and <var>last</var>
including bounds by increments of <var>step</var>.  For each iteration,
expand <var>expression</var> with the numeric value assigned to <var>var</var>.
If <var>step</var> is omitted, it defaults to &lsquo;<samp>1</samp>&rsquo; or &lsquo;<samp>-1</samp>&rsquo; depending
on the order of the limits.  If given, <var>step</var> has to match this
order.  The number of iterations is determined independently from
definition of <var>var</var>; iteration cannot be short-circuited or
lengthened by modifying <var>var</var> from within <var>expression</var>.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fforeach-1"></a>Macro: <strong>m4_foreach</strong> <em>(<var>var</var>, <var>list</var>, <var>expression</var>)</em></dt>
<dd><a name="index-m4_005fforeach"></a>
<p>Loop over the comma-separated M4 list <var>list</var>, assigning each value
to <var>var</var>, and expand <var>expression</var>.  The following example
outputs two lines:
</p>
<div class="example">
<pre class="example">m4_foreach([myvar], [[foo], [bar, baz]],
           [echo myvar
])dnl
&rArr;echo foo
&rArr;echo bar, baz
</pre></div>

<p>Note that for some forms of <var>expression</var>, it may be faster to use
<code>m4_map_args</code>.
</p></dd></dl>

<a name="m4_005fforeach_005fw"></a><dl>
<dt><a name="index-m4_005fforeach_005fw-1"></a>Macro: <strong>m4_foreach_w</strong> <em>(<var>var</var>, <var>list</var>, <var>expression</var>)</em></dt>
<dd><a name="index-m4_005fforeach_005fw"></a>
<p>Loop over the white-space-separated list <var>list</var>, assigning each value
to <var>var</var>, and expand <var>expression</var>.  If <var>var</var> is only
referenced once in <var>expression</var>, it is more efficient to use
<code>m4_map_args_w</code>.
</p>
<p>The deprecated macro <code>AC_FOREACH</code> is an alias of
<code>m4_foreach_w</code>.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fmap-1"></a>Macro: <strong>m4_map</strong> <em>(<var>macro</var>, <var>list</var>)</em></dt>
<dt><a name="index-m4_005fmapall-1"></a>Macro: <strong>m4_mapall</strong> <em>(<var>macro</var>, <var>list</var>)</em></dt>
<dt><a name="index-m4_005fmap_005fsep-1"></a>Macro: <strong>m4_map_sep</strong> <em>(<var>macro</var>, <var>separator</var>, <var>list</var>)</em></dt>
<dt><a name="index-m4_005fmapall_005fsep-1"></a>Macro: <strong>m4_mapall_sep</strong> <em>(<var>macro</var>, <var>separator</var>, <var>list</var>)</em></dt>
<dd><a name="index-m4_005fmap"></a>
<a name="index-m4_005fmapall"></a>
<a name="index-m4_005fmap_005fsep"></a>
<a name="index-m4_005fmapall_005fsep"></a>
<p>Loop over the comma separated quoted list of argument descriptions in
<var>list</var>, and invoke <var>macro</var> with the arguments.  An argument
description is in turn a comma-separated quoted list of quoted elements,
suitable for <code>m4_apply</code>.  The macros <code>m4_map</code> and
<code>m4_map_sep</code> ignore empty argument descriptions, while
<code>m4_mapall</code> and <code>m4_mapall_sep</code> invoke <var>macro</var> with no
arguments.  The macros <code>m4_map_sep</code> and <code>m4_mapall_sep</code>
additionally expand <var>separator</var> between invocations of <var>macro</var>.
</p>
<p>Note that <var>separator</var> is expanded, unlike in <code>m4_join</code>.  When
separating output with commas, this means that the map result can be
used as a series of arguments, by using a single-quoted comma as
<var>separator</var>, or as a single string, by using a double-quoted comma.
</p>
<div class="example">
<pre class="example">m4_map([m4_count], [])
&rArr;
m4_map([ m4_count], [[],
                     [[1]],
                     [[1], [2]]])
&rArr; 1 2
m4_mapall([ m4_count], [[],
                        [[1]],
                        [[1], [2]]])
&rArr; 0 1 2
m4_map_sep([m4_eval], [,], [[[1+2]],
                            [[10], [16]]])
&rArr;3,a
m4_map_sep([m4_echo], [,], [[[a]], [[b]]])
&rArr;a,b
m4_count(m4_map_sep([m4_echo], [,], [[[a]], [[b]]]))
&rArr;2
m4_map_sep([m4_echo], [[,]], [[[a]], [[b]]])
&rArr;a,b
m4_count(m4_map_sep([m4_echo], [[,]], [[[a]], [[b]]]))
&rArr;1
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-m4_005fmap_005fargs-1"></a>Macro: <strong>m4_map_args</strong> <em>(<var>macro</var>, <var>arg</var>&hellip;)</em></dt>
<dd><a name="index-m4_005fmap_005fargs"></a>
<p>Repeatedly invoke <var>macro</var> with each successive <var>arg</var> as its only
argument.  In the following example, three solutions are presented with
the same expansion; the solution using <code>m4_map_args</code> is the most
efficient.
</p><div class="example">
<pre class="example">m4_define([active], [ACTIVE])dnl
m4_foreach([var], [[plain], [active]], [ m4_echo(m4_defn([var]))])
&rArr; plain active
m4_map([ m4_echo], [[[plain]], [[active]]])
&rArr; plain active
m4_map_args([ m4_echo], [plain], [active])
&rArr; plain active
</pre></div>

<p>In cases where it is useful to operate on additional parameters besides
the list elements, the macro <code>m4_curry</code> can be used in <var>macro</var>
to supply the argument currying necessary to generate the desired
argument list.  In the following example, <code>list_add_n</code> is more
efficient than <code>list_add_x</code>.  On the other hand, using
<code>m4_map_args_sep</code> can be even more efficient.
</p>
<div class="example">
<pre class="example">m4_define([list], [[1], [2], [3]])dnl
m4_define([add], [m4_eval(([$1]) + ([$2]))])dnl
dnl list_add_n(N, ARG...)
dnl Output a list consisting of each ARG added to N
m4_define([list_add_n],
[m4_shift(m4_map_args([,m4_curry([add], [$1])], m4_shift($@)))])dnl
list_add_n([1], list)
&rArr;2,3,4
list_add_n([2], list)
&rArr;3,4,5
m4_define([list_add_x],
[m4_shift(m4_foreach([var], m4_dquote(m4_shift($@)),
  [,add([$1],m4_defn([var]))]))])dnl
list_add_x([1], list)
&rArr;2,3,4
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-m4_005fmap_005fargs_005fpair-1"></a>Macro: <strong>m4_map_args_pair</strong> <em>(<var>macro</var>, <span class="roman">[</span><var>macro-end</var> = <var>macro</var><span class="roman">]</span></em></dt>
<dd><p><var>arg</var>&hellip;)
<a name="index-m4_005fmap_005fargs_005fpair"></a>
For every pair of arguments <var>arg</var>, invoke <var>macro</var> with two
arguments.  If there is an odd number of arguments, invoke
<var>macro-end</var>, which defaults to <var>macro</var>, with the remaining
argument.
</p>
<div class="example">
<pre class="example">m4_map_args_pair([, m4_reverse], [], [1], [2], [3])
&rArr;, 2, 1, 3
m4_map_args_pair([, m4_reverse], [, m4_dquote], [1], [2], [3])
&rArr;, 2, 1, [3]
m4_map_args_pair([, m4_reverse], [, m4_dquote], [1], [2], [3], [4])
&rArr;, 2, 1, 4, 3
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-m4_005fmap_005fargs_005fsep-1"></a>Macro: <strong>m4_map_args_sep</strong> <em>(<span class="roman">[</span><var>pre</var><span class="roman">]</span>, <span class="roman">[</span><var>post</var><span class="roman">]</span>, <span class="roman">[</span><var>sep</var><span class="roman">]</span>, <var>arg</var>&hellip;)</em></dt>
<dd><a name="index-m4_005fmap_005fargs_005fsep"></a>
<p>Expand the sequence <code><var>pre</var>[<var>arg</var>]<var>post</var></code> for each
argument, additionally expanding <var>sep</var> between arguments.  One
common use of this macro is constructing a macro call, where the opening
and closing parentheses are split between <var>pre</var> and <var>post</var>; in
particular, <code>m4_map_args([<var>macro</var>], [<var>arg</var>])</code> is equivalent
to <code>m4_map_args_sep([<var>macro</var>(], [)], [], [<var>arg</var>])</code>.  This
macro provides the most efficient means for iterating over an arbitrary
list of arguments, particularly when repeatedly constructing a macro
call with more arguments than <var>arg</var>.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fmap_005fargs_005fw-1"></a>Macro: <strong>m4_map_args_w</strong> <em>(<var>string</var>, <span class="roman">[</span><var>pre</var><span class="roman">]</span>, <span class="roman">[</span><var>post</var><span class="roman">]</span>, <span class="roman">[</span><var>sep</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fmap_005fargs_005fw"></a>
<p>Expand the sequence <code><var>pre</var>[word]<var>post</var></code> for each word in
the whitespace-separated <var>string</var>, additionally expanding <var>sep</var>
between words.  This macro provides the most efficient means for
iterating over a whitespace-separated string.  In particular,
<code>m4_map_args_w([<var>string</var>], [<var>action</var>(], [)])</code> is more
efficient than <code>m4_foreach_w([var], [<var>string</var>],
[<var>action</var>(m4_defn([var]))])</code>.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fshiftn-1"></a>Macro: <strong>m4_shiftn</strong> <em>(<var>count</var>, &hellip;)</em></dt>
<dt><a name="index-m4_005fshift2-1"></a>Macro: <strong>m4_shift2</strong> <em>(&hellip;)</em></dt>
<dt><a name="index-m4_005fshift3-1"></a>Macro: <strong>m4_shift3</strong> <em>(&hellip;)</em></dt>
<dd><a name="index-m4_005fshift2"></a>
<a name="index-m4_005fshift3"></a>
<a name="index-m4_005fshiftn"></a>
<p><code>m4_shiftn</code> performs <var>count</var> iterations of <code>m4_shift</code>,
along with validation that enough arguments were passed in to match the
shift count, and that the count is positive.  <code>m4_shift2</code> and
<code>m4_shift3</code> are specializations
of <code>m4_shiftn</code>, introduced in Autoconf 2.62, and are more efficient
for two and three shifts, respectively.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fstack_005fforeach-1"></a>Macro: <strong>m4_stack_foreach</strong> <em>(<var>macro</var>, <var>action</var>)</em></dt>
<dt><a name="index-m4_005fstack_005fforeach_005flifo-1"></a>Macro: <strong>m4_stack_foreach_lifo</strong> <em>(<var>macro</var>, <var>action</var>)</em></dt>
<dd><a name="index-m4_005fstack_005fforeach"></a>
<a name="index-m4_005fstack_005fforeach_005flifo"></a>
<p>For each of the <code>m4_pushdef</code> definitions of <var>macro</var>, expand
<var>action</var> with the single argument of a definition of <var>macro</var>.
<code>m4_stack_foreach</code> starts with the oldest definition, while
<code>m4_stack_foreach_lifo</code> starts with the current definition.
<var>action</var> should not push or pop definitions of <var>macro</var>, nor is
there any guarantee that the current definition of <var>macro</var> matches
the argument that was passed to <var>action</var>.  The macro <code>m4_curry</code>
can be used if <var>action</var> needs more than one argument, although in
that case it is more efficient to use <var>m4_stack_foreach_sep</var>.
</p>
<p>Due to technical limitations, there are a few low-level m4sugar
functions, such as <code>m4_pushdef</code>, that cannot be used as the
<var>macro</var> argument.
</p>
<div class="example">
<pre class="example">m4_pushdef([a], [1])m4_pushdef([a], [2])dnl
m4_stack_foreach([a], [ m4_incr])
&rArr; 2 3
m4_stack_foreach_lifo([a], [ m4_curry([m4_substr], [abcd])])
&rArr; cd bcd
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-m4_005fstack_005fforeach_005fsep-1"></a>Macro: <strong>m4_stack_foreach_sep</strong> <em>(<var>macro</var>, <span class="roman">[</span><var>pre</var><span class="roman">]</span>, <span class="roman">[</span><var>post</var><span class="roman">]</span>, <span class="roman">[</span><var>sep</var><span class="roman">]</span>)</em></dt>
<dt><a name="index-m4_005fstack_005fforeach_005fsep_005flifo-1"></a>Macro: <strong>m4_stack_foreach_sep_lifo</strong> <em>(<var>macro</var>, <span class="roman">[</span><var>pre</var><span class="roman">]</span>, <span class="roman">[</span><var>post</var><span class="roman">]</span>,   <span class="roman">[</span><var>sep</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fstack_005fforeach_005fsep"></a>
<a name="index-m4_005fstack_005fforeach_005fsep_005flifo"></a>
<p>Expand the sequence <code><var>pre</var>[definition]<var>post</var></code> for each
<code>m4_pushdef</code> definition of <var>macro</var>, additionally expanding
<var>sep</var> between definitions.  <code>m4_stack_foreach_sep</code> visits the
oldest definition first, while <code>m4_stack_foreach_sep_lifo</code> visits
the current definition first.  This macro provides the most efficient
means for iterating over a pushdef stack.  In particular,
<code>m4_stack_foreach([<var>macro</var>], [<var>action</var>])</code> is short for
<code>m4_stack_foreach_sep([<var>macro</var>], [<var>action</var>(], [)])</code>.
</p></dd></dl>

<div class="header">
<p>
Next: <a href="Evaluation-Macros.html#Evaluation-Macros" accesskey="n" rel="next">Evaluation Macros</a>, Previous: <a href="Conditional-constructs.html#Conditional-constructs" accesskey="p" rel="prev">Conditional constructs</a>, Up: <a href="Programming-in-M4sugar.html#Programming-in-M4sugar" accesskey="u" rel="up">Programming in M4sugar</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
