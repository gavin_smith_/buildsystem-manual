<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Sources</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Sources">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Sources">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Other-Automake-features.html#Other-Automake-features" rel="up" title="Other Automake features">
<link href="Built-Sources-Example.html#Built-Sources-Example" rel="next" title="Built Sources Example">
<link href="Dependencies.html#Dependencies" rel="prev" title="Dependencies">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Sources"></a>
<div class="header">
<p>
Next: <a href="Strictness.html#Strictness" accesskey="n" rel="next">Strictness</a>, Previous: <a href="Dependencies.html#Dependencies" accesskey="p" rel="prev">Dependencies</a>, Up: <a href="Other-Automake-features.html#Other-Automake-features" accesskey="u" rel="up">Other Automake features</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Built-Sources"></a>
<h3 class="section">13.7 Built Sources</h3>

<p>Because Automake&rsquo;s automatic dependency tracking works as a side-effect
of compilation (see <a href="Dependencies.html#Dependencies">Dependencies</a>) there is a bootstrap issue: a
target should not be compiled before its dependencies are made, but
these dependencies are unknown until the target is first compiled.
</p>
<p>Ordinarily this is not a problem, because dependencies are distributed
sources: they preexist and do not need to be built.  Suppose that
<samp>foo.c</samp> includes <samp>foo.h</samp>.  When it first compiles
<samp>foo.o</samp>, <code>make</code> only knows that <samp>foo.o</samp> depends on
<samp>foo.c</samp>.  As a side-effect of this compilation <code>depcomp</code>
records the <samp>foo.h</samp> dependency so that following invocations of
<code>make</code> will honor it.  In these conditions, it&rsquo;s clear there is
no problem: either <samp>foo.o</samp> doesn&rsquo;t exist and has to be built
(regardless of the dependencies), or accurate dependencies exist and
they can be used to decide whether <samp>foo.o</samp> should be rebuilt.
</p>
<p>It&rsquo;s a different story if <samp>foo.h</samp> doesn&rsquo;t exist by the first
<code>make</code> run.  For instance, there might be a rule to build
<samp>foo.h</samp>.  This time <samp>file.o</samp>&rsquo;s build will fail because the
compiler can&rsquo;t find <samp>foo.h</samp>.  <code>make</code> failed to trigger the
rule to build <samp>foo.h</samp> first by lack of dependency information.
</p>
<a name="index-BUILT_005fSOURCES"></a>
<a name="index-BUILT_005fSOURCES_002c-defined"></a>

<p>The <code>BUILT_SOURCES</code> variable is a workaround for this problem.  A
source file listed in <code>BUILT_SOURCES</code> is made on &lsquo;<samp>make all</samp>&rsquo;
or &lsquo;<samp>make check</samp>&rsquo; (or even &lsquo;<samp>make install</samp>&rsquo;) before other
targets are processed.  However, such a source file is not
<em>compiled</em> unless explicitly requested by mentioning it in some
other <code>_SOURCES</code> variable.
</p>
<p>So, to conclude our introductory example, we could use
&lsquo;<samp>BUILT_SOURCES = foo.h</samp>&rsquo; to ensure <samp>foo.h</samp> gets built before
any other target (including <samp>foo.o</samp>) during &lsquo;<samp>make all</samp>&rsquo; or
&lsquo;<samp>make check</samp>&rsquo;.
</p>
<p><code>BUILT_SOURCES</code> is actually a bit of a misnomer, as any file which
must be created early in the build process can be listed in this
variable.  Moreover, all built sources do not necessarily have to be
listed in <code>BUILT_SOURCES</code>.  For instance, a generated <samp>.c</samp> file
doesn&rsquo;t need to appear in <code>BUILT_SOURCES</code> (unless it is included by
another source), because it&rsquo;s a known dependency of the associated
object.
</p>
<p>It might be important to emphasize that <code>BUILT_SOURCES</code> is
honored only by &lsquo;<samp>make all</samp>&rsquo;, &lsquo;<samp>make check</samp>&rsquo; and &lsquo;<samp>make
install</samp>&rsquo;.  This means you cannot build a specific target (e.g.,
&lsquo;<samp>make foo</samp>&rsquo;) in a clean tree if it depends on a built source.
However it will succeed if you have run &lsquo;<samp>make all</samp>&rsquo; earlier,
because accurate dependencies are already available.
</p>
<p>The next section illustrates and discusses the handling of built sources
on a toy example.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Built-Sources-Example.html#Built-Sources-Example" accesskey="1">Built Sources Example</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Several ways to handle built sources.
</td></tr>
</table>

<div class="header">
<p>
Next: <a href="Strictness.html#Strictness" accesskey="n" rel="next">Strictness</a>, Previous: <a href="Dependencies.html#Dependencies" accesskey="p" rel="prev">Dependencies</a>, Up: <a href="Other-Automake-features.html#Other-Automake-features" accesskey="u" rel="up">Other Automake features</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
