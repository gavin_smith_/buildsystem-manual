<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Objects created both with libtool and without</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Objects created both with libtool and without">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Objects created both with libtool and without">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Libtool-Issues.html#Libtool-Issues" rel="up" title="Libtool Issues">
<link href="Documentation.html#Documentation" rel="next" title="Documentation">
<link href="Error-required-file-ltmain_002esh-not-found.html#Error-required-file-ltmain_002esh-not-found" rel="prev" title="Error required file ltmain.sh not found">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Objects-created-both-with-libtool-and-without"></a>
<div class="header">
<p>
Previous: <a href="Error-required-file-ltmain_002esh-not-found.html#Error-required-file-ltmain_002esh-not-found" accesskey="p" rel="prev">Error required file ltmain.sh not found</a>, Up: <a href="Libtool-Issues.html#Libtool-Issues" accesskey="u" rel="up">Libtool Issues</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Objects-created-with-both-libtool-and-without"></a>
<h4 class="subsubsection">7.3.9.2 Objects &lsquo;<samp>created with both libtool and without</samp>&rsquo;</h4>

<p>Sometimes, the same source file is used both to build a libtool
library and to build another non-libtool target (be it a program or
another library).
</p>
<p>Let&rsquo;s consider the following <samp>Makefile.am</samp>.
</p>
<div class="example">
<pre class="example">bin_PROGRAMS = prog
prog_SOURCES = prog.c foo.c &hellip;

lib_LTLIBRARIES = libfoo.la
libfoo_la_SOURCES = foo.c &hellip;
</pre></div>

<p>(In this trivial case the issue could be avoided by linking
<samp>libfoo.la</samp> with <samp>prog</samp> instead of listing <samp>foo.c</samp> in
<code>prog_SOURCES</code>.  But let&rsquo;s assume we really want to keep
<samp>prog</samp> and <samp>libfoo.la</samp> separate.)
</p>
<p>Technically, it means that we should build <samp>foo.$(OBJEXT)</samp> for
<samp>prog</samp>, and <samp>foo.lo</samp> for <samp>libfoo.la</samp>.  The problem is
that in the course of creating <samp>foo.lo</samp>, libtool may erase (or
replace) <samp>foo.$(OBJEXT)</samp>, and this cannot be avoided.
</p>
<p>Therefore, when Automake detects this situation it will complain
with a message such as
</p><div class="example">
<pre class="example">object 'foo.$(OBJEXT)' created both with libtool and without
</pre></div>

<p>A workaround for this issue is to ensure that these two objects get
different basenames.  As explained in <a href="Program-and-Library-Variables.html#Renamed-Objects">Renamed Objects</a>, this
happens automatically when per-targets flags are used.
</p>
<div class="example">
<pre class="example">bin_PROGRAMS = prog
prog_SOURCES = prog.c foo.c &hellip;
prog_CFLAGS = $(AM_CFLAGS)

lib_LTLIBRARIES = libfoo.la
libfoo_la_SOURCES = foo.c &hellip;
</pre></div>

<p>Adding &lsquo;<samp>prog_CFLAGS = $(AM_CFLAGS)</samp>&rsquo; is almost a no-op, because
when the <code>prog_CFLAGS</code> is defined, it is used instead of
<code>AM_CFLAGS</code>.  However as a side effect it will cause
<samp>prog.c</samp> and <samp>foo.c</samp> to be compiled as
<samp>prog-prog.$(OBJEXT)</samp> and <samp>prog-foo.$(OBJEXT)</samp>, which solves
the issue.
</p>



</body>
</html>
