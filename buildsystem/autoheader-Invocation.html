<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: autoheader Invocation</title>

<meta name="description" content="Automake and Autoconf Reference Manual: autoheader Invocation">
<meta name="keywords" content="Automake and Autoconf Reference Manual: autoheader Invocation">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Defining-Symbols.html#Defining-Symbols" rel="up" title="Defining Symbols">
<link href="Autoheader-Macros.html#Autoheader-Macros" rel="next" title="Autoheader Macros">
<link href="Header-Templates.html#Header-Templates" rel="prev" title="Header Templates">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="autoheader-Invocation"></a>
<div class="header">
<p>
Next: <a href="Autoheader-Macros.html#Autoheader-Macros" accesskey="n" rel="next">Autoheader Macros</a>, Previous: <a href="Header-Templates.html#Header-Templates" accesskey="p" rel="prev">Header Templates</a>, Up: <a href="Defining-Symbols.html#Defining-Symbols" accesskey="u" rel="up">Defining Symbols</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Using-autoheader-to-Create-config_002eh_002ein"></a>
<h4 class="subsection">9.3.2 Using <code>autoheader</code> to Create <samp>config.h.in</samp></h4>
<a name="index-autoheader"></a>

<p>The <code>autoheader</code> program can create a template file of C
&lsquo;<samp>#define</samp>&rsquo; statements for <code>configure</code> to use.
It searches for the first invocation of <code>AC_CONFIG_HEADERS</code> in
<samp>configure</samp> sources to determine the name of the template.
(If the first call of <code>AC_CONFIG_HEADERS</code> specifies more than one
input file name, <code>autoheader</code> uses the first one.)
</p>
<p>It is recommended that only one input file is used.  If you want to append
a boilerplate code, it is preferable to use
&lsquo;<samp>AH_BOTTOM([#include &lt;conf_post.h&gt;])</samp>&rsquo;.
File <samp>conf_post.h</samp> is not processed during the configuration then,
which make things clearer.  Analogically, <code>AH_TOP</code> can be used to
prepend a boilerplate code.
</p>
<p>In order to do its job, <code>autoheader</code> needs you to document all
of the symbols that you might use.  Typically this is done via an
<code>AC_DEFINE</code> or <code>AC_DEFINE_UNQUOTED</code> call whose first argument
is a literal symbol and whose third argument describes the symbol
(see <a href="Defining-Symbols.html#Defining-Symbols">Defining Symbols</a>).  Alternatively, you can use
<code>AH_TEMPLATE</code> (see <a href="Autoheader-Macros.html#Autoheader-Macros">Autoheader Macros</a>), or you can supply a
suitable input file for a subsequent configuration header file.
Symbols defined by Autoconf&rsquo;s builtin tests are already documented properly;
you need to document only those that you
define yourself.
</p>
<p>You might wonder why <code>autoheader</code> is needed: after all, why
would <code>configure</code> need to &ldquo;patch&rdquo; a <samp>config.h.in</samp> to
produce a <samp>config.h</samp> instead of just creating <samp>config.h</samp> from
scratch?  Well, when everything rocks, the answer is just that we are
wasting our time maintaining <code>autoheader</code>: generating
<samp>config.h</samp> directly is all that is needed.  When things go wrong,
however, you&rsquo;ll be thankful for the existence of <code>autoheader</code>.
</p>
<p>The fact that the symbols are documented is important in order to
<em>check</em> that <samp>config.h</samp> makes sense.  The fact that there is a
well-defined list of symbols that should be defined (or not) is
also important for people who are porting packages to environments where
<code>configure</code> cannot be run: they just have to <em>fill in the
blanks</em>.
</p>
<p>But let&rsquo;s come back to the point: the invocation of <code>autoheader</code>&hellip;
</p>
<p>If you give <code>autoheader</code> an argument, it uses that file instead
of <samp>configure.ac</samp> and writes the header file to the standard output
instead of to <samp>config.h.in</samp>.  If you give <code>autoheader</code> an
argument of <samp>-</samp>, it reads the standard input instead of
<samp>configure.ac</samp> and writes the header file to the standard output.
</p>
<p><code>autoheader</code> accepts the following options:
</p>
<dl compact="compact">
<dt><samp>--help</samp></dt>
<dt><samp>-h</samp></dt>
<dd><p>Print a summary of the command line options and exit.
</p>
</dd>
<dt><samp>--version</samp></dt>
<dt><samp>-V</samp></dt>
<dd><p>Print the version number of Autoconf and exit.
</p>
</dd>
<dt><samp>--verbose</samp></dt>
<dt><samp>-v</samp></dt>
<dd><p>Report processing steps.
</p>
</dd>
<dt><samp>--debug</samp></dt>
<dt><samp>-d</samp></dt>
<dd><p>Don&rsquo;t remove the temporary files.
</p>
</dd>
<dt><samp>--force</samp></dt>
<dt><samp>-f</samp></dt>
<dd><p>Remake the template file even if newer than its input files.
</p>
</dd>
<dt><samp>--include=<var>dir</var></samp></dt>
<dt><samp>-I <var>dir</var></samp></dt>
<dd><p>Append <var>dir</var> to the include path.  Multiple invocations accumulate.
</p>
</dd>
<dt><samp>--prepend-include=<var>dir</var></samp></dt>
<dt><samp>-B <var>dir</var></samp></dt>
<dd><p>Prepend <var>dir</var> to the include path.  Multiple invocations accumulate.
</p>
</dd>
<dt><samp>--warnings=<var>category</var></samp></dt>
<dt><samp>-W <var>category</var></samp></dt>
<dd><a name="index-WARNINGS"></a>
<p>Report the warnings related to <var>category</var> (which can actually be a
comma separated list).  Current categories include:
</p>
<dl compact="compact">
<dt>&lsquo;<samp>obsolete</samp>&rsquo;</dt>
<dd><p>report the uses of obsolete constructs
</p>
</dd>
<dt>&lsquo;<samp>all</samp>&rsquo;</dt>
<dd><p>report all the warnings
</p>
</dd>
<dt>&lsquo;<samp>none</samp>&rsquo;</dt>
<dd><p>report none
</p>
</dd>
<dt>&lsquo;<samp>error</samp>&rsquo;</dt>
<dd><p>treats warnings as errors
</p>
</dd>
<dt>&lsquo;<samp>no-<var>category</var></samp>&rsquo;</dt>
<dd><p>disable warnings falling into <var>category</var>
</p></dd>
</dl>

</dd>
</dl>



<div class="header">
<p>
Next: <a href="Autoheader-Macros.html#Autoheader-Macros" accesskey="n" rel="next">Autoheader Macros</a>, Previous: <a href="Header-Templates.html#Header-Templates" accesskey="p" rel="prev">Header Templates</a>, Up: <a href="Defining-Symbols.html#Defining-Symbols" accesskey="u" rel="up">Defining Symbols</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
