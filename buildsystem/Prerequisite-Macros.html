<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Prerequisite Macros</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Prerequisite Macros">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Prerequisite Macros">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Dependencies-Between-Macros.html#Dependencies-Between-Macros" rel="up" title="Dependencies Between Macros">
<link href="Suggested-Ordering.html#Suggested-Ordering" rel="next" title="Suggested Ordering">
<link href="Dependencies-Between-Macros.html#Dependencies-Between-Macros" rel="prev" title="Dependencies Between Macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Prerequisite-Macros"></a>
<div class="header">
<p>
Next: <a href="Suggested-Ordering.html#Suggested-Ordering" accesskey="n" rel="next">Suggested Ordering</a>, Up: <a href="Dependencies-Between-Macros.html#Dependencies-Between-Macros" accesskey="u" rel="up">Dependencies Between Macros</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Prerequisite-Macros-1"></a>
<h4 class="subsubsection">8.5.4.1 Prerequisite Macros</h4>
<a name="index-Prerequisite-macros"></a>
<a name="index-Macros_002c-prerequisites"></a>

<p>A macro that you write might need to use values that have previously
been computed by other macros.  For example, <code>AC_DECL_YYTEXT</code>
examines the output of <code>flex</code> or <code>lex</code>, so it depends on
<code>AC_PROG_LEX</code> having been called first to set the shell variable
<code>LEX</code>.
</p>
<p>Rather than forcing the user of the macros to keep track of the
dependencies between them, you can use the <code>AC_REQUIRE</code> macro to do
it automatically.  <code>AC_REQUIRE</code> can ensure that a macro is only
called if it is needed, and only called once.
</p>
<dl>
<dt><a name="index-AC_005fREQUIRE-1"></a>Macro: <strong>AC_REQUIRE</strong> <em>(<var>macro-name</var>)</em></dt>
<dd><a name="index-AC_005fREQUIRE"></a>
<p>If the M4 macro <var>macro-name</var> has not already been called, call it
(without any arguments).  Make sure to quote <var>macro-name</var> with
square brackets.  <var>macro-name</var> must have been defined using
<code>AC_DEFUN</code> or else contain a call to <code>AC_PROVIDE</code> to indicate
that it has been called.
</p>
<p><code>AC_REQUIRE</code> must be used inside a macro defined by <code>AC_DEFUN</code>; it
must not be called from the top level.  Also, it does not make sense to
require a macro that takes parameters.
</p></dd></dl>

<p><code>AC_REQUIRE</code> is often misunderstood.  It really implements
dependencies between macros in the sense that if one macro depends upon
another, the latter is expanded <em>before</em> the body of the
former.  To be more precise, the required macro is expanded before
the outermost defined macro in the current expansion stack.
In particular, &lsquo;<samp>AC_REQUIRE([FOO])</samp>&rsquo; is not replaced with the body of
<code>FOO</code>.  For instance, this definition of macros:
</p>
<div class="example">
<pre class="example">AC_DEFUN([TRAVOLTA],
[test &quot;$body_temperature_in_celsius&quot; -gt &quot;38&quot; &amp;&amp;
  dance_floor=occupied])
AC_DEFUN([NEWTON_JOHN],
[test &quot;x$hair_style&quot; = xcurly &amp;&amp;
  dance_floor=occupied])
</pre><pre class="example">
</pre><pre class="example">AC_DEFUN([RESERVE_DANCE_FLOOR],
[if date | grep '^Sat.*pm' &gt;/dev/null 2&gt;&amp;1; then
  AC_REQUIRE([TRAVOLTA])
  AC_REQUIRE([NEWTON_JOHN])
fi])
</pre></div>

<p>with this <samp>configure.ac</samp>
</p>
<div class="example">
<pre class="example">AC_INIT([Dance Manager], [1.0], [bug-dance@example.org])
RESERVE_DANCE_FLOOR
if test &quot;x$dance_floor&quot; = xoccupied; then
  AC_MSG_ERROR([cannot pick up here, let's move])
fi
</pre></div>

<p>does not leave you with a better chance to meet a kindred soul at
other times than Saturday night since it expands into:
</p>
<div class="example">
<pre class="example">test &quot;$body_temperature_in_Celsius&quot; -gt &quot;38&quot; &amp;&amp;
  dance_floor=occupied
test &quot;x$hair_style&quot; = xcurly &amp;&amp;
  dance_floor=occupied
fi
if date | grep '^Sat.*pm' &gt;/dev/null 2&gt;&amp;1; then


fi
</pre></div>

<p>This behavior was chosen on purpose: (i) it prevents messages in
required macros from interrupting the messages in the requiring macros;
(ii) it avoids bad surprises when shell conditionals are used, as in:
</p>
<div class="example">
<pre class="example">if &hellip;; then
  AC_REQUIRE([SOME_CHECK])
fi
&hellip;
SOME_CHECK
</pre></div>

<p>However, this implementation can lead to another class of problems.
Consider the case where an outer macro first expands, then indirectly
requires, an inner macro:
</p>
<div class="example">
<pre class="example">AC_DEFUN([TESTA], [[echo in A
if test -n &quot;$SEEN_A&quot; ; then echo duplicate ; fi
SEEN_A=:]])
AC_DEFUN([TESTB], [AC_REQUIRE([TESTA])[echo in B
if test -z &quot;$SEEN_A&quot; ; then echo bug ; fi]])
AC_DEFUN([TESTC], [AC_REQUIRE([TESTB])[echo in C]])
AC_DEFUN([OUTER], [[echo in OUTER]
TESTA
TESTC])
OUTER
</pre></div>

<p>Prior to Autoconf 2.64, the implementation of <code>AC_REQUIRE</code>
recognized that <code>TESTB</code> needed to be hoisted prior to the expansion
of <code>OUTER</code>, but because <code>TESTA</code> had already been directly
expanded, it failed to hoist <code>TESTA</code>.  Therefore, the expansion of
<code>TESTB</code> occurs prior to its prerequisites, leading to the following
output:
</p>
<div class="example">
<pre class="example">in B
bug
in OUTER
in A
in C
</pre></div>

<p>Newer Autoconf is smart enough to recognize this situation, and hoists
<code>TESTA</code> even though it has already been expanded, but issues a
syntax warning in the process.  This is because the hoisted expansion of
<code>TESTA</code> defeats the purpose of using <code>AC_REQUIRE</code> to avoid
redundant code, and causes its own set of problems if the hoisted macro
is not idempotent:
</p>
<div class="example">
<pre class="example">in A
in B
in OUTER
in A
duplicate
in C
</pre></div>

<p>The bug is not in Autoconf, but in the macro definitions.  If you ever
pass a particular macro name to <code>AC_REQUIRE</code>, then you are implying
that the macro only needs to be expanded once.  But to enforce this,
either the macro must be declared with <code>AC_DEFUN_ONCE</code> (although
this only helps in Autoconf 2.64 or newer), or all
uses of that macro should be through <code>AC_REQUIRE</code>; directly
expanding the macro defeats the point of using <code>AC_REQUIRE</code> to
eliminate redundant expansion.  In the example, this rule of thumb was
violated because <code>TESTB</code> requires <code>TESTA</code> while <code>OUTER</code>
directly expands it.  One way of fixing the bug is to factor
<code>TESTA</code> into two macros, the portion designed for direct and
repeated use (here, named <code>TESTA</code>), and the portion designed for
one-shot output and used only inside <code>AC_REQUIRE</code> (here, named
<code>TESTA_PREREQ</code>).  Then, by fixing all clients to use the correct
calling convention according to their needs:
</p>
<div class="example">
<pre class="example">AC_DEFUN([TESTA], [AC_REQUIRE([TESTA_PREREQ])[echo in A]])
AC_DEFUN([TESTA_PREREQ], [[echo in A_PREREQ
if test -n &quot;$SEEN_A&quot; ; then echo duplicate ; fi
SEEN_A=:]])
AC_DEFUN([TESTB], [AC_REQUIRE([TESTA_PREREQ])[echo in B
if test -z &quot;$SEEN_A&quot; ; then echo bug ; fi]])
AC_DEFUN([TESTC], [AC_REQUIRE([TESTB])[echo in C]])
AC_DEFUN([OUTER], [[echo in OUTER]
TESTA
TESTC])
OUTER
</pre></div>

<p>the resulting output will then obey all dependency rules and avoid any
syntax warnings, whether the script is built with old or new Autoconf
versions:
</p>
<div class="example">
<pre class="example">in A_PREREQ
in B
in OUTER
in A
in C
</pre></div>

<p>The helper macros <code>AS_IF</code> and <code>AS_CASE</code> may be used to
enforce expansion of required macros outside of shell conditional
constructs.  You are furthermore encouraged, although not required, to
put all <code>AC_REQUIRE</code> calls
at the beginning of a macro.  You can use <code>dnl</code> to avoid the empty
lines they leave.
</p>
<p>Autoconf will normally warn if an <code>AC_REQUIRE</code> call refers to a
macro that has not been defined.  However, the <code>aclocal</code> tool
relies on parsing an incomplete set of input files to trace which macros
have been required, in order to then pull in additional files that
provide those macros; for this particular use case, pre-defining the
macro <code>m4_require_silent_probe</code> will avoid the warnings.
</p>
<div class="header">
<p>
Next: <a href="Suggested-Ordering.html#Suggested-Ordering" accesskey="n" rel="next">Suggested Ordering</a>, Up: <a href="Dependencies-Between-Macros.html#Dependencies-Between-Macros" accesskey="u" rel="up">Dependencies Between Macros</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
