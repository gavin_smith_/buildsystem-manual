<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Macro Names</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Macro Names">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Macro Names">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Writing-Autoconf-Macros.html#Writing-Autoconf-Macros" rel="up" title="Writing Autoconf Macros">
<link href="Reporting-Messages.html#Reporting-Messages" rel="next" title="Reporting Messages">
<link href="Macro-Definitions.html#Macro-Definitions" rel="prev" title="Macro Definitions">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Macro-Names"></a>
<div class="header">
<p>
Next: <a href="Reporting-Messages.html#Reporting-Messages" accesskey="n" rel="next">Reporting Messages</a>, Previous: <a href="Macro-Definitions.html#Macro-Definitions" accesskey="p" rel="prev">Macro Definitions</a>, Up: <a href="Writing-Autoconf-Macros.html#Writing-Autoconf-Macros" accesskey="u" rel="up">Writing Autoconf Macros</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Macro-Names-1"></a>
<h4 class="subsection">8.5.2 Macro Names</h4>

<p>All of the public Autoconf macros have all-uppercase names in the
namespace &lsquo;<samp>^AC_</samp>&rsquo; to prevent them from accidentally conflicting with
other text; Autoconf also reserves the namespace &lsquo;<samp>^_AC_</samp>&rsquo; for
internal macros.  All shell variables that they use for internal
purposes have mostly-lowercase names starting with &lsquo;<samp>ac_</samp>&rsquo;.  Autoconf
also uses here-document delimiters in the namespace &lsquo;<samp>^_AC[A-Z]</samp>&rsquo;.  During
<code>configure</code>, files produced by Autoconf make heavy use of the
file system namespace &lsquo;<samp>^conf</samp>&rsquo;.
</p>
<p>Since Autoconf is built on top of M4sugar (see <a href="Programming-in-M4sugar.html#Programming-in-M4sugar">Programming in M4sugar</a>) and M4sh (see <a href="Programming-in-M4sh.html#Programming-in-M4sh">Programming in M4sh</a>), you must also be aware
of those namespaces (&lsquo;<samp>^_?\(m4\|AS\)_</samp>&rsquo;).  And since
<samp>configure.ac</samp> is also designed to be scanned by Autoheader,
Autoscan, Autoupdate, and Automake, you should be aware of the
&lsquo;<samp>^_?A[HNUM]_</samp>&rsquo; namespaces.  In general, you <em>should not use</em>
the namespace of a package that does not own the macro or shell code you
are writing.
</p>
<p>To ensure that your macros don&rsquo;t conflict with present or future
Autoconf macros, you should prefix your own macro names and any shell
variables they use with some other sequence.  Possibilities include your
initials, or an abbreviation for the name of your organization or
software package.  Historically, people have not always followed the
rule of using a namespace appropriate for their package, and this has
made it difficult for determining the origin of a macro (and where to
report bugs about that macro), as well as difficult for the true
namespace owner to add new macros without interference from pre-existing
uses of third-party macros.  Perhaps the best example of this confusion
is the <code>AM_GNU_GETTEXT</code> macro, which belongs, not to Automake, but
to Gettext.
</p>
<p>Most of the Autoconf macros&rsquo; names follow a structured naming convention
that indicates the kind of feature check by the name.  The macro names
consist of several words, separated by underscores, going from most
general to most specific.  The names of their cache variables use the
same convention (see <a href="Cache-Variable-Names.html#Cache-Variable-Names">Cache Variable Names</a>, for more information on
them).
</p>
<p>The first word of the name after the namespace initials (such as
&lsquo;<samp>AC_</samp>&rsquo;) usually tells the category
of the feature being tested.  Here are the categories used in Autoconf for
specific test macros, the kind of macro that you are more likely to
write.  They are also used for cache variables, in all-lowercase.  Use
them where applicable; where they&rsquo;re not, invent your own categories.
</p>
<dl compact="compact">
<dt><code>C</code></dt>
<dd><p>C language builtin features.
</p></dd>
<dt><code>DECL</code></dt>
<dd><p>Declarations of C variables in header files.
</p></dd>
<dt><code>FUNC</code></dt>
<dd><p>Functions in libraries.
</p></dd>
<dt><code>GROUP</code></dt>
<dd><p>Posix group owners of files.
</p></dd>
<dt><code>HEADER</code></dt>
<dd><p>Header files.
</p></dd>
<dt><code>LIB</code></dt>
<dd><p>C libraries.
</p></dd>
<dt><code>PROG</code></dt>
<dd><p>The base names of programs.
</p></dd>
<dt><code>MEMBER</code></dt>
<dd><p>Members of aggregates.
</p></dd>
<dt><code>SYS</code></dt>
<dd><p>Operating system features.
</p></dd>
<dt><code>TYPE</code></dt>
<dd><p>C builtin or declared types.
</p></dd>
<dt><code>VAR</code></dt>
<dd><p>C variables in libraries.
</p></dd>
</dl>

<p>After the category comes the name of the particular feature being
tested.  Any further words in the macro name indicate particular aspects
of the feature.  For example, <code>AC_PROG_MAKE_SET</code> checks whether
<code>make</code> sets a variable to its own name.
</p>
<p>An internal macro should have a name that starts with an underscore;
Autoconf internals should therefore start with &lsquo;<samp>_AC_</samp>&rsquo;.
Additionally, a macro that is an internal subroutine of another macro
should have a name that starts with an underscore and the name of that
other macro, followed by one or more words saying what the internal
macro does.  For example, <code>AC_PATH_X</code> has internal macros
<code>_AC_PATH_X_XMKMF</code> and <code>_AC_PATH_X_DIRECT</code>.
</p>
<div class="header">
<p>
Next: <a href="Reporting-Messages.html#Reporting-Messages" accesskey="n" rel="next">Reporting Messages</a>, Previous: <a href="Macro-Definitions.html#Macro-Definitions" accesskey="p" rel="prev">Macro Definitions</a>, Up: <a href="Writing-Autoconf-Macros.html#Writing-Autoconf-Macros" accesskey="u" rel="up">Writing Autoconf Macros</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
