<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Set manipulation Macros</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Set manipulation Macros">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Set manipulation Macros">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Programming-in-M4sugar.html#Programming-in-M4sugar" rel="up" title="Programming in M4sugar">
<link href="Forbidden-Patterns.html#Forbidden-Patterns" rel="next" title="Forbidden Patterns">
<link href="Number-processing-Macros.html#Number-processing-Macros" rel="prev" title="Number processing Macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Set-manipulation-Macros"></a>
<div class="header">
<p>
Next: <a href="Forbidden-Patterns.html#Forbidden-Patterns" accesskey="n" rel="next">Forbidden Patterns</a>, Previous: <a href="Number-processing-Macros.html#Number-processing-Macros" accesskey="p" rel="prev">Number processing Macros</a>, Up: <a href="Programming-in-M4sugar.html#Programming-in-M4sugar" accesskey="u" rel="up">Programming in M4sugar</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Set-manipulation-in-M4"></a>
<h4 class="subsection">15.6.9 Set manipulation in M4</h4>
<a name="index-Set-manipulation"></a>
<a name="index-Data-structure_002c-set"></a>
<a name="index-Unordered-set-manipulation"></a>

<p>Sometimes, it is necessary to track a set of data, where the order does
not matter and where there are no duplicates in the set.  The following
macros facilitate set manipulations.  Each set is an opaque object,
which can only be accessed via these basic operations.  The underlying
implementation guarantees linear scaling for set creation, which is more
efficient than using the quadratic <code>m4_append_uniq</code>.  Both set
names and values can be arbitrary strings, except for unbalanced quotes.
This implementation ties up memory for removed elements until the next
operation that must traverse all the elements of a set; and although
that may slow down some operations until the memory for removed elements
is pruned, it still guarantees linear performance.
</p>
<dl>
<dt><a name="index-m4_005fset_005fadd-1"></a>Macro: <strong>m4_set_add</strong> <em>(<var>set</var>, <var>value</var>, <span class="roman">[</span><var>if-uniq</var><span class="roman">]</span>, <span class="roman">[</span><var>if-dup</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fset_005fadd"></a>
<p>Adds the string <var>value</var> as a member of set <var>set</var>.  Expand
<var>if-uniq</var> if the element was added, or <var>if-dup</var> if it was
previously in the set.  Operates in amortized constant time, so that set
creation scales linearly.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fadd_005fall-1"></a>Macro: <strong>m4_set_add_all</strong> <em>(<var>set</var>, <var>value</var>&hellip;)</em></dt>
<dd><a name="index-m4_005fset_005fadd_005fall"></a>
<p>Adds each <var>value</var> to the set <var>set</var>.  This is slightly more
efficient than repeatedly invoking <code>m4_set_add</code>.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fcontains-1"></a>Macro: <strong>m4_set_contains</strong> <em>(<var>set</var>, <var>value</var>, <span class="roman">[</span><var>if-present</var><span class="roman">]</span>,  <span class="roman">[</span><var>if-absent</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fset_005fcontains"></a>
<p>Expands <var>if-present</var> if the string <var>value</var> is a member of
<var>set</var>, otherwise <var>if-absent</var>.
</p>
<div class="example">
<pre class="example">m4_set_contains([a], [1], [yes], [no])
&rArr;no
m4_set_add([a], [1], [added], [dup])
&rArr;added
m4_set_add([a], [1], [added], [dup])
&rArr;dup
m4_set_contains([a], [1], [yes], [no])
&rArr;yes
m4_set_remove([a], [1], [removed], [missing])
&rArr;removed
m4_set_contains([a], [1], [yes], [no])
&rArr;no
m4_set_remove([a], [1], [removed], [missing])
&rArr;missing
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fcontents-1"></a>Macro: <strong>m4_set_contents</strong> <em>(<var>set</var>, <span class="roman">[</span><var>sep</var><span class="roman">]</span>)</em></dt>
<dt><a name="index-m4_005fset_005fdump-1"></a>Macro: <strong>m4_set_dump</strong> <em>(<var>set</var>, <span class="roman">[</span><var>sep</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fset_005fcontents"></a>
<a name="index-m4_005fset_005fdump"></a>
<p>Expands to a single string consisting of all the members of the set
<var>set</var>, each separated by <var>sep</var>, which is not expanded.
<code>m4_set_contents</code> leaves the elements in <var>set</var> but reclaims any
memory occupied by removed elements, while <code>m4_set_dump</code> is a
faster one-shot action that also deletes the set.  No provision is made
for disambiguating members that contain a non-empty <var>sep</var> as a
substring; use <code>m4_set_empty</code> to distinguish between an empty set
and the set containing only the empty string.  The order of the output
is unspecified; in the current implementation, part of the speed of
<code>m4_set_dump</code> results from using a different output order than
<code>m4_set_contents</code>.  These macros scale linearly in the size of the
set before memory pruning, and <code>m4_set_contents([<var>set</var>],
[<var>sep</var>])</code> is faster than
<code>m4_joinall([<var>sep</var>]m4_set_listc([<var>set</var>]))</code>.
</p>
<div class="example">
<pre class="example">m4_set_add_all([a], [1], [2], [3])
&rArr;
m4_set_contents([a], [-])
&rArr;1-2-3
m4_joinall([-]m4_set_listc([a]))
&rArr;1-2-3
m4_set_dump([a], [-])
&rArr;3-2-1
m4_set_contents([a])
&rArr;
m4_set_add([a], [])
&rArr;
m4_set_contents([a], [-])
&rArr;
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fdelete-1"></a>Macro: <strong>m4_set_delete</strong> <em>(<var>set</var>)</em></dt>
<dd><a name="index-m4_005fset_005fdelete"></a>
<p>Delete all elements and memory associated with <var>set</var>.  This is
linear in the set size, and faster than removing one element at a time.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fdifference-1"></a>Macro: <strong>m4_set_difference</strong> <em>(<var>seta</var>, <var>setb</var>)</em></dt>
<dt><a name="index-m4_005fset_005fintersection-1"></a>Macro: <strong>m4_set_intersection</strong> <em>(<var>seta</var>, <var>setb</var>)</em></dt>
<dt><a name="index-m4_005fset_005funion-1"></a>Macro: <strong>m4_set_union</strong> <em>(<var>seta</var>, <var>setb</var>)</em></dt>
<dd><a name="index-m4_005fset_005fdifference"></a>
<a name="index-m4_005fset_005fintersection"></a>
<a name="index-m4_005fset_005funion"></a>
<p>Compute the relation between <var>seta</var> and <var>setb</var>, and output the
result as a list of quoted arguments without duplicates and with a
leading comma.  Set difference selects the elements in <var>seta</var> but
not <var>setb</var>, intersection selects only elements in both sets, and
union selects elements in either set.  These actions are linear in the
sum of the set sizes.  The leading comma is necessary to distinguish
between no elements and the empty string as the only element.
</p>
<div class="example">
<pre class="example">m4_set_add_all([a], [1], [2], [3])
&rArr;
m4_set_add_all([b], [3], [], [4])
&rArr;
m4_set_difference([a], [b])
&rArr;,1,2
m4_set_difference([b], [a])
&rArr;,,4
m4_set_intersection([a], [b])
&rArr;,3
m4_set_union([a], [b])
&rArr;,1,2,3,,4
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fempty-1"></a>Macro: <strong>m4_set_empty</strong> <em>(<var>set</var>, <span class="roman">[</span><var>if-empty</var><span class="roman">]</span>, <span class="roman">[</span><var>if-elements</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fset_005fempty"></a>
<p>Expand <var>if-empty</var> if the set <var>set</var> has no elements, otherwise
expand <var>if-elements</var>.  This macro operates in constant time.  Using
this macro can help disambiguate output from <code>m4_set_contents</code> or
<code>m4_set_list</code>.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fforeach-1"></a>Macro: <strong>m4_set_foreach</strong> <em>(<var>set</var>, <var>variable</var>, <var>action</var>)</em></dt>
<dd><a name="index-m4_005fset_005fforeach"></a>
<p>For each element in the set <var>set</var>, expand <var>action</var> with the
macro <var>variable</var> defined as the set element.  Behavior is
unspecified if <var>action</var> recursively lists the contents of <var>set</var>
(although listing other sets is acceptable), or if it modifies the set
in any way other than removing the element currently contained in
<var>variable</var>.  This macro is faster than the corresponding
<code>m4_foreach([<var>variable</var>],
m4_indir([m4_dquote]m4_set_listc([<var>set</var>])), [<var>action</var>])</code>,
although <code>m4_set_map</code> might be faster still.
</p>
<div class="example">
<pre class="example">m4_set_add_all([a]m4_for([i], [1], [5], [], [,i]))
&rArr;
m4_set_contents([a])
&rArr;12345
m4_set_foreach([a], [i],
  [m4_if(m4_eval(i&amp;1), [0], [m4_set_remove([a], i, [i])])])
&rArr;24
m4_set_contents([a])
&rArr;135
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-m4_005fset_005flist-1"></a>Macro: <strong>m4_set_list</strong> <em>(<var>set</var>)</em></dt>
<dt><a name="index-m4_005fset_005flistc-1"></a>Macro: <strong>m4_set_listc</strong> <em>(<var>set</var>)</em></dt>
<dd><a name="index-m4_005fset_005flist"></a>
<a name="index-m4_005fset_005flistc"></a>
<p>Produce a list of arguments, where each argument is a quoted element
from the set <var>set</var>.  The variant <code>m4_set_listc</code> is unambiguous,
by adding a leading comma if there are any set elements, whereas the
variant <code>m4_set_list</code> cannot distinguish between an empty set and a
set containing only the empty string.  These can be directly used in
macros that take multiple arguments, such as <code>m4_join</code> or
<code>m4_set_add_all</code>, or wrapped by <code>m4_dquote</code> for macros that
take a quoted list, such as <code>m4_map</code> or <code>m4_foreach</code>.  Any
memory occupied by removed elements is reclaimed during these macros.
</p>
<div class="example">
<pre class="example">m4_set_add_all([a], [1], [2], [3])
&rArr;
m4_set_list([a])
&rArr;1,2,3
m4_set_list([b])
&rArr;
m4_set_listc([b])
&rArr;
m4_count(m4_set_list([b]))
&rArr;1
m4_set_empty([b], [0], [m4_count(m4_set_list([b]))])
&rArr;0
m4_set_add([b], [])
&rArr;
m4_set_list([b])
&rArr;
m4_set_listc([b])
&rArr;,
m4_count(m4_set_list([b]))
&rArr;1
m4_set_empty([b], [0], [m4_count(m4_set_list([b]))])
&rArr;1
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fmap-1"></a>Macro: <strong>m4_set_map</strong> <em>(<var>set</var>, <var>action</var>)</em></dt>
<dd><a name="index-m4_005fset_005fmap"></a>
<p>For each element in the set <var>set</var>, expand <var>action</var> with a single
argument of the set element.  Behavior is unspecified if <var>action</var>
recursively lists the contents of <var>set</var> (although listing other sets
is acceptable), or if it modifies the set in any way other than removing
the element passed as an argument.  This macro is faster than either
corresponding counterpart of
<code>m4_map_args([<var>action</var>]m4_set_listc([<var>set</var>]))</code> or
<code>m4_set_foreach([<var>set</var>], [var],
[<var>action</var>(m4_defn([var]))])</code>.  It is possible to use <code>m4_curry</code>
if more than one argument is needed for <var>action</var>, although it is
more efficient to use <code>m4_set_map_sep</code> in that case.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fmap_005fsep-1"></a>Macro: <strong>m4_set_map_sep</strong> <em>(<var>set</var>, <span class="roman">[</span><var>pre</var><span class="roman">]</span>, <span class="roman">[</span><var>post</var><span class="roman">]</span>, <span class="roman">[</span><var>sep</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fset_005fmap_005fsep"></a>
<p>For each element in the set <var>set</var>, expand
<code><var>pre</var>[element]<var>post</var></code>, additionally expanding <var>sep</var>
between elements.  Behavior is unspecified if the expansion recursively
lists the contents of <var>set</var> (although listing other sets
is acceptable), or if it modifies the set in any way other than removing
the element visited by the expansion.  This macro provides the most
efficient means for non-destructively visiting the elements of a set; in
particular, <code>m4_set_map([<var>set</var>], [<var>action</var>])</code> is equivalent
to <code>m4_set_map_sep([<var>set</var>], [<var>action</var>(], [)])</code>.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fremove-1"></a>Macro: <strong>m4_set_remove</strong> <em>(<var>set</var>, <var>value</var>, <span class="roman">[</span><var>if-present</var><span class="roman">]</span>,  <span class="roman">[</span><var>if-absent</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fset_005fremove"></a>
<p>If <var>value</var> is an element in the set <var>set</var>, then remove it and
expand <var>if-present</var>.  Otherwise expand <var>if-absent</var>.  This macro
operates in constant time so that multiple removals will scale linearly
rather than quadratically; but when used outside of
<code>m4_set_foreach</code> or <code>m4_set_map</code>, it leaves memory occupied
until the set is later
compacted by <code>m4_set_contents</code> or <code>m4_set_list</code>.  Several
other set operations are then less efficient between the time of element
removal and subsequent memory compaction, but still maintain their
guaranteed scaling performance.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fset_005fsize-1"></a>Macro: <strong>m4_set_size</strong> <em>(<var>set</var>)</em></dt>
<dd><a name="index-m4_005fset_005fsize"></a>
<p>Expand to the size of the set <var>set</var>.  This implementation operates
in constant time, and is thus more efficient than
<code>m4_eval(m4_count(m4_set_listc([set])) - 1)</code>.
</p></dd></dl>


<div class="header">
<p>
Next: <a href="Forbidden-Patterns.html#Forbidden-Patterns" accesskey="n" rel="next">Forbidden Patterns</a>, Previous: <a href="Number-processing-Macros.html#Number-processing-Macros" accesskey="p" rel="prev">Number processing Macros</a>, Up: <a href="Programming-in-M4sugar.html#Programming-in-M4sugar" accesskey="u" rel="up">Programming in M4sugar</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
