<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Signed Overflow Advice</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Signed Overflow Advice">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Signed Overflow Advice">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Integer-Overflow.html#Integer-Overflow" rel="up" title="Integer Overflow">
<link href="Signed-Integer-Division.html#Signed-Integer-Division" rel="next" title="Signed Integer Division">
<link href="Optimization-and-Wraparound.html#Optimization-and-Wraparound" rel="prev" title="Optimization and Wraparound">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Signed-Overflow-Advice"></a>
<div class="header">
<p>
Next: <a href="Signed-Integer-Division.html#Signed-Integer-Division" accesskey="n" rel="next">Signed Integer Division</a>, Previous: <a href="Optimization-and-Wraparound.html#Optimization-and-Wraparound" accesskey="p" rel="prev">Optimization and Wraparound</a>, Up: <a href="Integer-Overflow.html#Integer-Overflow" accesskey="u" rel="up">Integer Overflow</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Practical-Advice-for-Signed-Overflow-Issues"></a>
<h4 class="subsubsection">C.3.3.4 Practical Advice for Signed Overflow Issues</h4>
<a name="index-integer-overflow-3"></a>
<a name="index-overflow_002c-signed-integer-3"></a>
<a name="index-signed-integer-overflow-3"></a>
<a name="index-wraparound-arithmetic-3"></a>

<p>Ideally the safest approach is to avoid signed integer overflow
entirely.  For example, instead of multiplying two signed integers, you
can convert them to unsigned integers, multiply the unsigned values,
then test whether the result is in signed range.
</p>
<p>Rewriting code in this way will be inconvenient, though, particularly if
the signed values might be negative.  Also, it may hurt
performance.  Using unsigned arithmetic to check for overflow is
particularly painful to do portably and efficiently when dealing with an
integer type like <code>uid_t</code> whose width and signedness vary from
platform to platform.
</p>
<p>Furthermore, many C applications pervasively assume wraparound behavior
and typically it is not easy to find and remove all these assumptions.
Hence it is often useful to maintain nonstandard code that assumes
wraparound on overflow, instead of rewriting the code.  The rest of this
section attempts to give practical advice for this situation.
</p>
<p>If your code wants to detect signed integer overflow in <code>sum = a +
b</code>, it is generally safe to use an expression like <code>(sum &lt; a) != (b
&lt; 0)</code>.
</p>
<p>If your code uses a signed loop index, make sure that the index cannot
overflow, along with all signed expressions derived from the index.
Here is a contrived example of problematic code with two instances of
overflow.
</p>
<div class="example">
<pre class="example">for (i = INT_MAX - 10; i &lt;= INT_MAX; i++)
  if (i + 1 &lt; 0)
    {
      report_overflow ();
      break;
    }
</pre></div>

<p>Because of the two overflows, a compiler might optimize away or
transform the two comparisons in a way that is incompatible with the
wraparound assumption.
</p>
<p>If your code uses an expression like <code>(i * 2000) / 1000</code> and you
actually want the multiplication to wrap around on overflow, use
unsigned arithmetic
to do it, e.g., <code>((int) (i * 2000u)) / 1000</code>.
</p>
<p>If your code assumes wraparound behavior and you want to insulate it
against any GCC optimizations that would fail to support that
behavior, you should use GCC&rsquo;s <samp>-fwrapv</samp> option, which
causes signed overflow to wrap around reliably (except for division and
remainder, as discussed in the next section).
</p>
<p>If you need to port to platforms where signed integer overflow does not
reliably wrap around (e.g., due to hardware overflow checking, or to
highly aggressive optimizations), you should consider debugging with
GCC&rsquo;s <samp>-ftrapv</samp> option, which causes signed overflow to
raise an exception.
</p>
<div class="header">
<p>
Next: <a href="Signed-Integer-Division.html#Signed-Integer-Division" accesskey="n" rel="next">Signed Integer Division</a>, Previous: <a href="Optimization-and-Wraparound.html#Optimization-and-Wraparound" accesskey="p" rel="prev">Optimization and Wraparound</a>, Up: <a href="Integer-Overflow.html#Integer-Overflow" accesskey="u" rel="up">Integer Overflow</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
