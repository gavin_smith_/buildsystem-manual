<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Signed Overflow Examples</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Signed Overflow Examples">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Signed Overflow Examples">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Integer-Overflow.html#Integer-Overflow" rel="up" title="Integer Overflow">
<link href="Optimization-and-Wraparound.html#Optimization-and-Wraparound" rel="next" title="Optimization and Wraparound">
<link href="Integer-Overflow-Basics.html#Integer-Overflow-Basics" rel="prev" title="Integer Overflow Basics">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Signed-Overflow-Examples"></a>
<div class="header">
<p>
Next: <a href="Optimization-and-Wraparound.html#Optimization-and-Wraparound" accesskey="n" rel="next">Optimization and Wraparound</a>, Previous: <a href="Integer-Overflow-Basics.html#Integer-Overflow-Basics" accesskey="p" rel="prev">Integer Overflow Basics</a>, Up: <a href="Integer-Overflow.html#Integer-Overflow" accesskey="u" rel="up">Integer Overflow</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Examples-of-Code-Assuming-Wraparound-Overflow"></a>
<h4 class="subsubsection">C.3.3.2 Examples of Code Assuming Wraparound Overflow</h4>
<a name="index-integer-overflow-2"></a>
<a name="index-overflow_002c-signed-integer-2"></a>
<a name="index-signed-integer-overflow-2"></a>
<a name="index-wraparound-arithmetic-2"></a>

<p>There has long been a tension between what the C standard requires for
signed integer overflow, and what C programs commonly assume.  The
standard allows aggressive optimizations based on assumptions that
overflow never occurs, but many practical C programs rely on overflow
wrapping around.  These programs do not conform to the standard, but
they commonly work in practice because compiler writers are
understandably reluctant to implement optimizations that would break
many programs, unless perhaps a user specifies aggressive optimization.
</p>
<p>The C Standard says that if a program has signed integer overflow its
behavior is undefined, and the undefined behavior can even precede the
overflow.  To take an extreme example:
</p>
<div class="example">
<pre class="example">if (password == expected_password)
  allow_superuser_privileges ();
else if (counter++ == INT_MAX)
  abort ();
else
  printf (&quot;%d password mismatches\n&quot;, counter);
</pre></div>

<p>If the <code>int</code> variable <code>counter</code> equals <code>INT_MAX</code>,
<code>counter++</code> must overflow and the behavior is undefined, so the C
standard allows the compiler to optimize away the test against
<code>INT_MAX</code> and the <code>abort</code> call.
Worse, if an earlier bug in the program lets the compiler deduce that
<code>counter == INT_MAX</code> or that <code>counter</code> previously overflowed,
the C standard allows the compiler to optimize away the password test
and generate code that allows superuser privileges unconditionally.
</p>
<p>Despite this requirement by the standard, it has long been common for C
code to assume wraparound arithmetic after signed overflow, and all
known practical C implementations support some C idioms that assume
wraparound signed arithmetic, even if the idioms do not conform
strictly to the standard.  If your code looks like the following
examples it will almost surely work with real-world compilers.
</p>
<p>Here is an example derived from the 7th Edition Unix implementation of
<code>atoi</code> (1979-01-10):
</p>
<div class="example">
<pre class="example">char *p;
int f, n;
&hellip;
while (*p &gt;= '0' &amp;&amp; *p &lt;= '9')
  n = n * 10 + *p++ - '0';
return (f ? -n : n);
</pre></div>

<p>Even if the input string is in range, on most modern machines this has
signed overflow when computing the most negative integer (the <code>-n</code>
overflows) or a value near an extreme integer (the first <code>+</code>
overflows).
</p>
<p>Here is another example, derived from the 7th Edition implementation of
<code>rand</code> (1979-01-10).  Here the programmer expects both
multiplication and addition to wrap on overflow:
</p>
<div class="example">
<pre class="example">static long int randx = 1;
&hellip;
randx = randx * 1103515245 + 12345;
return (randx &gt;&gt; 16) &amp; 077777;
</pre></div>

<p>In the following example, derived from the GNU C Library 2.5
implementation of <code>mktime</code> (2006-09-09), the code assumes
wraparound arithmetic in <code>+</code> to detect signed overflow:
</p>
<div class="example">
<pre class="example">time_t t, t1, t2;
int sec_requested, sec_adjustment;
&hellip;
t1 = t + sec_requested;
t2 = t1 + sec_adjustment;
if (((t1 &lt; t) != (sec_requested &lt; 0))
    | ((t2 &lt; t1) != (sec_adjustment &lt; 0)))
  return -1;
</pre></div>

<p>If your code looks like these examples, it is probably safe even though
it does not strictly conform to the C standard.  This might lead one to
believe that one can generally assume wraparound on overflow, but that
is not always true, as can be seen in the next section.
</p>
<div class="header">
<p>
Next: <a href="Optimization-and-Wraparound.html#Optimization-and-Wraparound" accesskey="n" rel="next">Optimization and Wraparound</a>, Previous: <a href="Integer-Overflow-Basics.html#Integer-Overflow-Basics" accesskey="p" rel="prev">Integer Overflow Basics</a>, Up: <a href="Integer-Overflow.html#Integer-Overflow" accesskey="u" rel="up">Integer Overflow</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
