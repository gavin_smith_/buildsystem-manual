<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Forbidden Patterns</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Forbidden Patterns">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Forbidden Patterns">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Programming-in-M4sugar.html#Programming-in-M4sugar" rel="up" title="Programming in M4sugar">
<link href="Invocation-of-programs.html#Invocation-of-programs" rel="next" title="Invocation of programs">
<link href="Set-manipulation-Macros.html#Set-manipulation-Macros" rel="prev" title="Set manipulation Macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Forbidden-Patterns"></a>
<div class="header">
<p>
Previous: <a href="Set-manipulation-Macros.html#Set-manipulation-Macros" accesskey="p" rel="prev">Set manipulation Macros</a>, Up: <a href="Programming-in-M4sugar.html#Programming-in-M4sugar" accesskey="u" rel="up">Programming in M4sugar</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Forbidden-Patterns-1"></a>
<h4 class="subsection">15.6.10 Forbidden Patterns</h4>
<a name="index-Forbidden-patterns"></a>
<a name="index-Patterns_002c-forbidden"></a>

<p>M4sugar provides a means to define suspicious patterns, patterns
describing tokens which should not be found in the output.  For
instance, if an Autoconf <samp>configure</samp> script includes tokens such as
&lsquo;<samp>AC_DEFINE</samp>&rsquo;, or &lsquo;<samp>dnl</samp>&rsquo;, then most probably something went
wrong (typically a macro was not evaluated because of overquotation).
</p>
<p>M4sugar forbids all the tokens matching &lsquo;<samp>^_?m4_</samp>&rsquo; and &lsquo;<samp>^dnl$</samp>&rsquo;.
Additional layers, such as M4sh and Autoconf, add additional forbidden
patterns to the list.
</p>
<dl>
<dt><a name="index-m4_005fpattern_005fforbid-1"></a>Macro: <strong>m4_pattern_forbid</strong> <em>(<var>pattern</var>)</em></dt>
<dd><a name="index-m4_005fpattern_005fforbid"></a>
<p>Declare that no token matching <var>pattern</var> must be found in the output.
Comments are not checked; this can be a problem if, for instance, you
have some macro left unexpanded after an &lsquo;<samp>#include</samp>&rsquo;.  No consensus
is currently found in the Autoconf community, as some people consider it
should be valid to name macros in comments (which doesn&rsquo;t make sense to
the authors of this documentation: input, such as macros, should be
documented by &lsquo;<samp>dnl</samp>&rsquo; comments; reserving &lsquo;<samp>#</samp>&rsquo;-comments to
document the output).
</p>
<p>As an example of a common use of this macro, consider what happens in
packages that want to use the <code>pkg-config</code> script via the
third-party <code>PKG_CHECK_MODULES</code> macro.  By default, if a developer
checks out the development tree but has not yet installed the pkg-config
macros locally, they can manage to successfully run <code>autoconf</code>
on the package, but the resulting <samp>configure</samp> file will likely
result in a confusing shell message about a syntax error on the line
mentioning the unexpanded PKG_CHECK_MODULES macro.  On the other hand,
if <samp>configure.ac</samp> includes <code>m4_pattern_forbid([^PKG_])</code>, the
missing pkg-config macros will be detected immediately without allowing
<code>autoconf</code> to succeed.
</p></dd></dl>

<p>Of course, you might encounter exceptions to these generic rules, for
instance you might have to refer to &lsquo;<samp>$m4_flags</samp>&rsquo;.
</p>
<dl>
<dt><a name="index-m4_005fpattern_005fallow-1"></a>Macro: <strong>m4_pattern_allow</strong> <em>(<var>pattern</var>)</em></dt>
<dd><a name="index-m4_005fpattern_005fallow"></a>
<p>Any token matching <var>pattern</var> is allowed, including if it matches an
<code>m4_pattern_forbid</code> pattern.
</p>
<p>For example, gnulib uses <code>m4_pattern_forbid([^gl_])</code> to reserve the
<code>gl_</code> namespace for itself, but also uses
<code>m4_pattern_allow([^gl_ES$])</code> to avoid a false negative on the
valid locale name.
</p></dd></dl>




<div class="header">
<p>
Previous: <a href="Set-manipulation-Macros.html#Set-manipulation-Macros" accesskey="p" rel="prev">Set manipulation Macros</a>, Up: <a href="Programming-in-M4sugar.html#Programming-in-M4sugar" accesskey="u" rel="up">Programming in M4sugar</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
