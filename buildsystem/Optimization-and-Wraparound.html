<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Optimization and Wraparound</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Optimization and Wraparound">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Optimization and Wraparound">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Integer-Overflow.html#Integer-Overflow" rel="up" title="Integer Overflow">
<link href="Signed-Overflow-Advice.html#Signed-Overflow-Advice" rel="next" title="Signed Overflow Advice">
<link href="Signed-Overflow-Examples.html#Signed-Overflow-Examples" rel="prev" title="Signed Overflow Examples">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Optimization-and-Wraparound"></a>
<div class="header">
<p>
Next: <a href="Signed-Overflow-Advice.html#Signed-Overflow-Advice" accesskey="n" rel="next">Signed Overflow Advice</a>, Previous: <a href="Signed-Overflow-Examples.html#Signed-Overflow-Examples" accesskey="p" rel="prev">Signed Overflow Examples</a>, Up: <a href="Integer-Overflow.html#Integer-Overflow" accesskey="u" rel="up">Integer Overflow</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Optimizations-That-Break-Wraparound-Arithmetic"></a>
<h4 class="subsubsection">C.3.3.3 Optimizations That Break Wraparound Arithmetic</h4>
<a name="index-loop-induction"></a>

<p>Compilers sometimes generate code that is incompatible with wraparound
integer arithmetic.  A simple example is an algebraic simplification: a
compiler might translate <code>(i * 2000) / 1000</code> to <code>i * 2</code>
because it assumes that <code>i * 2000</code> does not overflow.  The
translation is not equivalent to the original when overflow occurs:
e.g., in the typical case of 32-bit signed two&rsquo;s complement wraparound
<code>int</code>, if <code>i</code> has type <code>int</code> and value <code>1073742</code>,
the original expression returns -2147483 but the optimized
version returns the mathematically correct value 2147484.
</p>
<p>More subtly, loop induction optimizations often exploit the undefined
behavior of signed overflow.  Consider the following contrived function
<code>sumc</code>:
</p>
<div class="example">
<pre class="example">int
sumc (int lo, int hi)
{
  int sum = 0;
  int i;
  for (i = lo; i &lt;= hi; i++)
    sum ^= i * 53;
  return sum;
}
</pre></div>

<p>To avoid multiplying by 53 each time through the loop, an optimizing
compiler might internally transform <code>sumc</code> to the equivalent of the
following:
</p>
<div class="example">
<pre class="example">int
transformed_sumc (int lo, int hi)
{
  int sum = 0;
  int hic = hi * 53;
  int ic;
  for (ic = lo * 53; ic &lt;= hic; ic += 53)
    sum ^= ic;
  return sum;
}
</pre></div>

<p>This transformation is allowed by the C standard, but it is invalid for
wraparound arithmetic when <code>INT_MAX / 53 &lt; hi</code>, because then the
overflow in computing expressions like <code>hi * 53</code> can cause the
expression <code>i &lt;= hi</code> to yield a different value from the
transformed expression <code>ic &lt;= hic</code>.
</p>
<p>For this reason, compilers that use loop induction and similar
techniques often do not support reliable wraparound arithmetic when a
loop induction variable like <code>ic</code> is involved.  Since loop
induction variables are generated by the compiler, and are not visible
in the source code, it is not always trivial to say whether the problem
affects your code.
</p>
<p>Hardly any code actually depends on wraparound arithmetic in cases like
these, so in practice these loop induction optimizations are almost
always useful.  However, edge cases in this area can cause problems.
For example:
</p>
<div class="example">
<pre class="example">int j;
for (j = 1; 0 &lt; j; j *= 2)
  test (j);
</pre></div>

<p>Here, the loop attempts to iterate through all powers of 2 that
<code>int</code> can represent, but the C standard allows a compiler to
optimize away the comparison and generate an infinite loop,
under the argument that behavior is undefined on overflow.  As of this
writing this optimization is not done by any production version of
GCC with <samp>-O2</samp>, but it might be performed by other
compilers, or by more aggressive GCC optimization options,
and the GCC developers have not decided whether it will
continue to work with GCC and <samp>-O2</samp>.
</p>
<div class="header">
<p>
Next: <a href="Signed-Overflow-Advice.html#Signed-Overflow-Advice" accesskey="n" rel="next">Signed Overflow Advice</a>, Previous: <a href="Signed-Overflow-Examples.html#Signed-Overflow-Examples" accesskey="p" rel="prev">Signed Overflow Examples</a>, Up: <a href="Integer-Overflow.html#Integer-Overflow" accesskey="u" rel="up">Integer Overflow</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
