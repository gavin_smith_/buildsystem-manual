<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Build system regeneration</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Build system regeneration">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Build system regeneration">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Other-Automake-features.html#Other-Automake-features" rel="up" title="Other Automake features">
<link href="Dependencies.html#Dependencies" rel="next" title="Dependencies">
<link href="Automake-Silent-Rules.html#Automake-Silent-Rules" rel="prev" title="Automake Silent Rules">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Build-system-regeneration"></a>
<div class="header">
<p>
Next: <a href="Dependencies.html#Dependencies" accesskey="n" rel="next">Dependencies</a>, Previous: <a href="Verbosity.html#Verbosity" accesskey="p" rel="prev">Verbosity</a>, Up: <a href="Other-Automake-features.html#Other-Automake-features" accesskey="u" rel="up">Other Automake features</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Build-system-regeneration-1"></a>
<h3 class="section">13.5 Build system regeneration</h3>
<a name="index-rebuild-rules"></a>

<p>Automake generates rules to automatically rebuild <samp>Makefile</samp>s,
<samp>configure</samp>, and other derived files like <samp>Makefile.in</samp>.
</p>
<a name="index-AM_005fMAINTAINER_005fMODE"></a>
<p>If you are using <code>AM_MAINTAINER_MODE</code> in <samp>configure.ac</samp>, then
these automatic rebuilding rules are only enabled in maintainer mode.
</p>
<a name="index-CONFIG_005fSTATUS_005fDEPENDENCIES"></a>
<a name="index-CONFIGURE_005fDEPENDENCIES"></a>
<a name="index-version_002esh_002c-example"></a>
<a name="index-version_002em4_002c-example"></a>

<p>Sometimes it is convenient to supplement the rebuild rules for
<samp>configure</samp> or <samp>config.status</samp> with additional dependencies.
The variables <code>CONFIGURE_DEPENDENCIES</code> and
<code>CONFIG_STATUS_DEPENDENCIES</code> can be used to list these extra
dependencies.  These variables should be defined in all
<samp>Makefile</samp>s of the tree (because these two rebuild rules are
output in all them), so it is safer and easier to <code>AC_SUBST</code> them
from <samp>configure.ac</samp>.  For instance, the following statement will
cause <samp>configure</samp> to be rerun each time <samp>version.sh</samp> is
changed.
</p>
<div class="example">
<pre class="example">AC_SUBST([CONFIG_STATUS_DEPENDENCIES], ['$(top_srcdir)/version.sh'])
</pre></div>

<p>Note the &lsquo;<samp>$(top_srcdir)/</samp>&rsquo; in the file name.  Since this variable
is to be used in all <samp>Makefile</samp>s, its value must be sensible at
any level in the build hierarchy.
</p>
<p>Beware not to mistake <code>CONFIGURE_DEPENDENCIES</code> for
<code>CONFIG_STATUS_DEPENDENCIES</code>.
</p>
<p><code>CONFIGURE_DEPENDENCIES</code> adds dependencies to the
<samp>configure</samp> rule, whose effect is to run <code>autoconf</code>.  This
variable should be seldom used, because <code>automake</code> already tracks
<code>m4_include</code>d files.  However it can be useful when playing
tricky games with <code>m4_esyscmd</code> or similar non-recommendable
macros with side effects.  Be also aware that interactions of this
variable with the <a href="http://www.gnu.org/software/autoconf/manual/html_node/Autom4te-Cache.html#Autom4te-Cache">autom4te cache</a> in <cite>The Autoconf Manual</cite> are quite problematic and can cause subtle
breakage, so you might want to disable the cache if you want to use
<code>CONFIGURE_DEPENDENCIES</code>.
</p>
<p><code>CONFIG_STATUS_DEPENDENCIES</code> adds dependencies to the
<samp>config.status</samp> rule, whose effect is to run <samp>configure</samp>.
This variable should therefore carry any non-standard source that may
be read as a side effect of running <code>configure</code>, like <samp>version.sh</samp>
in the example above.
</p>
<p>Speaking of <samp>version.sh</samp> scripts, we recommend against them
today.  They are mainly used when the version of a package is updated
automatically by a script (e.g., in daily builds).  Here is what some
old-style <samp>configure.ac</samp>s may look like:
</p>
<div class="example">
<pre class="example">AC_INIT
. $srcdir/version.sh
AM_INIT_AUTOMAKE([name], $VERSION_NUMBER)
&hellip;
</pre></div>

<p>Here, <samp>version.sh</samp> is a shell fragment that sets
<code>VERSION_NUMBER</code>.  The problem with this example is that
<code>automake</code> cannot track dependencies (listing <samp>version.sh</samp>
in <code>CONFIG_STATUS_DEPENDENCIES</code>, and distributing this file is up
to the user), and that it uses the obsolete form of <code>AC_INIT</code> and
<code>AM_INIT_AUTOMAKE</code>.  Upgrading to the new syntax is not
straightforward, because shell variables are not allowed in
<code>AC_INIT</code>&rsquo;s arguments.  We recommend that <samp>version.sh</samp> be
replaced by an M4 file that is included by <samp>configure.ac</samp>:
</p>
<div class="example">
<pre class="example">m4_include([version.m4])
AC_INIT([name], VERSION_NUMBER)
AM_INIT_AUTOMAKE
&hellip;
</pre></div>

<p>Here <samp>version.m4</samp> could contain something like
&lsquo;<samp>m4_define([VERSION_NUMBER], [1.2])</samp>&rsquo;.  The advantage of this
second form is that <code>automake</code> will take care of the
dependencies when defining the rebuild rule, and will also distribute
the file automatically.  An inconvenience is that <code>autoconf</code>
will now be rerun each time the version number is bumped, when only
<samp>configure</samp> had to be rerun in the previous setup.
</p>
<div class="header">
<p>
Next: <a href="Dependencies.html#Dependencies" accesskey="n" rel="next">Dependencies</a>, Previous: <a href="Verbosity.html#Verbosity" accesskey="p" rel="prev">Verbosity</a>, Up: <a href="Other-Automake-features.html#Other-Automake-features" accesskey="u" rel="up">Other Automake features</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
