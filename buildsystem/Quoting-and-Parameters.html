<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Quoting and Parameters</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Quoting and Parameters">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Quoting and Parameters">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="M4-Quotation.html#M4-Quotation" rel="up" title="M4 Quotation">
<link href="Quotation-and-Nested-Macros.html#Quotation-and-Nested-Macros" rel="next" title="Quotation and Nested Macros">
<link href="One-Macro-Call.html#One-Macro-Call" rel="prev" title="One Macro Call">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Quoting-and-Parameters"></a>
<div class="header">
<p>
Next: <a href="Quotation-and-Nested-Macros.html#Quotation-and-Nested-Macros" accesskey="n" rel="next">Quotation and Nested Macros</a>, Previous: <a href="One-Macro-Call.html#One-Macro-Call" accesskey="p" rel="prev">One Macro Call</a>, Up: <a href="M4-Quotation.html#M4-Quotation" accesskey="u" rel="up">M4 Quotation</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Quoting-and-Parameters-1"></a>
<h4 class="subsection">F.1.3 Quoting and Parameters</h4>

<p>When M4 encounters &lsquo;<samp>$</samp>&rsquo; within a macro definition, followed
immediately by a character it recognizes (&lsquo;<samp>0</samp>&rsquo;&hellip;&lsquo;<samp>9</samp>&rsquo;,
&lsquo;<samp>#</samp>&rsquo;, &lsquo;<samp>@</samp>&rsquo;, or &lsquo;<samp>*</samp>&rsquo;), it will perform M4 parameter
expansion.  This happens regardless of how many layers of quotes the
parameter expansion is nested within, or even if it occurs in text that
will be rescanned as a comment.
</p>
<div class="example">
<pre class="example">define([none], [$1])
&rArr;
define([one], [[$1]])
&rArr;
define([two], [[[$1]]])
&rArr;
define([comment], [# $1])
&rArr;
define([active], [ACTIVE])
&rArr;
none([active])
&rArr;ACTIVE
one([active])
&rArr;active
two([active])
&rArr;[active]
comment([active])
&rArr;# active
</pre></div>

<p>On the other hand, since autoconf generates shell code, you often want
to output shell variable expansion, rather than performing M4 parameter
expansion.  To do this, you must use M4 quoting to separate the &lsquo;<samp>$</samp>&rsquo;
from the next character in the definition of your macro.  If the macro
definition occurs in single-quoted text, then insert another level of
quoting; if the usage is already inside a double-quoted string, then
split it into concatenated strings.
</p>
<div class="example">
<pre class="example">define([single], [a single-quoted $[]1 definition])
&rArr;
define([double], [[a double-quoted $][1 definition]])
&rArr;
single
&rArr;a single-quoted $1 definition
double
&rArr;a double-quoted $1 definition
</pre></div>

<p>Posix states that M4 implementations are free to provide implementation
extensions when &lsquo;<samp>${</samp>&rsquo; is encountered in a macro definition.
Autoconf reserves the longer sequence &lsquo;<samp>${{</samp>&rsquo; for use with planned
extensions that will be available in the future GNU M4 2.0,
but guarantees that all other instances of &lsquo;<samp>${</samp>&rsquo; will be output
literally.  Therefore, this idiom can also be used to output shell code
parameter references:
</p>
<div class="example">
<pre class="example">define([first], [${1}])first
&rArr;${1}
</pre></div>

<p>Posix also states that &lsquo;<samp>$11</samp>&rsquo; should expand to the first parameter
concatenated with a literal &lsquo;<samp>1</samp>&rsquo;, although some versions of
GNU M4 expand the eleventh parameter instead.  For
portability, you should only use single-digit M4 parameter expansion.
</p>
<p>With this in mind, we can explore the cases where macros invoke
macros<small class="enddots">...</small>
</p>
<div class="header">
<p>
Next: <a href="Quotation-and-Nested-Macros.html#Quotation-and-Nested-Macros" accesskey="n" rel="next">Quotation and Nested Macros</a>, Previous: <a href="One-Macro-Call.html#One-Macro-Call" accesskey="p" rel="prev">One Macro Call</a>, Up: <a href="M4-Quotation.html#M4-Quotation" accesskey="u" rel="up">M4 Quotation</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
