<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Quotation and Nested Macros</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Quotation and Nested Macros">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Quotation and Nested Macros">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="M4-Quotation.html#M4-Quotation" rel="up" title="M4 Quotation">
<link href="Changequote-is-Evil.html#Changequote-is-Evil" rel="next" title="Changequote is Evil">
<link href="Quoting-and-Parameters.html#Quoting-and-Parameters" rel="prev" title="Quoting and Parameters">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Quotation-and-Nested-Macros"></a>
<div class="header">
<p>
Next: <a href="Changequote-is-Evil.html#Changequote-is-Evil" accesskey="n" rel="next">Changequote is Evil</a>, Previous: <a href="Quoting-and-Parameters.html#Quoting-and-Parameters" accesskey="p" rel="prev">Quoting and Parameters</a>, Up: <a href="M4-Quotation.html#M4-Quotation" accesskey="u" rel="up">M4 Quotation</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Quotation-and-Nested-Macros-1"></a>
<h4 class="subsection">F.1.4 Quotation and Nested Macros</h4>

<p>The examples below use the following macros:
</p>
<div class="example">
<pre class="example">define([car], [$1])
define([active], [ACT, IVE])
define([array], [int tab[10]])
</pre></div>

<p>Each additional embedded macro call introduces other possible
interesting quotations:
</p>
<div class="example">
<pre class="example">car(active)
&rArr;ACT
car([active])
&rArr;ACT, IVE
car([[active]])
&rArr;active
</pre></div>

<p>In the first case, the top level looks for the arguments of <code>car</code>,
and finds &lsquo;<samp>active</samp>&rsquo;.  Because M4 evaluates its arguments
before applying the macro, &lsquo;<samp>active</samp>&rsquo; is expanded, which results in:
</p>
<div class="example">
<pre class="example">car(ACT, IVE)
&rArr;ACT
</pre></div>

<p>In the second case, the top level gives &lsquo;<samp>active</samp>&rsquo; as first and only
argument of <code>car</code>, which results in:
</p>
<div class="example">
<pre class="example">active
&rArr;ACT, IVE
</pre></div>

<p>i.e., the argument is evaluated <em>after</em> the macro that invokes it.
In the third case, <code>car</code> receives &lsquo;<samp>[active]</samp>&rsquo;, which results in:
</p>
<div class="example">
<pre class="example">[active]
&rArr;active
</pre></div>

<p>exactly as we already saw above.
</p>
<p>The example above, applied to a more realistic example, gives:
</p>
<div class="example">
<pre class="example">car(int tab[10];)
&rArr;int tab10;
car([int tab[10];])
&rArr;int tab10;
car([[int tab[10];]])
&rArr;int tab[10];
</pre></div>

<p>Huh?  The first case is easily understood, but why is the second wrong,
and the third right?  To understand that, you must know that after
M4 expands a macro, the resulting text is immediately subjected
to macro expansion and quote removal.  This means that the quote removal
occurs twice&mdash;first before the argument is passed to the <code>car</code>
macro, and second after the <code>car</code> macro expands to the first
argument.
</p>
<p>As the author of the Autoconf macro <code>car</code>, you then consider it to
be incorrect that your users have to double-quote the arguments of
<code>car</code>, so you &ldquo;fix&rdquo; your macro.  Let&rsquo;s call it <code>qar</code> for
quoted car:
</p>
<div class="example">
<pre class="example">define([qar], [[$1]])
</pre></div>

<p>and check that <code>qar</code> is properly fixed:
</p>
<div class="example">
<pre class="example">qar([int tab[10];])
&rArr;int tab[10];
</pre></div>

<p>Ahhh!  That&rsquo;s much better.
</p>
<p>But note what you&rsquo;ve done: now that the result of <code>qar</code> is always
a literal string, the only time a user can use nested macros is if she
relies on an <em>unquoted</em> macro call:
</p>
<div class="example">
<pre class="example">qar(active)
&rArr;ACT
qar([active])
&rArr;active
</pre></div>

<p>leaving no way for her to reproduce what she used to do with <code>car</code>:
</p>
<div class="example">
<pre class="example">car([active])
&rArr;ACT, IVE
</pre></div>

<p>Worse yet: she wants to use a macro that produces a set of <code>cpp</code>
macros:
</p>
<div class="example">
<pre class="example">define([my_includes], [#include &lt;stdio.h&gt;])
car([my_includes])
&rArr;#include &lt;stdio.h&gt;
qar(my_includes)
error&rarr;EOF in argument list
</pre></div>

<p>This macro, <code>qar</code>, because it double quotes its arguments, forces
its users to leave their macro calls unquoted, which is dangerous.
Commas and other active symbols are interpreted by M4 before
they are given to the macro, often not in the way the users expect.
Also, because <code>qar</code> behaves differently from the other macros,
it&rsquo;s an exception that should be avoided in Autoconf.
</p>
<div class="header">
<p>
Next: <a href="Changequote-is-Evil.html#Changequote-is-Evil" accesskey="n" rel="next">Changequote is Evil</a>, Previous: <a href="Quoting-and-Parameters.html#Quoting-and-Parameters" accesskey="p" rel="prev">Quoting and Parameters</a>, Up: <a href="M4-Quotation.html#M4-Quotation" accesskey="u" rel="up">M4 Quotation</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
