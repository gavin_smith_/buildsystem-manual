<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: EXEEXT</title>

<meta name="description" content="Automake and Autoconf Reference Manual: EXEEXT">
<meta name="keywords" content="Automake and Autoconf Reference Manual: EXEEXT">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Programs.html#Programs" rel="up" title="Programs">
<link href="Libraries.html#Libraries" rel="next" title="Libraries">
<link href="Conditional-Programs.html#Conditional-Programs" rel="prev" title="Conditional Programs">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="EXEEXT"></a>
<div class="header">
<p>
Previous: <a href="Conditional-Programs.html#Conditional-Programs" accesskey="p" rel="prev">Conditional Programs</a>, Up: <a href="Programs.html#Programs" accesskey="u" rel="up">Programs</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Support-for-executable-extensions"></a>
<h4 class="subsection">7.1.8 Support for executable extensions</h4>

<a name="index-Executable-extension"></a>
<a name="index-Extension_002c-executable"></a>
<a name="index-Windows"></a>

<p>On some platforms, such as Windows, executables are expected to have an
extension such as <samp>.exe</samp>.  On these platforms, some compilers (GCC
among them) will automatically generate <samp>foo.exe</samp> when asked to
generate <samp>foo</samp>.  You will have to assist Automake if your package
must support those platforms.
</p>

<p>One thing you must be aware of is that, internally, Automake rewrites
something like this:
</p>
<div class="example">
<pre class="example">bin_PROGRAMS = liver
</pre></div>

<p>to this:
</p>
<div class="example">
<pre class="example">bin_PROGRAMS = liver$(EXEEXT)
</pre></div>

<p>The targets Automake generates are likewise given the &lsquo;<samp>$(EXEEXT)</samp>&rsquo;
extension.
</p>
<p>The variables <code>TESTS</code> and <code>XFAIL_TESTS</code> (see <a href="Simple-Tests.html#Simple-Tests">Simple Tests</a>)
are also rewritten if they contain filenames that have been declared as
programs in the same <samp>Makefile</samp>.  (This is mostly useful when some
programs from <code>check_PROGRAMS</code> are listed in <code>TESTS</code>.)
</p>
<p>However, Automake cannot apply this rewriting to <code>configure</code>
substitutions.  This means that if you are conditionally building a
program using such a substitution, then your <samp>configure.ac</samp> must
take care to add &lsquo;<samp>$(EXEEXT)</samp>&rsquo; when constructing the output variable.
</p>
<p>Sometimes maintainers like to write an explicit link rule for their
program.  Without executable extension support, this is easy&mdash;you
simply write a rule whose target is the name of the program.  However,
when executable extension support is enabled, you must instead add the
&lsquo;<samp>$(EXEEXT)</samp>&rsquo; suffix.
</p>
<p>This might be a nuisance for maintainers who know their package will
never run on a platform that has
executable extensions.  For those maintainers, the <samp>no-exeext</samp>
option (see <a href="Automake-options.html#Automake-options">Automake options</a>) will disable this feature.  This works 
in a
fairly ugly way; if <samp>no-exeext</samp> is seen, then the presence of a
rule for a target named <code>foo</code> in <samp>Makefile.am</samp> will override
an <code>automake</code>-generated rule for &lsquo;<samp>foo$(EXEEXT)</samp>&rsquo;.  Without
the <samp>no-exeext</samp> option, this use will give a diagnostic.
</p>


<div class="header">
<p>
Previous: <a href="Conditional-Programs.html#Conditional-Programs" accesskey="p" rel="prev">Conditional Programs</a>, Up: <a href="Programs.html#Programs" accesskey="u" rel="up">Programs</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
