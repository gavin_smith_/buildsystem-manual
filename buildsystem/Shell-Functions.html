<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Shell Functions</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Shell Functions">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Shell Functions">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Portable-Shell.html#Portable-Shell" rel="up" title="Portable Shell">
<link href="Limitations-of-Builtins.html#Limitations-of-Builtins" rel="next" title="Limitations of Builtins">
<link href="Special-Shell-Variables.html#Special-Shell-Variables" rel="prev" title="Special Shell Variables">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Shell-Functions"></a>
<div class="header">
<p>
Next: <a href="Limitations-of-Builtins.html#Limitations-of-Builtins" accesskey="n" rel="next">Limitations of Builtins</a>, Previous: <a href="Special-Shell-Variables.html#Special-Shell-Variables" accesskey="p" rel="prev">Special Shell Variables</a>, Up: <a href="Portable-Shell.html#Portable-Shell" accesskey="u" rel="up">Portable Shell</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Shell-Functions-1"></a>
<h4 class="subsection">C.1.13 Shell Functions</h4>
<a name="index-Shell-Functions"></a>

<p>Nowadays, it is difficult to find a shell that does not support
shell functions at all.  However, some differences should be expected.
</p>
<p>When declaring a shell function, you must include whitespace between the
&lsquo;<samp>)</samp>&rsquo; after the function name and the start of the compound
expression, to avoid upsetting <code>ksh</code>.  While it is possible to
use any compound command, most scripts use &lsquo;<samp>{&hellip;}</samp>&rsquo;.
</p>
<div class="example">
<pre class="example">$ <kbd>/bin/sh -c 'a(){ echo hi;}; a'</kbd>
hi
$ <kbd>ksh -c 'a(){ echo hi;}; a'</kbd>
ksh: syntax error at line 1: `}' unexpected
$ <kbd>ksh -c 'a() { echo hi;}; a'</kbd>
hi
</pre></div>

<p>Inside a shell function, you should not rely on the error status of a
subshell if the last command of that subshell was <code>exit</code> or
<code>trap</code>, as this triggers bugs in zsh 4.x; while Autoconf tries to
find a shell that does not exhibit the bug, zsh might be the only shell
present on the user&rsquo;s machine.
</p>
<p>Likewise, the state of &lsquo;<samp>$?</samp>&rsquo; is not reliable when entering a shell
function.  This has the effect that using a function as the first
command in a <code>trap</code> handler can cause problems.
</p>
<div class="example">
<pre class="example">$ <kbd>bash -c 'foo() { echo $?; }; trap foo 0; (exit 2); exit 2'; echo $?</kbd>
2
2
$ <kbd>ash -c 'foo() { echo $?; }; trap foo 0; (exit 2); exit 2'; echo $?</kbd>
0
2
</pre></div>

<p>DJGPP bash 2.04 has a bug in that <code>return</code> from a
shell function which also used a command substitution causes a
segmentation fault.  To work around the issue, you can use
<code>return</code> from a subshell, or &lsquo;<samp>AS_SET_STATUS</samp>&rsquo; as last command
in the execution flow of the function (see <a href="Common-Shell-Constructs.html#Common-Shell-Constructs">Common Shell Constructs</a>).
</p>
<p>Not all shells treat shell functions as simple commands impacted by
&lsquo;<samp>set -e</samp>&rsquo;, for example with Solaris 10 <code>/bin/sh</code>:
</p>
<div class="example">
<pre class="example">$ <kbd>bash -c 'f() { return 1; }; set -e; f; echo oops'</kbd>
$ <kbd>/bin/sh -c 'f() { return 1; }; set -e; f; echo oops'</kbd>
oops
</pre></div>

<p>Shell variables and functions may share the same namespace, for example
with Solaris 10 <code>/bin/sh</code>:
</p>
<div class="example">
<pre class="example">$ <kbd>f () { :; }; f=; f</kbd>
f: not found
</pre></div>

<p>For this reason, Autoconf (actually M4sh, see <a href="Programming-in-M4sh.html#Programming-in-M4sh">Programming in M4sh</a>)
uses the prefix &lsquo;<samp>as_fn_</samp>&rsquo; for its functions.
</p>
<p>Handling of positional parameters and shell options varies among shells.
For example, Korn shells reset and restore trace output (&lsquo;<samp>set -x</samp>&rsquo;)
and other options upon function entry and exit.  Inside a function,
IRIX sh sets &lsquo;<samp>$0</samp>&rsquo; to the function name.
</p>
<p>It is not portable to pass temporary environment variables to shell
functions.  Solaris <code>/bin/sh</code> does not see the variable.
Meanwhile, not all shells follow the Posix rule that the assignment must
affect the current environment in the same manner as special built-ins.
</p>
<div class="example">
<pre class="example">$ <kbd>/bin/sh -c 'func() { echo $a;}; a=1 func; echo $a'</kbd>
&rArr;
&rArr;
$ <kbd>ash -c 'func() { echo $a;}; a=1 func; echo $a'</kbd>
&rArr;1
&rArr;
$ <kbd>bash -c 'set -o posix; func() { echo $a;}; a=1 func; echo $a'</kbd>
&rArr;1
&rArr;1
</pre></div>

<p>Some ancient Bourne shell variants with function support did not reset
&lsquo;<samp>$<var>i</var>, <var>i</var> &gt;= 0</samp>&rsquo;, upon function exit, so effectively the
arguments of the script were lost after the first function invocation.
It is probably not worth worrying about these shells any more.
</p>
<p>With AIX sh, a <code>trap</code> on 0 installed in a shell function
triggers at function exit rather than at script exit.  See <a href="Limitations-of-Builtins.html#trap">Limitations of Shell Builtins</a>.
</p>
<div class="header">
<p>
Next: <a href="Limitations-of-Builtins.html#Limitations-of-Builtins" accesskey="n" rel="next">Limitations of Builtins</a>, Previous: <a href="Special-Shell-Variables.html#Special-Shell-Variables" accesskey="p" rel="prev">Special Shell Variables</a>, Up: <a href="Portable-Shell.html#Portable-Shell" accesskey="u" rel="up">Portable Shell</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
