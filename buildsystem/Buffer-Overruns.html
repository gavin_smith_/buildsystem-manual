<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Buffer Overruns</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Buffer Overruns">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Buffer Overruns">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Portable-C-and-C_002b_002b.html#Portable-C-and-C_002b_002b" rel="up" title="Portable C and C++">
<link href="Volatile-Objects.html#Volatile-Objects" rel="next" title="Volatile Objects">
<link href="Null-Pointers.html#Null-Pointers" rel="prev" title="Null Pointers">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Buffer-Overruns"></a>
<div class="header">
<p>
Next: <a href="Volatile-Objects.html#Volatile-Objects" accesskey="n" rel="next">Volatile Objects</a>, Previous: <a href="Null-Pointers.html#Null-Pointers" accesskey="p" rel="prev">Null Pointers</a>, Up: <a href="Portable-C-and-C_002b_002b.html#Portable-C-and-C_002b_002b" accesskey="u" rel="up">Portable C and C++</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Buffer-Overruns-and-Subscript-Errors"></a>
<h4 class="subsection">C.3.6 Buffer Overruns and Subscript Errors</h4>
<a name="index-buffer-overruns"></a>

<p>Buffer overruns and subscript errors are the most common dangerous
errors in C programs.  They result in undefined behavior because storing
outside an array typically modifies storage that is used by some other
object, and most modern systems lack runtime checks to catch these
errors.  Programs should not rely on buffer overruns being caught.
</p>
<p>There is one exception to the usual rule that a portable program cannot
address outside an array.  In C, it is valid to compute the address just
past an object, e.g., <code>&amp;a[N]</code> where <code>a</code> has <code>N</code> elements,
so long as you do not dereference the resulting pointer.  But it is not
valid to compute the address just before an object, e.g., <code>&amp;a[-1]</code>;
nor is it valid to compute two past the end, e.g., <code>&amp;a[N+1]</code>.  On
most platforms <code>&amp;a[-1] &lt; &amp;a[0] &amp;&amp; &amp;a[N] &lt; &amp;a[N+1]</code>, but this is not
reliable in general, and it is usually easy enough to avoid the
potential portability problem, e.g., by allocating an extra unused array
element at the start or end.
</p>
<p><a href="http://valgrind.org/">Valgrind</a> can catch many overruns.
GCC
users might also consider using the <samp>-fmudflap</samp> option to catch
overruns.
</p>
<p>Buffer overruns are usually caused by off-by-one errors, but there are
more subtle ways to get them.
</p>
<p>Using <code>int</code> values to index into an array or compute array sizes
causes problems on typical 64-bit hosts where an array index might
be <em>2^{31}</em> or larger.  Index values of type <code>size_t</code> avoid this
problem, but cannot be negative.  Index values of type <code>ptrdiff_t</code>
are signed, and are wide enough in practice.
</p>
<p>If you add or multiply two numbers to calculate an array size, e.g.,
<code>malloc (x * sizeof y + z)</code>, havoc ensues if the addition or
multiplication overflows.
</p>
<p>Many implementations of the <code>alloca</code> function silently misbehave
and can generate buffer overflows if given sizes that are too large.
The size limits are implementation dependent, but are at least 4000
bytes on all platforms that we know about.
</p>
<p>The standard functions <code>asctime</code>, <code>asctime_r</code>, <code>ctime</code>,
<code>ctime_r</code>, and <code>gets</code> are prone to buffer overflows, and
portable code should not use them unless the inputs are known to be
within certain limits.  The time-related functions can overflow their
buffers if given timestamps out of range (e.g., a year less than -999
or greater than 9999).  Time-related buffer overflows cannot happen with
recent-enough versions of the GNU C library, but are possible
with other
implementations.  The <code>gets</code> function is the worst, since it almost
invariably overflows its buffer when presented with an input line larger
than the buffer.
</p>
<div class="header">
<p>
Next: <a href="Volatile-Objects.html#Volatile-Objects" accesskey="n" rel="next">Volatile Objects</a>, Previous: <a href="Null-Pointers.html#Null-Pointers" accesskey="p" rel="prev">Null Pointers</a>, Up: <a href="Portable-C-and-C_002b_002b.html#Portable-C-and-C_002b_002b" accesskey="u" rel="up">Portable C and C++</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
