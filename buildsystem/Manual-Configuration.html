<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Manual Configuration</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Manual Configuration">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Manual Configuration">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Existing-Tests.html#Existing-Tests" rel="up" title="Existing Tests">
<link href="Specifying-Target-Triplets.html#Specifying-Target-Triplets" rel="next" title="Specifying Target Triplets">
<link href="Checks-for-Libraries.html#Checks-for-Libraries" rel="prev" title="Checks for Libraries">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Manual-Configuration"></a>
<div class="header">
<p>
Next: <a href="System-Services.html#System-Services" accesskey="n" rel="next">System Services</a>, Previous: <a href="Checks-for-Libraries.html#Checks-for-Libraries" accesskey="p" rel="prev">Checks for Libraries</a>, Up: <a href="Existing-Tests.html#Existing-Tests" accesskey="u" rel="up">Existing Tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Canonical-system-types-_002f-Manual-Configuration"></a>
<h4 class="subsection">8.3.6 Canonical system types / Manual Configuration</h4>

<p>A few kinds of features can&rsquo;t be guessed automatically by running test
programs.  For example, the details of the object-file format, or
special options that need to be passed to the compiler or linker.
</p>
<p>Autoconf provides a uniform method for getting a canonical name for
the system type, which an Autoconf-generated <code>configure</code>
script can base its decisions on.  (This saves you from using ad-hoc means
to determine the system type, such as having <code>configure</code> check
the output of the <code>uname</code> program, or looking for libraries that
are unique to particular systems.)  A canonical name is called a
<em>target triplet</em>, and has the form:
&lsquo;<samp><var>cpu</var>-<var>vendor</var>-<var>os</var></samp>&rsquo;, where <var>os</var> can be
&lsquo;<samp><var>system</var></samp>&rsquo; or &lsquo;<samp><var>kernel</var>-<var>system</var></samp>&rsquo;
</p>
<p>Whenever you&rsquo;re tempted to use this feature it&rsquo;s worth considering
whether some sort of probe would be better.  New system types come along
periodically or previously missing features are added.  Well-written
probes can adapt themselves to such things, but hard-coded lists of
names can&rsquo;t.  Here are some guidelines,
</p>
<ul>
<li> Availability of libraries and library functions should always be checked
by probing.
</li><li> Variant behavior of system calls is best identified with runtime tests
if possible, but bug workarounds or obscure difficulties might have to
be driven from the system type.
</li><li> Assembler code is inevitably highly CPU-specific and is best selected
according to &lsquo;<samp>$host_cpu</samp>&rsquo; (see <a href="Getting-System-Types.html#Getting-System-Types">Getting System Types</a>).
</li><li> Assembler variations like underscore prefix on globals or ELF versus
COFF type directives are however best determined by probing, perhaps
even examining the compiler output.
</li></ul>


<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Specifying-Target-Triplets.html#Specifying-Target-Triplets" accesskey="1">Specifying Target Triplets</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Specifying target triplets
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Getting-System-Types.html#Getting-System-Types" accesskey="2">Getting System Types</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Getting the canonical system type
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Using-System-Type.html#Using-System-Type" accesskey="3">Using System Type</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">What to do with the system type
</td></tr>
</table>

<div class="header">
<p>
Next: <a href="System-Services.html#System-Services" accesskey="n" rel="next">System Services</a>, Previous: <a href="Checks-for-Libraries.html#Checks-for-Libraries" accesskey="p" rel="prev">Checks for Libraries</a>, Up: <a href="Existing-Tests.html#Existing-Tests" accesskey="u" rel="up">Existing Tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
