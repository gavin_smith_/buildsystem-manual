<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Shellology</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Shellology">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Shellology">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Portable-Shell.html#Portable-Shell" rel="up" title="Portable Shell">
<link href="Invoking-the-Shell.html#Invoking-the-Shell" rel="next" title="Invoking the Shell">
<link href="Portable-Shell.html#Portable-Shell" rel="prev" title="Portable Shell">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Shellology"></a>
<div class="header">
<p>
Next: <a href="Invoking-the-Shell.html#Invoking-the-Shell" accesskey="n" rel="next">Invoking the Shell</a>, Up: <a href="Portable-Shell.html#Portable-Shell" accesskey="u" rel="up">Portable Shell</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Shellology-1"></a>
<h4 class="subsection">C.1.1 Shellology</h4>
<a name="index-Shellology"></a>

<p>There are several families of shells, most prominently the Bourne family
and the C shell family which are deeply incompatible.  If you want to
write portable shell scripts, avoid members of the C shell family.  The
<a href="http://www.faqs.org/faqs/unix-faq/shell/shell-differences/">the
Shell difference FAQ</a> includes a small history of Posix shells, and a
comparison between several of them.
</p>
<p>Below we describe some of the members of the Bourne shell family.
</p>
<dl compact="compact">
<dt>Ash</dt>
<dd><a name="index-Ash"></a>
<p>Ash is often used on GNU/Linux and BSD
systems as a light-weight Bourne-compatible shell.  Ash 0.2 has some
bugs that are fixed in the 0.3.x series, but portable shell scripts
should work around them, since version 0.2 is still shipped with many
GNU/Linux distributions.
</p>
<p>To be compatible with Ash 0.2:
</p>
<ul class="no-bullet">
<li>- don&rsquo;t use &lsquo;<samp>$?</samp>&rsquo; after expanding empty or unset variables,
or at the start of an <code>eval</code>:

<div class="example">
<pre class="example">foo=
false
$foo
echo &quot;Do not use it: $?&quot;
false
eval 'echo &quot;Do not use it: $?&quot;'
</pre></div>

</li><li>- don&rsquo;t use command substitution within variable expansion:

<div class="example">
<pre class="example">cat ${FOO=`bar`}
</pre></div>

</li><li>- beware that single builtin substitutions are not performed by a
subshell, hence their effect applies to the current shell!  See <a href="Shell-Substitutions.html#Shell-Substitutions">Shell Substitutions</a>, item &ldquo;Command Substitution&rdquo;.
</li></ul>

</dd>
<dt>Bash</dt>
<dd><a name="index-Bash"></a>
<p>To detect whether you are running Bash, test whether
<code>BASH_VERSION</code> is set.  To require
Posix compatibility, run &lsquo;<samp>set -o posix</samp>&rsquo;.  See <a href="http://www.gnu.org/software/bash/manual/html_node/Bash-POSIX-Mode.html#Bash-POSIX-Mode">Bash Posix Mode</a> in <cite>The GNU Bash Reference
Manual</cite>, for details.
</p>
</dd>
<dt>Bash 2.05 and later</dt>
<dd><a name="index-Bash-2_002e05-and-later"></a>
<p>Versions 2.05 and later of Bash use a different format for the
output of the <code>set</code> builtin, designed to make evaluating its
output easier.  However, this output is not compatible with earlier
versions of Bash (or with many other shells, probably).  So if
you use Bash 2.05 or higher to execute <code>configure</code>,
you&rsquo;ll need to use Bash 2.05 for all other build tasks as well.
</p>
</dd>
<dt>Ksh</dt>
<dd><a name="index-Ksh"></a>
<a name="index-Korn-shell"></a>
<a name="index-ksh"></a>
<a name="index-ksh88"></a>
<a name="index-ksh93"></a>
<p>The Korn shell is compatible with the Bourne family and it mostly
conforms to Posix.  It has two major variants commonly
called &lsquo;<samp>ksh88</samp>&rsquo; and &lsquo;<samp>ksh93</samp>&rsquo;, named after the years of initial
release.  It is usually called <code>ksh</code>, but is called <code>sh</code>
on some hosts if you set your path appropriately.
</p>
<p>Solaris systems have three variants:
<a name="index-_002fusr_002fbin_002fksh-on-Solaris"></a>
<code>/usr/bin/ksh</code> is &lsquo;<samp>ksh88</samp>&rsquo;; it is
standard on Solaris 2.0 and later.
<a name="index-_002fusr_002fxpg4_002fbin_002fsh-on-Solaris"></a>
<code>/usr/xpg4/bin/sh</code> is a Posix-compliant variant of
&lsquo;<samp>ksh88</samp>&rsquo;; it is standard on Solaris 9 and later.
<a name="index-_002fusr_002fdt_002fbin_002fdtksh-on-Solaris"></a>
<code>/usr/dt/bin/dtksh</code> is &lsquo;<samp>ksh93</samp>&rsquo;.
Variants that are not standard may be parts of optional
packages.  There is no extra charge for these packages, but they are
not part of a minimal OS install and therefore some installations may
not have it.
</p>
<p>Starting with Tru64 Version 4.0, the Korn shell <code>/usr/bin/ksh</code>
is also available as <code>/usr/bin/posix/sh</code>.  If the environment
variable <code>BIN_SH</code> is set to <code>xpg4</code>, subsidiary invocations of
the standard shell conform to Posix.
</p>
</dd>
<dt>Pdksh</dt>
<dd><a name="index-pdksh"></a>
<p>A public-domain clone of the Korn shell called <code>pdksh</code> is widely
available: it has most of the &lsquo;<samp>ksh88</samp>&rsquo; features along with a few of
its own.  It usually sets <code>KSH_VERSION</code>, except if invoked as
<code>/bin/sh</code> on OpenBSD, and similarly to Bash you can require
Posix compatibility by running &lsquo;<samp>set -o posix</samp>&rsquo;.  Unfortunately, with
<code>pdksh</code> 5.2.14 (the latest stable version as of January 2007)
Posix mode is buggy and causes <code>pdksh</code> to depart from Posix in
at least one respect, see <a href="Shell-Substitutions.html#Shell-Substitutions">Shell Substitutions</a>.
</p>
</dd>
<dt>Zsh</dt>
<dd><a name="index-Zsh"></a>
<p>To detect whether you are running <code>zsh</code>, test whether
<code>ZSH_VERSION</code> is set.  By default <code>zsh</code> is <em>not</em>
compatible with the Bourne shell: you must execute &lsquo;<samp>emulate sh</samp>&rsquo;,
and for <code>zsh</code> versions before 3.1.6-dev-18 you must also
set <code>NULLCMD</code> to &lsquo;<samp>:</samp>&rsquo;.  See <a href="../zsh/Compatibility.html#Compatibility">Compatibility</a> in <cite>The Z Shell Manual</cite>, for details.
</p>
<p>The default Mac OS X <code>sh</code> was originally Zsh; it was changed to
Bash in Mac OS X 10.2.
</p></dd>
</dl>

<div class="header">
<p>
Next: <a href="Invoking-the-Shell.html#Invoking-the-Shell" accesskey="n" rel="next">Invoking the Shell</a>, Up: <a href="Portable-Shell.html#Portable-Shell" accesskey="u" rel="up">Portable Shell</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
