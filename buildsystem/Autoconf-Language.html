<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Autoconf Language</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Autoconf Language">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Autoconf Language">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Format-of-configure_002eac.html#Format-of-configure_002eac" rel="up" title="Format of configure.ac">
<link href="Quoting-macro-arguments.html#Quoting-macro-arguments" rel="next" title="Quoting macro arguments">
<link href="Format-of-configure_002eac.html#Format-of-configure_002eac" rel="prev" title="Format of configure.ac">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Autoconf-Language"></a>
<div class="header">
<p>
Next: <a href="Autoconf-Input-Layout.html#Autoconf-Input-Layout" accesskey="n" rel="next">Autoconf Input Layout</a>, Up: <a href="Format-of-configure_002eac.html#Format-of-configure_002eac" accesskey="u" rel="up">Format of <samp>configure.ac</samp></a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="The-Autoconf-Language"></a>
<h4 class="subsection">5.2.1 The Autoconf Language</h4>
<a name="index-quotation"></a>

<p>The Autoconf language is shell script augmented with the M4 
preprocessor.  See <a href="http://www.gnu.org/software/m4/manual/html_node/Syntax.html#Syntax">Syntax</a> in <cite>The GNU M4 Manual</cite>, for more 
information about the format of M4 input files.
</p>
<p><samp>configure.ac</samp> may contain references to macros, whose names are 
sequences of letters, digits and underscores (&lsquo;<samp>_</samp>&rsquo;).
Any macro names appearing in <samp>configure.ac</samp> that are not to be 
expanded, or any strings looking like a macro, need to be quoted
to prevent them being expanded.  See <a href="Macro-name-quoting.html#Macro-name-quoting">Macro name quoting</a>.
</p>

<p>Arguments given to macros should be surrounded in parentheses and
separated by commas.  Any blanks or newlines immediately following 
either the open parenthesis, or a comma separating arguments, are 
ignored.  There must not be any white space between the macro name and 
the open parenthesis:
</p>
<div class="example">
<pre class="example">AC_INIT([hello], [1.0]) # good
AC_INIT ([oops], [1.0]) # incorrect
</pre></div>

<p>It is usually advisable to enclose the arguments
with the quote characters &lsquo;<samp>[</samp>&rsquo; and &lsquo;<samp>]</samp>&rsquo;.  See <a href="Quoting-macro-arguments.html#Quoting-macro-arguments">Quoting macro arguments</a>.
</p>
<p>Some macros take optional arguments, which this documentation represents
as <span class="roman">[</span><var>arg</var><span class="roman">]</span> (not to be confused with the quote characters).
You may
just leave them empty,
omit the trailing commas,
or use &lsquo;<samp>[]</samp>&rsquo; to make the emptiness of the
argument explicit. The
three lines below are equivalent:
</p>
<div class="example">
<pre class="example">AC_CHECK_HEADERS([stdio.h],,,)
AC_CHECK_HEADERS([stdio.h])
AC_CHECK_HEADERS([stdio.h], [], [], [])
</pre></div>

<p>It is best to put each macro call on its own line in
<samp>configure.ac</samp>.  Most of the macros don&rsquo;t add extra newlines; they
rely on the newline after the macro call to terminate the commands.
This approach makes the generated <code>configure</code> script a little
easier to read by not inserting lots of blank lines.  It is generally
safe to set shell variables on the same line as a macro call, because
the shell allows assignments without intervening newlines.
</p>
<p>You can include comments in <samp>configure.ac</samp> files by starting them
with the &lsquo;<samp>#</samp>&rsquo;.  For example, it is helpful to begin
<samp>configure.ac</samp> files with a line like this:
</p>
<div class="example">
<pre class="example"># Process this file with autoconf to produce a configure script.
</pre></div>

<p><strong>TODO: improve wording</strong><br>
There are special sequences called <em>quadrigraphs</em> that may be used 
to effect the presence of characters special to Autoconf in the produced 
configure script.  For example, &lsquo;<samp>@&lt;:@</samp>&rsquo; is transformed into 
&lsquo;<samp>[</samp>&rsquo;, and &lsquo;<samp>@:&gt;@</samp>&rsquo; into &lsquo;<samp>]</samp>&rsquo;.  See <a href="Quadrigraphs.html#Quadrigraphs">Quadrigraphs</a>.
</p>

<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Quoting-macro-arguments.html#Quoting-macro-arguments" accesskey="1">Quoting macro arguments</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Quadrigraphs.html#Quadrigraphs" accesskey="2">Quadrigraphs</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Macro-name-quoting.html#Macro-name-quoting" accesskey="3">Macro name quoting</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<div class="header">
<p>
Next: <a href="Autoconf-Input-Layout.html#Autoconf-Input-Layout" accesskey="n" rel="next">Autoconf Input Layout</a>, Up: <a href="Format-of-configure_002eac.html#Format-of-configure_002eac" accesskey="u" rel="up">Format of <samp>configure.ac</samp></a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
