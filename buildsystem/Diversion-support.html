<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Diversion support</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Diversion support">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Diversion support">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Programming-in-M4sugar.html#Programming-in-M4sugar" rel="up" title="Programming in M4sugar">
<link href="Conditional-constructs.html#Conditional-constructs" rel="next" title="Conditional constructs">
<link href="Diagnostic-Macros.html#Diagnostic-Macros" rel="prev" title="Diagnostic Macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Diversion-support"></a>
<div class="header">
<p>
Next: <a href="Conditional-constructs.html#Conditional-constructs" accesskey="n" rel="next">Conditional constructs</a>, Previous: <a href="Diagnostic-Macros.html#Diagnostic-Macros" accesskey="p" rel="prev">Diagnostic Macros</a>, Up: <a href="Programming-in-M4sugar.html#Programming-in-M4sugar" accesskey="u" rel="up">Programming in M4sugar</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Diversion-support-1"></a>
<h4 class="subsection">15.6.3 Diversion support</h4>

<p>M4sugar makes heavy use of diversions under the hood, because it is
often the case that
text that must appear early in the output is not discovered until late
in the input.  Additionally, some of the topological sorting algorithms
used in resolving macro dependencies use diversions.  However, most
macros should not need to change diversions directly, but rather rely on
higher-level M4sugar macros to manage diversions transparently.  If you
change diversions improperly, you risk generating a syntactically
invalid script, because an incorrect diversion will violate assumptions
made by many macros about whether prerequisite text has been previously
output.  In short, if you manually change the diversion, you should not
expect any macros provided by the Autoconf package to work until you
have restored the diversion stack back to its original state.
</p>
<p>In the rare case that it is necessary to write a macro that explicitly
outputs text to a different diversion, it is important to be aware of an
M4 limitation regarding diversions: text only goes to a diversion if it
is not part of argument collection.  Therefore, any macro that changes
the current diversion cannot be used as an unquoted argument to another
macro, but must be expanded at the top level.  The macro
<code>m4_expand</code> will diagnose any attempt to change diversions, since
it is generally useful only as an argument to another macro.  The
following example shows what happens when diversion manipulation is
attempted within macro arguments:
</p>
<div class="example">
<pre class="example">m4_do([normal text]
m4_divert_push([KILL])unwanted[]m4_divert_pop([KILL])
[m4_divert_push([KILL])discarded[]m4_divert_pop([KILL])])dnl
&rArr;normal text
&rArr;unwanted
</pre></div>

<p>Notice that the unquoted text <code>unwanted</code> is output, even though it
was processed while the current diversion was <code>KILL</code>, because it
was collected as part of the argument to <code>m4_do</code>.  However, the
text <code>discarded</code> disappeared as desired, because the diversion
changes were single-quoted, and were not expanded until the top-level
rescan of the output of <code>m4_do</code>.
</p>
<p>To make diversion management easier, M4sugar uses the concept of named
diversions.  Rather than using diversion numbers directly, it is nicer
to associate a name with each diversion.  The diversion number associated
with a particular diversion name is an implementation detail, and a
syntax warning is issued if a diversion number is used instead of a
name.  In general, you should not output text
to a named diversion until after calling the appropriate initialization
routine for your language (<code>m4_init</code>, <code>AS_INIT</code>,
<code>AT_INIT</code>, &hellip;), although there are some exceptions documented
below.
</p>
<p>M4sugar defines two named diversions.
</p><dl compact="compact">
<dt><code>KILL</code></dt>
<dd><p>Text written to this diversion is discarded.  This is the default
diversion once M4sugar is initialized.
</p></dd>
<dt><code>GROW</code></dt>
<dd><p>This diversion is used behind the scenes by topological sorting macros,
such as <code>AC_REQUIRE</code>.
</p></dd>
</dl>

<p>M4sh adds several more named diversions.
</p><dl compact="compact">
<dt><code>BINSH</code></dt>
<dd><p>This diversion is reserved for the &lsquo;<samp>#!</samp>&rsquo; interpreter line.
</p></dd>
<dt><code>HEADER-REVISION</code></dt>
<dd><p>This diversion holds text from <code>AC_REVISION</code>.
</p></dd>
<dt><code>HEADER-COMMENT</code></dt>
<dd><p>This diversion holds comments about the purpose of a file.
</p></dd>
<dt><code>HEADER-COPYRIGHT</code></dt>
<dd><p>This diversion is managed by <code>AC_COPYRIGHT</code>.
</p></dd>
<dt><code>M4SH-SANITIZE</code></dt>
<dd><p>This diversion contains M4sh sanitization code, used to ensure M4sh is
executing in a reasonable shell environment.
</p></dd>
<dt><code>M4SH-INIT</code></dt>
<dd><p>This diversion contains M4sh initialization code, initializing variables
that are required by other M4sh macros.
</p></dd>
<dt><code>BODY</code></dt>
<dd><p>This diversion contains the body of the shell code, and is the default
diversion once M4sh is initialized.
</p></dd>
</dl>

<p>Autotest inherits diversions from M4sh, and changes the default
diversion from <code>BODY</code> back to <code>KILL</code>.  It also adds several
more named diversions, with the following subset designed for developer
use.
</p><dl compact="compact">
<dt><code>PREPARE_TESTS</code></dt>
<dd><p>This diversion contains initialization sequences which are executed
after <samp>atconfig</samp> and <samp>atlocal</samp>, and after all command line
arguments have been parsed, but prior to running any tests.  It can be
used to set up state that is required across all tests.  This diversion
will work even before <code>AT_INIT</code>.
</p></dd>
</dl>

<p>Autoconf inherits diversions from M4sh, and adds the following named
diversions which developers can utilize.
</p><dl compact="compact">
<dt><code>DEFAULTS</code></dt>
<dd><p>This diversion contains shell variable assignments to set defaults that
must be in place before arguments are parsed.  This diversion is placed
early enough in <samp>configure</samp> that it is unsafe to expand any
autoconf macros into this diversion.
</p></dd>
<dt><code>HELP_ENABLE</code></dt>
<dd><p>If <code>AC_PRESERVE_HELP_ORDER</code> was used, then text placed in this
diversion will be included as part of a quoted here-doc providing all of
the <samp>--help</samp> output of <samp>configure</samp> related to options
created by <code>AC_ARG_WITH</code> and <code>AC_ARG_ENABLE</code>.
</p></dd>
<dt><code>INIT_PREPARE</code></dt>
<dd><p>This diversion occurs after all command line options have been parsed,
but prior to the main body of the <samp>configure</samp> script.  This
diversion is the last chance to insert shell code such as variable
assignments or shell function declarations that will used by the
expansion of other macros.
</p></dd>
</dl>

<p>For now, the remaining named diversions of Autoconf, Autoheader, and
Autotest are not documented.  In other words,
intentionally outputting text into an undocumented diversion is subject
to breakage in a future release of Autoconf.
</p>
<dl>
<dt><a name="index-m4_005fcleardivert-1"></a>Macro: <strong>m4_cleardivert</strong> <em>(<var>diversion</var>&hellip;)</em></dt>
<dd><a name="index-m4_005fcleardivert"></a>
<p>Permanently discard any text that has been diverted into
<var>diversion</var>.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fdivert_005fonce-1"></a>Macro: <strong>m4_divert_once</strong> <em>(<var>diversion</var>, <span class="roman">[</span><var>content</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fdivert_005fonce"></a>
<p>Similar to <code>m4_divert_text</code>, except that <var>content</var> is only
output to <var>diversion</var> if this is the first time that
<code>m4_divert_once</code> has been called with its particular arguments.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fdivert_005fpop-1"></a>Macro: <strong>m4_divert_pop</strong> <em>(<span class="roman">[</span><var>diversion</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fdivert_005fpop"></a>
<p>If provided, check that the current diversion is indeed <var>diversion</var>.
Then change to the diversion located earlier on the stack, giving an
error if an attempt is made to pop beyond the initial m4sugar diversion
of <code>KILL</code>.
</p></dd></dl>

<dl>
<dt><a name="index-m4_005fdivert_005fpush-1"></a>Macro: <strong>m4_divert_push</strong> <em>(<var>diversion</var>)</em></dt>
<dd><a name="index-m4_005fdivert_005fpush"></a>
<p>Remember the former diversion on the diversion stack, and output
subsequent text into <var>diversion</var>.  M4sugar maintains a diversion
stack, and issues an error if there is not a matching pop for every
push.
</p></dd></dl>

<a name="m4_005fdivert_005ftext"></a><dl>
<dt><a name="index-m4_005fdivert_005ftext-1"></a>Macro: <strong>m4_divert_text</strong> <em>(<var>diversion</var>, <span class="roman">[</span><var>content</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-m4_005fdivert_005ftext"></a>
<p>Output <var>content</var> and a newline into <var>diversion</var>, without
affecting the current diversion.  Shorthand for:
</p><div class="example">
<pre class="example">m4_divert_push([<var>diversion</var>])<var>content</var>
m4_divert_pop([<var>diversion</var>])dnl
</pre></div>

<p>One use of <code>m4_divert_text</code> is to develop two related macros, where
macro &lsquo;<samp>MY_A</samp>&rsquo; does the work, but adjusts what work is performed
based on whether the optional macro &lsquo;<samp>MY_B</samp>&rsquo; has also been expanded.
Of course, it is possible to use <code>AC_BEFORE</code> within <code>MY_A</code> to
require that &lsquo;<samp>MY_B</samp>&rsquo; occurs first, if it occurs at all.  But this
imposes an ordering restriction on the user; it would be nicer if macros
&lsquo;<samp>MY_A</samp>&rsquo; and &lsquo;<samp>MY_B</samp>&rsquo; can be invoked in either order.  The trick
is to let &lsquo;<samp>MY_B</samp>&rsquo; leave a breadcrumb in an early diversion, which
&lsquo;<samp>MY_A</samp>&rsquo; can then use to determine whether &lsquo;<samp>MY_B</samp>&rsquo; has been
expanded.
</p>
<div class="example">
<pre class="example">AC_DEFUN([MY_A],
[# various actions
if test -n &quot;$b_was_used&quot;; then
  # extra action
fi])
AC_DEFUN([MY_B],
[AC_REQUIRE([MY_A])dnl
m4_divert_text([INIT_PREPARE], [b_was_used=true])])
</pre></div>

</dd></dl>

<dl>
<dt><a name="index-m4_005finit-1"></a>Macro: <strong>m4_init</strong></dt>
<dd><a name="index-m4_005finit"></a>
<p>Initialize the M4sugar environment, setting up the default named
diversion to be <code>KILL</code>.
</p></dd></dl>

<div class="header">
<p>
Next: <a href="Conditional-constructs.html#Conditional-constructs" accesskey="n" rel="next">Conditional constructs</a>, Previous: <a href="Diagnostic-Macros.html#Diagnostic-Macros" accesskey="p" rel="prev">Diagnostic Macros</a>, Up: <a href="Programming-in-M4sugar.html#Programming-in-M4sugar" accesskey="u" rel="up">Programming in M4sugar</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
