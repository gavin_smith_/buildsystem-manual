<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Configuring Other Packages in Subdirectories</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Configuring Other Packages in Subdirectories">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Configuring Other Packages in Subdirectories">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Other-Autoconf-macros.html#Other-Autoconf-macros" rel="up" title="Other Autoconf macros">
<link href="Default-Prefix.html#Default-Prefix" rel="next" title="Default Prefix">
<link href="Printing-Messages.html#Printing-Messages" rel="prev" title="Printing Messages">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Configuring-Other-Packages-in-Subdirectories"></a>
<div class="header">
<p>
Next: <a href="Default-Prefix.html#Default-Prefix" accesskey="n" rel="next">Default Prefix</a>, Previous: <a href="Printing-Messages.html#Printing-Messages" accesskey="p" rel="prev">Printing Messages</a>, Up: <a href="Other-Autoconf-macros.html#Other-Autoconf-macros" accesskey="u" rel="up">Other Autoconf macros</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Configuring-Other-Packages-in-Subdirectories-1"></a>
<h3 class="section">15.4 Configuring Other Packages in Subdirectories</h3>
<a name="index-Configure-subdirectories"></a>
<a name="index-Subdirectory-configure"></a>

<p>In most situations, calling <code>AC_OUTPUT</code> is sufficient to produce
makefiles in subdirectories.  However, <code>configure</code> scripts
that control more than one independent package can use
<code>AC_CONFIG_SUBDIRS</code> to run <code>configure</code> scripts for other
packages in subdirectories.
</p>
<dl>
<dt><a name="index-AC_005fCONFIG_005fSUBDIRS-2"></a>Macro: <strong>AC_CONFIG_SUBDIRS</strong> <em>(<var>dir</var> &hellip;)</em></dt>
<dd><a name="index-AC_005fCONFIG_005fSUBDIRS-1"></a>
<a name="index-subdirs"></a>
<p>Make <code>AC_OUTPUT</code> run <code>configure</code> in each subdirectory
<var>dir</var> in the given blank-or-newline-separated list.  Each <var>dir</var> should
be a literal, i.e., please do not use:
</p>
<div class="example">
<pre class="example">if test &quot;x$package_foo_enabled&quot; = xyes; then
  my_subdirs=&quot;$my_subdirs foo&quot;
fi
AC_CONFIG_SUBDIRS([$my_subdirs])
</pre></div>

<p>because this prevents &lsquo;<samp>./configure --help=recursive</samp>&rsquo; from
displaying the options of the package <code>foo</code>.  Instead, you should
write:
</p>
<div class="example">
<pre class="example">if test &quot;x$package_foo_enabled&quot; = xyes; then
  AC_CONFIG_SUBDIRS([foo])
fi
</pre></div>

<p>If a given <var>dir</var> is not found at <code>configure</code> run time, a
warning is reported; if the subdirectory is optional, write:
</p>
<div class="example">
<pre class="example">if test -d &quot;$srcdir/foo&quot;; then
  AC_CONFIG_SUBDIRS([foo])
fi
</pre></div>

<p>If a given <var>dir</var> contains <code>configure.gnu</code>, it is run instead
of <code>configure</code>.  This is for packages that might use a
non-Autoconf script <code>Configure</code>, which can&rsquo;t be called through a
wrapper <code>configure</code> since it would be the same file on
case-insensitive file systems.  Likewise, if a <var>dir</var> contains
<samp>configure.in</samp> but no <code>configure</code>, the Cygnus
<code>configure</code> script found by <code>AC_CONFIG_AUX_DIR</code> is used.
</p>
<p>The subdirectory <code>configure</code> scripts are given the same command
line options that were given to this <code>configure</code> script, with minor
changes if needed, which include:
</p>
<ul class="no-bullet">
<li>- adjusting a relative name for the cache file;

</li><li>- adjusting a relative name for the source directory;

</li><li>- propagating the current value of <code>$prefix</code>, including if it was
defaulted, and if the default values of the top level and of the subdirectory
<samp>configure</samp> differ.
</li></ul>

<p>This macro also sets the output variable <code>subdirs</code> to the list of
directories &lsquo;<samp><var>dir</var> &hellip;</samp>&rsquo;.  Make rules can use
this variable to determine which subdirectories to recurse into.
</p>
<p>This macro may be called multiple times.
</p></dd></dl>


<div class="header">
<p>
Next: <a href="Default-Prefix.html#Default-Prefix" accesskey="n" rel="next">Default Prefix</a>, Previous: <a href="Printing-Messages.html#Printing-Messages" accesskey="p" rel="prev">Printing Messages</a>, Up: <a href="Other-Autoconf-macros.html#Other-Autoconf-macros" accesskey="u" rel="up">Other Autoconf macros</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
