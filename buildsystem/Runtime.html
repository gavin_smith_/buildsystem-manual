<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Runtime</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Runtime">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Runtime">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Writing-Tests.html#Writing-Tests" rel="up" title="Writing Tests">
<link href="Systemology.html#Systemology" rel="next" title="Systemology">
<link href="Running-the-Linker.html#Running-the-Linker" rel="prev" title="Running the Linker">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Runtime"></a>
<div class="header">
<p>
Next: <a href="Systemology.html#Systemology" accesskey="n" rel="next">Systemology</a>, Previous: <a href="Running-the-Linker.html#Running-the-Linker" accesskey="p" rel="prev">Running the Linker</a>, Up: <a href="Writing-Tests.html#Writing-Tests" accesskey="u" rel="up">Writing Tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Checking-Runtime-Behavior"></a>
<h4 class="subsection">8.4.6 Checking Runtime Behavior</h4>

<p>Sometimes you need to find out how a system performs at runtime, such
as whether a given function has a certain capability or bug.  If you
can, make such checks when your program runs instead of when it is
configured.  You can check for things like the machine&rsquo;s endianness when
your program initializes itself.
</p>
<p>If you really need to test for a runtime behavior while configuring,
you can write a test program to determine the result, and compile and
run it using <code>AC_RUN_IFELSE</code>.  Avoid running test programs if
possible, because this prevents people from configuring your package for
cross-compiling.
</p>
<a name="AC_005fRUN_005fIFELSE"></a><dl>
<dt><a name="index-AC_005fRUN_005fIFELSE-1"></a>Macro: <strong>AC_RUN_IFELSE</strong> <em>(<var>input</var>, <span class="roman">[</span><var>action-if-true</var><span class="roman">]</span>,   <span class="roman">[</span><var>action-if-false</var><span class="roman">]</span>, <span class="roman">[</span><var>action-if-cross-compiling</var> = &lsquo;<samp>AC_MSG_FAILURE</samp>&rsquo;<span class="roman">]</span>)</em></dt>
<dd><a name="index-AC_005fRUN_005fIFELSE"></a>
<p>Run the compiler (and compilation flags) and the linker of the current
language (see <a href="Language-Choice.html#Language-Choice">Language Choice</a>) on the <var>input</var>, then execute the
resulting program.  If the program returns an exit
status of 0 when executed, run shell commands <var>action-if-true</var>.
Otherwise, run shell commands <var>action-if-false</var>.
</p>
<p>The <var>input</var> can be made by <code>AC_LANG_PROGRAM</code> and friends.
<code>LDFLAGS</code> and <code>LIBS</code> are used for linking, in addition to the
compilation flags of the current language (see <a href="Language-Choice.html#Language-Choice">Language Choice</a>).
Additionally, <var>action-if-true</var> can run <code>./conftest$EXEEXT</code>
for further testing.
</p>
<p>In the <var>action-if-false</var> section, the failing exit status is
available in the shell variable &lsquo;<samp>$?</samp>&rsquo;.  This exit status might be
that of a failed compilation, or it might be that of a failed program
execution.
</p>
<p>If cross-compilation mode is enabled (this is the case if either the
compiler being used does not produce executables that run on the system
where <code>configure</code> is being run, or if the options <code>--build</code>
and <code>--host</code> were both specified and their values are different),
then the test program is
not run.  If the optional shell commands <var>action-if-cross-compiling</var>
are given, those commands are run instead; typically these commands
provide pessimistic defaults that allow cross-compilation to work even
if the guess was wrong.  If the fourth argument is empty or omitted, but
cross-compilation is detected, then <code>configure</code> prints an error
message and exits.  If you want your package to be useful in a
cross-compilation scenario, you <em>should</em> provide a non-empty
<var>action-if-cross-compiling</var> clause, as well as wrap the
<code>AC_RUN_IFELSE</code> compilation inside an <code>AC_CACHE_CHECK</code>
(see <a href="Caching-Results.html#Caching-Results">Caching Results</a>) which allows the user to override the
pessimistic default if needed.
</p>
<p>It is customary to report unexpected failures with
<code>AC_MSG_FAILURE</code>.
</p></dd></dl>

<p><code>autoconf</code> prints a warning message when creating
<code>configure</code> each time it encounters a call to
<code>AC_RUN_IFELSE</code> with no <var>action-if-cross-compiling</var> argument
given.  If you are not concerned about users configuring your package
for cross-compilation, you may ignore the warning.  A few of the macros
distributed with Autoconf produce this warning message; but if this is a
problem for you, please report it as a bug, along with an appropriate
pessimistic guess to use instead.
</p>
<p>To configure for cross-compiling you can also choose a value for those
parameters based on the canonical system name (see <a href="Manual-Configuration.html#Manual-Configuration">Manual Configuration</a>).  Alternatively, set up a test results cache file with
the correct values for the host system (see <a href="Caching-Results.html#Caching-Results">Caching Results</a>).
</p>
<a name="index-cross_005fcompiling"></a>
<p>To provide a default for calls of <code>AC_RUN_IFELSE</code> that are embedded
in other macros, including a few of the ones that come with Autoconf,
you can test whether the shell variable <code>cross_compiling</code> is set to
&lsquo;<samp>yes</samp>&rsquo;, and then use an alternate method to get the results instead
of calling the macros.
</p>
<p>It is also permissible to temporarily assign to <code>cross_compiling</code>
in order to force tests to behave as though they are in a
cross-compilation environment, particularly since this provides a way to
test your <var>action-if-cross-compiling</var> even when you are not using a
cross-compiler.
</p>
<div class="example">
<pre class="example"># We temporarily set cross-compile mode to force AC_COMPUTE_INT
# to use the slow link-only method
save_cross_compiling=$cross_compiling
cross_compiling=yes
AC_COMPUTE_INT([&hellip;])
cross_compiling=$save_cross_compiling
</pre></div>

<p>A C or C++ runtime test should be portable.
See <a href="Portable-C-and-C_002b_002b.html#Portable-C-and-C_002b_002b">Portable C and C++</a>.
</p>
<p>Erlang tests must exit themselves the Erlang VM by calling the <code>halt/1</code>
function: the given status code is used to determine the success of the test
(status is <code>0</code>) or its failure (status is different than <code>0</code>), as
explained above.  It must be noted that data output through the standard output
(e.g., using <code>io:format/2</code>) may be truncated when halting the VM.
Therefore, if a test must output configuration information, it is recommended
to create and to output data into the temporary file named <samp>conftest.out</samp>,
using the functions of module <code>file</code>.  The <code>conftest.out</code> file is
automatically deleted by the <code>AC_RUN_IFELSE</code> macro.  For instance, a
simplified implementation of Autoconf&rsquo;s <code>AC_ERLANG_SUBST_LIB_DIR</code>
macro is:
</p>
<div class="example">
<pre class="example">AC_INIT([LibdirTest], [1.0], [bug-libdirtest@example.org])
AC_ERLANG_NEED_ERL
AC_LANG(Erlang)
AC_RUN_IFELSE(
  [AC_LANG_PROGRAM([], [dnl
    file:write_file(&quot;conftest.out&quot;, code:lib_dir()),
    halt(0)])],
  [echo &quot;code:lib_dir() returned: `cat conftest.out`&quot;],
  [AC_MSG_FAILURE([test Erlang program execution failed])])
</pre></div>


<div class="header">
<p>
Next: <a href="Systemology.html#Systemology" accesskey="n" rel="next">Systemology</a>, Previous: <a href="Running-the-Linker.html#Running-the-Linker" accesskey="p" rel="prev">Running the Linker</a>, Up: <a href="Writing-Tests.html#Writing-Tests" accesskey="u" rel="up">Writing Tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
