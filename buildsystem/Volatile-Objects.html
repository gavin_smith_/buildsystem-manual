<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Volatile Objects</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Volatile Objects">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Volatile Objects">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Portable-C-and-C_002b_002b.html#Portable-C-and-C_002b_002b" rel="up" title="Portable C and C++">
<link href="Floating-Point-Portability.html#Floating-Point-Portability" rel="next" title="Floating Point Portability">
<link href="Buffer-Overruns.html#Buffer-Overruns" rel="prev" title="Buffer Overruns">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Volatile-Objects"></a>
<div class="header">
<p>
Next: <a href="Floating-Point-Portability.html#Floating-Point-Portability" accesskey="n" rel="next">Floating Point Portability</a>, Previous: <a href="Buffer-Overruns.html#Buffer-Overruns" accesskey="p" rel="prev">Buffer Overruns</a>, Up: <a href="Portable-C-and-C_002b_002b.html#Portable-C-and-C_002b_002b" accesskey="u" rel="up">Portable C and C++</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Volatile-Objects-1"></a>
<h4 class="subsection">C.3.7 Volatile Objects</h4>
<a name="index-volatile-objects"></a>

<p>The keyword <code>volatile</code> is often misunderstood in portable code.
Its use inhibits some memory-access optimizations, but programmers often
wish that it had a different meaning than it actually does.
</p>
<p><code>volatile</code> was designed for code that accesses special objects like
memory-mapped device registers whose contents spontaneously change.
Such code is inherently low-level, and it is difficult to specify
portably what <code>volatile</code> means in these cases.  The C standard
says, &ldquo;What constitutes an access to an object that has
volatile-qualified type is implementation-defined,&rdquo; so in theory each
implementation is supposed to fill in the gap by documenting what
<code>volatile</code> means for that implementation.  In practice, though,
this documentation is usually absent or incomplete.
</p>
<p>One area of confusion is the distinction between objects defined with
volatile types, and volatile lvalues.  From the C standard&rsquo;s point of
view, an object defined with a volatile type has externally visible
behavior.  You can think of such objects as having little oscilloscope
probes attached to them, so that the user can observe some properties of
accesses to them, just as the user can observe data written to output
files.  However, the standard does not make it clear whether users can
observe accesses by volatile lvalues to ordinary objects.  For example:
</p>
<div class="example">
<pre class="example">/* Declare and access a volatile object.
   Accesses to X are &quot;visible&quot; to users.  */
static int volatile x;
x = 1;

/* Access two ordinary objects via a volatile lvalue.
   It's not clear whether accesses to *P are &quot;visible&quot;.  */
int y;
int *z = malloc (sizeof (int));
int volatile *p;
p = &amp;y;
*p = 1;
p = z;
*p = 1;
</pre></div>

<p>Programmers often wish that <code>volatile</code> meant &ldquo;Perform the memory
access here and now, without merging several memory accesses, without
changing the memory word size, and without reordering.&rdquo;  But the C
standard does not require this.  For objects defined with a volatile
type, accesses must be done before the next sequence point; but
otherwise merging, reordering, and word-size change is allowed.  Worse,
it is not clear from the standard whether volatile lvalues provide more
guarantees in general than nonvolatile lvalues, if the underlying
objects are ordinary.
</p>
<p>Even when accessing objects defined with a volatile type,
the C standard allows only
extremely limited signal handlers: in C99 the behavior is undefined if a signal
handler reads any nonlocal object, or writes to any nonlocal object
whose type is not <code>sig_atomic_t volatile</code>, or calls any standard
library function other than <code>abort</code>, <code>signal</code>, and
<code>_Exit</code>.  Hence C compilers need not worry about a signal handler
disturbing ordinary computation.  C11 and Posix allow some additional
behavior in a portable signal handler, but are still quite restrictive.
</p>
<p>Some C implementations allow memory-access optimizations within each
translation unit, such that actual behavior agrees with the behavior
required by the standard only when calling a function in some other
translation unit, and a signal handler acts like it was called from a
different translation unit.  The C99 standard hints that in these
implementations, objects referred to by signal handlers &ldquo;would require
explicit specification of <code>volatile</code> storage, as well as other
implementation-defined restrictions.&rdquo;  But unfortunately even for this
special case these other restrictions are often not documented well.
This area was significantly changed in C11, and eventually implementations
will probably head in the C11 direction, but this will take some time.
See <a href="http://gcc.gnu.org/onlinedocs/gcc/Volatiles.html#Volatiles">When is a Volatile Object Accessed?</a> in <cite>Using the
GNU Compiler Collection (GCC)</cite>, for some
restrictions imposed by GCC.  See <a href="http://www.gnu.org/software/libc/manual/html_node/Defining-Handlers.html#Defining-Handlers">Defining Signal Handlers</a> in <cite>The GNU C Library</cite>, for some
restrictions imposed by the GNU C library.  Restrictions
differ on other platforms.
</p>
<p>If possible, it is best to use a signal handler that fits within the
limits imposed by the C and Posix standards.
</p>
<p>If this is not practical, you can try the following rules of thumb.  A
signal handler should access only volatile lvalues, preferably lvalues
that refer to objects defined with a volatile type, and should not
assume that the accessed objects have an internally consistent state
if they are larger than a machine word.  Furthermore, installers
should employ compilers and compiler options that are commonly used
for building operating system kernels, because kernels often need more
from <code>volatile</code> than the C Standard requires, and installers who
compile an application in a similar environment can sometimes benefit
from the extra constraints imposed by kernels on compilers.
Admittedly we are handwaving somewhat here, as there are few
guarantees in this area; the rules of thumb may help to fix some bugs
but there is a good chance that they will not fix them all.
</p>
<p>For <code>volatile</code>, C++ has the same problems that C does.
Multithreaded applications have even more problems with <code>volatile</code>,
but they are beyond the scope of this section.
</p>
<p>The bottom line is that using <code>volatile</code> typically hurts
performance but should not hurt correctness.  In some cases its use
does help correctness, but these cases are often so poorly understood
that all too often adding <code>volatile</code> to a data structure merely
alleviates some symptoms of a bug while not fixing the bug in general.
</p>
<div class="header">
<p>
Next: <a href="Floating-Point-Portability.html#Floating-Point-Portability" accesskey="n" rel="next">Floating Point Portability</a>, Previous: <a href="Buffer-Overruns.html#Buffer-Overruns" accesskey="p" rel="prev">Buffer Overruns</a>, Up: <a href="Portable-C-and-C_002b_002b.html#Portable-C-and-C_002b_002b" accesskey="u" rel="up">Portable C and C++</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
