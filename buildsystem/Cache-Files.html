<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Cache Files</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Cache Files">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Cache Files">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Caching-Results.html#Caching-Results" rel="up" title="Caching Results">
<link href="Cache-Checkpointing.html#Cache-Checkpointing" rel="next" title="Cache Checkpointing">
<link href="Cache-Variable-Names.html#Cache-Variable-Names" rel="prev" title="Cache Variable Names">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Cache-Files"></a>
<div class="header">
<p>
Next: <a href="Cache-Checkpointing.html#Cache-Checkpointing" accesskey="n" rel="next">Cache Checkpointing</a>, Previous: <a href="Cache-Variable-Names.html#Cache-Variable-Names" accesskey="p" rel="prev">Cache Variable Names</a>, Up: <a href="Caching-Results.html#Caching-Results" accesskey="u" rel="up">Caching Results</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Cache-Files-1"></a>
<h4 class="subsection">9.6.2 Cache Files</h4>

<p>A cache file is a shell script that caches the results of configure
tests run on one system so they can be shared between configure scripts
and configure runs.  It is not useful on other systems.  If its contents
are invalid for some reason, the user may delete or edit it, or override
documented cache variables on the <code>configure</code> command line.
</p>
<p>By default, <code>configure</code> uses no cache file,
to avoid problems caused by accidental
use of stale cache files.
</p>
<p>To enable caching, <code>configure</code> accepts <samp>--config-cache</samp> (or
<samp>-C</samp>) to cache results in the file <samp>config.cache</samp>.
Alternatively, <samp>--cache-file=<var>file</var></samp> specifies that
<var>file</var> be the cache file.  The cache file is created if it does not
exist already.  When <code>configure</code> calls <code>configure</code> scripts in
subdirectories, it uses the <samp>--cache-file</samp> argument so that they
share the same cache.  See <a href="Subdirectories.html#Subdirectories">Subdirectories</a>, for information on
configuring subdirectories with the <code>AC_CONFIG_SUBDIRS</code> macro.
</p>
<p><samp>config.status</samp> only pays attention to the cache file if it is
given the <samp>--recheck</samp> option, which makes it rerun
<code>configure</code>.
</p>
<p>It is wrong to try to distribute cache files for particular system types.
There is too much room for error in doing that, and too much
administrative overhead in maintaining them.  For any features that
can&rsquo;t be guessed automatically, use the standard method of the canonical
system type and linking files (see <a href="Manual-Configuration.html#Manual-Configuration">Manual Configuration</a>).
</p>
<p>The site initialization script can specify a site-wide cache file to
use, instead of the usual per-program cache.  In this case, the cache
file gradually accumulates information whenever someone runs a new
<code>configure</code> script.  (Running <code>configure</code> merges the new cache
results with the existing cache file.)  This may cause problems,
however, if the system configuration (e.g., the installed libraries or
compilers) changes and the stale cache file is not deleted.
</p>
<p>If <code>configure</code> is interrupted at the right time when it updates
a cache file outside of the build directory where the <code>configure</code>
script is run, it may leave behind a temporary file named after the
cache file with digits following it.  You may safely delete such a file.
</p>

<div class="header">
<p>
Next: <a href="Cache-Checkpointing.html#Cache-Checkpointing" accesskey="n" rel="next">Cache Checkpointing</a>, Previous: <a href="Cache-Variable-Names.html#Cache-Variable-Names" accesskey="p" rel="prev">Cache Variable Names</a>, Up: <a href="Caching-Results.html#Caching-Results" accesskey="u" rel="up">Caching Results</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
