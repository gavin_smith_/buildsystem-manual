<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Header Templates</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Header Templates">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Header Templates">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Defining-Symbols.html#Defining-Symbols" rel="up" title="Defining Symbols">
<link href="autoheader-Invocation.html#autoheader-Invocation" rel="next" title="autoheader Invocation">
<link href="Defining-Symbols.html#Defining-Symbols" rel="prev" title="Defining Symbols">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Header-Templates"></a>
<div class="header">
<p>
Next: <a href="autoheader-Invocation.html#autoheader-Invocation" accesskey="n" rel="next">autoheader Invocation</a>, Up: <a href="Defining-Symbols.html#Defining-Symbols" accesskey="u" rel="up">Defining Symbols</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Configuration-Header-Templates"></a>
<h4 class="subsection">9.3.1 Configuration Header Templates</h4>
<a name="index-Configuration-Header-Template"></a>
<a name="index-Header-templates"></a>
<a name="index-config_002eh_002ein"></a>

<p>Your distribution should contain a template file that looks as you want
the final header file to look, including comments, with <code>#undef</code>
statements which are used as hooks.  For example, suppose your
<samp>configure.ac</samp> makes these calls:
</p>
<div class="example">
<pre class="example">AC_CONFIG_HEADERS([conf.h])
AC_CHECK_HEADERS([unistd.h])
</pre></div>

<p>Then you could have code like the following in <samp>conf.h.in</samp>.
The <samp>conf.h</samp> created by <code>configure</code> defines &lsquo;<samp>HAVE_UNISTD_H</samp>&rsquo;
to 1, if and only if the system has <samp>unistd.h</samp>.
</p>
<div class="example">
<pre class="example">/* Define as 1 if you have unistd.h.  */
#undef HAVE_UNISTD_H
</pre></div>

<p>The format of the template file is stricter than what the C preprocessor
is required to accept.  A directive line should contain only whitespace,
&lsquo;<samp>#undef</samp>&rsquo;, and &lsquo;<samp>HAVE_UNISTD_H</samp>&rsquo;.  The use of &lsquo;<samp>#define</samp>&rsquo;
instead of &lsquo;<samp>#undef</samp>&rsquo;, or of comments on the same line as
&lsquo;<samp>#undef</samp>&rsquo;, is strongly discouraged.  Each hook should only be listed
once.  Other preprocessor lines, such as &lsquo;<samp>#ifdef</samp>&rsquo; or
&lsquo;<samp>#include</samp>&rsquo;, are copied verbatim from the template into the
generated header.
</p>
<p>Since it is a tedious task to keep a template header up to date, you may
use <code>autoheader</code> to generate it, see <a href="autoheader-Invocation.html#autoheader-Invocation">autoheader Invocation</a>.
</p>
<p>During the instantiation of the header, each &lsquo;<samp>#undef</samp>&rsquo; line in the
template file for each symbol defined by &lsquo;<samp>AC_DEFINE</samp>&rsquo; is changed to an
appropriate &lsquo;<samp>#define</samp>&rsquo;. If the corresponding &lsquo;<samp>AC_DEFINE</samp>&rsquo; has not
been executed during the <code>configure</code> run, the &lsquo;<samp>#undef</samp>&rsquo; line is
commented out.  (This is important, e.g., for &lsquo;<samp>_POSIX_SOURCE</samp>&rsquo;:
on many systems, it can be implicitly defined by the compiler, and
undefining it in the header would then break compilation of subsequent
headers.)
</p>
<p>Currently, <em>all</em> remaining &lsquo;<samp>#undef</samp>&rsquo; lines in the header
template are commented out, whether or not there was a corresponding
&lsquo;<samp>AC_DEFINE</samp>&rsquo; for the macro name; but this behavior is not guaranteed
for future releases of Autoconf.
</p>
<p>Generally speaking, since you should not use &lsquo;<samp>#define</samp>&rsquo;, and you
cannot guarantee whether a &lsquo;<samp>#undef</samp>&rsquo; directive in the header
template will be converted to a &lsquo;<samp>#define</samp>&rsquo; or commented out in the
generated header file, the template file cannot be used for conditional
definition effects.  Consequently, if you need to use the construct
</p>
<div class="example">
<pre class="example">#ifdef THIS
# define THAT
#endif
</pre></div>

<p>you must place it outside of the template.
If you absolutely need to hook it to the config header itself, please put
the directives to a separate file, and &lsquo;<samp>#include</samp>&rsquo; that file from the
config header template.  If you are using <code>autoheader</code>, you would
probably use &lsquo;<samp>AH_BOTTOM</samp>&rsquo; to append the &lsquo;<samp>#include</samp>&rsquo; directive.
</p>

<div class="header">
<p>
Next: <a href="autoheader-Invocation.html#autoheader-Invocation" accesskey="n" rel="next">autoheader Invocation</a>, Up: <a href="Defining-Symbols.html#Defining-Symbols" accesskey="u" rel="up">Defining Symbols</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
