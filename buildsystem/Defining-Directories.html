<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Defining Directories</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Defining Directories">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Defining Directories">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Autoconf-FAQ.html#Autoconf-FAQ" rel="up" title="Autoconf FAQ">
<link href="Present-But-Cannot-Be-Compiled.html#Present-But-Cannot-Be-Compiled" rel="next" title="Present But Cannot Be Compiled">
<link href="Build-Directories.html#Build-Directories" rel="prev" title="Build Directories">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Defining-Directories"></a>
<div class="header">
<p>
Next: <a href="Present-But-Cannot-Be-Compiled.html#Present-But-Cannot-Be-Compiled" accesskey="n" rel="next">Present But Cannot Be Compiled</a>, Previous: <a href="Build-Directories.html#Build-Directories" accesskey="p" rel="prev">Build Directories</a>, Up: <a href="Autoconf-FAQ.html#Autoconf-FAQ" accesskey="u" rel="up">Autoconf FAQ</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="How-Do-I-_0023define-Installation-Directories_003f"></a>
<h4 class="subsection">H.2.3 How Do I <code>#define</code> Installation Directories?</h4>

<div class="display">
<pre class="display">My program needs library files, installed in <code>datadir</code> and
similar.  If I use

</pre><div class="example">
<pre class="example">AC_DEFINE_UNQUOTED([DATADIR], [$datadir],
  [Define to the read-only architecture-independent
   data directory.])
</pre></div>
<pre class="display">
I get

</pre><div class="example">
<pre class="example">#define DATADIR &quot;${prefix}/share&quot;
</pre></div>
</div>

<p>As already explained, this behavior is on purpose, mandated by the
GNU Coding Standards, see <a href="Installation-Directory-Variables.html#Installation-Directory-Variables">Installation Directory Variables</a>.  There are several means to achieve a similar goal:
</p>
<ul class="no-bullet">
<li>- Do not use <code>AC_DEFINE</code> but use your makefile to pass the
actual value of <code>datadir</code> via compilation flags.
See <a href="Installation-Directory-Variables.html#Installation-Directory-Variables">Installation Directory Variables</a>, for the details.

</li><li>- This solution can be simplified when compiling a program: you may either
extend the <code>CPPFLAGS</code>:

<div class="example">
<pre class="example">CPPFLAGS = -DDATADIR='&quot;$(datadir)&quot;' @CPPFLAGS@
</pre></div>

<p>If you are using Automake, you should use <code>AM_CPPFLAGS</code> instead:
</p>
<div class="example">
<pre class="example">AM_CPPFLAGS = -DDATADIR='&quot;$(datadir)&quot;'
</pre></div>

<p>Alternatively, create a dedicated header file:
</p>
<div class="example">
<pre class="example">DISTCLEANFILES = myprog-paths.h
myprog-paths.h: Makefile
        echo '#define DATADIR &quot;$(datadir)&quot;' &gt;$@
</pre></div>

<p>The gnulib module &lsquo;<samp>configmake</samp>&rsquo; provides such a header with all the
standard directory variables defined, see <a href="http://www.gnu.org/software/gnulib/manual/html_node/configmake.html#configmake">configmake</a> in <cite>GNU
Gnulib</cite>.
</p>
</li><li>- Use <code>AC_DEFINE</code> but have <code>configure</code> compute the literal
value of <code>datadir</code> and others.  Many people have wrapped macros to
automate this task; for an example, see the macro <code>AC_DEFINE_DIR</code> from
the <a href="http://www.gnu.org/software/autoconf-archive/">Autoconf Macro
Archive</a>.

<p>This solution does not conform to the GNU Coding Standards.
</p>
</li><li>- Note that all the previous solutions hard wire the absolute name of
these directories in the executables, which is not a good property.  You
may try to compute the names relative to <code>prefix</code>, and try to
find <code>prefix</code> at runtime, this way your package is relocatable.
</li></ul>



<div class="header">
<p>
Next: <a href="Present-But-Cannot-Be-Compiled.html#Present-But-Cannot-Be-Compiled" accesskey="n" rel="next">Present But Cannot Be Compiled</a>, Previous: <a href="Build-Directories.html#Build-Directories" accesskey="p" rel="prev">Build Directories</a>, Up: <a href="Autoconf-FAQ.html#Autoconf-FAQ" accesskey="u" rel="up">Autoconf FAQ</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
