<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Specifying Target Triplets</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Specifying Target Triplets">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Specifying Target Triplets">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Manual-Configuration.html#Manual-Configuration" rel="up" title="Manual Configuration">
<link href="Getting-System-Types.html#Getting-System-Types" rel="next" title="Getting System Types">
<link href="Manual-Configuration.html#Manual-Configuration" rel="prev" title="Manual Configuration">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Specifying-Target-Triplets"></a>
<div class="header">
<p>
Next: <a href="Getting-System-Types.html#Getting-System-Types" accesskey="n" rel="next">Getting System Types</a>, Up: <a href="Manual-Configuration.html#Manual-Configuration" accesskey="u" rel="up">Manual Configuration</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Specifying-target-triplets"></a>
<h4 class="subsubsection">8.3.6.1 Specifying target triplets</h4>
<a name="index-System-type"></a>
<a name="index-Target-triplet"></a>
<a name="Specifying-Names"></a>
<p><code>configure</code> can usually guess the canonical name for the type of
system it&rsquo;s running on.  To do so it runs a script called
<code>config.guess</code>, which infers the name using the <code>uname</code>
command or symbols predefined by the C preprocessor.
</p>
<p>Alternately, the user can specify the system type with command line
arguments to <code>configure</code> (see <a href="System-Type.html#System-Type">System Type</a>).  Doing so is
necessary when cross-compiling.  In the most complex case of
cross-compiling, three system types are involved.  The options to
specify them are:
</p>
<dl compact="compact">
<dt><samp>--build=<var>build-type</var></samp></dt>
<dd><p>The type of system on which the package is being configured and
compiled.  It defaults to the result of running <code>config.guess</code>.
Specifying a <var>build-type</var> that differs from <var>host-type</var> enables
cross-compilation mode.
</p>
</dd>
<dt><samp>--host=<var>host-type</var></samp></dt>
<dd><p>The type of system on which the package runs.  By default it is the
same as the build machine.  Specifying a <var>host-type</var> that differs
from <var>build-type</var>, when <var>build-type</var> was also explicitly
specified, enables cross-compilation mode.
</p>
</dd>
<dt><samp>--target=<var>target-type</var></samp></dt>
<dd><p>The type of system for which any compiler tools in the package
produce code.  By default, it is the same as host.
</p></dd>
</dl>

<p>For ordinary packages the target is meaningless and should not be used.
The target is for use by a package creating a compiler or similar,
and indicates what the created compiler should generate code for, if it
can cross-compile.  It generally selects various hard-coded CPU and
system conventions, since usually the compiler or tools under
construction themselves determine how the target works.
</p>
<p>If you mean to override the result of <code>config.guess</code>, use
<samp>--build</samp>, not <samp>--host</samp>, since the latter enables
cross-compilation.  For historical reasons,
whenever you specify <samp>--host</samp>,
be sure to specify <samp>--build</samp> too; this will be fixed in the
future.  So, to enter cross-compilation mode, use a command like this
</p>
<div class="example">
<pre class="example">./configure --build=i686-pc-linux-gnu --host=m68k-coff
</pre></div>

<p>Note that if you do not specify <samp>--host</samp>, <code>configure</code>
fails if it can&rsquo;t run the code generated by the specified compiler.  For
example, configuring as follows fails:
</p>
<div class="example">
<pre class="example">./configure CC=m68k-coff-gcc
</pre></div>

<p>When cross-compiling, <code>configure</code> will warn about any tools
(compilers, linkers, assemblers) whose name is not prefixed with the
host type.  This is an aid to users performing cross-compilation.
Continuing the example above, if a cross-compiler named <code>cc</code> is
used with a native <code>pkg-config</code>, then libraries found by
<code>pkg-config</code> will likely cause subtle build failures; but using
the names <code>m68k-coff-cc</code> and <code>m68k-coff-pkg-config</code>
avoids any confusion.  Avoiding the warning is as simple as creating the
correct symlinks naming the cross tools.
</p>
<a name="index-config_002esub"></a>
<p><code>configure</code> recognizes short aliases for many system types; for
example, &lsquo;<samp>decstation</samp>&rsquo; can be used instead of
&lsquo;<samp>mips-dec-ultrix4.2</samp>&rsquo;.  <code>configure</code> runs a script called
<code>config.sub</code> to canonicalize system type aliases.
</p>
<p>This section omits the description of the obsolete interface; see
<a href="Hosts-and-Cross_002dCompilation.html#Hosts-and-Cross_002dCompilation">Hosts and Cross-Compilation</a>.
</p>

<div class="header">
<p>
Next: <a href="Getting-System-Types.html#Getting-System-Types" accesskey="n" rel="next">Getting System Types</a>, Up: <a href="Manual-Configuration.html#Manual-Configuration" accesskey="u" rel="up">Manual Configuration</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
