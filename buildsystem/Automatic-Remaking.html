<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Automatic Remaking</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Automatic Remaking">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Automatic Remaking">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Autoconf-FAQ.html#Autoconf-FAQ" rel="up" title="Autoconf FAQ">
<link href="Build-Directories.html#Build-Directories" rel="next" title="Build Directories">
<link href="Autoconf-FAQ.html#Autoconf-FAQ" rel="prev" title="Autoconf FAQ">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Automatic-Remaking"></a>
<div class="header">
<p>
Next: <a href="Build-Directories.html#Build-Directories" accesskey="n" rel="next">Build Directories</a>, Up: <a href="Autoconf-FAQ.html#Autoconf-FAQ" accesskey="u" rel="up">Autoconf FAQ</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Automatic-Remaking-1"></a>
<h4 class="subsection">H.2.1 Automatic Remaking</h4>
<a name="index-Automatic-remaking"></a>
<a name="index-Remaking-automatically"></a>

<p>You can put rules like the following in the top-level <samp>Makefile.in</samp>
for a package to automatically update the configuration information when
you change the configuration files.  This example includes all of the
optional files, such as <samp>aclocal.m4</samp> and those related to
configuration header files.  Omit from the <samp>Makefile.in</samp> rules for
any of these files that your package does not use.
</p>
<p>The &lsquo;<samp>$(srcdir)/</samp>&rsquo; prefix is included because of limitations in the
<code>VPATH</code> mechanism.
</p>
<p>The <samp>stamp-</samp> files are necessary because the timestamps of
<samp>config.h.in</samp> and <samp>config.h</samp> are not changed if remaking
them does not change their contents.  This feature avoids unnecessary
recompilation.  You should include the file <samp>stamp-h.in</samp> in your
package&rsquo;s distribution, so that <code>make</code> considers
<samp>config.h.in</samp> up to date.  Don&rsquo;t use <code>touch</code>
(see <a href="Limitations-of-Usual-Tools.html#touch">Limitations of Usual Tools</a>); instead, use
<code>echo</code> (using
<code>date</code> would cause needless differences, hence CVS
conflicts, etc.).
</p>
<div class="example">
<pre class="example">$(srcdir)/configure: configure.ac aclocal.m4
        cd '$(srcdir)' &amp;&amp; autoconf

# autoheader might not change config.h.in, so touch a stamp file.
$(srcdir)/config.h.in: stamp-h.in ;
$(srcdir)/stamp-h.in: configure.ac aclocal.m4
        cd '$(srcdir)' &amp;&amp; autoheader
        echo timestamp &gt; '$(srcdir)/stamp-h.in'

config.h: stamp-h ;
stamp-h: config.h.in config.status
        ./config.status

Makefile: Makefile.in config.status
        ./config.status

config.status: configure
        ./config.status --recheck
</pre></div>

<p>(Be careful if you copy these lines directly into your makefile, as you
need to convert the indented lines to start with the tab character.)
</p>
<p>In addition, you should use
</p>
<div class="example">
<pre class="example">AC_CONFIG_FILES([stamp-h], [echo timestamp &gt; stamp-h])
</pre></div>

<p>so <samp>config.status</samp> ensures that <samp>config.h</samp> is considered up to
date.  See &lsquo;Output&rsquo;, for more information about <code>AC_OUTPUT</code>.
</p>
<p>See <a href="config_002estatus-Invocation.html#config_002estatus-Invocation">config.status Invocation</a>, for more examples of handling
configuration-related dependencies.
</p>
<div class="header">
<p>
Next: <a href="Build-Directories.html#Build-Directories" accesskey="n" rel="next">Build Directories</a>, Up: <a href="Autoconf-FAQ.html#Autoconf-FAQ" accesskey="u" rel="up">Autoconf FAQ</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
