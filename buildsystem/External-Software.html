<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: External Software</title>

<meta name="description" content="Automake and Autoconf Reference Manual: External Software">
<meta name="keywords" content="Automake and Autoconf Reference Manual: External Software">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Adding-options-to-a-configure-script.html#Adding-options-to-a-configure-script" rel="up" title="Adding options to a configure script">
<link href="Package-Options.html#Package-Options" rel="next" title="Package Options">
<link href="Help-Formatting.html#Help-Formatting" rel="prev" title="Help Formatting">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="External-Software"></a>
<div class="header">
<p>
Next: <a href="Package-Options.html#Package-Options" accesskey="n" rel="next">Package Options</a>, Previous: <a href="Help-Formatting.html#Help-Formatting" accesskey="p" rel="prev">Help Formatting</a>, Up: <a href="Adding-options-to-a-configure-script.html#Adding-options-to-a-configure-script" accesskey="u" rel="up">Adding options to a configure script</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Working-With-External-Software"></a>
<h3 class="section">11.2 Working With External Software</h3>
<a name="index-External-software"></a>

<p>Some packages require, or can optionally use, other software packages
that are already installed.  The user can give <code>configure</code>
command line options to specify which such external software to use.
The options have one of these forms:
</p>
<div class="example">
<pre class="example">--with-<var>package</var><span class="roman">[</span>=<var>arg</var><span class="roman">]</span>
--without-<var>package</var>
</pre></div>

<p>For example, <samp>--with-gnu-ld</samp> means work with the GNU linker
instead of some other linker.  <samp>--with-x</samp> means work with The X
Window System.
</p>
<p>The user can give an argument by following the package name with
&lsquo;<samp>=</samp>&rsquo; and the argument.  Giving an argument of &lsquo;<samp>no</samp>&rsquo; is for
packages that are used by default; it says to <em>not</em> use the
package.  An argument that is neither &lsquo;<samp>yes</samp>&rsquo; nor &lsquo;<samp>no</samp>&rsquo; could
include a name or number of a version of the other package, to specify
more precisely which other package this program is supposed to work
with.  If no argument is given, it defaults to &lsquo;<samp>yes</samp>&rsquo;.
<samp>--without-<var>package</var></samp> is equivalent to
<samp>--with-<var>package</var>=no</samp>.
</p>
<p>Normally <code>configure</code> scripts complain about
<samp>--with-<var>package</var></samp> options that they do not support.
See <a href="Option-Checking.html#Option-Checking">Option Checking</a>, for details, and for how to override the
defaults.
</p>
<p>For each external software package that may be used, <samp>configure.ac</samp>
should call <code>AC_ARG_WITH</code> to detect whether the <code>configure</code>
user asked to use it.  Whether each package is used or not by default,
and which arguments are valid, is up to you.
</p>
<a name="AC_005fARG_005fWITH"></a><dl>
<dt><a name="index-AC_005fARG_005fWITH-1"></a>Macro: <strong>AC_ARG_WITH</strong> <em>(<var>package</var>, <var>help-string</var>,   <span class="roman">[</span><var>action-if-given</var><span class="roman">]</span>, <span class="roman">[</span><var>action-if-not-given</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AC_005fARG_005fWITH"></a>
<p>If the user gave <code>configure</code> the option <samp>--with-<var>package</var></samp>
or <samp>--without-<var>package</var></samp>, run shell commands
<var>action-if-given</var>.  If neither option was given, run shell commands
<var>action-if-not-given</var>.  The name <var>package</var> indicates another
software package that this program should work with.  It should consist
only of alphanumeric characters, dashes, plus signs, and dots.
</p>
<p>The option&rsquo;s argument is available to the shell commands
<var>action-if-given</var> in the shell variable <code>withval</code>, which is
actually just the value of the shell variable named
<code>with_<var>package</var></code>, with any non-alphanumeric characters in
<var>package</var> changed into &lsquo;<samp>_</samp>&rsquo;.  You may use that variable instead,
if you wish.
</p>
<p>Note that <var>action-if-not-given</var> is not expanded until the point that
<code>AC_ARG_WITH</code> was expanded.  If you need the value of
<code>with_<var>package</var></code> set to a default value by the time argument
parsing is completed, use <code>m4_divert_text</code> to the <code>DEFAULTS</code>
diversion (see <a href="Diversion-support.html#m4_005fdivert_005ftext">m4_divert_text</a>) (if done as an argument to
<code>AC_ARG_WITH</code>, also provide non-diverted text to avoid a shell
syntax error).
</p>
<p>The argument <var>help-string</var> is a description of the option that
looks like this:
</p><div class="example">
<pre class="example">  --with-readline         support fancy command line editing
</pre></div>

<p><var>help-string</var> may be more than one line long, if more detail is
needed.  Just make sure the columns line up in &lsquo;<samp>configure
--help</samp>&rsquo;.  Avoid tabs in the help string.  The easiest way to provide the
proper leading whitespace is to format your <var>help-string</var> with the macro
<code>AS_HELP_STRING</code> (see <a href="Pretty-Help-Strings.html#Pretty-Help-Strings">Pretty Help Strings</a>).
</p>
<p>The following example shows how to use the <code>AC_ARG_WITH</code> macro in
a common situation.  You want to let the user decide whether to enable
support for an external library (e.g., the readline library); if the user
specified neither <samp>--with-readline</samp> nor <samp>--without-readline</samp>,
you want to enable support for readline only if the library is available
on the system.
</p>
<div class="example">
<pre class="example">AC_ARG_WITH([readline],
  [AS_HELP_STRING([--with-readline],
    [support fancy command line editing @&lt;:@default=check@:&gt;@])],
  [],
  [: m4_divert_text([DEFAULTS], [with_readline=check])])

LIBREADLINE=
AS_IF([test &quot;x$with_readline&quot; != xno],
  [AC_CHECK_LIB([readline], [main],
    [AC_SUBST([LIBREADLINE], [&quot;-lreadline -lncurses&quot;])
     AC_DEFINE([HAVE_LIBREADLINE], [1],
               [Define if you have libreadline])
    ],
    [if test &quot;x$with_readline&quot; != xcheck; then
       AC_MSG_FAILURE(
         [--with-readline was given, but test for readline failed])
     fi
    ], -lncurses)])
</pre></div>

<p>The next example shows how to use <code>AC_ARG_WITH</code> to give the user the
possibility to enable support for the readline library, in case it is still
experimental and not well tested, and is therefore disabled by default.
</p>
<div class="example">
<pre class="example">AC_ARG_WITH([readline],
  [AS_HELP_STRING([--with-readline],
    [enable experimental support for readline])],
  [],
  [with_readline=no])

LIBREADLINE=
AS_IF([test &quot;x$with_readline&quot; != xno],
  [AC_CHECK_LIB([readline], [main],
    [AC_SUBST([LIBREADLINE], [&quot;-lreadline -lncurses&quot;])
     AC_DEFINE([HAVE_LIBREADLINE], [1],
               [Define if you have libreadline])
    ],
    [AC_MSG_FAILURE(
       [--with-readline was given, but test for readline failed])],
    [-lncurses])])
</pre></div>

<p>The last example shows how to use <code>AC_ARG_WITH</code> to give the user the
possibility to disable support for the readline library, given that it is
an important feature and that it should be enabled by default.
</p>
<div class="example">
<pre class="example">AC_ARG_WITH([readline],
  [AS_HELP_STRING([--without-readline],
    [disable support for readline])],
  [],
  [with_readline=yes])

LIBREADLINE=
AS_IF([test &quot;x$with_readline&quot; != xno],
  [AC_CHECK_LIB([readline], [main],
    [AC_SUBST([LIBREADLINE], [&quot;-lreadline -lncurses&quot;])
     AC_DEFINE([HAVE_LIBREADLINE], [1],
               [Define if you have libreadline])
    ],
    [AC_MSG_FAILURE(
       [readline test failed (--without-readline to disable)])],
    [-lncurses])])
</pre></div>

<p>These three examples can be easily adapted to the case where
<code>AC_ARG_ENABLE</code> should be preferred to <code>AC_ARG_WITH</code> (see
<a href="Package-Options.html#Package-Options">Package Options</a>).
</p></dd></dl>

<div class="header">
<p>
Next: <a href="Package-Options.html#Package-Options" accesskey="n" rel="next">Package Options</a>, Previous: <a href="Help-Formatting.html#Help-Formatting" accesskey="p" rel="prev">Help Formatting</a>, Up: <a href="Adding-options-to-a-configure-script.html#Adding-options-to-a-configure-script" accesskey="u" rel="up">Adding options to a configure script</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
