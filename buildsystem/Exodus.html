<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Exodus</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Exodus">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Exodus">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="History-of-Autoconf.html#History-of-Autoconf" rel="up" title="History of Autoconf">
<link href="Leviticus.html#Leviticus" rel="next" title="Leviticus">
<link href="Genesis.html#Genesis" rel="prev" title="Genesis">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Exodus"></a>
<div class="header">
<p>
Next: <a href="Leviticus.html#Leviticus" accesskey="n" rel="next">Leviticus</a>, Previous: <a href="Genesis.html#Genesis" accesskey="p" rel="prev">Genesis</a>, Up: <a href="History-of-Autoconf.html#History-of-Autoconf" accesskey="u" rel="up">History of Autoconf</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Exodus-1"></a>
<h4 class="subsection">I.5.2 Exodus</h4>

<p>As I got feedback from users, I incorporated many improvements, using
Emacs to search and replace, cut and paste, similar changes in each of
the scripts.  As I adapted more GNU utilities packages to use
<code>configure</code> scripts, updating them all by hand became impractical.
Rich Murphey, the maintainer of the GNU graphics utilities, sent me
mail saying that the <code>configure</code> scripts were great, and asking if
I had a tool for generating them that I could send him.  No, I thought,
but I should!  So I started to work out how to generate them.  And the
journey from the slavery of hand-written <code>configure</code> scripts to the
abundance and ease of Autoconf began.
</p>
<p>Cygnus <code>configure</code>, which was being developed at around that time,
is table driven; it is meant to deal mainly with a discrete number of
system types with a small number of mainly unguessable features (such as
details of the object file format).  The automatic configuration system
that Brian Fox had developed for Bash takes a similar approach.  For
general use, it seems to me a hopeless cause to try to maintain an
up-to-date database of which features each variant of each operating
system has.  It&rsquo;s easier and more reliable to check for most features on
the fly&mdash;especially on hybrid systems that people have hacked on
locally or that have patches from vendors installed.
</p>
<p>I considered using an architecture similar to that of Cygnus
<code>configure</code>, where there is a single <code>configure</code> script that
reads pieces of <samp>configure.in</samp> when run.  But I didn&rsquo;t want to have
to distribute all of the feature tests with every package, so I settled
on having a different <code>configure</code> made from each
<samp>configure.in</samp> by a preprocessor.  That approach also offered more
control and flexibility.
</p>
<p>I looked briefly into using the Metaconfig package, by Larry Wall,
Harlan Stenn, and Raphael Manfredi, but I decided not to for several
reasons.  The <code>Configure</code> scripts it produces are interactive,
which I find quite inconvenient; I didn&rsquo;t like the ways it checked for
some features (such as library functions); I didn&rsquo;t know that it was
still being maintained, and the <code>Configure</code> scripts I had
seen didn&rsquo;t work on many modern systems (such as System V R4 and NeXT);
it wasn&rsquo;t flexible in what it could do in response to a feature&rsquo;s
presence or absence; I found it confusing to learn; and it was too big
and complex for my needs (I didn&rsquo;t realize then how much Autoconf would
eventually have to grow).
</p>
<p>I considered using Perl to generate my style of <code>configure</code>
scripts, but decided that M4 was better suited to the job of simple
textual substitutions: it gets in the way less, because output is
implicit.  Plus, everyone already has it.  (Initially I didn&rsquo;t rely on
the GNU extensions to M4.)  Also, some of my friends at the
University of Maryland had recently been putting M4 front ends on
several programs, including <code>tvtwm</code>, and I was interested in trying
out a new language.
</p>
<div class="header">
<p>
Next: <a href="Leviticus.html#Leviticus" accesskey="n" rel="next">Leviticus</a>, Previous: <a href="Genesis.html#Genesis" accesskey="p" rel="prev">Genesis</a>, Up: <a href="History-of-Autoconf.html#History-of-Autoconf" accesskey="u" rel="up">History of Autoconf</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
