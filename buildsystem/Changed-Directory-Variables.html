<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Changed Directory Variables</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Changed Directory Variables">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Changed Directory Variables">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Setting-Output-Variables.html#Setting-Output-Variables" rel="up" title="Setting Output Variables">
<link href="Defining-Symbols.html#Defining-Symbols" rel="next" title="Defining Symbols">
<link href="Installation-Directory-Variables.html#Installation-Directory-Variables" rel="prev" title="Installation Directory Variables">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Changed-Directory-Variables"></a>
<div class="header">
<p>
Previous: <a href="Installation-Directory-Variables.html#Installation-Directory-Variables" accesskey="p" rel="prev">Installation Directory Variables</a>, Up: <a href="Setting-Output-Variables.html#Setting-Output-Variables" accesskey="u" rel="up">Setting Output Variables</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Changed-Directory-Variables-1"></a>
<h4 class="subsection">9.2.5 Changed Directory Variables</h4>
<a name="index-datarootdir"></a>

<p>In Autoconf 2.60, the set of directory variables has changed, and the
defaults of some variables have been adjusted
(see <a href="Installation-Directory-Variables.html#Installation-Directory-Variables">Installation Directory Variables</a>) to changes in the
GNU Coding Standards.  Notably, <samp>datadir</samp>, <samp>infodir</samp>, and
<samp>mandir</samp> are now expressed in terms of <samp>datarootdir</samp>.  If you are
upgrading from an earlier Autoconf version, you may need to adjust your files
to ensure that the directory variables are substituted correctly
(see <a href="Defining-Directories.html#Defining-Directories">Defining Directories</a>), and that a definition of <samp>datarootdir</samp> is
in place.  For example, in a <samp>Makefile.in</samp>, adding
</p>
<div class="example">
<pre class="example">datarootdir = @datarootdir@
</pre></div>

<p>is usually sufficient.  If you use Automake to create <samp>Makefile.in</samp>,
it will add this for you.
</p>
<p>To help with the transition, Autoconf warns about files that seem to use
<code>datarootdir</code> without defining it.  In some cases, it then expands
the value of <code>$datarootdir</code> in substitutions of the directory
variables.  The following example shows such a warning:
</p>
<div class="example">
<pre class="example">$ <kbd>cat configure.ac</kbd>
AC_INIT
AC_CONFIG_FILES([Makefile])
AC_OUTPUT
$ <kbd>cat Makefile.in</kbd>
prefix = @prefix@
datadir = @datadir@
$ <kbd>autoconf</kbd>
$ <kbd>configure</kbd>
configure: creating ./config.status
config.status: creating Makefile
config.status: WARNING:
               Makefile.in seems to ignore the --datarootdir setting
$ <kbd>cat Makefile</kbd>
prefix = /usr/local
datadir = ${prefix}/share
</pre></div>

<p>Usually one can easily change the file to accommodate both older and newer
Autoconf releases:
</p>
<div class="example">
<pre class="example">$ <kbd>cat Makefile.in</kbd>
prefix = @prefix@
datarootdir = @datarootdir@
datadir = @datadir@
$ <kbd>configure</kbd>
configure: creating ./config.status
config.status: creating Makefile
$ <kbd>cat Makefile</kbd>
prefix = /usr/local
datarootdir = ${prefix}/share
datadir = ${datarootdir}
</pre></div>

<a name="index-AC_005fDATAROOTDIR_005fCHECKED"></a>
<p>In some cases, however, the checks may not be able to detect that a suitable
initialization of <code>datarootdir</code> is in place, or they may fail to detect
that such an initialization is necessary in the output file.  If, after
auditing your package, there are still spurious <samp>configure</samp> warnings about
<code>datarootdir</code>, you may add the line
</p>
<div class="example">
<pre class="example">AC_DEFUN([AC_DATAROOTDIR_CHECKED])
</pre></div>

<p>to your <samp>configure.ac</samp> to disable the warnings.  This is an exception
to the usual rule that you should not define a macro whose name begins with
<code>AC_</code> (see <a href="Macro-Names.html#Macro-Names">Macro Names</a>).
</p>
<div class="header">
<p>
Previous: <a href="Installation-Directory-Variables.html#Installation-Directory-Variables" accesskey="p" rel="prev">Installation Directory Variables</a>, Up: <a href="Setting-Output-Variables.html#Setting-Output-Variables" accesskey="u" rel="up">Setting Output Variables</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
