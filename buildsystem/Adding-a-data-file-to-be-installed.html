<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Adding a data file to be installed</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Adding a data file to be installed">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Adding a data file to be installed">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Tutorial.html#Tutorial" rel="up" title="Tutorial">
<link href="Creating-a-distribution.html#Creating-a-distribution" rel="next" title="Creating a distribution">
<link href="Adding-a-configure-flag.html#Adding-a-configure-flag" rel="prev" title="Adding a configure flag">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Adding-a-data-file-to-be-installed"></a>
<div class="header">
<p>
Next: <a href="Creating-a-distribution.html#Creating-a-distribution" accesskey="n" rel="next">Creating a distribution</a>, Previous: <a href="Adding-a-configure-flag.html#Adding-a-configure-flag" accesskey="p" rel="prev">Adding a configure flag</a>, Up: <a href="Tutorial.html#Tutorial" accesskey="u" rel="up">Tutorial</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Adding-a-data-file-to-be-installed-1"></a>
<h3 class="section">4.5 Adding a data file to be installed</h3>

<p>Some programs need to access data files when they are run.  For example, 
suppose we print a message from a file, <samp>message</samp>.
</p>
<p><code>hello.c</code>:
</p><div class="example">
<pre class="example">#include &lt;config.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

#define MESSAGE_FILE &quot;message&quot;

int
main (void)
{
  FILE *in;
  char *line = 0;
  size_t n = 0;
  ssize_t result;

  in = fopen (MESSAGE_FILE, &quot;r&quot;);
  if (!in)
    exit (1);

  result = getline (&amp;line, &amp;n, in);
  if (result == -1)
    exit (1);

  printf (&quot;%s&quot;, line);
}
</pre></div>

<p>Instead of hard-coding the location of the data file,
we could define &lsquo;<samp>MESSAGE_FILE</samp>&rsquo; in <samp>Makefile.am</samp>:
</p>
<div class="example">
<pre class="example">AM_CPPFLAGS = -DMESSAGE_FILE=\&quot;$(pkgdatadir)/message\&quot;
</pre></div>

<p><code>AM_CPPFLAGS</code> specifies C preprocessor flags to be used when 
building the program.  <code>pkgdatadir</code> is a Make variable that is 
automatically set from the <code>pkgdatadir</code> Autoconf output variable, 
which in turn is among the several output variables that Autoconf 
configure scripts always set specifying installation directories.  By 
default, this variable will have the value 
&lsquo;<samp>/usr/local/share/hello</samp>&rsquo;, so the data file will be looked for at 
&lsquo;<samp>/usr/local/share/hello/message</samp>&rsquo;.  The value of <code>pkgdatadir</code> 
can be changed using the <samp>--datadir</samp> option to 
<code>configure</code>.
</p>
<p>The &lsquo;<samp>AM_</samp>&rsquo; prefix on <code>AM_CPPFLAGS</code> is there to distinguish it 
from the <code>CPPFLAGS</code> Makefile variable, which may be set by the 
configure script and/or overridden from the command-line.
</p>
<p>We also need the following line in <samp>Makefile.am</samp> to distribute and 
install the data file:
</p>
<div class="example">
<pre class="example">dist_pkgdata_DATA = message
</pre></div>

<p>The &lsquo;<samp>DATA</samp>&rsquo; part of the variable name corresponds to the kind of 
object being listed in the variable&rsquo;s value, in this case data files.  
The value here is a single file called &lsquo;<samp>message</samp>&rsquo;.  The 
&lsquo;<samp>pkgdata_</samp>&rsquo; prefix indicates that the file should be installed in 
<code>pkgdatadir</code> by &lsquo;<samp>make install</samp>&rsquo;, and the &lsquo;<samp>dist_</samp>&rsquo; prefix 
indicates that the file should be included in the distribution archive 
created by &lsquo;<samp>make dist</samp>&rsquo;.
</p>
<div class="header">
<p>
Next: <a href="Creating-a-distribution.html#Creating-a-distribution" accesskey="n" rel="next">Creating a distribution</a>, Previous: <a href="Adding-a-configure-flag.html#Adding-a-configure-flag" accesskey="p" rel="prev">Adding a configure flag</a>, Up: <a href="Tutorial.html#Tutorial" accesskey="u" rel="up">Tutorial</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
