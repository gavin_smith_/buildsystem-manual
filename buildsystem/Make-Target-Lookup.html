<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Make Target Lookup</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Make Target Lookup">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Make Target Lookup">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="VPATH-and-Make.html#VPATH-and-Make" rel="up" title="VPATH and Make">
<link href="Single-Suffix-Rules.html#Single-Suffix-Rules" rel="next" title="Single Suffix Rules">
<link href="Tru64-Directory-Magic.html#Tru64-Directory-Magic" rel="prev" title="Tru64 Directory Magic">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Make-Target-Lookup"></a>
<div class="header">
<p>
Previous: <a href="Tru64-Directory-Magic.html#Tru64-Directory-Magic" accesskey="p" rel="prev">Tru64 Directory Magic</a>, Up: <a href="VPATH-and-Make.html#VPATH-and-Make" accesskey="u" rel="up">VPATH and Make</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Make-Target-Lookup-1"></a>
<h4 class="subsubsection">C.2.18.6 Make Target Lookup</h4>
<a name="index-VPATH_002c-resolving-target-pathnames"></a>

<p>GNU <code>make</code> uses a complex algorithm to decide when it
should use files found via a <code>VPATH</code> search.  See <a href="http://www.gnu.org/software/make/manual/html_node/Search-Algorithm.html#Search-Algorithm">How Directory Searches are Performed</a> in <cite>The GNU Make
Manual</cite>.
</p>
<p>If a target needs to be rebuilt, GNU <code>make</code> discards the
file name found during the <code>VPATH</code> search for this target, and
builds the file locally using the file name given in the makefile.
If a target does not need to be rebuilt, GNU <code>make</code> uses the
file name found during the <code>VPATH</code> search.
</p>
<p>Other <code>make</code> implementations, like NetBSD <code>make</code>, are
easier to describe: the file name found during the <code>VPATH</code> search
is used whether the target needs to be rebuilt or not.  Therefore
new files are created locally, but existing files are updated at their
<code>VPATH</code> location.
</p>
<p>OpenBSD and FreeBSD <code>make</code>, however,
never perform a
<code>VPATH</code> search for a dependency that has an explicit rule.
This is extremely annoying.
</p>
<p>When attempting a <code>VPATH</code> build for an autoconfiscated package
(e.g., <code>mkdir build &amp;&amp; cd build &amp;&amp; ../configure</code>), this means
GNU
<code>make</code> builds everything locally in the <samp>build</samp>
directory, while BSD <code>make</code> builds new files locally and
updates existing files in the source directory.
</p>
<div class="example">
<pre class="example">$ <kbd>cat Makefile</kbd>
VPATH = ..
all: foo.x bar.x
foo.x bar.x: newer.x
        @echo Building $@
$ <kbd>touch ../bar.x</kbd>
$ <kbd>touch ../newer.x</kbd>
$ <kbd>make</kbd>        # GNU make
Building foo.x
Building bar.x
$ <kbd>pmake</kbd>       # NetBSD make
Building foo.x
Building ../bar.x
$ <kbd>fmake</kbd>       # FreeBSD make, OpenBSD make
Building foo.x
Building bar.x
$ <kbd>tmake</kbd>       # Tru64 make
Building foo.x
Building bar.x
$ <kbd>touch ../bar.x</kbd>
$ <kbd>make</kbd>        # GNU make
Building foo.x
$ <kbd>pmake</kbd>       # NetBSD make
Building foo.x
$ <kbd>fmake</kbd>       # FreeBSD make, OpenBSD make
Building foo.x
Building bar.x
$ <kbd>tmake</kbd>       # Tru64 make
Building foo.x
Building bar.x
</pre></div>

<p>Note how NetBSD <code>make</code> updates <samp>../bar.x</samp> in its
VPATH location, and how FreeBSD, OpenBSD, and Tru64
<code>make</code> always
update <samp>bar.x</samp>, even when <samp>../bar.x</samp> is up to date.
</p>
<p>Another point worth mentioning is that once GNU <code>make</code> has
decided to ignore a <code>VPATH</code> file name (e.g., it ignored
<samp>../bar.x</samp> in the above example) it continues to ignore it when
the target occurs as a prerequisite of another rule.
</p>
<p>The following example shows that GNU <code>make</code> does not look up
<samp>bar.x</samp> in <code>VPATH</code> before performing the <code>.x.y</code> rule,
because it ignored the <code>VPATH</code> result of <samp>bar.x</samp> while running
the <code>bar.x: newer.x</code> rule.
</p>
<div class="example">
<pre class="example">$ <kbd>cat Makefile</kbd>
VPATH = ..
all: bar.y
bar.x: newer.x
        @echo Building $@
.SUFFIXES: .x .y
.x.y:
        cp $&lt; $@
$ <kbd>touch ../bar.x</kbd>
$ <kbd>touch ../newer.x</kbd>
$ <kbd>make</kbd>        # GNU make
Building bar.x
cp bar.x bar.y
cp: cannot stat `bar.x': No such file or directory
make: *** [bar.y] Error 1
$ <kbd>pmake</kbd>       # NetBSD make
Building ../bar.x
cp ../bar.x bar.y
$ <kbd>rm bar.y</kbd>
$ <kbd>fmake</kbd>       # FreeBSD make, OpenBSD make
echo Building bar.x
cp bar.x bar.y
cp: cannot stat `bar.x': No such file or directory
*** Error code 1
$ <kbd>tmake</kbd>       # Tru64 make
Building bar.x
cp: bar.x: No such file or directory
*** Exit 1
</pre></div>

<p>Note that if you drop away the command from the <code>bar.x: newer.x</code>
rule, GNU <code>make</code> magically starts to work: it
knows that <code>bar.x</code> hasn&rsquo;t been updated, therefore it doesn&rsquo;t
discard the result from <code>VPATH</code> (<samp>../bar.x</samp>) in succeeding
uses.  Tru64 also works, but FreeBSD and OpenBSD
still don&rsquo;t.
</p>
<div class="example">
<pre class="example">$ <kbd>cat Makefile</kbd>
VPATH = ..
all: bar.y
bar.x: newer.x
.SUFFIXES: .x .y
.x.y:
        cp $&lt; $@
$ <kbd>touch ../bar.x</kbd>
$ <kbd>touch ../newer.x</kbd>
$ <kbd>make</kbd>        # GNU make
cp ../bar.x bar.y
$ <kbd>rm bar.y</kbd>
$ <kbd>pmake</kbd>       # NetBSD make
cp ../bar.x bar.y
$ <kbd>rm bar.y</kbd>
$ <kbd>fmake</kbd>       # FreeBSD make, OpenBSD make
cp bar.x bar.y
cp: cannot stat `bar.x': No such file or directory
*** Error code 1
$ <kbd>tmake</kbd>       # Tru64 make
cp ../bar.x bar.y
</pre></div>

<p>It seems the sole solution that would please every <code>make</code>
implementation is to never rely on <code>VPATH</code> searches for targets.
In other words, <code>VPATH</code> should be reserved to unbuilt sources.
</p>

<div class="header">
<p>
Previous: <a href="Tru64-Directory-Magic.html#Tru64-Directory-Magic" accesskey="p" rel="prev">Tru64 Directory Magic</a>, Up: <a href="VPATH-and-Make.html#VPATH-and-Make" accesskey="u" rel="up">VPATH and Make</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
