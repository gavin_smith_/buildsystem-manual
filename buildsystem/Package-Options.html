<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Package Options</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Package Options">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Package Options">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Adding-options-to-a-configure-script.html#Adding-options-to-a-configure-script" rel="up" title="Adding options to a configure script">
<link href="Pretty-Help-Strings.html#Pretty-Help-Strings" rel="next" title="Pretty Help Strings">
<link href="External-Software.html#External-Software" rel="prev" title="External Software">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Package-Options"></a>
<div class="header">
<p>
Next: <a href="Pretty-Help-Strings.html#Pretty-Help-Strings" accesskey="n" rel="next">Pretty Help Strings</a>, Previous: <a href="External-Software.html#External-Software" accesskey="p" rel="prev">External Software</a>, Up: <a href="Adding-options-to-a-configure-script.html#Adding-options-to-a-configure-script" accesskey="u" rel="up">Adding options to a configure script</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Choosing-Package-Options"></a>
<h3 class="section">11.3 Choosing Package Options</h3>
<a name="index-Package-options"></a>
<a name="index-Options_002c-package"></a>

<p>If a software package has optional compile-time features, the user can
give <code>configure</code> command line options to specify whether to
compile them.  The options have one of these forms:
</p>
<div class="example">
<pre class="example">--enable-<var>feature</var><span class="roman">[</span>=<var>arg</var><span class="roman">]</span>
--disable-<var>feature</var>
</pre></div>

<p>These options allow users to choose which optional features to build and
install.  <samp>--enable-<var>feature</var></samp> options should never make a
feature behave differently or cause one feature to replace another.
They should only cause parts of the program to be built rather than left
out.
</p>
<p>The user can give an argument by following the feature name with
&lsquo;<samp>=</samp>&rsquo; and the argument.  Giving an argument of &lsquo;<samp>no</samp>&rsquo; requests
that the feature <em>not</em> be made available.  A feature with an
argument looks like <samp>--enable-debug=stabs</samp>.  If no argument is
given, it defaults to &lsquo;<samp>yes</samp>&rsquo;.  <samp>--disable-<var>feature</var></samp> is
equivalent to <samp>--enable-<var>feature</var>=no</samp>.
</p>
<p>Normally <code>configure</code> scripts complain about
<samp>--enable-<var>package</var></samp> options that they do not support.
See <a href="Option-Checking.html#Option-Checking">Option Checking</a>, for details, and for how to override the
defaults.
</p>
<p>For each optional feature, <samp>configure.ac</samp> should call
<code>AC_ARG_ENABLE</code> to detect whether the <code>configure</code> user asked
to include it.  Whether each feature is included or not by default, and
which arguments are valid, is up to you.
</p>
<a name="AC_005fARG_005fENABLE"></a><dl>
<dt><a name="index-AC_005fARG_005fENABLE-1"></a>Macro: <strong>AC_ARG_ENABLE</strong> <em>(<var>feature</var>, <var>help-string</var>,   <span class="roman">[</span><var>action-if-given</var><span class="roman">]</span>, <span class="roman">[</span><var>action-if-not-given</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AC_005fARG_005fENABLE"></a>
<p>If the user gave <code>configure</code> the option
<samp>--enable-<var>feature</var></samp> or <samp>--disable-<var>feature</var></samp>, run
shell commands <var>action-if-given</var>.  If neither option was given, run
shell commands <var>action-if-not-given</var>.  The name <var>feature</var>
indicates an optional user-level facility.  It should consist only of
alphanumeric characters, dashes, plus signs, and dots.
</p>
<p>The option&rsquo;s argument is available to the shell commands
<var>action-if-given</var> in the shell variable <code>enableval</code>, which is
actually just the value of the shell variable named
<code>enable_<var>feature</var></code>, with any non-alphanumeric characters in
<var>feature</var> changed into &lsquo;<samp>_</samp>&rsquo;.  You may use that variable instead,
if you wish.  The <var>help-string</var> argument is like that of
<code>AC_ARG_WITH</code> (see <a href="External-Software.html#External-Software">External Software</a>).
</p>
<p>Note that <var>action-if-not-given</var> is not expanded until the point that
<code>AC_ARG_ENABLE</code> was expanded.  If you need the value of
<code>enable_<var>feature</var></code> set to a default value by the time argument
parsing is completed, use <code>m4_divert_text</code> to the <code>DEFAULTS</code>
diversion (see <a href="Diversion-support.html#m4_005fdivert_005ftext">m4_divert_text</a>) (if done as an argument to
<code>AC_ARG_ENABLE</code>, also provide non-diverted text to avoid a shell
syntax error).
</p>
<p>You should format your <var>help-string</var> with the macro
<code>AS_HELP_STRING</code> (see <a href="Pretty-Help-Strings.html#Pretty-Help-Strings">Pretty Help Strings</a>).
</p>
<p>See the examples suggested with the definition of <code>AC_ARG_WITH</code>
(see <a href="External-Software.html#External-Software">External Software</a>) to get an idea of possible applications of
<code>AC_ARG_ENABLE</code>.
</p></dd></dl>

<div class="header">
<p>
Next: <a href="Pretty-Help-Strings.html#Pretty-Help-Strings" accesskey="n" rel="next">Pretty Help Strings</a>, Previous: <a href="External-Software.html#External-Software" accesskey="p" rel="prev">External Software</a>, Up: <a href="Adding-options-to-a-configure-script.html#Adding-options-to-a-configure-script" accesskey="u" rel="up">Adding options to a configure script</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
