<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Length Limitations</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Length Limitations">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Length Limitations">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Automake-variable-definitions.html#Automake-variable-definitions" rel="up" title="Automake variable definitions">
<link href="Rules-in-Makefile_002eam.html#Rules-in-Makefile_002eam" rel="next" title="Rules in Makefile.am">
<link href="Other-Automake-variables.html#Other-Automake-variables" rel="prev" title="Other Automake variables">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Length-Limitations"></a>
<div class="header">
<p>
Previous: <a href="Other-Automake-variables.html#Other-Automake-variables" accesskey="p" rel="prev">Other Automake variables</a>, Up: <a href="Automake-variable-definitions.html#Automake-variable-definitions" accesskey="u" rel="up">Automake variable definitions</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Staying-below-the-command-line-length-limit"></a>
<h4 class="subsubsection">5.1.1.4 Staying below the command line length limit</h4>

<a name="index-command-line-length-limit"></a>
<a name="index-ARG_005fMAX"></a>

<p>Traditionally, most unix-like systems have a length limitation for the
command line arguments and environment contents when creating new
processes (see for example
<a href="http://www.in-ulm.de/~mascheck/various/argmax/">http://www.in-ulm.de/~mascheck/various/argmax/</a> for an
overview on this issue),
which of course also applies to commands spawned by <code>make</code>.
POSIX requires this limit to be at least 4096 bytes, and most modern
systems have quite high limits (or are unlimited).
</p>
<p>In order to create portable Makefiles that do not trip over these
limits, it is necessary to keep the length of file lists bounded.
Unfortunately, it is not possible to do so fully transparently within
Automake, so your help may be needed.  Typically, you can split long
file lists manually and use different installation directory names for
each list.  For example,
</p>
<div class="example">
<pre class="example">data_DATA = file1 &hellip; file<var>N</var> file<var>N+1</var> &hellip; file<var>2N</var>
</pre></div>

<p>may also be written as
</p>
<div class="example">
<pre class="example">data_DATA = file1 &hellip; file<var>N</var>
data2dir = $(datadir)
data2_DATA = file<var>N+1</var> &hellip; file<var>2N</var>
</pre></div>

<p>and will cause Automake to treat the two lists separately during
<code>make install</code>.  See <a href="The-Two-Parts-of-Install.html#The-Two-Parts-of-Install">The Two Parts of Install</a> for choosing
directory names that will keep the ordering of the two parts of
installation Note that <code>make dist</code> may still only work on a host
with a higher length limit in this example.
</p>
<p>Automake itself employs a couple of strategies to avoid long command
lines.  For example, when &lsquo;<samp>${srcdir}/</samp>&rsquo; is prepended to file
names, as can happen with above <code>$(data_DATA)</code> lists, it limits
the amount of arguments passed to external commands.
</p>
<p>Unfortunately, some system&rsquo;s <code>make</code> commands may prepend
<code>VPATH</code> prefixes like &lsquo;<samp>${srcdir}/</samp>&rsquo; to file names from the
source tree automatically (see <a href="Automatic-Rule-Rewriting.html#Automatic-Rule-Rewriting">Automatic Rule Rewriting</a>).
In this case, the user
may have to switch to use GNU Make, or refrain from using VPATH builds,
in order to stay below the length limit.
</p>
<p>For libraries and programs built from many sources, convenience archives
may be used as intermediates in order to limit the object list length
(see <a href="Libtool-Convenience-Libraries.html#Libtool-Convenience-Libraries">Libtool Convenience Libraries</a>).
</p>


<div class="header">
<p>
Previous: <a href="Other-Automake-variables.html#Other-Automake-variables" accesskey="p" rel="prev">Other Automake variables</a>, Up: <a href="Automake-variable-definitions.html#Automake-variable-definitions" accesskey="u" rel="up">Automake variable definitions</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
