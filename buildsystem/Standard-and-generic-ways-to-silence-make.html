<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Standard and generic ways to silence make</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Standard and generic ways to silence make">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Standard and generic ways to silence make">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Verbosity.html#Verbosity" rel="up" title="Verbosity">
<link href="Automake-Silent-Rules.html#Automake-Silent-Rules" rel="next" title="Automake Silent Rules">
<link href="Make-is-verbose-by-default.html#Make-is-verbose-by-default" rel="prev" title="Make is verbose by default">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Standard-and-generic-ways-to-silence-make"></a>
<div class="header">
<p>
Next: <a href="Automake-Silent-Rules.html#Automake-Silent-Rules" accesskey="n" rel="next">Automake Silent Rules</a>, Previous: <a href="Make-is-verbose-by-default.html#Make-is-verbose-by-default" accesskey="p" rel="prev">Make is verbose by default</a>, Up: <a href="Verbosity.html#Verbosity" accesskey="u" rel="up">Verbosity</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Standard-and-generic-ways-to-silence-make-1"></a>
<h4 class="subsection">13.4.2 Standard and generic ways to silence make</h4>

<p>Here we describe some common idioms/tricks to obtain a quieter make
output, with their relative advantages and drawbacks.  In the next
section (<a href="Automake-Silent-Rules.html#Automake-Silent-Rules">Automake Silent Rules</a>) we&rsquo;ll see how Automake can help
in this respect, providing more elaborate and flexible idioms.
</p>
<ul>
<li> <code>make -s</code>

<p>This simply causes <code>make</code> not to print <em>any</em> rule before
executing it.
</p>
<p>The <samp>-s</samp> flag is mandated by POSIX, universally supported, and
its purpose and function are easy to understand.
</p>
<p>But it also has its serious limitations too.  First of all, it embodies
an &ldquo;all or nothing&rdquo; strategy, i.e., either everything is silenced, or
nothing is; this lack of granularity can sometimes be a fatal flaw.
Moreover, when the <samp>-s</samp> flag is used, the <code>make</code> output
might turn out to be too much terse; in case of errors, the user won&rsquo;t
be able to easily see what rule or command have caused them, or even,
in case of tools with poor error reporting, what the errors were!
</p>
</li><li> <code>make &gt;/dev/null || make</code>

<p>Apparently, this perfectly obeys the &ldquo;silence is golden&rdquo; rule: warnings
from stderr are passed through, output reporting is done only in case of
error, and in that case it should provide a verbose-enough report to allow
an easy determination of the error location and causes.
</p>
<p>However, calling <code>make</code> two times in a row might hide errors
(especially intermittent ones), or subtly change the expected semantic
of the <code>make</code> calls &mdash; things these which can clearly make
debugging and error assessment very difficult.
</p>
</li><li> <code>make --no-print-directory</code>

<p>This is GNU <code>make</code> specific.  When called with the
<samp>--no-print-directory</samp> option, GNU <code>make</code> will disable
printing of the working directory by invoked sub-<code>make</code>s (the
well-known &ldquo;<i>Entering/Leaving directory ...</i>&rdquo; messages).  This helps
to decrease the verbosity of the output, but experience has shown that
it can also often render debugging considerably harder in projects using
deeply-nested <code>make</code> recursion.
</p>
<p>As an aside, notice that the <samp>--no-print-directory</samp> option is
automatically activated if the <samp>-s</samp> flag is used.
</p>

</li></ul>

<div class="header">
<p>
Next: <a href="Automake-Silent-Rules.html#Automake-Silent-Rules" accesskey="n" rel="next">Automake Silent Rules</a>, Previous: <a href="Make-is-verbose-by-default.html#Make-is-verbose-by-default" accesskey="p" rel="prev">Make is verbose by default</a>, Up: <a href="Verbosity.html#Verbosity" accesskey="u" rel="up">Verbosity</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
