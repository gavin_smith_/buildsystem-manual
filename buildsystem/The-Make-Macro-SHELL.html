<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: The Make Macro SHELL</title>

<meta name="description" content="Automake and Autoconf Reference Manual: The Make Macro SHELL">
<meta name="keywords" content="Automake and Autoconf Reference Manual: The Make Macro SHELL">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Portable-Make-Programming.html#Portable-Make-Programming" rel="up" title="Portable Make Programming">
<link href="Parallel-Make.html#Parallel-Make" rel="next" title="Parallel Make">
<link href="The-Make-Macro-MAKEFLAGS.html#The-Make-Macro-MAKEFLAGS" rel="prev" title="The Make Macro MAKEFLAGS">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="The-Make-Macro-SHELL"></a>
<div class="header">
<p>
Next: <a href="Parallel-Make.html#Parallel-Make" accesskey="n" rel="next">Parallel Make</a>, Previous: <a href="The-Make-Macro-MAKEFLAGS.html#The-Make-Macro-MAKEFLAGS" accesskey="p" rel="prev">The Make Macro MAKEFLAGS</a>, Up: <a href="Portable-Make-Programming.html#Portable-Make-Programming" accesskey="u" rel="up">Portable Make Programming</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="The-Make-Macro-SHELL-1"></a>
<h4 class="subsection">C.2.9 The Make Macro <code>SHELL</code></h4>
<a name="index-SHELL-and-make"></a>
<a name="index-make-and-SHELL"></a>

<p>Posix-compliant <code>make</code> internally uses the <code>$(SHELL)</code>
macro to spawn shell processes and execute Make rules.  This
is a builtin macro supplied by <code>make</code>, but it can be modified
by a makefile or by a command-line argument.
</p>
<p>Not all <code>make</code> implementations define this <code>SHELL</code> macro.
Tru64
<code>make</code> is an example; this implementation always uses
<code>/bin/sh</code>.  So it&rsquo;s a good idea to always define <code>SHELL</code> in
your makefiles.  If you use Autoconf, do
</p>
<div class="example">
<pre class="example">SHELL = @SHELL@
</pre></div>

<p>If you use Automake, this is done for you.
</p>
<p>Do not force <code>SHELL = /bin/sh</code> because that is not correct
everywhere.  Remember, <samp>/bin/sh</samp> is not Posix compliant on many
systems, such as FreeBSD 4, NetBSD 3, AIX 3, Solaris 10, or Tru64.
Additionally, DJGPP lacks <code>/bin/sh</code>, and when its
GNU <code>make</code> port sees such a setting it enters a
special emulation mode where features like pipes and redirections are
emulated on top of DOS&rsquo;s <code>command.com</code>.  Unfortunately this
emulation is incomplete; for instance it does not handle command
substitutions.  Using <code>@SHELL@</code> means that your makefile will
benefit from the same improved shell, such as <code>bash</code> or
<code>ksh</code>, that was discovered during <code>configure</code>, so that
you aren&rsquo;t fighting two different sets of shell bugs between the two
contexts.
</p>
<p>Posix-compliant <code>make</code> should never acquire the value of
$(SHELL) from the environment, even when <code>make -e</code> is used
(otherwise, think about what would happen to your rules if
<code>SHELL=/bin/tcsh</code>).
</p>
<p>However not all <code>make</code> implementations have this exception.
For instance it&rsquo;s not surprising that Tru64 <code>make</code> doesn&rsquo;t
protect <code>SHELL</code>, since it doesn&rsquo;t use it.
</p>
<div class="example">
<pre class="example">$ <kbd>cat Makefile</kbd>
SHELL = /bin/sh
FOO = foo
all:
        @echo $(SHELL)
        @echo $(FOO)
$ <kbd>env SHELL=/bin/tcsh FOO=bar make -e</kbd>   # Tru64 Make
/bin/tcsh
bar
$ <kbd>env SHELL=/bin/tcsh FOO=bar gmake -e</kbd>  # GNU make
/bin/sh
bar
</pre></div>

<p>Conversely, <code>make</code> is not supposed to export any changes to the
macro <code>SHELL</code> to child processes.  Again, many implementations
break this rule:
</p>
<div class="example">
<pre class="example">$ <kbd>cat Makefile</kbd>
all:
        @echo $(SHELL)
        @printenv SHELL
$ <kbd>env SHELL=sh make -e SHELL=/bin/ksh</kbd>   # BSD Make, GNU make 3.80
/bin/ksh
/bin/ksh
$ <kbd>env SHELL=sh gmake -e SHELL=/bin/ksh</kbd>  # GNU make 3.81
/bin/ksh
sh
</pre></div>

<div class="header">
<p>
Next: <a href="Parallel-Make.html#Parallel-Make" accesskey="n" rel="next">Parallel Make</a>, Previous: <a href="The-Make-Macro-MAKEFLAGS.html#The-Make-Macro-MAKEFLAGS" accesskey="p" rel="prev">The Make Macro MAKEFLAGS</a>, Up: <a href="Portable-Make-Programming.html#Portable-Make-Programming" accesskey="u" rel="up">Portable Make Programming</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
