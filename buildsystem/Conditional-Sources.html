<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Conditional Sources</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Conditional Sources">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Conditional Sources">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Programs.html#Programs" rel="up" title="Programs">
<link href="Conditional-Programs.html#Conditional-Programs" rel="next" title="Conditional Programs">
<link href="Linking.html#Linking" rel="prev" title="Linking">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Conditional-Sources"></a>
<div class="header">
<p>
Next: <a href="Conditional-Programs.html#Conditional-Programs" accesskey="n" rel="next">Conditional Programs</a>, Previous: <a href="Linking.html#Linking" accesskey="p" rel="prev">Linking</a>, Up: <a href="Programs.html#Programs" accesskey="u" rel="up">Programs</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Conditional-compilation-of-sources"></a>
<h4 class="subsection">7.1.6 Conditional compilation of sources</h4>

<p>You can&rsquo;t put a configure substitution (e.g., &lsquo;<samp>@FOO@</samp>&rsquo; or
&lsquo;<samp>$(FOO)</samp>&rsquo; where <code>FOO</code> is defined via <code>AC_SUBST</code>) into a
<code>_SOURCES</code> variable.  The reason for this is a bit hard to
explain, but suffice to say that it simply won&rsquo;t work.  Automake will
give an error if you try to do this.
</p>
<p>Fortunately there are two other ways to achieve the same result.  One is
to use configure substitutions in <code>_LDADD</code> variables, the other is
to use an Automake conditional.
</p>
<a name="Conditional-Compilation-using-_005fLDADD-Substitutions"></a>
<h4 class="subsubheading">Conditional Compilation using <code>_LDADD</code> Substitutions</h4>

<a name="index-EXTRA_005fprog_005fSOURCES_002c-defined"></a>

<p>Automake must know all the source files that could possibly go into a
program, even if not all the files are built in every circumstance.  Any
files that are only conditionally built should be listed in the
appropriate <code>EXTRA_</code> variable.  For instance, if
<samp>hello-linux.c</samp> or <samp>hello-generic.c</samp> were conditionally included
in <code>hello</code>, the <samp>Makefile.am</samp> would contain:
</p>
<div class="example">
<pre class="example">bin_PROGRAMS = hello
hello_SOURCES = hello-common.c
EXTRA_hello_SOURCES = hello-linux.c hello-generic.c
hello_LDADD = $(HELLO_SYSTEM)
hello_DEPENDENCIES = $(HELLO_SYSTEM)
</pre></div>

<p>You can then setup the &lsquo;<samp>$(HELLO_SYSTEM)</samp>&rsquo; substitution from
<samp>configure.ac</samp>:
</p>
<div class="example">
<pre class="example">&hellip;
case $host in
  *linux*) HELLO_SYSTEM='hello-linux.$(OBJEXT)' ;;
  *)       HELLO_SYSTEM='hello-generic.$(OBJEXT)' ;;
esac
AC_SUBST([HELLO_SYSTEM])
&hellip;
</pre></div>

<p>In this case, the variable <code>HELLO_SYSTEM</code> should be replaced by
either <samp>hello-linux.o</samp> or <samp>hello-generic.o</samp>, and added to
both <code>hello_DEPENDENCIES</code> and <code>hello_LDADD</code> in order to be
built and linked in.
</p>
<a name="Conditional-Compilation-using-Automake-Conditionals"></a>
<h4 class="subsubheading">Conditional Compilation using Automake Conditionals</h4>

<p>An often simpler way to compile source files conditionally is to use
Automake conditionals.  For instance, you could use this
<samp>Makefile.am</samp> construct to build the same <samp>hello</samp> example:
</p>
<div class="example">
<pre class="example">bin_PROGRAMS = hello
if LINUX
hello_SOURCES = hello-linux.c hello-common.c
else
hello_SOURCES = hello-generic.c hello-common.c
endif
</pre></div>

<p>In this case, <samp>configure.ac</samp> should setup the <code>LINUX</code>
conditional using <code>AM_CONDITIONAL</code> (see <a href="Conditionals.html#Conditionals">Conditionals</a>).
</p>
<p>When using conditionals like this you don&rsquo;t need to use the
<code>EXTRA_</code> variable, because Automake will examine the contents of
each variable to construct the complete list of source files.
</p>
<p>If your program uses a lot of files, you will probably prefer a
conditional &lsquo;<samp>+=</samp>&rsquo;.
</p>
<div class="example">
<pre class="example">bin_PROGRAMS = hello
hello_SOURCES = hello-common.c
if LINUX
hello_SOURCES += hello-linux.c
else
hello_SOURCES += hello-generic.c
endif
</pre></div>

<div class="header">
<p>
Next: <a href="Conditional-Programs.html#Conditional-Programs" accesskey="n" rel="next">Conditional Programs</a>, Previous: <a href="Linking.html#Linking" accesskey="p" rel="prev">Linking</a>, Up: <a href="Programs.html#Programs" accesskey="u" rel="up">Programs</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
