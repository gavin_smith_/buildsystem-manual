<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Build Directories</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Build Directories">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Build Directories">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Autoconf-FAQ.html#Autoconf-FAQ" rel="up" title="Autoconf FAQ">
<link href="Defining-Directories.html#Defining-Directories" rel="next" title="Defining Directories">
<link href="Automatic-Remaking.html#Automatic-Remaking" rel="prev" title="Automatic Remaking">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Build-Directories"></a>
<div class="header">
<p>
Next: <a href="Defining-Directories.html#Defining-Directories" accesskey="n" rel="next">Defining Directories</a>, Previous: <a href="Automatic-Remaking.html#Automatic-Remaking" accesskey="p" rel="prev">Automatic Remaking</a>, Up: <a href="Autoconf-FAQ.html#Autoconf-FAQ" accesskey="u" rel="up">Autoconf FAQ</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Build-Directories-1"></a>
<h4 class="subsection">H.2.2 Build Directories</h4>
<a name="index-Build-directories"></a>
<a name="index-Directories_002c-build"></a>

<p>You can support compiling a software package for several architectures
simultaneously from the same copy of the source code.  The object files
for each architecture are kept in their own directory.
</p>
<p>To support doing this, <code>make</code> uses the <code>VPATH</code> variable to
find the files that are in the source directory.  GNU Make
can do this.  Most other recent <code>make</code> programs can do this as
well, though they may have difficulties and it is often simpler to
recommend GNU <code>make</code> (see <a href="VPATH-and-Make.html#VPATH-and-Make">VPATH and Make</a>).  Older
<code>make</code> programs do not support <code>VPATH</code>; when using them, the
source code must be in the same directory as the object files.
</p>
<p>If you are using GNU Automake, the remaining details in this
section are already covered for you, based on the contents of your
<samp>Makefile.am</samp>.  But if you are using Autoconf in isolation, then
supporting <code>VPATH</code> requires the following in your
<samp>Makefile.in</samp>:
</p>
<div class="example">
<pre class="example">srcdir = @srcdir@
VPATH = @srcdir@
</pre></div>

<p>Do not set <code>VPATH</code> to the value of another variable (see <a href="Variables-listed-in-VPATH.html#Variables-listed-in-VPATH">Variables listed in VPATH</a>.
</p>
<p><code>configure</code> substitutes the correct value for <code>srcdir</code> when
it produces <samp>Makefile</samp>.
</p>
<p>Do not use the <code>make</code> variable <code>$&lt;</code>, which expands to the
file name of the file in the source directory (found with <code>VPATH</code>),
except in implicit rules.  (An implicit rule is one such as &lsquo;<samp>.c.o</samp>&rsquo;,
which tells how to create a <samp>.o</samp> file from a <samp>.c</samp> file.)  Some
versions of <code>make</code> do not set <code>$&lt;</code> in explicit rules; they
expand it to an empty value.
</p>
<p>Instead, Make command lines should always refer to source
files by prefixing them with &lsquo;<samp>$(srcdir)/</samp>&rsquo;.  It&rsquo;s safer
to quote the source directory name, in case it contains characters that
are special to the shell.  Because &lsquo;<samp>$(srcdir)</samp>&rsquo; is expanded by Make,
single-quoting works and is safer than double-quoting.  For example:
</p>
<div class="example">
<pre class="example">time.info: time.texinfo
        $(MAKEINFO) '$(srcdir)/time.texinfo'
</pre></div>

<div class="header">
<p>
Next: <a href="Defining-Directories.html#Defining-Directories" accesskey="n" rel="next">Defining Directories</a>, Previous: <a href="Automatic-Remaking.html#Automatic-Remaking" accesskey="p" rel="prev">Automatic Remaking</a>, Up: <a href="Autoconf-FAQ.html#Autoconf-FAQ" accesskey="u" rel="up">Autoconf FAQ</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
