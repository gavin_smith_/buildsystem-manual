<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Linking</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Linking">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Linking">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Programs.html#Programs" rel="up" title="Programs">
<link href="Conditional-Sources.html#Conditional-Sources" rel="next" title="Conditional Sources">
<link href="Program-Variables.html#Program-Variables" rel="prev" title="Program Variables">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Linking"></a>
<div class="header">
<p>
Next: <a href="Conditional-Sources.html#Conditional-Sources" accesskey="n" rel="next">Conditional Sources</a>, Previous: <a href="Program-Variables.html#Program-Variables" accesskey="p" rel="prev">Program Variables</a>, Up: <a href="Programs.html#Programs" accesskey="u" rel="up">Programs</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Linking-the-program"></a>
<h4 class="subsection">7.1.5 Linking the program</h4>

<a name="index-LDADD"></a>
<a name="index-AM_005fLDFLAGS-1"></a>
<p>If you need to link against libraries that are not found by
<code>configure</code>, you can use <code>LDADD</code> to do so.  This variable is
used to specify additional objects or libraries to link with; it is
inappropriate for specifying specific linker flags, you should use
<code>AM_LDFLAGS</code> for this purpose.
</p>
<a name="index-prog_005fLDADD_002c-defined"></a>
<p>Sometimes, multiple programs are built in one directory but do not share
the same link-time requirements.  In this case, you can use the
<code><var>prog</var>_LDADD</code> variable (where <var>prog</var> is the name of the
program as it appears in some <code>_PROGRAMS</code> variable, and usually
written in lowercase) to override <code>LDADD</code>.  If this variable exists
for a given program, then that program is not linked using <code>LDADD</code>.
<a name="index-maude_005fLDADD"></a>
</p>
<p>For instance, in GNU cpio, <code>pax</code>, <code>cpio</code> and <code>mt</code> are
linked against the library <samp>libcpio.a</samp>.  However, <code>rmt</code> is
built in the same directory, and has no such link requirement.  Also,
<code>mt</code> and <code>rmt</code> are only built on certain architectures.  Here
is what cpio&rsquo;s <samp>src/Makefile.am</samp> looks like (abridged):
</p>
<div class="example">
<pre class="example">bin_PROGRAMS = cpio pax $(MT)
libexec_PROGRAMS = $(RMT)
EXTRA_PROGRAMS = mt rmt

LDADD = ../lib/libcpio.a $(INTLLIBS)
rmt_LDADD =

cpio_SOURCES = &hellip;
pax_SOURCES = &hellip;
mt_SOURCES = &hellip;
rmt_SOURCES = &hellip;
</pre></div>

<a name="index-_005fLDFLAGS_002c-defined"></a>
<a name="index-maude_005fLDFLAGS"></a>
<p><code><var>prog</var>_LDADD</code> is inappropriate for passing program-specific
linker flags (except for <samp>-l</samp>, <samp>-L</samp>, <samp>-dlopen</samp> and
<samp>-dlpreopen</samp>).  So, use the <code><var>prog</var>_LDFLAGS</code> variable for
this purpose.
</p>
<a name="index-_005fDEPENDENCIES_002c-defined"></a>
<a name="index-maude_005fDEPENDENCIES"></a>
<a name="index-EXTRA_005fmaude_005fDEPENDENCIES"></a>
<p>It is also occasionally useful to have a program depend on some other
target that is not actually part of that program.  This can be done
using either the <code><var>prog</var>_DEPENDENCIES</code> or the
<code>EXTRA_<var>prog</var>_DEPENDENCIES</code> variable.  Each program depends on
the contents both variables, but no further interpretation is done.
</p>
<p>Since these dependencies are associated to the link rule used to
create the programs they should normally list files used by the link
command.  That is <samp>*.$(OBJEXT)</samp>, <samp>*.a</samp>, or <samp>*.la</samp>
files.  In rare cases you may need to add other kinds of files such as
linker scripts, but <em>listing a source file in
<code>_DEPENDENCIES</code> is wrong</em>.  If some source file needs to be built
before all the components of a program are built, consider using the
<code>BUILT_SOURCES</code> variable instead (see <a href="Sources.html#Sources">Sources</a>).
</p>
<p>If <code><var>prog</var>_DEPENDENCIES</code> is not supplied, it is computed by
Automake.  The automatically-assigned value is the contents of
<code><var>prog</var>_LDADD</code>, with most configure substitutions, <samp>-l</samp>,
<samp>-L</samp>, <samp>-dlopen</samp> and <samp>-dlpreopen</samp> options removed.  The
configure substitutions that are left in are only &lsquo;<samp>$(LIBOBJS)</samp>&rsquo; and
&lsquo;<samp>$(ALLOCA)</samp>&rsquo;; these are left because it is known that they will not
cause an invalid value for <code><var>prog</var>_DEPENDENCIES</code> to be
generated.
</p>
<p><a href="Conditional-Sources.html#Conditional-Sources">Conditional Sources</a> shows a situation where <code>_DEPENDENCIES</code>
may be used.
</p>
<p>The <code>EXTRA_<var>prog</var>_DEPENDENCIES</code> may be useful for cases where
you merely want to augment the <code>automake</code>-generated
<code><var>prog</var>_DEPENDENCIES</code> rather than replacing it.
</p>
<a name="index-LDADD-and-_002dl"></a>
<a name="index-_002dl-and-LDADD"></a>
<p>We recommend that you avoid using <samp>-l</samp> options in <code>LDADD</code>
or <code><var>prog</var>_LDADD</code> when referring to libraries built by your
package.  Instead, write the file name of the library explicitly as in
the above <code>cpio</code> example.  Use <samp>-l</samp> only to list
third-party libraries.  If you follow this rule, the default value of
<code><var>prog</var>_DEPENDENCIES</code> will list all your local libraries and
omit the other ones.
</p>

<div class="header">
<p>
Next: <a href="Conditional-Sources.html#Conditional-Sources" accesskey="n" rel="next">Conditional Sources</a>, Previous: <a href="Program-Variables.html#Program-Variables" accesskey="p" rel="prev">Program Variables</a>, Up: <a href="Programs.html#Programs" accesskey="u" rel="up">Programs</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
