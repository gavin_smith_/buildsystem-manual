<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Balancing Parentheses</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Balancing Parentheses">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Balancing Parentheses">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="M4-Quotation.html#M4-Quotation" rel="up" title="M4 Quotation">
<link href="Quotation-Rule-Of-Thumb.html#Quotation-Rule-Of-Thumb" rel="next" title="Quotation Rule Of Thumb">
<link href="Changequote-is-Evil.html#Changequote-is-Evil" rel="prev" title="Changequote is Evil">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Balancing-Parentheses"></a>
<div class="header">
<p>
Next: <a href="Quotation-Rule-Of-Thumb.html#Quotation-Rule-Of-Thumb" accesskey="n" rel="next">Quotation Rule Of Thumb</a>, Previous: <a href="Changequote-is-Evil.html#Changequote-is-Evil" accesskey="p" rel="prev">Changequote is Evil</a>, Up: <a href="M4-Quotation.html#M4-Quotation" accesskey="u" rel="up">M4 Quotation</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Dealing-with-unbalanced-parentheses"></a>
<h4 class="subsection">F.1.6 Dealing with unbalanced parentheses</h4>
<a name="index-balancing-parentheses"></a>
<a name="index-parentheses_002c-balancing"></a>
<a name="index-unbalanced-parentheses_002c-managing"></a>

<p>One of the pitfalls of portable shell programming is that <code>case</code>
statements require unbalanced parentheses (see <a href="Limitations-of-Builtins.html#case">Limitations of
Shell Builtins</a>).  With syntax highlighting
editors, the presence of unbalanced &lsquo;<samp>)</samp>&rsquo; can interfere with editors
that perform syntax highlighting of macro contents based on finding the
matching &lsquo;<samp>(</samp>&rsquo;.  Another concern is how much editing must be done
when transferring code snippets between shell scripts and macro
definitions.  But most importantly, the presence of unbalanced
parentheses can introduce expansion bugs.
</p>
<p>For an example, here is an underquoted attempt to use the macro
<code>my_case</code>, which happens to expand to a portable <code>case</code>
statement:
</p>
<div class="example">
<pre class="example">AC_DEFUN([my_case],
[case $file_name in
  *.c) echo &quot;C source code&quot;;;
esac])
AS_IF(:, my_case)
</pre></div>

<p>In the above example, the <code>AS_IF</code> call underquotes its arguments.
As a result, the unbalanced &lsquo;<samp>)</samp>&rsquo; generated by the premature
expansion of <code>my_case</code> results in expanding <code>AS_IF</code> with a
truncated parameter, and the expansion is syntactically invalid:
</p>
<div class="example">
<pre class="example">if :; then
  case $file_name in
  *.c
fi echo &quot;C source code&quot;;;
esac)
</pre></div>

<p>If nothing else, this should emphasize the importance of the quoting
arguments to macro calls.  On the other hand, there are several
variations for defining <code>my_case</code> to be more robust, even when used
without proper quoting, each with some benefits and some drawbacks.
</p>
<ul class="no-bullet">
<li><!-- /@w --> Creative literal shell comment
<div class="example">
<pre class="example">AC_DEFUN([my_case],
[case $file_name in #(
  *.c) echo &quot;C source code&quot;;;
esac])
</pre></div>
<p>This version provides balanced parentheses to several editors, and can
be copied and pasted into a terminal as is.  Unfortunately, it is still
unbalanced as an Autoconf argument, since &lsquo;<samp>#(</samp>&rsquo; is an M4 comment
that masks the normal properties of &lsquo;<samp>(</samp>&rsquo;.
</p>
</li><li><!-- /@w --> Quadrigraph shell comment
<div class="example">
<pre class="example">AC_DEFUN([my_case],
[case $file_name in @%:@(
  *.c) echo &quot;C source code&quot;;;
esac])
</pre></div>
<p>This version provides balanced parentheses to even more editors, and can
be used as a balanced Autoconf argument.  Unfortunately, it requires
some editing before it can be copied and pasted into a terminal, and the
use of the quadrigraph &lsquo;<samp>@%:@</samp>&rsquo; for &lsquo;<samp>#</samp>&rsquo; reduces readability.
</p>
</li><li><!-- /@w --> Quoting just the parenthesis
<div class="example">
<pre class="example">AC_DEFUN([my_case],
[case $file_name in
  *.c[)] echo &quot;C source code&quot;;;
esac])
</pre></div>
<p>This version quotes the &lsquo;<samp>)</samp>&rsquo;, so that it can be used as a balanced
Autoconf argument.  As written, this is not balanced to an editor, but
it can be coupled with &lsquo;<samp>[#(]</samp>&rsquo; to meet that need, too.  However, it
still requires some edits before it can be copied and pasted into a
terminal.
</p>
</li><li><!-- /@w --> Double-quoting the entire statement
<div class="example">
<pre class="example">AC_DEFUN([my_case],
[[case $file_name in #(
  *.c) echo &quot;C source code&quot;;;
esac]])
</pre></div>
<p>Since the entire macro is double-quoted, there is no problem with using
this as an Autoconf argument; and since the double-quoting is over the
entire statement, this code can be easily copied and pasted into a
terminal.  However, the double quoting prevents the expansion of any
macros inside the case statement, which may cause its own set of
problems.
</p>
</li><li><!-- /@w --> Using <code>AS_CASE</code>
<div class="example">
<pre class="example">AC_DEFUN([my_case],
[AS_CASE([$file_name],
  [*.c], [echo &quot;C source code&quot;])])
</pre></div>
<p>This version avoids the balancing issue altogether, by relying on
<code>AS_CASE</code> (see <a href="Common-Shell-Constructs.html#Common-Shell-Constructs">Common Shell Constructs</a>); it also allows for the
expansion of <code>AC_REQUIRE</code> to occur prior to the entire case
statement, rather than within a branch of the case statement that might
not be taken.  However, the abstraction comes with a penalty that it is
no longer a quick copy, paste, and edit to get back to shell code.
</p></li></ul>


<div class="header">
<p>
Next: <a href="Quotation-Rule-Of-Thumb.html#Quotation-Rule-Of-Thumb" accesskey="n" rel="next">Quotation Rule Of Thumb</a>, Previous: <a href="Changequote-is-Evil.html#Changequote-is-Evil" accesskey="p" rel="prev">Changequote is Evil</a>, Up: <a href="M4-Quotation.html#M4-Quotation" accesskey="u" rel="up">M4 Quotation</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
