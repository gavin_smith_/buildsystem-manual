<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Emacs Lisp</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Emacs Lisp">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Emacs Lisp">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Programming-language-support.html#Programming-language-support" rel="up" title="Programming language support">
<link href="Support-for-Other-Languages.html#Support-for-Other-Languages" rel="next" title="Support for Other Languages">
<link href="Python.html#Python" rel="prev" title="Python">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Emacs-Lisp"></a>
<div class="header">
<p>
Next: <a href="Support-for-Other-Languages.html#Support-for-Other-Languages" accesskey="n" rel="next">Support for Other Languages</a>, Previous: <a href="Python.html#Python" accesskey="p" rel="prev">Python</a>, Up: <a href="Programming-language-support.html#Programming-language-support" accesskey="u" rel="up">Programming language support</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Emacs-Lisp-1"></a>
<h3 class="section">10.18 Emacs Lisp</h3>

<a name="index-_005fLISP-primary_002c-defined"></a>
<a name="index-LISP-primary_002c-defined"></a>
<a name="index-Primary-variable_002c-LISP"></a>

<a name="index-_005fLISP"></a>
<a name="index-lisp_005fLISP"></a>
<a name="index-noinst_005fLISP"></a>

<p>Automake provides some support for Emacs Lisp.  The <code>LISP</code> primary
is used to hold a list of <samp>.el</samp> files.  Possible prefixes for this
primary are <code>lisp_</code> and <code>noinst_</code>.  Note that if
<code>lisp_LISP</code> is defined, then <samp>configure.ac</samp> must run
<code>AM_PATH_LISPDIR</code> (see <a href="Macros-supplied-with-Automake.html#Macros-supplied-with-Automake">Macros supplied with Automake</a>).
</p>
<a name="index-dist_005flisp_005fLISP"></a>
<a name="index-dist_005fnoinst_005fLISP"></a>
<p>Lisp sources are not distributed by default.  You can prefix the
<code>LISP</code> primary with <code>dist_</code>, as in <code>dist_lisp_LISP</code> or
<code>dist_noinst_LISP</code>, to indicate that these files should be
distributed.
</p>
<p>Automake will byte-compile all Emacs Lisp source files using the Emacs
found by <code>AM_PATH_LISPDIR</code>, if any was found.  When performing such
byte-compilation, the flags specified in the (developer-reserved)
<code>AM_ELCFLAGS</code> and (user-reserved) <code>ELCFLAGS</code> make variables
will be passed to the Emacs invocation.
</p>
<p>Byte-compiled Emacs Lisp files are not portable among all versions of
Emacs, so it makes sense to turn this off if you expect sites to have
more than one version of Emacs installed.  Furthermore, many packages
don&rsquo;t actually benefit from byte-compilation.  Still, we recommend
that you byte-compile your Emacs Lisp sources.  It is probably better
for sites with strange setups to cope for themselves than to make the
installation less nice for everybody else.
</p>
<p>There are two ways to avoid byte-compiling.  Historically, we have
recommended the following construct.
</p>
<div class="example">
<pre class="example">lisp_LISP = file1.el file2.el
ELCFILES =
</pre></div>

<p><code>ELCFILES</code> is an internal Automake variable that normally lists
all <samp>.elc</samp> files that must be byte-compiled.  Automake defines
<code>ELCFILES</code> automatically from <code>lisp_LISP</code>.  Emptying this
variable explicitly prevents byte-compilation.
</p>
<p>Since Automake 1.8, we now recommend using <code>lisp_DATA</code> instead:
</p>
<div class="example">
<pre class="example">lisp_DATA = file1.el file2.el
</pre></div>

<p>Note that these two constructs are not equivalent.  <code>_LISP</code> will
not install a file if Emacs is not installed, while <code>_DATA</code> will
always install its files.
</p>
<div class="header">
<p>
Next: <a href="Support-for-Other-Languages.html#Support-for-Other-Languages" accesskey="n" rel="next">Support for Other Languages</a>, Previous: <a href="Python.html#Python" accesskey="p" rel="prev">Python</a>, Up: <a href="Programming-language-support.html#Programming-language-support" accesskey="u" rel="up">Programming language support</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
