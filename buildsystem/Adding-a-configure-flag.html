<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Adding a configure flag</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Adding a configure flag">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Adding a configure flag">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Tutorial.html#Tutorial" rel="up" title="Tutorial">
<link href="Adding-a-data-file-to-be-installed.html#Adding-a-data-file-to-be-installed" rel="next" title="Adding a data file to be installed">
<link href="Adding-a-check-for-a-library.html#Adding-a-check-for-a-library" rel="prev" title="Adding a check for a library">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Adding-a-configure-flag"></a>
<div class="header">
<p>
Next: <a href="Adding-a-data-file-to-be-installed.html#Adding-a-data-file-to-be-installed" accesskey="n" rel="next">Adding a data file to be installed</a>, Previous: <a href="Adding-a-check-for-a-library.html#Adding-a-check-for-a-library" accesskey="p" rel="prev">Adding a check for a library</a>, Up: <a href="Tutorial.html#Tutorial" accesskey="u" rel="up">Tutorial</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Adding-a-configure-flag-1"></a>
<h3 class="section">4.4 Adding a configure flag</h3>

<p>You may wish to allow users invoking your configure scripts to enable or 
disable certain features when the program is built.
</p>
<p>For example, suppose the features implemented in the program in the 
previous section with the GMP library are optional.  We can provide a 
<samp>--without-libgmp</samp> option to disable the use of the library.
</p>
<p>Here is the check in <samp>configure.ac</samp>:
</p>
<div class="example">
<pre class="example">AC_ARG_WITH([libgmp],
    [AS_HELP_STRING([--without-libgmp],[Disable use of libgmp]),,
    [with_libgmp=yes])
if test &quot;x$with_libgmp&quot; != xno; then
  AC_CHECK_LIB([gmp], [__gmpz_add])
  if test &quot;x$ac_cv_lib_gmp___gmpz_add&quot; = xno; then
    AC_MSG_ERROR([libgmp is required])
  fi
  # Is the AC_DEFINE actualy required?
  AC_DEFINE([HAVE_LIBGMP], [1], [Define if you have libgmp])
fi

AC_CONFIG_HEADERS([config.h])
</pre></div>

<p><code>AC_ARG_WITH</code> adds two command-line options to the configure script 
based on its first argument, in this case &lsquo;<samp>--with-libgmp</samp>&rsquo; and 
&lsquo;<samp>--without-libgmp</samp>&rsquo;.  If either of these is given, the shell 
variable &lsquo;<samp>with_libgmp</samp>&rsquo; is set to an appropriate value.
</p>
<p>The second argument uses <code>AS_HELP_STRING</code> to format a line which is 
added to the output of &lsquo;<samp>./configure --help</samp>&rsquo;:
</p>
<div class="example">
<pre class="example">Optional Packages:
  --with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]
  --without-PACKAGE       do not use PACKAGE (same as --with-PACKAGE=no)
  --without-libgmp        Disable use of libgmp
</pre></div>

<p>The third argument to <code>AC_ARG_WITH</code> is shell code to execute if 
either of the <code>--with</code> and <code>--without</code> options are not given; 
in this case this argument is blank.  The fourth argument is shell code 
to execute if neither of these options were given; in this case we 
default to trying to use the library.
</p>
<p>This example introduces the use of the <code>AC_DEFINE</code> macro, which 
defines a preprocessor symbol, just as &lsquo;<samp>HAVE_LIBGMP</samp>&rsquo; was defined by 
<code>AC_CHECK_LIB</code>. This provides a way other than output variables for 
a configure script to affect how software is built.  The first argument 
to <code>AC_DEFINE</code> is the name of a C preprocessor symbol and the 
second is its value.
</p>
<p>This use of the <code>AC_CONFIG_HEADERS</code> macro places preprocessor 
symbol definitions set by <code>AC_DEFINE</code> in a C header file called 
<samp>config.h</samp>, which can be included by the other source files for the 
program to get these definitions.  Before we passed the values of these 
symbols on the command-line via the <code>DEFS</code> output variable.  
<code>DEFS</code> is still set, but its value is now &lsquo;<samp>-DHAVE_CONFIG_H 
-I.</samp>&rsquo;.
</p>
<p>In order to update the build system after adding a use of 
<code>AC_DEFINE</code>, you need to run <code>autoreconf</code>.  This creates a 
template file for <samp>config.h</samp>, called <samp>config.h.in</samp>.  
(<code>autoreconf</code> runs a program called <code>autoheader</code> to do 
this.)
</p>
<p>In your C source files you can then include the header file with
</p>
<div class="example">
<pre class="example">#include &lt;config.h&gt;
</pre></div>

<p>and conditionally use code using preprocessor conditionals:
</p>
<div class="example">
<pre class="example">#ifdef HAVE_LIBGMP
/* Code using libgmp */
#endif
</pre></div>

<div class="header">
<p>
Next: <a href="Adding-a-data-file-to-be-installed.html#Adding-a-data-file-to-be-installed" accesskey="n" rel="next">Adding a data file to be installed</a>, Previous: <a href="Adding-a-check-for-a-library.html#Adding-a-check-for-a-library" accesskey="p" rel="prev">Adding a check for a library</a>, Up: <a href="Tutorial.html#Tutorial" accesskey="u" rel="up">Tutorial</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
