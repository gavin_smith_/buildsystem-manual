<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Polymorphic Variables</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Polymorphic Variables">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Polymorphic Variables">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Programming-in-M4sh.html#Programming-in-M4sh" rel="up" title="Programming in M4sh">
<link href="Initialization-Macros.html#Initialization-Macros" rel="next" title="Initialization Macros">
<link href="Common-Shell-Constructs.html#Common-Shell-Constructs" rel="prev" title="Common Shell Constructs">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Polymorphic-Variables"></a>
<div class="header">
<p>
Next: <a href="Initialization-Macros.html#Initialization-Macros" accesskey="n" rel="next">Initialization Macros</a>, Previous: <a href="Common-Shell-Constructs.html#Common-Shell-Constructs" accesskey="p" rel="prev">Common Shell Constructs</a>, Up: <a href="Programming-in-M4sh.html#Programming-in-M4sh" accesskey="u" rel="up">Programming in M4sh</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Support-for-indirect-variable-names"></a>
<h4 class="subsection">15.2.2 Support for indirect variable names</h4>
<a name="index-variable-name-indirection"></a>
<a name="index-polymorphic-variable-name"></a>
<a name="index-indirection_002c-variable-name"></a>

<p>Often, it is convenient to write a macro that will emit shell code
operating on a shell variable.  The simplest case is when the variable
name is known.  But a more powerful idiom is writing shell code that can
work through an indirection, where another variable or command
substitution produces the name of the variable to actually manipulate.
M4sh supports the notion of polymorphic shell variables, making it easy
to write a macro that can deal with either literal or indirect variable
names and output shell code appropriate for both use cases.  Behavior is
undefined if expansion of an indirect variable does not result in a
literal variable name.
</p>
<dl>
<dt><a name="index-AS_005fLITERAL_005fIF-1"></a>Macro: <strong>AS_LITERAL_IF</strong> <em>(<var>expression</var>, <span class="roman">[</span><var>if-literal</var><span class="roman">]</span>, <span class="roman">[</span><var>if-not</var><span class="roman">]</span>,   <span class="roman">[</span><var>if-simple-ref</var> = <var>if-not</var><span class="roman">]</span></em></dt>
<dt><a name="index-AS_005fLITERAL_005fWORD_005fIF-1"></a>Macro: <strong>AS_LITERAL_WORD_IF</strong> <em>(<var>expression</var>, <span class="roman">[</span><var>if-literal</var><span class="roman">]</span>,   <span class="roman">[</span><var>if-not</var><span class="roman">]</span>, <span class="roman">[</span><var>if-simple-ref</var> = <var>if-not</var><span class="roman">]</span></em></dt>
<dd><a name="index-AS_005fLITERAL_005fIF"></a>
<a name="index-AS_005fLITERAL_005fWORD_005fIF"></a>
<p>If the expansion of <var>expression</var> is definitely a shell literal,
expand <var>if-literal</var>.  If the expansion of <var>expression</var> looks
like it might contain shell indirections (such as <code>$var</code> or
<code>`expr`</code>), then <var>if-not</var> is expanded.  Sometimes, it is
possible to output optimized code if <var>expression</var> consists only of
shell variable expansions (such as <code>${var}</code>), in which case
<var>if-simple-ref</var> can be provided; but defaulting to <var>if-not</var>
should always be safe.  <code>AS_LITERAL_WORD_IF</code> only expands
<var>if-literal</var> if <var>expression</var> looks like a single shell word,
containing no whitespace; while <code>AS_LITERAL_IF</code> allows whitespace
in <var>expression</var>.
</p>
<p>In order to reduce the time spent recognizing whether an
<var>expression</var> qualifies as a literal or a simple indirection, the
implementation is somewhat conservative: <var>expression</var> must be a
single shell word (possibly after stripping whitespace), consisting only
of bytes that would have the same meaning whether unquoted or enclosed
in double quotes (for example, &lsquo;<samp>a.b</samp>&rsquo; results in <var>if-literal</var>,
even though it is not a valid shell variable name; while both &lsquo;<samp>'a'</samp>&rsquo;
and &lsquo;<samp>[$]</samp>&rsquo; result in <var>if-not</var>, because they behave differently
than &lsquo;<samp>&quot;'a'&quot;</samp>&rsquo; and &lsquo;<samp>&quot;[$]&quot;</samp>&rsquo;).  This macro can be used in contexts
for recognizing portable file names (such as in the implementation of
<code>AC_LIBSOURCE</code>), or coupled with some transliterations for forming
valid variable names (such as in the implementation of <code>AS_TR_SH</code>,
which uses an additional <code>m4_translit</code> to convert &lsquo;<samp>.</samp>&rsquo; to
&lsquo;<samp>_</samp>&rsquo;).
</p>
<p>This example shows how to read the contents of the shell variable
<code>bar</code>, exercising all three arguments to <code>AS_LITERAL_IF</code>.  It
results in a script that will output the line &lsquo;<samp>hello</samp>&rsquo; three times.
</p>
<div class="example">
<pre class="example">AC_DEFUN([MY_ACTION],
[AS_LITERAL_IF([$1],
  [echo &quot;$$1&quot;],
  [AS_VAR_COPY([var], [$1])
   echo &quot;$var&quot;],
  [eval 'echo &quot;$'&quot;$1&quot;\&quot;])])
foo=bar bar=hello
MY_ACTION([bar])
MY_ACTION([`echo bar`])
MY_ACTION([$foo])
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-AS_005fVAR_005fAPPEND-1"></a>Macro: <strong>AS_VAR_APPEND</strong> <em>(<var>var</var>, <var>text</var>)</em></dt>
<dd><a name="index-AS_005fVAR_005fAPPEND"></a>
<p>Emit shell code to append the shell expansion of <var>text</var> to the end
of the current contents of the polymorphic shell variable <var>var</var>,
taking advantage of shells that provide the &lsquo;<samp>+=</samp>&rsquo; extension for more
efficient scaling.
</p>
<p>For situations where the final contents of <var>var</var> are relatively
short (less than 256 bytes), it is more efficient to use the simpler
code sequence of <code><var>var</var>=${<var>var</var>}<var>text</var></code> (or its
polymorphic equivalent of <code>AS_VAR_COPY([t], [<var>var</var>])</code> and
<code>AS_VAR_SET([<var>var</var>], [&quot;$t&quot;<var>text</var>])</code>).  But in the case
when the script will be repeatedly appending text into <code>var</code>,
issues of scaling start to become apparent.  A naive implementation
requires execution time linear to the length of the current contents of
<var>var</var> as well as the length of <var>text</var> for a single append, for
an overall quadratic scaling with multiple appends.  This macro takes
advantage of shells which provide the extension
<code><var>var</var>+=<var>text</var></code>, which can provide amortized constant time
for a single append, for an overall linear scaling with multiple
appends.  Note that unlike <code>AS_VAR_SET</code>, this macro requires that
<var>text</var> be quoted properly to avoid field splitting and file name
expansion.
</p></dd></dl>

<dl>
<dt><a name="index-AS_005fVAR_005fARITH-1"></a>Macro: <strong>AS_VAR_ARITH</strong> <em>(<var>var</var>, <var>expression</var>)</em></dt>
<dd><a name="index-AS_005fVAR_005fARITH"></a>
<p>Emit shell code to compute the arithmetic expansion of <var>expression</var>,
assigning the result as the contents of the polymorphic shell variable
<var>var</var>.  The code takes advantage of shells that provide &lsquo;<samp>$(())</samp>&rsquo;
for fewer forks, but uses <code>expr</code> as a fallback.  Therefore, the
syntax for a valid <var>expression</var> is rather limited: all operators
must occur as separate shell arguments and with proper quoting, there is
no portable equality operator, all variables containing numeric values
must be expanded prior to the computation, all numeric values must be
provided in decimal without leading zeroes, and the first shell argument
should not be a negative number.  In the following example, this snippet
will print &lsquo;<samp>(2+3)*4 == 20</samp>&rsquo;.
</p>
<div class="example">
<pre class="example">bar=3
AS_VAR_ARITH([foo], [\( 2 + $bar \) \* 4])
echo &quot;(2+$bar)*4 == $foo&quot;
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-AS_005fVAR_005fCOPY-1"></a>Macro: <strong>AS_VAR_COPY</strong> <em>(<var>dest</var>, <var>source</var>)</em></dt>
<dd><a name="index-AS_005fVAR_005fCOPY"></a>
<p>Emit shell code to assign the contents of the polymorphic shell variable
<var>source</var> to the polymorphic shell variable <var>dest</var>.  For example,
executing this M4sh snippet will output &lsquo;<samp>bar hi</samp>&rsquo;:
</p>
<div class="example">
<pre class="example">foo=bar bar=hi
AS_VAR_COPY([a], [foo])
AS_VAR_COPY([b], [$foo])
echo &quot;$a $b&quot;
</pre></div>

<p>When it is necessary to access the contents of an indirect variable
inside a shell double-quoted context, the recommended idiom is to first
copy the contents into a temporary literal shell variable.
</p>
<div class="smallexample">
<pre class="smallexample">for header in stdint_h inttypes_h ; do
  AS_VAR_COPY([var], [ac_cv_header_$header])
  echo &quot;$header detected: $var&quot;
done
</pre></div>
</dd></dl>


<dl>
<dt><a name="index-AS_005fVAR_005fIF-1"></a>Macro: <strong>AS_VAR_IF</strong> <em>(<var>var</var>, <span class="roman">[</span><var>word</var><span class="roman">]</span>, <span class="roman">[</span><var>if-equal</var><span class="roman">]</span>,   <span class="roman">[</span><var>if-not-equal</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AS_005fVAR_005fIF"></a>
<p>Output a shell conditional statement.  If the contents of the
polymorphic shell variable <var>var</var> match the string <var>word</var>,
execute <var>if-equal</var>; otherwise execute <var>if-not-equal</var>.  <var>word</var>
must be a single shell word (typically a quoted string).  Avoids
shell bugs if an interrupt signal arrives while a command substitution
in <var>var</var> is being expanded.
</p></dd></dl>

<dl>
<dt><a name="index-AS_005fVAR_005fPUSHDEF-1"></a>Macro: <strong>AS_VAR_PUSHDEF</strong> <em>(<var>m4-name</var>, <var>value</var>)</em></dt>
<dt><a name="index-AS_005fVAR_005fPOPDEF-1"></a>Macro: <strong>AS_VAR_POPDEF</strong> <em>(<var>m4-name</var>)</em></dt>
<dd><a name="index-AS_005fVAR_005fPUSHDEF"></a>
<a name="index-AS_005fVAR_005fPOPDEF"></a>
<a name="index-composing-variable-names"></a>
<a name="index-variable-names_002c-composing"></a>
<p>A common M4sh idiom involves composing shell variable names from an m4
argument (for example, writing a macro that uses a cache variable).
<var>value</var> can be an arbitrary string, which will be transliterated
into a valid shell name by <code>AS_TR_SH</code>.  In order to access the
composed variable name based on <var>value</var>, it is easier to declare a
temporary m4 macro <var>m4-name</var> with <code>AS_VAR_PUSHDEF</code>, then use
that macro as the argument to subsequent <code>AS_VAR</code> macros as a
polymorphic variable name, and finally free the temporary macro with
<code>AS_VAR_POPDEF</code>.  These macros are often followed with <code>dnl</code>,
to avoid excess newlines in the output.
</p>
<p>Here is an involved example, that shows the power of writing macros that
can handle composed shell variable names:
</p>
<div class="example">
<pre class="example">m4_define([MY_CHECK_HEADER],
[AS_VAR_PUSHDEF([my_Header], [ac_cv_header_$1])dnl
AS_VAR_IF([my_Header], [yes], [echo &quot;header $1 detected&quot;])dnl
AS_VAR_POPDEF([my_Header])dnl
])
MY_CHECK_HEADER([stdint.h])
for header in inttypes.h stdlib.h ; do
  MY_CHECK_HEADER([$header])
done
</pre></div>

<p>In the above example, <code>MY_CHECK_HEADER</code> can operate on polymorphic
variable names.  In the first invocation, the m4 argument is
<code>stdint.h</code>, which transliterates into a literal <code>stdint_h</code>.
As a result, the temporary macro <code>my_Header</code> expands to the literal
shell name &lsquo;<samp>ac_cv_header_stdint_h</samp>&rsquo;.  In the second invocation, the
m4 argument to <code>MY_CHECK_HEADER</code> is <code>$header</code>, and the
temporary macro <code>my_Header</code> expands to the indirect shell name
&lsquo;<samp>$as_my_Header</samp>&rsquo;.  During the shell execution of the for loop, when
&lsquo;<samp>$header</samp>&rsquo; contains &lsquo;<samp>inttypes.h</samp>&rsquo;, then &lsquo;<samp>$as_my_Header</samp>&rsquo;
contains &lsquo;<samp>ac_cv_header_inttypes_h</samp>&rsquo;.  If this script is then run on a
platform where all three headers have been previously detected, the
output of the script will include:
</p>
<div class="smallexample">
<pre class="smallexample">header stdint.h detected
header inttypes.h detected
header stdlib.h detected
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-AS_005fVAR_005fSET-1"></a>Macro: <strong>AS_VAR_SET</strong> <em>(<var>var</var>, <span class="roman">[</span><var>value</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AS_005fVAR_005fSET"></a>
<p>Emit shell code to assign the contents of the polymorphic shell variable
<var>var</var> to the shell expansion of <var>value</var>.  <var>value</var> is not
subject to field splitting or file name expansion, so if command
substitution is used, it may be done with &lsquo;<samp>`&quot;&quot;`</samp>&rsquo; rather than using
an intermediate variable (see <a href="Shell-Substitutions.html#Shell-Substitutions">Shell Substitutions</a>).  However,
<var>value</var> does undergo rescanning for additional macro names; behavior
is unspecified if late expansion results in any shell meta-characters.
</p></dd></dl>

<dl>
<dt><a name="index-AS_005fVAR_005fSET_005fIF-1"></a>Macro: <strong>AS_VAR_SET_IF</strong> <em>(<var>var</var>, <span class="roman">[</span><var>if-set</var><span class="roman">]</span>, <span class="roman">[</span><var>if-undef</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AS_005fVAR_005fSET_005fIF"></a>
<p>Emit a shell conditional statement, which executes <var>if-set</var> if the
polymorphic shell variable <code>var</code> is set to any value, and
<var>if-undef</var> otherwise.
</p></dd></dl>

<dl>
<dt><a name="index-AS_005fVAR_005fTEST_005fSET-1"></a>Macro: <strong>AS_VAR_TEST_SET</strong> <em>(<var>var</var>)</em></dt>
<dd><a name="index-AS_005fVAR_005fTEST_005fSET"></a>
<p>Emit a shell statement that results in a successful exit status only if
the polymorphic shell variable <code>var</code> is set.
</p></dd></dl>

<div class="header">
<p>
Next: <a href="Initialization-Macros.html#Initialization-Macros" accesskey="n" rel="next">Initialization Macros</a>, Previous: <a href="Common-Shell-Constructs.html#Common-Shell-Constructs" accesskey="p" rel="prev">Common Shell Constructs</a>, Up: <a href="Programming-in-M4sh.html#Programming-in-M4sh" accesskey="u" rel="up">Programming in M4sh</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
