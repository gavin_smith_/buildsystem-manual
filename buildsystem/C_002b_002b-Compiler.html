<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: C++ Compiler</title>

<meta name="description" content="Automake and Autoconf Reference Manual: C++ Compiler">
<meta name="keywords" content="Automake and Autoconf Reference Manual: C++ Compiler">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="C_002b_002b-Support.html#C_002b_002b-Support" rel="up" title="C++ Support">
<link href="Yacc-and-Lex.html#Yacc-and-Lex" rel="next" title="Yacc and Lex">
<link href="C_002b_002b-Support.html#C_002b_002b-Support" rel="prev" title="C++ Support">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="C_002b_002b-Compiler"></a>
<div class="header">
<p>
Up: <a href="C_002b_002b-Support.html#C_002b_002b-Support" accesskey="u" rel="up">C++ Support</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="C_002b_002b-Compiler-Characteristics"></a>
<h4 class="subsection">10.3.1 C++ Compiler Characteristics</h4>


<dl>
<dt><a name="index-AC_005fPROG_005fCXX-2"></a>Macro: <strong>AC_PROG_CXX</strong> <em>(<span class="roman">[</span><var>compiler-search-list</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AC_005fPROG_005fCXX"></a>
<a name="index-CXX"></a>
<a name="index-CXXFLAGS-1"></a>
<a name="index-CXX-1"></a>
<a name="index-CXXFLAGS-3"></a>
<p>Determine a C++ compiler to use.  Check whether the environment variable
<code>CXX</code> or <code>CCC</code> (in that order) is set; if so, then set output
variable <code>CXX</code> to its value.
</p>
<p>Otherwise, if the macro is invoked without an argument, then search for
a C++ compiler under the likely names (first <code>g++</code> and <code>c++</code>
then other names).  If none of those checks succeed, then as a last
resort set <code>CXX</code> to <code>g++</code>.
</p>
<p>This macro may, however, be invoked with an optional first argument
which, if specified, must be a blank-separated list of C++ compilers to
search for.  This just gives the user an opportunity to specify an
alternative search list for the C++ compiler.  For example, if you
didn&rsquo;t like the default order, then you could invoke <code>AC_PROG_CXX</code>
like this:
</p>
<div class="example">
<pre class="example">AC_PROG_CXX([gcc cl KCC CC cxx cc++ xlC aCC c++ g++])
</pre></div>

<p>If necessary, add an option to output variable <code>CXX</code> to enable
support for ISO Standard C++ features with extensions.  Prefer the
newest C++ standard that is supported.  Currently the newest standard is
ISO C++11, with ISO C++98 being the previous standard.  After calling
this macro you can check whether the C++ compiler has been set to accept
Standard C++; if not, the shell variable <code>ac_cv_prog_cxx_stdcxx</code> is
set to &lsquo;<samp>no</samp>&rsquo;.  If the C++ compiler will not accept C++11, the shell
variable <code>ac_cv_prog_cxx_cxx11</code> is set to &lsquo;<samp>no</samp>&rsquo;, and if it will
not accept C++98, the shell variable <code>ac_cv_prog_cxx_cxx98</code> is set
to &lsquo;<samp>no</samp>&rsquo;.
</p>
<p>When attempting to add compiler options, prefer extended functionality
to strict conformance: the goal is to enable whatever standard features
that are available, not to check for full conformance to the standard or
to prohibit incompatible extensions.  Test for C++11 support by checking
for the language features <code>auto</code>, <code>constexpr</code>,
<code>decltype</code>, <code>default</code>ed and <code>delete</code>ed constructors,
delegate constructors, <code>final</code>, initializer lists, lambda
functions, <code>nullptr</code>, <code>override</code>, range-based for loops,
template brackets without spaces and unicode literals, and library
features <code>std::array</code>, <code>std::shared_ptr</code>,
<code>std::weak_ptr</code>, <code>std::regex</code> and <code>std::tuple</code>.  Test for
C++98 support using basic features of the <code>std</code> namespace including
<code>std::string</code>, containers (<code>std::list</code>, <code>std::map</code>,
<code>std::set</code>, <code>std::vector</code>), streams (fstreams, iostreams,
stringstreams, iomanip), <code>std::pair</code>, exceptions (<code>try</code>,
<code>catch</code> and <code>std::runtime_error</code>) and algorithms.  Tests for
more recent standards include all the tests for older standards.
</p>
<p>If using the GNU C++ compiler, set shell variable <code>GXX</code> to
&lsquo;<samp>yes</samp>&rsquo;.  If output variable <code>CXXFLAGS</code> was not already set, set
it to <samp>-g -O2</samp> for the GNU C++ compiler (<samp>-O2</samp> on
systems where G++ does not accept <samp>-g</samp>), or <samp>-g</samp> for other
compilers.  If your package does not like this default, then it is
acceptable to insert the line &lsquo;<samp>: ${CXXFLAGS=&quot;&quot;}</samp>&rsquo; after <code>AC_INIT</code>
and before <code>AC_PROG_CXX</code> to select an empty default instead.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fPROG_005fCXXCPP-1"></a>Macro: <strong>AC_PROG_CXXCPP</strong></dt>
<dd><a name="index-AC_005fPROG_005fCXXCPP"></a>
<a name="index-CXXCPP"></a>
<a name="index-CXXCPP-1"></a>
<p>Set output variable <code>CXXCPP</code> to a command that runs the C++
preprocessor.  If &lsquo;<samp>$CXX -E</samp>&rsquo; doesn&rsquo;t work, <samp>/lib/cpp</samp> is used.
It is portable to run <code>CXXCPP</code> only on files with a <samp>.c</samp>,
<samp>.C</samp>, <samp>.cc</samp>, or <samp>.cpp</samp> extension.
</p>
<p>Some preprocessors don&rsquo;t indicate missing include files by the error
status.  For such preprocessors an internal variable is set that causes
other macros to check the standard error from the preprocessor and
consider the test failed if any warnings have been reported.  However,
it is not known whether such broken preprocessors exist for C++.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fPROG_005fCXX_005fC_005fO-1"></a>Macro: <strong>AC_PROG_CXX_C_O</strong></dt>
<dd><a name="index-AC_005fPROG_005fCXX_005fC_005fO"></a>
<a name="index-CXX_005fNO_005fMINUS_005fC_005fMINUS_005fO"></a>
<p>Test whether the C++ compiler accepts the options <samp>-c</samp> and
<samp>-o</samp> simultaneously, and define <code>CXX_NO_MINUS_C_MINUS_O</code>,
if it does not.
</p></dd></dl>


<div class="header">
<p>
Up: <a href="C_002b_002b-Support.html#C_002b_002b-Support" accesskey="u" rel="up">C++ Support</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
