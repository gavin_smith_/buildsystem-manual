<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Wildcards</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Wildcards">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Wildcards">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Rationales.html#Rationales" rel="up" title="Rationales">
<link href="Why-GNU-M4.html#Why-GNU-M4" rel="next" title="Why GNU M4">
<link href="Rationales.html#Rationales" rel="prev" title="Rationales">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Wildcards"></a>
<div class="header">
<p>
Next: <a href="Why-GNU-M4.html#Why-GNU-M4" accesskey="n" rel="next">Why GNU M4</a>, Up: <a href="Rationales.html#Rationales" accesskey="u" rel="up">Rationales</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Why-doesn_0027t-Automake-support-wildcards_003f"></a>
<h3 class="section">I.1 Why doesn&rsquo;t Automake support wildcards?</h3>
<a name="index-wildcards"></a>

<p>Developers are lazy.  They would often like to use wildcards in
<samp>Makefile.am</samp>s, so that they would not need to remember to
update <samp>Makefile.am</samp>s every time they add, delete, or rename
a file.
</p>
<p>There are several objections to this:
</p><ul>
<li> When using CVS (or similar) developers need to remember they have to
run &lsquo;<samp>cvs add</samp>&rsquo; or &lsquo;<samp>cvs rm</samp>&rsquo; anyway.  Updating
<samp>Makefile.am</samp> accordingly quickly becomes a reflex.

<p>Conversely, if your application doesn&rsquo;t compile
because you forgot to add a file in <samp>Makefile.am</samp>, it will help
you remember to &lsquo;<samp>cvs add</samp>&rsquo; it.
</p>
</li><li> Using wildcards makes it easy to distribute files by mistake.  For
instance, some code a developer is experimenting with (a test case,
say) that should not be part of the distribution.

</li><li> Using wildcards it&rsquo;s easy to omit some files by mistake.  For
instance, one developer creates a new file, uses it in many places,
but forgets to commit it.  Another developer then checks out the
incomplete project and is able to run &lsquo;<samp>make dist</samp>&rsquo; successfully,
even though a file is missing. By listing files, &lsquo;<samp>make dist</samp>&rsquo;
<em>will</em> complain.

</li><li> Wildcards are not portable to some non-GNU <code>make</code> implementations,
e.g., NetBSD <code>make</code> will not expand globs such as &lsquo;<samp>*</samp>&rsquo; in
prerequisites of a target.

</li><li> Finally, it&rsquo;s really hard to <em>forget</em> to add a file to
<samp>Makefile.am</samp>: files that are not listed in <samp>Makefile.am</samp> are
not compiled or installed, so you can&rsquo;t even test them.
</li></ul>

<p>Still, these are philosophical objections, and as such you may disagree,
or find enough value in wildcards to dismiss all of them.  Before you
start writing a patch against Automake to teach it about wildcards,
let&rsquo;s see the main technical issue: portability.
</p>
<p>Although &lsquo;<samp>$(wildcard ...)</samp>&rsquo; works with GNU <code>make</code>, it is
not portable to other <code>make</code> implementations.
</p>
<p>The only way Automake could support <code>$(wildcard ...)</code> is by
expanding <code>$(wildcard ...)</code> when <code>automake</code> is run.
The resulting <samp>Makefile.in</samp>s would be portable since they would
list all files and not use &lsquo;<samp>$(wildcard ...)</samp>&rsquo;.  However that
means developers would need to remember to run <code>automake</code> each
time they add, delete, or rename files.
</p>
<p>Compared to editing <samp>Makefile.am</samp>, this is a very small gain.  Sure,
it&rsquo;s easier and faster to type &lsquo;<samp>automake; make</samp>&rsquo; than to type
&lsquo;<samp>emacs Makefile.am; make</samp>&rsquo;.  But nobody bothered enough to write a
patch to add support for this syntax.  Some people use scripts to
generate file lists in <samp>Makefile.am</samp> or in separate
<samp>Makefile</samp> fragments.
</p>
<p>Even if you don&rsquo;t care about portability, and are tempted to use
&lsquo;<samp>$(wildcard ...)</samp>&rsquo; anyway because you target only GNU Make, you
should know there are many places where Automake needs to know exactly
which files should be processed.  As Automake doesn&rsquo;t know how to
expand &lsquo;<samp>$(wildcard ...)</samp>&rsquo;, you cannot use it in these places.
&lsquo;<samp>$(wildcard ...)</samp>&rsquo; is a black box comparable to <code>AC_SUBST</code>ed
variables as far Automake is concerned.
</p>
<p>You can get warnings about &lsquo;<samp>$(wildcard ...</samp>&rsquo;) constructs using the
<samp>-Wportability</samp> flag.
</p>
<div class="header">
<p>
Next: <a href="Why-GNU-M4.html#Why-GNU-M4" accesskey="n" rel="next">Why GNU M4</a>, Up: <a href="Rationales.html#Rationales" accesskey="u" rel="up">Rationales</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
