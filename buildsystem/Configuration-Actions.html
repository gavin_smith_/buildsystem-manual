<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Configuration Actions</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Configuration Actions">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Configuration Actions">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="What-to-do-with-results-of-tests.html#What-to-do-with-results-of-tests" rel="up" title="What to do with results of tests">
<link href="Setting-Output-Variables.html#Setting-Output-Variables" rel="next" title="Setting Output Variables">
<link href="What-to-do-with-results-of-tests.html#What-to-do-with-results-of-tests" rel="prev" title="What to do with results of tests">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Configuration-Actions"></a>
<div class="header">
<p>
Next: <a href="Setting-Output-Variables.html#Setting-Output-Variables" accesskey="n" rel="next">Setting Output Variables</a>, Up: <a href="What-to-do-with-results-of-tests.html#What-to-do-with-results-of-tests" accesskey="u" rel="up">What to do with results of tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Performing-Configuration-Actions"></a>
<h3 class="section">9.1 Performing Configuration Actions</h3>
<a name="index-Configuration-actions"></a>

<p><samp>configure</samp> is designed so that it appears to do everything itself,
but there is actually a hidden slave: <samp>config.status</samp>.
<samp>configure</samp> is in charge of examining your system, but it is
<samp>config.status</samp> that actually takes the proper actions based on the
results of <samp>configure</samp>.  The most typical task of
<samp>config.status</samp> is to <em>instantiate</em> files.
</p>
<a name="index-AC_005fCONFIG_005fitems"></a>
<p>This section describes the common behavior of the four standard
instantiating macros: <code>AC_CONFIG_FILES</code>, <code>AC_CONFIG_HEADERS</code>,
<code>AC_CONFIG_COMMANDS</code> and <code>AC_CONFIG_LINKS</code>.  They all
have this prototype:
</p>
<div class="example">
<pre class="example">AC_CONFIG_<var>items</var>(<var>tag</var>&hellip;, <span class="roman">[</span><var>commands</var><span class="roman">]</span>, <span class="roman">[</span><var>init-cmds</var><span class="roman">]</span>)
</pre></div>

<p>where the arguments are:
</p>
<dl compact="compact">
<dt><var>tag&hellip;</var></dt>
<dd><p>A blank-or-newline-separated list of tags, which are typically the names of
the files to instantiate.
</p>
<p>You are encouraged to use literals as <var>tags</var>.  In particular, you
should avoid
</p>
<div class="example">
<pre class="example">&hellip; &amp;&amp; my_foos=&quot;$my_foos fooo&quot;
&hellip; &amp;&amp; my_foos=&quot;$my_foos foooo&quot;
AC_CONFIG_<var>items</var>([$my_foos])
</pre></div>

<p>and use this instead:
</p>
<div class="example">
<pre class="example">&hellip; &amp;&amp; AC_CONFIG_<var>items</var>([fooo])
&hellip; &amp;&amp; AC_CONFIG_<var>items</var>([foooo])
</pre></div>

<p>The macros <code>AC_CONFIG_FILES</code> and <code>AC_CONFIG_HEADERS</code> use
special <var>tag</var> values: they may have the form &lsquo;<samp><var>output</var></samp>&rsquo; or
&lsquo;<samp><var>output</var>:<var>inputs</var></samp>&rsquo;.  The file <var>output</var> is instantiated
from its templates, <var>inputs</var> (defaulting to &lsquo;<samp><var>output</var>.in</samp>&rsquo;).
</p>
<p>&lsquo;<samp>AC_CONFIG_FILES([Makefile:boiler/top.mk:boiler/bot.mk])</samp>&rsquo;,
for example, asks for
the creation of the file <samp>Makefile</samp> that contains the expansion of the
nutput variables in the concatenation of <samp>boiler/top.mk</samp> and
<samp>boiler/bot.mk</samp>.
</p>
<p>The special value &lsquo;<samp>-</samp>&rsquo; might be used to denote the standard output
when used in <var>output</var>, or the standard input when used in the
<var>inputs</var>.  You most probably don&rsquo;t need to use this in
<samp>configure.ac</samp>, but it is convenient when using the command line
interface of <samp>./config.status</samp>, see <a href="config_002estatus-Invocation.html#config_002estatus-Invocation">config.status Invocation</a>,
for more details.
</p>
<p>The <var>inputs</var> may be absolute or relative file names.  In the latter
case they are first looked for in the build tree, and then in the source
tree.  Input files should be text files, and a line length below 2000
bytes should be safe.
</p>
</dd>
<dt><var>commands</var></dt>
<dd><p>Shell commands output literally into <samp>config.status</samp>, and
associated with a tag that the user can use to tell <samp>config.status</samp>
which commands to run.  The commands are run each time a <var>tag</var>
request is given to <samp>config.status</samp>, typically each time the file
<samp><var>tag</var></samp> is created.
</p>
<p>The variables set during the execution of <code>configure</code> are
<em>not</em> available here: you first need to set them via the
<var>init-cmds</var>.  Nonetheless the following variables are precomputed:
</p>
<dl compact="compact">
<dt><code>srcdir</code></dt>
<dd><a name="index-srcdir-1"></a>
<p>The name of the top source directory, assuming that the working
directory is the top build directory.  This
is what the <code>configure</code> option <samp>--srcdir</samp> sets.
</p>
</dd>
<dt><code>ac_top_srcdir</code></dt>
<dd><a name="index-ac_005ftop_005fsrcdir"></a>
<p>The name of the top source directory, assuming that the working
directory is the current build directory.
</p>
</dd>
<dt><code>ac_top_build_prefix</code></dt>
<dd><a name="index-ac_005ftop_005fbuild_005fprefix"></a>
<p>The name of the top build directory, assuming that the working
directory is the current build directory.
It can be empty, or else ends with a slash, so that you may concatenate
it.
</p>
</dd>
<dt><code>ac_srcdir</code></dt>
<dd><a name="index-ac_005fsrcdir"></a>
<p>The name of the corresponding source directory, assuming that the
working directory is the current build directory.
</p>
</dd>
<dt><code>tmp</code></dt>
<dd><a name="index-tmp-1"></a>
<p>The name of a temporary directory within the build tree, which you
can use if you need to create additional temporary files.  The
directory is cleaned up when <code>config.status</code> is done or
interrupted.  Please use package-specific file name prefixes to
avoid clashing with files that <code>config.status</code> may use
internally.
</p></dd>
</dl>

<p>The <em>current</em> directory refers to the directory (or
pseudo-directory) containing the input part of <var>tags</var>.  For
instance, running
</p>
<div class="example">
<pre class="example">AC_CONFIG_COMMANDS([deep/dir/out:in/in.in], [&hellip;], [&hellip;])
</pre></div>

<p>with <samp>--srcdir=../package</samp> produces the following values:
</p>
<div class="example">
<pre class="example"># Argument of --srcdir
srcdir='../package'
# Reversing deep/dir
ac_top_build_prefix='../../'
# Concatenation of $ac_top_build_prefix and srcdir
ac_top_srcdir='../../../package'
# Concatenation of $ac_top_srcdir and deep/dir
ac_srcdir='../../../package/deep/dir'
</pre></div>

<p>independently of &lsquo;<samp>in/in.in</samp>&rsquo;.
</p>
</dd>
<dt><var>init-cmds</var></dt>
<dd><p>Shell commands output <em>unquoted</em> near the beginning of
<samp>config.status</samp>, and executed each time <samp>config.status</samp> runs
(regardless of the tag).  Because they are unquoted, for example,
&lsquo;<samp>$var</samp>&rsquo; is output as the value of <code>var</code>.  <var>init-cmds</var>
is typically used by <samp>configure</samp> to give <samp>config.status</samp> some
variables it needs to run the <var>commands</var>.
</p>
<p>You should be extremely cautious in your variable names: all the
<var>init-cmds</var> share the same name space and may overwrite each other
in unpredictable ways.  Sorry<small class="enddots">...</small>
</p></dd>
</dl>

<p>All these macros can be called multiple times, with different
<var>tag</var> values, of course!
</p>

<div class="header">
<p>
Next: <a href="Setting-Output-Variables.html#Setting-Output-Variables" accesskey="n" rel="next">Setting Output Variables</a>, Up: <a href="What-to-do-with-results-of-tests.html#What-to-do-with-results-of-tests" accesskey="u" rel="up">What to do with results of tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
