<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Quotation Rule Of Thumb</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Quotation Rule Of Thumb">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Quotation Rule Of Thumb">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="M4-Quotation.html#M4-Quotation" rel="up" title="M4 Quotation">
<link href="Using-autom4te.html#Using-autom4te" rel="next" title="Using autom4te">
<link href="Balancing-Parentheses.html#Balancing-Parentheses" rel="prev" title="Balancing Parentheses">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Quotation-Rule-Of-Thumb"></a>
<div class="header">
<p>
Previous: <a href="Balancing-Parentheses.html#Balancing-Parentheses" accesskey="p" rel="prev">Balancing Parentheses</a>, Up: <a href="M4-Quotation.html#M4-Quotation" accesskey="u" rel="up">M4 Quotation</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Quotation-Rule-Of-Thumb-1"></a>
<h4 class="subsection">F.1.7 Quotation Rule Of Thumb</h4>

<p>To conclude, the quotation rule of thumb is:
</p>
<div align="center"><em>One pair of quotes per pair of parentheses.</em>
</div>
<p>Never over-quote, never under-quote, in particular in the definition of
macros.  In the few places where the macros need to use brackets
(usually in C program text or regular expressions), properly quote
<em>the arguments</em>!
</p>
<p>It is common to read Autoconf programs with snippets like:
</p>
<div class="example">
<pre class="example">AC_TRY_LINK(
changequote(&lt;&lt;, &gt;&gt;)dnl
&lt;&lt;#include &lt;time.h&gt;
#ifndef tzname /* For SGI.  */
extern char *tzname[]; /* RS6000 and others reject char **tzname.  */
#endif&gt;&gt;,
changequote([, ])dnl
[atoi (*tzname);], ac_cv_var_tzname=yes, ac_cv_var_tzname=no)
</pre></div>

<p>which is incredibly useless since <code>AC_TRY_LINK</code> is <em>already</em>
double quoting, so you just need:
</p>
<div class="example">
<pre class="example">AC_TRY_LINK(
[#include &lt;time.h&gt;
#ifndef tzname /* For SGI.  */
extern char *tzname[]; /* RS6000 and others reject char **tzname.  */
#endif],
            [atoi (*tzname);],
            [ac_cv_var_tzname=yes],
            [ac_cv_var_tzname=no])
</pre></div>

<p>The M4-fluent reader might note that these two examples are rigorously
equivalent, since M4 swallows both the &lsquo;<samp>changequote(&lt;&lt;, &gt;&gt;)</samp>&rsquo;
and &lsquo;<samp>&lt;&lt;</samp>&rsquo; &lsquo;<samp>&gt;&gt;</samp>&rsquo; when it <em>collects</em> the arguments: these
quotes are not part of the arguments!
</p>
<p>Simplified, the example above is just doing this:
</p>
<div class="example">
<pre class="example">changequote(&lt;&lt;, &gt;&gt;)dnl
&lt;&lt;[]&gt;&gt;
changequote([, ])dnl
</pre></div>

<p>instead of simply:
</p>
<div class="example">
<pre class="example">[[]]
</pre></div>

<p>With macros that do not double quote their arguments (which is the
rule), double-quote the (risky) literals:
</p>
<div class="example">
<pre class="example">AC_LINK_IFELSE([AC_LANG_PROGRAM(
[[#include &lt;time.h&gt;
#ifndef tzname /* For SGI.  */
extern char *tzname[]; /* RS6000 and others reject char **tzname.  */
#endif]],
                                [atoi (*tzname);])],
               [ac_cv_var_tzname=yes],
               [ac_cv_var_tzname=no])
</pre></div>

<p>Please note that the macro <code>AC_TRY_LINK</code> is obsolete, so you really
should be using <code>AC_LINK_IFELSE</code> instead.
</p>
<p>See <a href="Quadrigraphs.html#Quadrigraphs">Quadrigraphs</a>, for what to do if you run into a hopeless case
where quoting does not suffice.
</p>
<p>When you create a <code>configure</code> script using newly written macros,
examine it carefully to check whether you need to add more quotes in
your macros.  If one or more words have disappeared in the M4
output, you need more quotes.  When in doubt, quote.
</p>
<p>However, it&rsquo;s also possible to put on too many layers of quotes.  If
this happens, the resulting <code>configure</code> script may contain
unexpanded macros.  The <code>autoconf</code> program checks for this problem
by looking for the string &lsquo;<samp>AC_</samp>&rsquo; in <samp>configure</samp>.  However, this
heuristic does not work in general: for example, it does not catch
overquoting in <code>AC_DEFINE</code> descriptions.
</p>


<div class="header">
<p>
Previous: <a href="Balancing-Parentheses.html#Balancing-Parentheses" accesskey="p" rel="prev">Balancing Parentheses</a>, Up: <a href="M4-Quotation.html#M4-Quotation" accesskey="u" rel="up">M4 Quotation</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
