<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Configuration Commands</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Configuration Commands">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Configuration Commands">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="What-to-do-with-results-of-tests.html#What-to-do-with-results-of-tests" rel="up" title="What to do with results of tests">
<link href="Configuration-Links.html#Configuration-Links" rel="next" title="Configuration Links">
<link href="Autoheader-Macros.html#Autoheader-Macros" rel="prev" title="Autoheader Macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Configuration-Commands"></a>
<div class="header">
<p>
Next: <a href="Configuration-Links.html#Configuration-Links" accesskey="n" rel="next">Configuration Links</a>, Previous: <a href="Defining-Symbols.html#Defining-Symbols" accesskey="p" rel="prev">Defining Symbols</a>, Up: <a href="What-to-do-with-results-of-tests.html#What-to-do-with-results-of-tests" accesskey="u" rel="up">What to do with results of tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Running-Arbitrary-Configuration-Commands"></a>
<h3 class="section">9.4 Running Arbitrary Configuration Commands</h3>
<a name="index-Configuration-commands"></a>
<a name="index-Commands-for-configuration"></a>

<p>You can execute arbitrary commands before, during, and after
<samp>config.status</samp> is run.  The three following macros accumulate the
commands to run when they are called multiple times.
<code>AC_CONFIG_COMMANDS</code> replaces the obsolete macro
<code>AC_OUTPUT_COMMANDS</code>; see <a href="Obsolete-Macros.html#Obsolete-Macros">Obsolete Macros</a>, for details.
</p>
<a name="AC_005fCONFIG_005fCOMMANDS"></a><dl>
<dt><a name="index-AC_005fCONFIG_005fCOMMANDS-1"></a>Macro: <strong>AC_CONFIG_COMMANDS</strong> <em>(<var>tag</var>&hellip;, <span class="roman">[</span><var>cmds</var><span class="roman">]</span>, <span class="roman">[</span><var>init-cmds</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AC_005fCONFIG_005fCOMMANDS"></a>
<p>Specify additional shell commands to run at the end of
<samp>config.status</samp>, and shell commands to initialize any variables
from <code>configure</code>.  Associate the commands with <var>tag</var>.
Since typically the <var>cmds</var> create a file, <var>tag</var> should
naturally be the name of that file.  If needed, the directory hosting
<var>tag</var> is created.  This macro is one of the instantiating macros;
see <a href="Configuration-Actions.html#Configuration-Actions">Configuration Actions</a>.
</p>
<p>Here is an unrealistic example:
</p><div class="example">
<pre class="example">fubar=42
AC_CONFIG_COMMANDS([fubar],
                   [echo this is extra $fubar, and so on.],
                   [fubar=$fubar])
</pre></div>

<p>Here is a better one:
</p><div class="example">
<pre class="example">AC_CONFIG_COMMANDS([timestamp], [date &gt;timestamp])
</pre></div>
</dd></dl>

<p>The following two macros look similar, but in fact they are not of the same
breed: they are executed directly by <samp>configure</samp>, so you cannot use
<samp>config.status</samp> to rerun them.
</p>

<dl>
<dt><a name="index-AC_005fCONFIG_005fCOMMANDS_005fPRE-1"></a>Macro: <strong>AC_CONFIG_COMMANDS_PRE</strong> <em>(<var>cmds</var>)</em></dt>
<dd><a name="index-AC_005fCONFIG_005fCOMMANDS_005fPRE"></a>
<p>Execute the <var>cmds</var> right before creating <samp>config.status</samp>.
</p>
<p>This macro presents the last opportunity to call <code>AC_SUBST</code>,
<code>AC_DEFINE</code>, or <code>AC_CONFIG_<var>items</var></code> macros.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fCONFIG_005fCOMMANDS_005fPOST-1"></a>Macro: <strong>AC_CONFIG_COMMANDS_POST</strong> <em>(<var>cmds</var>)</em></dt>
<dd><a name="index-AC_005fCONFIG_005fCOMMANDS_005fPOST"></a>
<p>Execute the <var>cmds</var> right after creating <samp>config.status</samp>.
</p></dd></dl>







</body>
</html>
