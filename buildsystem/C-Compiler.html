<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: C Compiler</title>

<meta name="description" content="Automake and Autoconf Reference Manual: C Compiler">
<meta name="keywords" content="Automake and Autoconf Reference Manual: C Compiler">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="C.html#C" rel="up" title="C">
<link href="Particular-Functions.html#Particular-Functions" rel="next" title="Particular Functions">
<link href="Generic-Compiler-Characteristics.html#Generic-Compiler-Characteristics" rel="prev" title="Generic Compiler Characteristics">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="C-Compiler"></a>
<div class="header">
<p>
Next: <a href="Particular-Functions.html#Particular-Functions" accesskey="n" rel="next">Particular Functions</a>, Previous: <a href="Generic-Compiler-Characteristics.html#Generic-Compiler-Characteristics" accesskey="p" rel="prev">Generic Compiler Characteristics</a>, Up: <a href="C.html#C" accesskey="u" rel="up">C</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="C-Compiler-Characteristics"></a>
<h4 class="subsection">10.2.2 C Compiler Characteristics</h4>

<p>The following macros provide ways to find and exercise a C Compiler.
There are a few constructs that ought to be avoided, but do not deserve
being checked for, since they can easily be worked around.
</p>
<p><strong>TODO: Shunt this portability stuff elsewhere.</strong>
</p>
<dl compact="compact">
<dt>Don&rsquo;t use lines containing solitary backslashes</dt>
<dd><p>They tickle a bug in the HP-UX C compiler (checked on
HP-UX 10.20,
11.00, and 11i).  When given the following source:
</p>
<div class="example">
<pre class="example">#ifdef __STDC__
/\
* A comment with backslash-newlines in it.  %{ %} *\
\
/
char str[] = &quot;\\
&quot; A string with backslash-newlines in it %{ %} \\
&quot;&quot;;
char apostrophe = '\\
\
'\
';
#endif
</pre></div>

<p>the compiler incorrectly fails with the diagnostics &ldquo;Non-terminating
comment at end of file&rdquo; and &ldquo;Missing &lsquo;<samp>#endif</samp>&rsquo; at end of file.&rdquo;
Removing the lines with solitary backslashes solves the problem.
</p>
</dd>
<dt>Don&rsquo;t compile several files at once if output matters to you</dt>
<dd><p>Some compilers, such as HP&rsquo;s, report names of files being
compiled when given more than one file operand.  For instance:
</p>
<div class="example">
<pre class="example">$ <kbd>cc a.c b.c</kbd>
a.c:
b.c:
</pre></div>

<p>This can cause problems if you observe the output of the compiler to
detect failures.  Invoking &lsquo;<samp>cc -c a.c &amp;&amp; cc -c b.c &amp;&amp; cc -o c a.o
b.o</samp>&rsquo; solves the issue.
</p>
</dd>
<dt>Don&rsquo;t rely on <code>#error</code> failing</dt>
<dd><p>The IRIX C compiler does not fail when #error is preprocessed; it
simply emits a diagnostic and continues, exiting successfully.  So,
instead of an error directive like <code>#error &quot;Unsupported word size&quot;</code>
it is more portable to use an invalid directive like <code>#Unsupported
word size</code> in Autoconf tests.  In ordinary source code, <code>#error</code> is
OK, since installers with inadequate compilers like IRIX can simply
examine these compilers&rsquo; diagnostic output.
</p>
</dd>
<dt>Don&rsquo;t rely on correct <code>#line</code> support</dt>
<dd><p>On Solaris, <code>c89</code> (at least Sun C 5.3 through 5.8)
diagnoses <code>#line</code> directives whose line
numbers are greater than 32767.  Nothing in Posix
makes this invalid.  That is why Autoconf stopped issuing
<code>#line</code> directives.
</p></dd>
</dl>

<a name="AC_005fPROG_005fCC"></a><dl>
<dt><a name="index-AC_005fPROG_005fCC-1"></a>Macro: <strong>AC_PROG_CC</strong> <em>(<span class="roman">[</span><var>compiler-search-list</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AC_005fPROG_005fCC"></a>
<a name="index-CC"></a>
<a name="index-CFLAGS-1"></a>
<a name="index-CC-2"></a>
<a name="index-CFLAGS-3"></a>
<a name="index-ac_005fcv_005fprog_005fcc_005fstdc"></a>
<p>Determine a C compiler to use.  If <code>CC</code> is not already set in the
environment, check for <code>gcc</code> and <code>cc</code>, then for other C
compilers.  Set output variable <code>CC</code> to the name of the compiler
found.
</p>
<p>This macro may, however, be invoked with an optional first argument
which, if specified, must be a blank-separated list of C compilers to
search for.  This just gives the user an opportunity to specify an
alternative search list for the C compiler.  For example, if you didn&rsquo;t
like the default order, then you could invoke <code>AC_PROG_CC</code> like
this:
</p>
<div class="example">
<pre class="example">AC_PROG_CC([gcc cl cc])
</pre></div>

<p>If necessary, add an option to output variable <code>CC</code> to enable
support for ISO Standard C features with extensions.  Prefer the newest
C standard that is supported.  Currently the newest standard is ISO C11,
with ISO C99 and ANSI C89 (ISO C90) being the older versions.
After calling this macro you can check whether the C compiler has been
set to accept Standard C; if not, the shell variable
<code>ac_cv_prog_cc_stdc</code> is set to &lsquo;<samp>no</samp>&rsquo;.
</p>
<p>When attempting to add compiler options, prefer extended functionality
to strict conformance: the goal is to enable whatever standard features
that are available, not to check for full conformance to the standard or
to prohibit incompatible extensions.  Test for C11 support by checking
for <code>_Alignas</code>, <code>_Alignof</code>, <code>_Noreturn</code>,
<code>_Static_assert</code>, UTF-8 string literals, duplicate <code>typedef</code>s,
and anonymous structures and unions.  Test for C99 support by checking
for <code>_Bool</code>, <code>//</code> comments, flexible array members,
<code>inline</code>, signed and unsigned <code>long long int</code>, mixed code and
declarations, named initialization of structs, <code>restrict</code>,
<code>va_copy</code>, varargs macros, variable declarations in <code>for</code>
loops, and variable length arrays.  Test for C89 support by checking for
function prototypes.
</p>
<p>If using a compiler that supports GNU C, set shell variable <code>GCC</code> to
&lsquo;<samp>yes</samp>&rsquo;.  If output variable <code>CFLAGS</code> was not already set, set
it to <samp>-g -O2</samp> for a GNU C compiler (<samp>-O2</samp> on systems
where the compiler does not accept <samp>-g</samp>), or <samp>-g</samp> for
other compilers.  If your package does not like this default, then it is
acceptable to insert the line &lsquo;<samp>: ${CFLAGS=&quot;&quot;}</samp>&rsquo; after <code>AC_INIT</code>
and before <code>AC_PROG_CC</code> to select an empty default instead.
</p>
<p>Many Autoconf macros use a compiler, and thus call
&lsquo;<samp>AC_REQUIRE([AC_PROG_CC])</samp>&rsquo; to ensure that the compiler has been
determined before the body of the outermost <code>AC_DEFUN</code> macro.
<code>AC_PROG_CC</code> is therefore defined via <code>AC_DEFUN_ONCE</code> to avoid
needless reexpansion (see <a href="One_002dShot-Macros.html#One_002dShot-Macros">One-Shot Macros</a>).
</p></dd></dl>

<a name="AC_005fPROG_005fCC_005fC_005fO"></a><dl>
<dt><a name="index-AC_005fPROG_005fCC_005fC_005fO-1"></a>Macro: <strong>AC_PROG_CC_C_O</strong></dt>
<dd><a name="index-AC_005fPROG_005fCC_005fC_005fO"></a>
<a name="index-NO_005fMINUS_005fC_005fMINUS_005fO"></a>
<a name="index-ac_005fcv_005fprog_005fcc_005fcompiler_005fc_005fo"></a>
<p>If the C compiler does not accept the <samp>-c</samp> and <samp>-o</samp> options
simultaneously, define <code>NO_MINUS_C_MINUS_O</code>.  This macro actually
tests both the compiler found by <code>AC_PROG_CC</code>, and, if different,
the first <code>cc</code> in the path.  The test fails if one fails.  This
macro was created for GNU Make to choose the default C compilation
rule.
</p>
<p>For the compiler <var>compiler</var>, this macro caches its result in the
<code>ac_cv_prog_cc_<var>compiler</var>_c_o</code> variable.
</p></dd></dl>


<dl>
<dt><a name="index-AC_005fPROG_005fCPP-1"></a>Macro: <strong>AC_PROG_CPP</strong></dt>
<dd><a name="index-AC_005fPROG_005fCPP"></a>
<a name="index-CPP"></a>
<a name="index-CPP-1"></a>
<p>Set output variable <code>CPP</code> to a command that runs the
C preprocessor.  If &lsquo;<samp>$CC -E</samp>&rsquo; doesn&rsquo;t work, <samp>/lib/cpp</samp> is used.
It is only portable to run <code>CPP</code> on files with a <samp>.c</samp>
extension.
</p>
<p>Some preprocessors don&rsquo;t indicate missing include files by the error
status.  For such preprocessors an internal variable is set that causes
other macros to check the standard error from the preprocessor and
consider the test failed if any warnings have been reported.
For most preprocessors, though, warnings do not cause include-file
tests to fail unless <code>AC_PROG_CPP_WERROR</code> is also specified.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fPROG_005fCPP_005fWERROR-1"></a>Macro: <strong>AC_PROG_CPP_WERROR</strong></dt>
<dd><a name="index-AC_005fPROG_005fCPP_005fWERROR"></a>
<a name="index-CPP-2"></a>
<p>This acts like <code>AC_PROG_CPP</code>, except it treats warnings from the
preprocessor as errors even if the preprocessor exit status indicates
success.  This is useful for avoiding headers that generate mandatory
warnings, such as deprecation notices.
</p></dd></dl>


<p>The following macros check for C compiler or machine architecture
features.  To check for characteristics not listed here, use
<code>AC_COMPILE_IFELSE</code> (see <a href="Running-the-Compiler.html#Running-the-Compiler">Running the Compiler</a>) or
<code>AC_RUN_IFELSE</code> (see <a href="Runtime.html#Runtime">Runtime</a>).
</p>
<dl>
<dt><a name="index-AC_005fC_005fBACKSLASH_005fA-1"></a>Macro: <strong>AC_C_BACKSLASH_A</strong></dt>
<dd><a name="index-AC_005fC_005fBACKSLASH_005fA"></a>
<a name="index-HAVE_005fC_005fBACKSLASH_005fA"></a>
<p>Define &lsquo;<samp>HAVE_C_BACKSLASH_A</samp>&rsquo; to 1 if the C compiler understands
&lsquo;<samp>\a</samp>&rsquo;.
</p>
<p>This macro is obsolescent, as current C compilers understand &lsquo;<samp>\a</samp>&rsquo;.
New programs need not use this macro.
</p></dd></dl>

<a name="AC_005fC_005fBIGENDIAN"></a><dl>
<dt><a name="index-AC_005fC_005fBIGENDIAN-1"></a>Macro: <strong>AC_C_BIGENDIAN</strong> <em>(<span class="roman">[</span><var>action-if-true</var><span class="roman">]</span>, <span class="roman">[</span><var>action-if-false</var><span class="roman">]</span>,   <span class="roman">[</span><var>action-if-unknown</var><span class="roman">]</span>, <span class="roman">[</span><var>action-if-universal</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AC_005fC_005fBIGENDIAN"></a>
<a name="index-WORDS_005fBIGENDIAN"></a>
<a name="index-Endianness"></a>
<p>If words are stored with the most significant byte first (like Motorola
and SPARC CPUs), execute <var>action-if-true</var>.  If words are stored with
the least significant byte first (like Intel and VAX CPUs), execute
<var>action-if-false</var>.
</p>
<p>This macro runs a test-case if endianness cannot be determined from the
system header files.  When cross-compiling, the test-case is not run but
grep&rsquo;ed for some magic values.  <var>action-if-unknown</var> is executed if
the latter case fails to determine the byte sex of the host system.
</p>
<p>In some cases a single run of a compiler can generate code for multiple
architectures.  This can happen, for example, when generating Mac OS X
universal binary files, which work on both PowerPC and Intel
architectures.  In this case, the different variants might be for
different architectures whose endiannesses differ.  If
<code>configure</code> detects this, it executes <var>action-if-universal</var>
instead of <var>action-if-unknown</var>.
</p>
<p>The default for <var>action-if-true</var> is to define
&lsquo;<samp>WORDS_BIGENDIAN</samp>&rsquo;.  The default for <var>action-if-false</var> is to do
nothing.  The default for <var>action-if-unknown</var> is to
abort configure and tell the installer how to bypass this test.
And finally, the default for <var>action-if-universal</var> is to ensure that
&lsquo;<samp>WORDS_BIGENDIAN</samp>&rsquo; is defined if and only if a universal build is
detected and the current code is big-endian; this default works only if
<code>autoheader</code> is used (see <a href="autoheader-Invocation.html#autoheader-Invocation">autoheader Invocation</a>).
</p>
<p>If you use this macro without specifying <var>action-if-universal</var>, you
should also use <code>AC_CONFIG_HEADERS</code>; otherwise
&lsquo;<samp>WORDS_BIGENDIAN</samp>&rsquo; may be set incorrectly for Mac OS X universal
binary files.
</p></dd></dl>

<a name="AC_005fC_005fCONST"></a><dl>
<dt><a name="index-AC_005fC_005fCONST-1"></a>Macro: <strong>AC_C_CONST</strong></dt>
<dd><a name="index-AC_005fC_005fCONST"></a>
<a name="index-const"></a>
<a name="index-ac_005fcv_005fc_005fconst"></a>
<p>If the C compiler does not fully support the <code>const</code> keyword,
define <code>const</code> to be empty.  Some C compilers that do
not define <code>__STDC__</code> do support <code>const</code>; some compilers that
define <code>__STDC__</code> do not completely support <code>const</code>.  Programs
can simply use <code>const</code> as if every C compiler supported it; for
those that don&rsquo;t, the makefile or configuration header file
defines it as empty.
</p>
<p>Occasionally installers use a C++ compiler to compile C code, typically
because they lack a C compiler.  This causes problems with <code>const</code>,
because C and C++ treat <code>const</code> differently.  For example:
</p>
<div class="example">
<pre class="example">const int foo;
</pre></div>

<p>is valid in C but not in C++.  These differences unfortunately cannot be
papered over by defining <code>const</code> to be empty.
</p>
<p>If <code>autoconf</code> detects this situation, it leaves <code>const</code> alone,
as this generally yields better results in practice.  However, using a
C++ compiler to compile C code is not recommended or supported, and
installers who run into trouble in this area should get a C compiler
like GCC to compile their C code.
</p>
<p>This macro caches its result in the <code>ac_cv_c_const</code> variable.
</p>
<p>This macro is obsolescent, as current C compilers support <code>const</code>.
New programs need not use this macro.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fC_005f_005fGENERIC-1"></a>Macro: <strong>AC_C__GENERIC</strong></dt>
<dd><a name="index-AC_005fC_005f_005fGENERIC"></a>
<a name="index-_005fGeneric"></a>
<p>If the C compiler supports C11-style generic selection using the
<code>_Generic</code> keyword, define <code>HAVE_C__GENERIC</code>.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fC_005fRESTRICT-1"></a>Macro: <strong>AC_C_RESTRICT</strong></dt>
<dd><a name="index-AC_005fC_005fRESTRICT"></a>
<a name="index-restrict"></a>
<a name="index-ac_005fcv_005fc_005frestrict"></a>
<p>If the C compiler recognizes a variant spelling for the <code>restrict</code>
keyword (<code>__restrict</code>, <code>__restrict__</code>, or <code>_Restrict</code>),
then define <code>restrict</code> to that; this is more likely to do the right
thing with compilers that support language variants where plain
<code>restrict</code> is not a keyword.  Otherwise, if the C compiler
recognizes the <code>restrict</code> keyword, don&rsquo;t do anything.
Otherwise, define <code>restrict</code> to be empty.
Thus, programs may simply use <code>restrict</code> as if every C compiler
supported it; for those that do not, the makefile
or configuration header defines it away.
</p>
<p>Although support in C++ for the <code>restrict</code> keyword is not
required, several C++ compilers do accept the keyword.
This macro works for them, too.
</p>
<p>This macro caches &lsquo;<samp>no</samp>&rsquo; in the <code>ac_cv_c_restrict</code> variable
if <code>restrict</code> is not supported, and a supported spelling otherwise.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fC_005fVOLATILE-1"></a>Macro: <strong>AC_C_VOLATILE</strong></dt>
<dd><a name="index-AC_005fC_005fVOLATILE"></a>
<a name="index-volatile"></a>
<p>If the C compiler does not understand the keyword <code>volatile</code>,
define <code>volatile</code> to be empty.  Programs can simply use
<code>volatile</code> as if every C compiler supported it; for those that do
not, the makefile or configuration header defines it as
empty.
</p>
<p>If the correctness of your program depends on the semantics of
<code>volatile</code>, simply defining it to be empty does, in a sense, break
your code.  However, given that the compiler does not support
<code>volatile</code>, you are at its mercy anyway.  At least your
program compiles, when it wouldn&rsquo;t before.
See <a href="Volatile-Objects.html#Volatile-Objects">Volatile Objects</a>, for more about <code>volatile</code>.
</p>
<p>In general, the <code>volatile</code> keyword is a standard C feature, so
you might expect that <code>volatile</code> is available only when
<code>__STDC__</code> is defined.  However, Ultrix 4.3&rsquo;s native compiler does
support volatile, but does not define <code>__STDC__</code>.
</p>
<p>This macro is obsolescent, as current C compilers support <code>volatile</code>.
New programs need not use this macro.
</p></dd></dl>

<a name="AC_005fC_005fINLINE"></a><dl>
<dt><a name="index-AC_005fC_005fINLINE-1"></a>Macro: <strong>AC_C_INLINE</strong></dt>
<dd><a name="index-AC_005fC_005fINLINE"></a>
<a name="index-inline"></a>
<p>If the C compiler supports the keyword <code>inline</code>, do nothing.
Otherwise define <code>inline</code> to <code>__inline__</code> or <code>__inline</code>
if it accepts one of those, otherwise define <code>inline</code> to be empty.
</p></dd></dl>

<a name="AC_005fC_005fCHAR_005fUNSIGNED"></a><dl>
<dt><a name="index-AC_005fC_005fCHAR_005fUNSIGNED-1"></a>Macro: <strong>AC_C_CHAR_UNSIGNED</strong></dt>
<dd><a name="index-AC_005fC_005fCHAR_005fUNSIGNED"></a>
<a name="index-_005f_005fCHAR_005fUNSIGNED_005f_005f"></a>
<p>If the C type <code>char</code> is unsigned, define <code>__CHAR_UNSIGNED__</code>,
unless the C compiler predefines it.
</p>
<p>These days, using this macro is not necessary.  The same information can
be determined by this portable alternative, thus avoiding the use of
preprocessor macros in the namespace reserved for the implementation.
</p>
<div class="example">
<pre class="example">#include &lt;limits.h&gt;
#if CHAR_MIN == 0
# define CHAR_UNSIGNED 1
#endif
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-AC_005fC_005fSTRINGIZE-1"></a>Macro: <strong>AC_C_STRINGIZE</strong></dt>
<dd><a name="index-AC_005fC_005fSTRINGIZE"></a>
<a name="index-HAVE_005fSTRINGIZE"></a>
<p>If the C preprocessor supports the stringizing operator, define
<code>HAVE_STRINGIZE</code>.  The stringizing operator is &lsquo;<samp>#</samp>&rsquo; and is
found in macros such as this:
</p>
<div class="example">
<pre class="example">#define x(y) #y
</pre></div>

<p>This macro is obsolescent, as current C compilers support the
stringizing operator.  New programs need not use this macro.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fC_005fFLEXIBLE_005fARRAY_005fMEMBER-1"></a>Macro: <strong>AC_C_FLEXIBLE_ARRAY_MEMBER</strong></dt>
<dd><a name="index-AC_005fC_005fFLEXIBLE_005fARRAY_005fMEMBER"></a>
<a name="index-FLEXIBLE_005fARRAY_005fMEMBER"></a>
<p>If the C compiler supports flexible array members, define
<code>FLEXIBLE_ARRAY_MEMBER</code> to nothing; otherwise define it to 1.
That way, a declaration like this:
</p>
<div class="example">
<pre class="example">struct s
  {
    size_t n_vals;
    double val[FLEXIBLE_ARRAY_MEMBER];
  };
</pre></div>

<p>will let applications use the &ldquo;struct hack&rdquo; even with compilers that
do not support flexible array members.  To allocate and use such an
object, you can use code like this:
</p>
<div class="example">
<pre class="example">size_t i;
size_t n = compute_value_count ();
struct s *p =
   malloc (offsetof (struct s, val)
           + n * sizeof (double));
p-&gt;n_vals = n;
for (i = 0; i &lt; n; i++)
  p-&gt;val[i] = compute_value (i);
</pre></div>
</dd></dl>

<dl>
<dt><a name="index-AC_005fC_005fVARARRAYS-1"></a>Macro: <strong>AC_C_VARARRAYS</strong></dt>
<dd><a name="index-AC_005fC_005fVARARRAYS"></a>
<a name="index-_005f_005fSTDC_005fNO_005fVLA_005f_005f"></a>
<a name="index-HAVE_005fC_005fVARARRAYS"></a>
<p>If the C compiler does not support variable-length arrays, define the
macro <code>__STDC_NO_VLA__</code> to be 1 if it is not already defined.  A
variable-length array is an array of automatic storage duration whose
length is determined at run time, when the array is declared.  For
backward compatibility this macro also defines <code>HAVE_C_VARARRAYS</code>
if the C compiler supports variable-length arrays, but this usage is
obsolescent and new programs should use <code>__STDC_NO_VLA__</code>.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fC_005fTYPEOF-1"></a>Macro: <strong>AC_C_TYPEOF</strong></dt>
<dd><a name="index-AC_005fC_005fTYPEOF"></a>
<a name="index-HAVE_005fTYPEOF"></a>
<a name="index-typeof"></a>
<p>If the C compiler supports GNU C&rsquo;s <code>typeof</code> syntax either
directly or
through a different spelling of the keyword (e.g., <code>__typeof__</code>),
define <code>HAVE_TYPEOF</code>.  If the support is available only through a
different spelling, define <code>typeof</code> to that spelling.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fC_005fPROTOTYPES-1"></a>Macro: <strong>AC_C_PROTOTYPES</strong></dt>
<dd><a name="index-AC_005fC_005fPROTOTYPES"></a>
<a name="index-PROTOTYPES"></a>
<a name="index-_005f_005fPROTOTYPES"></a>
<a name="index-PARAMS"></a>
<p>If function prototypes are understood by the compiler (as determined by
<code>AC_PROG_CC</code>), define <code>PROTOTYPES</code> and <code>__PROTOTYPES</code>.
Defining <code>__PROTOTYPES</code> is for the benefit of
header files that cannot use macros that infringe on user name space.
</p>
<p>This macro is obsolescent, as current C compilers support prototypes.
New programs need not use this macro.
</p></dd></dl>

<a name="AC_005fPROG_005fGCC_005fTRADITIONAL"></a><dl>
<dt><a name="index-AC_005fPROG_005fGCC_005fTRADITIONAL-1"></a>Macro: <strong>AC_PROG_GCC_TRADITIONAL</strong></dt>
<dd><a name="index-AC_005fPROG_005fGCC_005fTRADITIONAL"></a>
<a name="index-CC-3"></a>
<p>Add <samp>-traditional</samp> to output variable <code>CC</code> if using a
GNU C compiler and <code>ioctl</code> does not work properly without
<samp>-traditional</samp>.  That usually happens when the fixed header files
have not been installed on an old system.
</p>
<p>This macro is obsolescent, since current versions of the GNU C
compiler fix the header files automatically when installed.
</p></dd></dl>


<div class="header">
<p>
Next: <a href="Particular-Functions.html#Particular-Functions" accesskey="n" rel="next">Particular Functions</a>, Previous: <a href="Generic-Compiler-Characteristics.html#Generic-Compiler-Characteristics" accesskey="p" rel="prev">Generic Compiler Characteristics</a>, Up: <a href="C.html#C" accesskey="u" rel="up">C</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
