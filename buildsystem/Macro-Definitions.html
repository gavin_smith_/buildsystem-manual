<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Macro Definitions</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Macro Definitions">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Macro Definitions">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Writing-Autoconf-Macros.html#Writing-Autoconf-Macros" rel="up" title="Writing Autoconf Macros">
<link href="Macro-Names.html#Macro-Names" rel="next" title="Macro Names">
<link href="Writing-Autoconf-Macros.html#Writing-Autoconf-Macros" rel="prev" title="Writing Autoconf Macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Macro-Definitions"></a>
<div class="header">
<p>
Next: <a href="Macro-Names.html#Macro-Names" accesskey="n" rel="next">Macro Names</a>, Up: <a href="Writing-Autoconf-Macros.html#Writing-Autoconf-Macros" accesskey="u" rel="up">Writing Autoconf Macros</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Macro-Definitions-1"></a>
<h4 class="subsection">8.5.1 Macro Definitions</h4>

<dl>
<dt><a name="index-AC_005fDEFUN-2"></a>Macro: <strong>AC_DEFUN</strong> <em>(<var>name</var>, <span class="roman">[</span><var>body</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AC_005fDEFUN"></a>
<p>Autoconf macros are defined using the <code>AC_DEFUN</code> macro, which is
similar to the M4 builtin <code>m4_define</code> macro; this creates a macro
named <var>name</var> and with <var>body</var> as its expansion.  In addition to
defining a macro, <code>AC_DEFUN</code> adds to it some code that is used to
constrain the order in which macros are called, while avoiding redundant
output (see <a href="Prerequisite-Macros.html#Prerequisite-Macros">Prerequisite Macros</a>).
</p></dd></dl>

<p>An Autoconf macro definition looks like this:
</p>
<div class="example">
<pre class="example">AC_DEFUN(<var>macro-name</var>, <var>macro-body</var>)
</pre></div>

<p>You can refer to any arguments passed to the macro as &lsquo;<samp>$1</samp>&rsquo;,
&lsquo;<samp>$2</samp>&rsquo;, etc.  See <a href="http://www.gnu.org/software/m4/manual/html_node/Definitions.html#Definitions">How to define new macros</a> in <cite>GNU M4</cite>, for more complete information on writing M4 macros.
</p>
<p>Most macros fall in one of two general categories.  The first category
includes macros which take arguments, in order to generate output
parameterized by those arguments.  Macros in this category are designed
to be directly expanded, often multiple times, and should not be used as
the argument to <code>AC_REQUIRE</code>.  The other category includes macros
which are shorthand for a fixed block of text, and therefore do not take
arguments.  For this category of macros, directly expanding the macro
multiple times results in redundant output, so it is more common to use
the macro as the argument to <code>AC_REQUIRE</code>, or to declare the macro
with <code>AC_DEFUN_ONCE</code> (see <a href="One_002dShot-Macros.html#One_002dShot-Macros">One-Shot Macros</a>).
</p>
<p>Be sure to properly quote both the <var>macro-body</var> <em>and</em> the
<var>macro-name</var> to avoid any problems if the macro happens to have
been previously defined.
</p>
<p>Each macro should have a header comment that gives its prototype, and a
brief description.  When arguments have default values, display them in
the prototype.  For example:
</p>
<div class="example">
<pre class="example"># AC_MSG_ERROR(ERROR, [EXIT-STATUS = 1])
# --------------------------------------
m4_define([AC_MSG_ERROR],
  [{ AS_MESSAGE([error: $1], [2])
     exit m4_default([$2], [1]); }])
</pre></div>

<p>Comments about the macro should be left in the header comment.  Most
other comments make their way into <samp>configure</samp>, so just keep
using &lsquo;<samp>#</samp>&rsquo; to introduce comments.
</p>
<a name="index-dnl-1"></a>
<p>If you have some special comments about pure M4 code, comments
that make no sense in <samp>configure</samp> and in the header comment, then
use the builtin <code>dnl</code>: it causes M4 to discard the text
through the next newline.
</p>
<p>Keep in mind that <code>dnl</code> is rarely needed to introduce comments;
<code>dnl</code> is more useful to get rid of the newlines following macros
that produce no output, such as <code>AC_REQUIRE</code>.
</p>
<p>Public third-party macros need to use <code>AC_DEFUN</code>, and not
<code>m4_define</code>, in order to be found by <code>aclocal</code>
(see <a href="http://www.gnu.org/software/automake/manual/html_node/Extending-aclocal.html#Extending-aclocal">Extending aclocal</a> in <cite>GNU Automake</cite>).
Additionally, if it is ever determined that a macro should be made
obsolete, it is easy to convert from <code>AC_DEFUN</code> to <code>AU_DEFUN</code>
in order to have <code>autoupdate</code> assist the user in choosing a
better alternative, but there is no corresponding way to make
<code>m4_define</code> issue an upgrade notice (see <a href="Obsoleting-Macros.html#AU_005fDEFUN">AU_DEFUN</a>).
</p>
<p>There is another subtle, but important, difference between using
<code>m4_define</code> and <code>AC_DEFUN</code>: only the former is unaffected by
<code>AC_REQUIRE</code>.  When writing a file, it is always safe to replace a
block of text with a <code>m4_define</code> macro that will expand to the same
text.  But replacing a block of text with an <code>AC_DEFUN</code> macro with
the same content does not necessarily give the same results, because it
changes the location where any embedded but unsatisfied
<code>AC_REQUIRE</code> invocations within the block will be expanded.  For an
example of this, see <a href="Expanded-Before-Required.html#Expanded-Before-Required">Expanded Before Required</a>.
</p>
<div class="header">
<p>
Next: <a href="Macro-Names.html#Macro-Names" accesskey="n" rel="next">Macro Names</a>, Up: <a href="Writing-Autoconf-Macros.html#Writing-Autoconf-Macros" accesskey="u" rel="up">Writing Autoconf Macros</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
