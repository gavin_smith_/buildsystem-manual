<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Guidelines</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Guidelines">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Guidelines">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Writing-Test-Programs.html#Writing-Test-Programs" rel="up" title="Writing Test Programs">
<link href="Test-Functions.html#Test-Functions" rel="next" title="Test Functions">
<link href="Writing-Test-Programs.html#Writing-Test-Programs" rel="prev" title="Writing Test Programs">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Guidelines"></a>
<div class="header">
<p>
Next: <a href="Test-Functions.html#Test-Functions" accesskey="n" rel="next">Test Functions</a>, Up: <a href="Writing-Test-Programs.html#Writing-Test-Programs" accesskey="u" rel="up">Writing Test Programs</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Guidelines-for-Test-Programs"></a>
<h4 class="subsubsection">8.4.2.1 Guidelines for Test Programs</h4>

<p>The most important rule to follow when writing testing samples is:
</p>
<div align="center"><em>Look for realism.</em>
</div>
<p>This motto means that testing samples must be written with the same
strictness as real programs are written.  In particular, you should
avoid &ldquo;shortcuts&rdquo; and simplifications.
</p>
<p>Don&rsquo;t just play with the preprocessor if you want to prepare a
compilation.  For instance, using <code>cpp</code> to check whether a header is
functional might let your <code>configure</code> accept a header which
causes some <em>compiler</em> error.  Do not hesitate to check a header with
other headers included before, especially required headers.
</p>
<p>Make sure the symbols you use are properly defined, i.e., refrain from
simply declaring a function yourself instead of including the proper
header.
</p>
<p>Test programs should not write to standard output.  They
should exit with status 0 if the test succeeds, and with status 1
otherwise, so that success
can be distinguished easily from a core dump or other failure;
segmentation violations and other failures produce a nonzero exit
status.  Unless you arrange for <code>exit</code> to be declared, test
programs should <code>return</code>, not <code>exit</code>, from <code>main</code>,
because on many systems <code>exit</code> is not declared by default.
</p>
<p>Test programs can use <code>#if</code> or <code>#ifdef</code> to check the values of
preprocessor macros defined by tests that have already run.  For
example, if you call <code>AC_HEADER_STDBOOL</code>, then later on in
<samp>configure.ac</samp> you can have a test program that includes
<samp>stdbool.h</samp> conditionally:
</p>
<div class="example">
<pre class="example">#ifdef HAVE_STDBOOL_H
# include &lt;stdbool.h&gt;
#endif
</pre></div>

<p>Both <code>#if HAVE_STDBOOL_H</code> and <code>#ifdef HAVE_STDBOOL_H</code> will
work with any standard C compiler.  Some developers prefer <code>#if</code>
because it is easier to read, while others prefer <code>#ifdef</code> because
it avoids diagnostics with picky compilers like GCC with the
<samp>-Wundef</samp> option.
</p>
<p>If a test program needs to use or create a data file, give it a name
that starts with <samp>conftest</samp>, such as <samp>conftest.data</samp>.  The
<code>configure</code> script cleans up by running &lsquo;<samp>rm -f -r conftest*</samp>&rsquo;
after running test programs and if the script is interrupted.
</p>
<div class="header">
<p>
Next: <a href="Test-Functions.html#Test-Functions" accesskey="n" rel="next">Test Functions</a>, Up: <a href="Writing-Test-Programs.html#Writing-Test-Programs" accesskey="u" rel="up">Writing Test Programs</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
