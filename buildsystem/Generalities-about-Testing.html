<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Generalities about Testing</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Generalities about Testing">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Generalities about Testing">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Tests.html#Tests" rel="up" title="Tests">
<link href="Simple-Tests.html#Simple-Tests" rel="next" title="Simple Tests">
<link href="Tests.html#Tests" rel="prev" title="Tests">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Generalities-about-Testing"></a>
<div class="header">
<p>
Next: <a href="Simple-Tests.html#Simple-Tests" accesskey="n" rel="next">Simple Tests</a>, Up: <a href="Tests.html#Tests" accesskey="u" rel="up">Tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Generalities-about-Testing-1"></a>
<h4 class="subsection">12.1.1 Generalities about Testing</h4>

<p>The purpose of testing is to determine whether a program or system behaves
as expected (e.g., known inputs produce the expected outputs, error
conditions are correctly handled or reported, and older bugs do not
resurface).
</p>
<a name="index-test-case"></a>
<p>The minimal unit of testing is usually called <em>test case</em>, or simply
<em>test</em>.  How a test case is defined or delimited, and even what
exactly <em>constitutes</em> a test case, depends heavily on the testing
paradigm and/or framework in use, so we won&rsquo;t attempt any more precise
definition.  The set of the test cases for a given program or system
constitutes its <em>testsuite</em>.
</p>
<a name="index-test-harness"></a>
<a name="index-testsuite-harness"></a>
<p>A <em>test harness</em> (also <em>testsuite harness</em>) is a program or
software component that executes all (or part of) the defined test cases,
analyzes their outcomes, and report or register these outcomes
appropriately.  Again, the details of how this is accomplished (and how
the developer and user can influence it or interface with it) varies
wildly, and we&rsquo;ll attempt no precise definition.
</p>
<a name="index-test-pass"></a>
<a name="index-test-failure"></a>
<p>A test is said to <em>pass</em> when it can determine that the condition or
behaviour it means to verify holds, and is said to <em>fail</em> when it can
determine that such condition of behaviour does <em>not</em> hold.
</p>
<a name="index-test-skip"></a>
<p>Sometimes, tests can rely on non-portable tools or prerequisites, or
simply make no sense on a given system (for example, a test checking a
Windows-specific feature makes no sense on a GNU/Linux system).  In this
case, accordingly to the definition above, the tests can neither be
considered passed nor failed; instead, they are <em>skipped</em> &ndash; i.e.,
they are not run, or their result is anyway ignored for what concerns
the count of failures an successes.  Skips are usually explicitly
reported though, so that the user will be aware that not all of the
testsuite has really run.
</p>
<a name="index-xfail"></a>
<a name="index-expected-failure"></a>
<a name="index-expected-test-failure"></a>
<a name="index-xpass"></a>
<a name="index-unexpected-pass"></a>
<a name="index-unexpected-test-pass"></a>
<p>It&rsquo;s not uncommon, especially during early development stages, that some
tests fail for known reasons, and that the developer doesn&rsquo;t want to
tackle these failures immediately (this is especially true when the
failing tests deal with corner cases).  In this situation, the better
policy is to declare that each of those failures is an <em>expected
failure</em> (or <em>xfail</em>).  In case a test that is expected to fail ends
up passing instead, many testing environments will flag the result as a
special kind of failure called <em>unexpected pass</em> (or <em>xpass</em>).
</p>
<a name="index-hard-error"></a>
<a name="index-Distinction-between-errors-and-failures-in-testsuites"></a>
<p>Many testing environments and frameworks distinguish between test failures
and hard errors.  As we&rsquo;ve seen, a test failure happens when some invariant
or expected behaviour of the software under test is not met.  An <em>hard
error</em> happens when e.g., the set-up of a test case scenario fails, or when
some other unexpected or highly undesirable condition is encountered (for
example, the program under test experiences a segmentation fault).
</p>
<div class="header">
<p>
Next: <a href="Simple-Tests.html#Simple-Tests" accesskey="n" rel="next">Simple Tests</a>, Up: <a href="Tests.html#Tests" accesskey="u" rel="up">Tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
