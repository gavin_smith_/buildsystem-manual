<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: File Descriptors</title>

<meta name="description" content="Automake and Autoconf Reference Manual: File Descriptors">
<meta name="keywords" content="Automake and Autoconf Reference Manual: File Descriptors">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Portable-Shell.html#Portable-Shell" rel="up" title="Portable Shell">
<link href="Signal-Handling.html#Signal-Handling" rel="next" title="Signal Handling">
<link href="Here_002dDocuments.html#Here_002dDocuments" rel="prev" title="Here-Documents">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="File-Descriptors"></a>
<div class="header">
<p>
Next: <a href="Signal-Handling.html#Signal-Handling" accesskey="n" rel="next">Signal Handling</a>, Previous: <a href="Here_002dDocuments.html#Here_002dDocuments" accesskey="p" rel="prev">Here-Documents</a>, Up: <a href="Portable-Shell.html#Portable-Shell" accesskey="u" rel="up">Portable Shell</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="File-Descriptors-1"></a>
<h4 class="subsection">C.1.4 File Descriptors</h4>
<a name="index-Descriptors"></a>
<a name="index-File-descriptors"></a>
<a name="index-Shell-file-descriptors"></a>

<p>Most shells, if not all (including Bash, Zsh, Ash), output traces on
stderr, even for subshells.  This might result in undesirable content
if you meant to capture the standard-error output of the inner command:
</p>
<div class="example">
<pre class="example">$ <kbd>ash -x -c '(eval &quot;echo foo &gt;&amp;2&quot;) 2&gt;stderr'</kbd>
$ <kbd>cat stderr</kbd>
+ eval echo foo &gt;&amp;2
+ echo foo
foo
$ <kbd>bash -x -c '(eval &quot;echo foo &gt;&amp;2&quot;) 2&gt;stderr'</kbd>
$ <kbd>cat stderr</kbd>
+ eval 'echo foo &gt;&amp;2'
++ echo foo
foo
$ <kbd>zsh -x -c '(eval &quot;echo foo &gt;&amp;2&quot;) 2&gt;stderr'</kbd>
<i># Traces on startup files deleted here.</i>
$ <kbd>cat stderr</kbd>
+zsh:1&gt; eval echo foo &gt;&amp;2
+zsh:1&gt; echo foo
foo
</pre></div>

<p>One workaround is to grep out uninteresting lines, hoping not to remove
good ones.
</p>
<p>If you intend to redirect both standard error and standard output,
redirect standard output first.  This works better with HP-UX,
since its shell mishandles tracing if standard error is redirected
first:
</p>
<div class="example">
<pre class="example">$ <kbd>sh -x -c ': 2&gt;err &gt;out'</kbd>
+ :
+ 2&gt; err $ <kbd>cat err</kbd>
1&gt; out
</pre></div>

<p>Don&rsquo;t try to redirect the standard error of a command substitution.  It
must be done <em>inside</em> the command substitution.  When running
&lsquo;<samp>: `cd /zorglub` 2&gt;/dev/null</samp>&rsquo; expect the error message to
escape, while &lsquo;<samp>: `cd /zorglub 2&gt;/dev/null`</samp>&rsquo; works properly.
</p>
<p>On the other hand, some shells, such as Solaris or FreeBSD
<code>/bin/sh</code>, warn about missing programs before performing
redirections.  Therefore, to silently check whether a program exists, it
is necessary to perform redirections on a subshell or brace group:
</p><div class="example">
<pre class="example">$ <kbd>/bin/sh -c 'nosuch 2&gt;/dev/null'</kbd>
nosuch: not found
$ <kbd>/bin/sh -c '(nosuch) 2&gt;/dev/null'</kbd>
$ <kbd>/bin/sh -c '{ nosuch; } 2&gt;/dev/null'</kbd>
$ <kbd>bash -c 'nosuch 2&gt;/dev/null'</kbd>
</pre></div>

<p>FreeBSD 6.2 sh may mix the trace output lines from the statements in a
shell pipeline.
</p>
<p>It is worth noting that Zsh (but not Ash nor Bash) makes it possible
in assignments though: &lsquo;<samp>foo=`cd /zorglub` 2&gt;/dev/null</samp>&rsquo;.
</p>
<p>Some shells, like <code>ash</code>, don&rsquo;t recognize bi-directional
redirection (&lsquo;<samp>&lt;&gt;</samp>&rsquo;).  And even on shells that recognize it, it is
not portable to use on fifos: Posix does not require read-write support
for named pipes, and Cygwin does not support it:
</p>
<div class="example">
<pre class="example">$ <kbd>mkfifo fifo</kbd>
$ <kbd>exec 5&lt;&gt;fifo</kbd>
$ <kbd>echo hi &gt;&amp;5</kbd>
bash: echo: write error: Communication error on send
</pre></div>

<p>Furthermore, versions of <code>dash</code> before 0.5.6 mistakenly truncate
regular files when using &lsquo;<samp>&lt;&gt;</samp>&rsquo;:
</p>
<div class="example">
<pre class="example">$ <kbd>echo a &gt; file</kbd>
$ <kbd>bash -c ': 1&lt;&gt;file'; cat file</kbd>
a
$ <kbd>dash -c ': 1&lt;&gt;file'; cat file</kbd>
$ rm a
</pre></div>

<p>When catering to old systems, don&rsquo;t redirect the same file descriptor
several times, as you are doomed to failure under Ultrix.
</p>
<div class="example">
<pre class="example">ULTRIX V4.4 (Rev. 69) System #31: Thu Aug 10 19:42:23 GMT 1995
UWS V4.4 (Rev. 11)
$ <kbd>eval 'echo matter &gt;fullness' &gt;void</kbd>
illegal io
$ <kbd>eval '(echo matter &gt;fullness)' &gt;void</kbd>
illegal io
$ <kbd>(eval '(echo matter &gt;fullness)') &gt;void</kbd>
Ambiguous output redirect.
</pre></div>

<p>In each case the expected result is of course <samp>fullness</samp> containing
&lsquo;<samp>matter</samp>&rsquo; and <samp>void</samp> being empty.  However, this bug is
probably not of practical concern to modern platforms.
</p>
<p>Solaris 10 <code>sh</code> will try to optimize away a <code>:</code> command
(even if it is redirected) in a loop after the first iteration, or in a
shell function after the first call:
</p>
<div class="example">
<pre class="example">$ <kbd>for i in 1 2 3 ; do : &gt;x$i; done</kbd>
$ <kbd>ls x*</kbd>
x1
$ <kbd>f () { : &gt;$1; }; f y1; f y2; f y3;</kbd>
$ <kbd>ls y*</kbd>
y1
</pre></div>

<p>As a workaround, <code>echo</code> or <code>eval</code> can be used.
</p>
<p>Don&rsquo;t rely on file descriptors 0, 1, and 2 remaining closed in a
subsidiary program.  If any of these descriptors is closed, the
operating system may open an unspecified file for the descriptor in the
new process image.  Posix 2008 says this may be done only if the
subsidiary program is set-user-ID or set-group-ID, but HP-UX 11.23 does
it even for ordinary programs, and the next version of Posix will allow
HP-UX behavior.
</p>
<p>If you want a file descriptor above 2 to be inherited into a child
process, then you must use redirections specific to that command or a
containing subshell or command group, rather than relying on
<code>exec</code> in the shell. In <code>ksh</code> as well as HP-UX
<code>sh</code>, file descriptors above 2 which are opened using
&lsquo;<samp>exec <var>n</var>&gt;file</samp>&rsquo; are closed by a subsequent &lsquo;<samp>exec</samp>&rsquo; (such as
that involved in the fork-and-exec which runs a program or script):
</p>
<div class="example">
<pre class="example">$ <kbd>echo 'echo hello &gt;&amp;5' &gt;k</kbd>
$ <kbd>/bin/sh -c 'exec 5&gt;t; ksh ./k; exec 5&gt;&amp;-; cat t</kbd>
hello
$ <kbd>bash -c 'exec 5&gt;t; ksh ./k; exec 5&gt;&amp;-; cat t</kbd>
hello
$ <kbd>ksh -c 'exec 5&gt;t; ksh ./k; exec 5&gt;&amp;-; cat t</kbd>
./k[1]: 5: cannot open [Bad file number]
$ <kbd>ksh -c '(ksh ./k) 5&gt;t; cat t'</kbd>
hello
$ <kbd>ksh -c '{ ksh ./k; } 5&gt;t; cat t'</kbd>
hello
$ <kbd>ksh -c '5&gt;t ksh ./k; cat t</kbd>
hello
</pre></div>

<p>Don&rsquo;t rely on duplicating a closed file descriptor to cause an
error.  With Solaris <code>/bin/sh</code>, failed duplication is silently
ignored, which can cause unintended leaks to the original file
descriptor.  In this example, observe the leak to standard output:
</p>
<div class="example">
<pre class="example">$ <kbd>bash -c 'echo hi &gt;&amp;3' 3&gt;&amp;-; echo $?</kbd>
bash: 3: Bad file descriptor
1
$ <kbd>/bin/sh -c 'echo hi &gt;&amp;3' 3&gt;&amp;-; echo $?</kbd>
hi
0
</pre></div>

<p>Fortunately, an attempt to close an already closed file descriptor will
portably succeed.  Likewise, it is safe to use either style of
&lsquo;<samp><var>n</var>&lt;&amp;-</samp>&rsquo; or &lsquo;<samp><var>n</var>&gt;&amp;-</samp>&rsquo; for closing a file descriptor,
even if it doesn&rsquo;t match the read/write mode that the file descriptor
was opened with.
</p>
<p>DOS variants cannot rename or remove open files, such as in
&lsquo;<samp>mv foo bar &gt;foo</samp>&rsquo; or &lsquo;<samp>rm foo &gt;foo</samp>&rsquo;, even though this is
perfectly portable among Posix hosts.
</p>
<p>A few ancient systems reserved some file descriptors.  By convention,
file descriptor 3 was opened to <samp>/dev/tty</samp> when you logged into
Eighth Edition (1985) through Tenth Edition Unix (1989).  File
descriptor 4 had a special use on the Stardent/Kubota Titan (circa
1990), though we don&rsquo;t now remember what it was.  Both these systems are
obsolete, so it&rsquo;s now safe to treat file descriptors 3 and 4 like any
other file descriptors.
</p>
<p>On the other hand, you can&rsquo;t portably use multi-digit file descriptors.
Solaris <code>ksh</code> doesn&rsquo;t understand any file descriptor larger than
&lsquo;<samp>9</samp>&rsquo;:
</p>
<div class="example">
<pre class="example">$ <kbd>bash -c 'exec 10&gt;&amp;-'; echo $?</kbd>
0
$ <kbd>ksh -c 'exec 9&gt;&amp;-'; echo $?</kbd>
0
$ <kbd>ksh -c 'exec 10&gt;&amp;-'; echo $?</kbd>
ksh[1]: exec: 10: not found
127
</pre></div>

<div class="header">
<p>
Next: <a href="Signal-Handling.html#Signal-Handling" accesskey="n" rel="next">Signal Handling</a>, Previous: <a href="Here_002dDocuments.html#Here_002dDocuments" accesskey="p" rel="prev">Here-Documents</a>, Up: <a href="Portable-Shell.html#Portable-Shell" accesskey="u" rel="up">Portable Shell</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
