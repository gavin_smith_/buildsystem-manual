<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Hosts and Cross-Compilation</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Hosts and Cross-Compilation">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Hosts and Cross-Compilation">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Autoconf-2_002e13.html#Autoconf-2_002e13" rel="up" title="Autoconf 2.13">
<link href="AC_005fLIBOBJ-vs-LIBOBJS.html#AC_005fLIBOBJ-vs-LIBOBJS" rel="next" title="AC_LIBOBJ vs LIBOBJS">
<link href="New-Macros.html#New-Macros" rel="prev" title="New Macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Hosts-and-Cross_002dCompilation"></a>
<div class="header">
<p>
Next: <a href="AC_005fLIBOBJ-vs-LIBOBJS.html#AC_005fLIBOBJ-vs-LIBOBJS" accesskey="n" rel="next">AC_LIBOBJ vs LIBOBJS</a>, Previous: <a href="New-Macros.html#New-Macros" accesskey="p" rel="prev">New Macros</a>, Up: <a href="Autoconf-2_002e13.html#Autoconf-2_002e13" accesskey="u" rel="up">Autoconf 2.13</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Hosts-and-Cross_002dCompilation-1"></a>
<h4 class="subsection">E.6.3 Hosts and Cross-Compilation</h4>
<a name="index-Cross-compilation"></a>

<p>Based on the experience of compiler writers, and after long public
debates, many aspects of the cross-compilation chain have changed:
</p>
<ul class="no-bullet">
<li>- the relationship between the build, host, and target architecture types,

</li><li>- the command line interface for specifying them to <code>configure</code>,

</li><li>- the variables defined in <code>configure</code>,

</li><li>- the enabling of cross-compilation mode.
</li></ul>

<br>

<p>The relationship between build, host, and target have been cleaned up:
the chain of default is now simply: target defaults to host, host to
build, and build to the result of <code>config.guess</code>.  Nevertheless,
in order to ease the transition from 2.13 to 2.50, the following
transition scheme is implemented.  <em>Do not rely on it</em>, as it will
be completely disabled in a couple of releases (we cannot keep it, as it
proves to cause more problems than it cures).
</p>
<p>They all default to the result of running <code>config.guess</code>, unless
you specify either <samp>--build</samp> or <samp>--host</samp>.  In this case,
the default becomes the system type you specified.  If you specify both,
and they&rsquo;re different, <code>configure</code> enters cross compilation
mode, so it doesn&rsquo;t run any tests that require execution.
</p>
<p>Hint: if you mean to override the result of <code>config.guess</code>,
prefer <samp>--build</samp> over <samp>--host</samp>.
</p>
<br>

<p>For backward compatibility, <code>configure</code> accepts a system
type as an option by itself.  Such an option overrides the
defaults for build, host, and target system types.  The following
configure statement configures a cross toolchain that runs on
NetBSD/alpha but generates code for GNU Hurd/sparc,
which is also the build platform.
</p>
<div class="example">
<pre class="example">./configure --host=alpha-netbsd sparc-gnu
</pre></div>

<br>

<p>In Autoconf 2.13 and before, the variables <code>build</code>, <code>host</code>,
and <code>target</code> had a different semantics before and after the
invocation of <code>AC_CANONICAL_BUILD</code> etc.  Now, the argument of
<samp>--build</samp> is strictly copied into <code>build_alias</code>, and is left
empty otherwise.  After the <code>AC_CANONICAL_BUILD</code>, <code>build</code> is
set to the canonicalized build type.  To ease the transition, before,
its contents is the same as that of <code>build_alias</code>.  Do <em>not</em>
rely on this broken feature.
</p>
<p>For consistency with the backward compatibility scheme exposed above,
when <samp>--host</samp> is specified but <samp>--build</samp> isn&rsquo;t, the build
system is assumed to be the same as <samp>--host</samp>, and
&lsquo;<samp>build_alias</samp>&rsquo; is set to that value.  Eventually, this
historically incorrect behavior will go away.
</p>
<br>

<p>The former scheme to enable cross-compilation proved to cause more harm
than good, in particular, it used to be triggered too easily, leaving
regular end users puzzled in front of cryptic error messages.
<code>configure</code> could even enter cross-compilation mode only
because the compiler was not functional.  This is mainly because
<code>configure</code> used to try to detect cross-compilation, instead of
waiting for an explicit flag from the user.
</p>
<p>Now, <code>configure</code> enters cross-compilation mode if and only if
<samp>--host</samp> is passed.
</p>
<p>That&rsquo;s the short documentation.  To ease the transition between 2.13 and
its successors, a more complicated scheme is implemented.  <em>Do not
rely on the following</em>, as it will be removed in the near future.
</p>
<p>If you specify <samp>--host</samp>, but not <samp>--build</samp>, when
<code>configure</code> performs the first compiler test it tries to run
an executable produced by the compiler.  If the execution fails, it
enters cross-compilation mode.  This is fragile.  Moreover, by the time
the compiler test is performed, it may be too late to modify the
build-system type: other tests may have already been performed.
Therefore, whenever you specify <samp>--host</samp>, be sure to specify
<samp>--build</samp> too.
</p>
<div class="example">
<pre class="example">./configure --build=i686-pc-linux-gnu --host=m68k-coff
</pre></div>

<p>enters cross-compilation mode.  The former interface, which
consisted in setting the compiler to a cross-compiler without informing
<code>configure</code> is obsolete.  For instance, <code>configure</code>
fails if it can&rsquo;t run the code generated by the specified compiler if you
configure as follows:
</p>
<div class="example">
<pre class="example">./configure CC=m68k-coff-gcc
</pre></div>


<div class="header">
<p>
Next: <a href="AC_005fLIBOBJ-vs-LIBOBJS.html#AC_005fLIBOBJ-vs-LIBOBJS" accesskey="n" rel="next">AC_LIBOBJ vs LIBOBJS</a>, Previous: <a href="New-Macros.html#New-Macros" accesskey="p" rel="prev">New Macros</a>, Up: <a href="Autoconf-2_002e13.html#Autoconf-2_002e13" accesskey="u" rel="up">Autoconf 2.13</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
