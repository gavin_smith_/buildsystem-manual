<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: System overview</title>

<meta name="description" content="Automake and Autoconf Reference Manual: System overview">
<meta name="keywords" content="Automake and Autoconf Reference Manual: System overview">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html#Top" rel="up" title="Top">
<link href="Generic-installation-instructions.html#Generic-installation-instructions" rel="next" title="Generic installation instructions">
<link href="Libtool.html#Libtool" rel="prev" title="Libtool">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="System-overview"></a>
<div class="header">
<p>
Next: <a href="Generic-installation-instructions.html#Generic-installation-instructions" accesskey="n" rel="next">Generic installation instructions</a>, Previous: <a href="Introduction.html#Introduction" accesskey="p" rel="prev">Introduction</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="System-overview-1"></a>
<h2 class="chapter">2 System overview</h2>

<p>Automake has two input files, called <samp>Makefile.am</samp> and 
<samp>configure.ac</samp>, giving information about the build system to be 
generated.  <samp>Makefile.am</samp> contains information about the project, 
such as lists of source code files.  <samp>configure.ac</samp> is processed to 
produce a configure script.
</p>
<p>The flowchart below shows a overview of the process, showing which files 
are input and output to which programs.  (Some steps are omitted for 
simplicity.  See <a href="Flowchart-of-files-and-programs.html#Flowchart-of-files-and-programs">Flowchart of files and programs</a>.)
</p>
<img src="pictures/overview.png" alt="pictures/overview">

<p>As this flowchart shows, <samp>Makefile.am</samp> is processed by the 
<code>automake</code> program to produce a file called <samp>Makefile.in</samp>.  
(The contents of <samp>configure.ac</samp> also affect how <samp>Makefile.in</samp> 
is produced.)  <samp>Makefile.in</samp> is a template for the input to the 
<code>make</code> program.  The <code>autoconf</code> program converts 
<samp>configure.ac</samp> into the <samp>configure</samp> shell script.
</p>
<p>The developer or user of the program runs <samp>configure</samp>, which then 
runs the various checks specified by the developer in 
<samp>configure.ac</samp>.  These checks may include checks for working 
compilers or libraries needed by the project.  <samp>configure</samp> outputs 
this information by modifying the
<samp>Makefile.in</samp> template to create <samp>Makefile</samp>.  After 
<code>configure</code> has been run, the directory containing the source 
code is said to be <em>configured</em>.
</p>
<p>They then run &lsquo;<samp>make</samp>&rsquo; to <em>build</em> the project, that is, generate 
files that are derived from others.  For example, an executable file is 
compiled from source code files.
We refer to the directory containing the input files as <code>srcdir</code>, 
and the directory where generated files are placed as <code>builddir</code>.
Usually these are the same as each other, but they can be different: see 
<a href="VPATH-Builds.html#VPATH-Builds">VPATH Builds</a>, for more information.
</p>
<p>This is followed by &lsquo;<samp>make install</samp>&rsquo; to <em>install</em> the project, 
that is, place files on their system in the locations they will be used.  
This may include files generated by the build.  For example, executable 
files may be placed in directories in the shell&rsquo;s command search path.
Automake and Autoconf support several predefined installation 
directories, and the choice of each directory can be specified with 
options to <code>configure</code>.  See <a href="Installation-Names.html#Installation-Names">Installation Names</a>.
</p>
<p>We sometimes distinguish the various stages of processing input files by 
referring to them as
&ldquo;<code>automake</code> time&rdquo;,
&ldquo;<code>autoconf</code> time&rdquo;,
&ldquo;<code>configure</code> time&rdquo; and
&ldquo;<code>make</code> time&rdquo;.  What happens in earlier stages affects what 
happens at later stages.
</p>
<p>In order to distribute the project to others, the developer would run 
&lsquo;<samp>make dist</samp>&rsquo;, which performs the instructions in <samp>Makefile</samp>
to create a <em>distribution</em> file.  A distribution file is an archive 
file (usually a compressed <code>tar</code> file or a <code>zip</code> file)
comprising the files to be used in the project, including a copy of the 
build system.
</p>
<p>An individual who only wants to build or install the program can use the 
build system in the distribution to do this, without having Autoconf or 
Automake installed.  The procedure for doing is the same on their 
machine as on the original developer&rsquo;s.  The region in the flowchart 
above with a broken boundary shows the part of the build system 
available to such an individual.  They will not need Automake or 
Autoconf to be installed unless <samp>Makefile.am</samp> or 
<samp>configure.ac</samp> is modified.
</p>
  







<div class="header">
<p>
Next: <a href="Generic-installation-instructions.html#Generic-installation-instructions" accesskey="n" rel="next">Generic installation instructions</a>, Previous: <a href="Introduction.html#Introduction" accesskey="p" rel="prev">Introduction</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
