<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Defining Symbols</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Defining Symbols">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Defining Symbols">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="What-to-do-with-results-of-tests.html#What-to-do-with-results-of-tests" rel="up" title="What to do with results of tests">
<link href="Header-Templates.html#Header-Templates" rel="next" title="Header Templates">
<link href="Changed-Directory-Variables.html#Changed-Directory-Variables" rel="prev" title="Changed Directory Variables">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Defining-Symbols"></a>
<div class="header">
<p>
Next: <a href="Configuration-Commands.html#Configuration-Commands" accesskey="n" rel="next">Configuration Commands</a>, Previous: <a href="Setting-Output-Variables.html#Setting-Output-Variables" accesskey="p" rel="prev">Setting Output Variables</a>, Up: <a href="What-to-do-with-results-of-tests.html#What-to-do-with-results-of-tests" accesskey="u" rel="up">What to do with results of tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Defining-C-Preprocessor-Symbols"></a>
<h3 class="section">9.3 Defining C Preprocessor Symbols</h3>

<p>A common action to take in response to a feature test is to define a C
preprocessor symbol indicating the results of the test.  That is done by
calling <code>AC_DEFINE</code> or <code>AC_DEFINE_UNQUOTED</code>.
</p>
<p>By default, <code>AC_OUTPUT</code> places the symbols defined by these macros
into the output variable <code>DEFS</code>, which contains an option
<samp>-D<var>symbol</var>=<var>value</var></samp> for each symbol defined.  Unlike in
Autoconf version 1, there is no variable <code>DEFS</code> defined while
<code>configure</code> is running.  To check whether Autoconf macros have
already defined a certain C preprocessor symbol, test the value of the
appropriate cache variable, as in this example:
</p>
<div class="example">
<pre class="example">AC_CHECK_FUNC([vprintf], [AC_DEFINE([HAVE_VPRINTF], [1],
                          [Define if vprintf exists.])])
if test &quot;x$ac_cv_func_vprintf&quot; != xyes; then
  AC_CHECK_FUNC([_doprnt], [AC_DEFINE([HAVE_DOPRNT], [1],
                            [Define if _doprnt exists.])])
fi
</pre></div>

<p>If <code>AC_CONFIG_HEADERS</code> has been called, then instead of creating
<code>DEFS</code>, <code>AC_OUTPUT</code> creates a header file by substituting the
correct values into <code>#define</code> statements in a template file.
See <a href="#Defining-Symbols">Defining Symbols</a>, for more information about this kind of
output.
</p>
<dl>
<dt><a name="index-AC_005fDEFINE-1"></a>Macro: <strong>AC_DEFINE</strong> <em>(<var>variable</var>, <var>value</var>, <span class="roman">[</span><var>description</var><span class="roman">]</span>)</em></dt>
<dt><a name="index-AC_005fDEFINE-2"></a>Macro: <strong>AC_DEFINE</strong> <em>(<var>variable</var>)</em></dt>
<dd><a name="index-variable"></a>
<a name="index-AC_005fDEFINE"></a>
<p>Define <var>variable</var> to <var>value</var> (verbatim), by defining a C
preprocessor macro for <var>variable</var>.  <var>variable</var> should be a C
identifier, optionally suffixed by a parenthesized argument list to
define a C preprocessor macro with arguments.  The macro argument list,
if present, should be a comma-separated list of C identifiers, possibly
terminated by an ellipsis &lsquo;<samp>...</samp>&rsquo; if C99-or-later syntax is employed.
<var>variable</var> should not contain comments, white space, trigraphs,
backslash-newlines, universal character names, or non-ASCII
characters.
</p>
<p><var>value</var> may contain backslash-escaped newlines, which will be
preserved if you use <code>AC_CONFIG_HEADERS</code> but flattened if passed
via <code>@DEFS@</code> (with no effect on the compilation, since the
preprocessor sees only one line in the first place).  <var>value</var> should
not contain raw newlines.  If you are not using
<code>AC_CONFIG_HEADERS</code>, <var>value</var> should not contain any &lsquo;<samp>#</samp>&rsquo;
characters, as <code>make</code> tends to eat them.  To use a shell
variable, use <code>AC_DEFINE_UNQUOTED</code> instead.
</p>
<p><var>description</var> is only useful if you are using
<code>AC_CONFIG_HEADERS</code>.  In this case, <var>description</var> is put into
the generated <samp>config.h.in</samp> as the comment before the macro define.
The following example defines the C preprocessor variable
<code>EQUATION</code> to be the string constant &lsquo;<samp>&quot;$a &gt; $b&quot;</samp>&rsquo;:
</p>
<div class="example">
<pre class="example">AC_DEFINE([EQUATION], [&quot;$a &gt; $b&quot;],
  [Equation string.])
</pre></div>

<p>If neither <var>value</var> nor <var>description</var> are given, then
<var>value</var> defaults to 1 instead of to the empty string.  This is for
backwards compatibility with older versions of Autoconf, but this usage
is obsolescent and may be withdrawn in future versions of Autoconf.
</p>
<p>If the <var>variable</var> is a literal string, it is passed to
<code>m4_pattern_allow</code> (see <a href="Forbidden-Patterns.html#Forbidden-Patterns">Forbidden Patterns</a>).
</p>
<p>If multiple <code>AC_DEFINE</code> statements are executed for the same
<var>variable</var> name (not counting any parenthesized argument list),
the last one wins.
</p></dd></dl>

<dl>
<dt><a name="index-AC_005fDEFINE_005fUNQUOTED-1"></a>Macro: <strong>AC_DEFINE_UNQUOTED</strong> <em>(<var>variable</var>, <var>value</var>, <span class="roman">[</span><var>description</var><span class="roman">]</span>)</em></dt>
<dt><a name="index-AC_005fDEFINE_005fUNQUOTED-2"></a>Macro: <strong>AC_DEFINE_UNQUOTED</strong> <em>(<var>variable</var>)</em></dt>
<dd><a name="index-AC_005fDEFINE_005fUNQUOTED"></a>
<a name="index-variable-1"></a>
<p>Like <code>AC_DEFINE</code>, but three shell expansions are
performed&mdash;once&mdash;on <var>variable</var> and <var>value</var>: variable expansion
(&lsquo;<samp>$</samp>&rsquo;), command substitution (&lsquo;<samp>`</samp>&rsquo;), and backslash escaping
(&lsquo;<samp>\</samp>&rsquo;), as if in an unquoted here-document.  Single and double quote
characters in the value have no
special meaning.  Use this macro instead of <code>AC_DEFINE</code> when
<var>variable</var> or <var>value</var> is a shell variable.  Examples:
</p>
<div class="example">
<pre class="example">AC_DEFINE_UNQUOTED([config_machfile], [&quot;$machfile&quot;],
  [Configuration machine file.])
AC_DEFINE_UNQUOTED([GETGROUPS_T], [$ac_cv_type_getgroups],
  [getgroups return type.])
AC_DEFINE_UNQUOTED([$ac_tr_hdr], [1],
  [Translated header name.])
</pre></div>
</dd></dl>

<p>Due to a syntactical bizarreness of the Bourne shell, do not use
semicolons to separate <code>AC_DEFINE</code> or <code>AC_DEFINE_UNQUOTED</code>
calls from other macro calls or shell code; that can cause syntax errors
in the resulting <code>configure</code> script.  Use either blanks or
newlines.  That is, do this:
</p>
<div class="example">
<pre class="example">AC_CHECK_HEADER([elf.h],
  [AC_DEFINE([SVR4], [1], [System V Release 4]) LIBS=&quot;-lelf $LIBS&quot;])
</pre></div>

<p>or this:
</p>
<div class="example">
<pre class="example">AC_CHECK_HEADER([elf.h],
  [AC_DEFINE([SVR4], [1], [System V Release 4])
   LIBS=&quot;-lelf $LIBS&quot;])
</pre></div>

<p>instead of this:
</p>
<div class="example">
<pre class="example">AC_CHECK_HEADER([elf.h],
  [AC_DEFINE([SVR4], [1], [System V Release 4]); LIBS=&quot;-lelf $LIBS&quot;])
</pre></div>


<a name="index-Configuration-Header"></a>
<a name="index-config_002eh"></a>

<p>When a package contains more than a few tests that define C preprocessor
symbols, the command lines to pass <samp>-D</samp> options to the compiler
can get quite long.  This causes two problems.  One is that the
<code>make</code> output is hard to visually scan for errors.  More
seriously, the command lines can exceed the length limits of some
operating systems.  As an alternative to passing <samp>-D</samp> options to
the compiler, <code>configure</code> scripts can create a C header file
containing &lsquo;<samp>#define</samp>&rsquo; directives.  The <code>AC_CONFIG_HEADERS</code>
macro selects this kind of output.  Though it can be called anywhere
between <code>AC_INIT</code> and <code>AC_OUTPUT</code>, it is customary to call
it right after <code>AC_INIT</code>.
</p>
<p>The package should &lsquo;<samp>#include</samp>&rsquo; the configuration header file before
any other header files, to prevent inconsistencies in declarations (for
example, if it redefines <code>const</code>).
</p>
<p>To provide for VPATH builds, remember to pass the C compiler a <samp>-I.</samp>
option (or <samp>-I..</samp>; whichever directory contains <samp>config.h</samp>).
Even if you use &lsquo;<samp>#include &quot;config.h&quot;</samp>&rsquo;, the preprocessor searches only
the directory of the currently read file, i.e., the source directory, not
the build directory.
</p>
<p>With the appropriate <samp>-I</samp> option, you can use
&lsquo;<samp>#include &lt;config.h&gt;</samp>&rsquo;.  Actually, it&rsquo;s a good habit to use it,
because in the rare case when the source directory contains another
<samp>config.h</samp>, the build directory should be searched first.
</p>

<dl>
<dt><a name="index-AC_005fCONFIG_005fHEADERS-2"></a>Macro: <strong>AC_CONFIG_HEADERS</strong> <em>(<var>header</var> &hellip;, <span class="roman">[</span><var>cmds</var><span class="roman">]</span>, <span class="roman">[</span><var>init-cmds</var><span class="roman">]</span>)</em></dt>
<dd><a name="index-AC_005fCONFIG_005fHEADERS"></a>
<a name="index-HAVE_005fCONFIG_005fH"></a>
<p>This macro is one of the instantiating macros; see <a href="Configuration-Actions.html#Configuration-Actions">Configuration Actions</a>.  Make <code>AC_OUTPUT</code> create the file(s) in the
blank-or-newline-separated list <var>header</var> containing C preprocessor
<code>#define</code> statements, and replace &lsquo;<samp>@DEFS@</samp>&rsquo; in generated
files with <samp>-DHAVE_CONFIG_H</samp> instead of the value of <code>DEFS</code>.
The usual name for <var>header</var> is <samp>config.h</samp>.
</p>
<p>If <var>header</var> already exists and its contents are identical to what
<code>AC_OUTPUT</code> would put in it, it is left alone.  Doing this allows
making some changes in the configuration without needlessly causing
object files that depend on the header file to be recompiled.
</p>
<p>Usually the input file is named <samp><var>header</var>.in</samp>; however, you can
override the input file name by appending to <var>header</var> a
colon-separated list of input files.  For example, you might need to make
the input file name acceptable to DOS variants:
</p>
<div class="example">
<pre class="example">AC_CONFIG_HEADERS([config.h:config.hin])
</pre></div>

</dd></dl>

<dl>
<dt><a name="index-AH_005fHEADER-1"></a>Macro: <strong>AH_HEADER</strong></dt>
<dd><a name="index-AH_005fHEADER"></a>
<p>This macro is defined as the name of the first declared config header
and undefined if no config headers have been declared up to this point.
A third-party macro may, for example, require use of a config header
without invoking AC_CONFIG_HEADERS twice, like this:
</p>
<div class="example">
<pre class="example">AC_CONFIG_COMMANDS_PRE(
        [m4_ifndef([AH_HEADER], [AC_CONFIG_HEADERS([config.h])])])
</pre></div>

</dd></dl>

<p>See <a href="Configuration-Actions.html#Configuration-Actions">Configuration Actions</a>, for more details on <var>header</var>.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Header-Templates.html#Header-Templates" accesskey="1">Header Templates</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Input for the configuration headers
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="autoheader-Invocation.html#autoheader-Invocation" accesskey="2">autoheader Invocation</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">How to create configuration templates
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Autoheader-Macros.html#Autoheader-Macros" accesskey="3">Autoheader Macros</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">How to specify CPP templates
</td></tr>
</table>

<div class="header">
<p>
Next: <a href="Configuration-Commands.html#Configuration-Commands" accesskey="n" rel="next">Configuration Commands</a>, Previous: <a href="Setting-Output-Variables.html#Setting-Output-Variables" accesskey="p" rel="prev">Setting Output Variables</a>, Up: <a href="What-to-do-with-results-of-tests.html#What-to-do-with-results-of-tests" accesskey="u" rel="up">What to do with results of tests</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
