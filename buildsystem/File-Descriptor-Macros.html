<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: File Descriptor Macros</title>

<meta name="description" content="Automake and Autoconf Reference Manual: File Descriptor Macros">
<meta name="keywords" content="Automake and Autoconf Reference Manual: File Descriptor Macros">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Programming-in-M4sh.html#Programming-in-M4sh" rel="up" title="Programming in M4sh">
<link href="Printing-Messages.html#Printing-Messages" rel="next" title="Printing Messages">
<link href="Initialization-Macros.html#Initialization-Macros" rel="prev" title="Initialization Macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="File-Descriptor-Macros"></a>
<div class="header">
<p>
Previous: <a href="Initialization-Macros.html#Initialization-Macros" accesskey="p" rel="prev">Initialization Macros</a>, Up: <a href="Programming-in-M4sh.html#Programming-in-M4sh" accesskey="u" rel="up">Programming in M4sh</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="File-Descriptor-Macros-1"></a>
<h4 class="subsection">15.2.4 File Descriptor Macros</h4>
<a name="index-input"></a>
<a name="index-standard-input"></a>
<a name="index-file-descriptors"></a>
<a name="index-descriptors"></a>
<a name="index-low_002dlevel-output"></a>
<a name="index-output_002c-low_002dlevel"></a>

<p>The following macros define file descriptors used to output messages
(or input values) from <samp>configure</samp> scripts.
For example:
</p>
<div class="example">
<pre class="example">echo &quot;$wombats found&quot; &gt;&amp;AS_MESSAGE_LOG_FD
echo 'Enter desired kangaroo count:' &gt;&amp;AS_MESSAGE_FD
read kangaroos &lt;&amp;AS_ORIGINAL_STDIN_FD`
</pre></div>

<p>However doing so is seldom needed, because Autoconf provides higher
level macros as described below.
</p>
<dl>
<dt><a name="index-AS_005fMESSAGE_005fFD-1"></a>Macro: <strong>AS_MESSAGE_FD</strong></dt>
<dd><a name="index-AS_005fMESSAGE_005fFD"></a>
<p>The file descriptor for &lsquo;<samp>checking for...</samp>&rsquo;  messages and results.
By default, <code>AS_INIT</code> sets this to &lsquo;<samp>1</samp>&rsquo; for standalone M4sh
clients.  However, <code>AC_INIT</code> shuffles things around to another file
descriptor, in order to allow the <samp>-q</samp> option of
<code>configure</code> to choose whether messages should go to the script&rsquo;s
standard output or be discarded.
</p>
<p>If you want to display some messages, consider using one of the printing
macros (see <a href="Printing-Messages.html#Printing-Messages">Printing Messages</a>) instead.  Copies of messages output
via these macros are also recorded in <samp>config.log</samp>.
</p></dd></dl>

<a name="AS_005fMESSAGE_005fLOG_005fFD"></a><dl>
<dt><a name="index-AS_005fMESSAGE_005fLOG_005fFD-1"></a>Macro: <strong>AS_MESSAGE_LOG_FD</strong></dt>
<dd><a name="index-AS_005fMESSAGE_005fLOG_005fFD"></a>
<p>This must either be empty, or expand to a file descriptor for log
messages.  By default, <code>AS_INIT</code> sets this macro to the empty
string for standalone M4sh clients, thus disabling logging.  However,
<code>AC_INIT</code> shuffles things around so that both <code>configure</code>
and <code>config.status</code> use <samp>config.log</samp> for log messages.
Macros that run tools, like <code>AC_COMPILE_IFELSE</code> (see <a href="Running-the-Compiler.html#Running-the-Compiler">Running the Compiler</a>), redirect all output to this descriptor.  You may want to do
so if you develop such a low-level macro.
</p></dd></dl>

<dl>
<dt><a name="index-AS_005fORIGINAL_005fSTDIN_005fFD-1"></a>Macro: <strong>AS_ORIGINAL_STDIN_FD</strong></dt>
<dd><a name="index-AS_005fORIGINAL_005fSTDIN_005fFD"></a>
<p>This must expand to a file descriptor for the original standard input.
By default, <code>AS_INIT</code> sets this macro to &lsquo;<samp>0</samp>&rsquo; for standalone
M4sh clients.  However, <code>AC_INIT</code> shuffles things around for
safety.
</p>
<p>When <code>configure</code> runs, it may accidentally execute an
interactive command that has the same name as the non-interactive meant
to be used or checked.  If the standard input was the terminal, such
interactive programs would cause <code>configure</code> to stop, pending
some user input.  Therefore <code>configure</code> redirects its standard
input from <samp>/dev/null</samp> during its initialization.  This is not
normally a problem, since <code>configure</code> normally does not need
user input.
</p>
<p>In the extreme case where your <samp>configure</samp> script really needs to
obtain some values from the original standard input, you can read them
explicitly from <code>AS_ORIGINAL_STDIN_FD</code>.
</p></dd></dl>


<div class="header">
<p>
Previous: <a href="Initialization-Macros.html#Initialization-Macros" accesskey="p" rel="prev">Initialization Macros</a>, Up: <a href="Programming-in-M4sh.html#Programming-in-M4sh" accesskey="u" rel="up">Programming in M4sh</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
