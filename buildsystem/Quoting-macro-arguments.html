<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Quoting macro arguments</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Quoting macro arguments">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Quoting macro arguments">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Autoconf-Language.html#Autoconf-Language" rel="up" title="Autoconf Language">
<link href="Quadrigraphs.html#Quadrigraphs" rel="next" title="Quadrigraphs">
<link href="Autoconf-Language.html#Autoconf-Language" rel="prev" title="Autoconf Language">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Quoting-macro-arguments"></a>
<div class="header">
<p>
Next: <a href="Quadrigraphs.html#Quadrigraphs" accesskey="n" rel="next">Quadrigraphs</a>, Up: <a href="Autoconf-Language.html#Autoconf-Language" accesskey="u" rel="up">Autoconf Language</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Quoting-macro-arguments-1"></a>
<h4 class="subsubsection">5.2.1.1 Quoting macro arguments</h4>

<p>You should always quote an argument that
might contain a macro name, comma, parenthesis, or a leading blank or
newline.  This rule applies recursively for every macro
call, including macros called from other macros.  For more details on
quoting rules, see <a href="Programming-in-M4.html#Programming-in-M4">Programming in M4</a>.
</p>
<p>For instance:
</p>
<div class="example">
<pre class="example">AC_CHECK_HEADER([stdio.h],
                [AC_DEFINE([HAVE_STDIO_H], [1],
                   [Define to 1 if you have &lt;stdio.h&gt;.])],
                [AC_MSG_ERROR([sorry, can't do anything for you])])
</pre></div>

<p>is quoted properly.  You may safely simplify its quotation to:
</p>
<div class="example">
<pre class="example">AC_CHECK_HEADER([stdio.h],
                [AC_DEFINE([HAVE_STDIO_H], 1,
                   [Define to 1 if you have &lt;stdio.h&gt;.])],
                [AC_MSG_ERROR([sorry, can't do anything for you])])
</pre></div>

<p>because &lsquo;<samp>1</samp>&rsquo; cannot contain a macro call.  Here, the argument of
<code>AC_MSG_ERROR</code> must be quoted; otherwise, its comma would be
interpreted as an argument separator.  Also, the second and third arguments
of &lsquo;<samp>AC_CHECK_HEADER</samp>&rsquo; must be quoted, since they contain
macro calls.  The three arguments &lsquo;<samp>HAVE_STDIO_H</samp>&rsquo;, &lsquo;<samp>stdio.h</samp>&rsquo;,
and &lsquo;<samp>Define to 1 if you have &lt;stdio.h&gt;.</samp>&rsquo; do not need quoting, but
if you unwisely defined a macro with a name like &lsquo;<samp>Define</samp>&rsquo; or
&lsquo;<samp>stdio</samp>&rsquo; then they would need quoting.  Cautious Autoconf users
would keep the quotes, but many Autoconf users find such precautions
annoying, and would rewrite the example as follows:
</p>
<div class="example">
<pre class="example">AC_CHECK_HEADER(stdio.h,
                [AC_DEFINE(HAVE_STDIO_H, 1,
                   [Define to 1 if you have &lt;stdio.h&gt;.])],
                [AC_MSG_ERROR([sorry, can't do anything for you])])
</pre></div>

<p>This is safe, so long as you
do not
define macros with names like &lsquo;<samp>HAVE_STDIO_H</samp>&rsquo;, &lsquo;<samp>stdio</samp>&rsquo;, or
&lsquo;<samp>h</samp>&rsquo;.  Though it is also safe here to omit the quotes around
&lsquo;<samp>Define to 1 if you have &lt;stdio.h&gt;.</samp>&rsquo; this is not recommended, as
message strings are more likely to inadvertently contain commas.
</p>
<p>The following example is wrong and dangerous, as it is underquoted:
</p>
<div class="example">
<pre class="example">AC_CHECK_HEADER(stdio.h,
                AC_DEFINE(HAVE_STDIO_H, 1,
                   Define to 1 if you have &lt;stdio.h&gt;.),
                AC_MSG_ERROR([sorry, can't do anything for you]))
</pre></div>



<p>The rule of thumb is that
<em>whenever you expect macro expansion, expect quote expansion</em>;
i.e., expect one level of quotes to be lost.  For instance:
</p>
<div class="example">
<pre class="example">AC_COMPILE_IFELSE(AC_LANG_SOURCE([char b[10];]), [],
 [AC_MSG_ERROR([you lose])])
</pre></div>

<p>is incorrect: here, the first argument of <code>AC_LANG_SOURCE</code> is
&lsquo;<samp>char b[10];</samp>&rsquo; and is expanded once, which results in
&lsquo;<samp>char b10;</samp>&rsquo;; and the <code>AC_LANG_SOURCE</code> is also expanded prior
to being passed to <code>AC_COMPILE_IFELSE</code>.
The author meant the first argument
to be understood as a literal, and therefore it must be quoted twice;
likewise, the intermediate <code>AC_LANG_SOURCE</code> macro should be quoted
once so that it is only expanded after the rest of the body of
<code>AC_COMPILE_IFELSE</code> is in place:
</p>
<div class="example">
<pre class="example">AC_COMPILE_IFELSE([AC_LANG_SOURCE([[char b[10];]])], [],
  [AC_MSG_ERROR([you lose])])
</pre></div>

<p>Voil&agrave;, you actually produce &lsquo;<samp>char b[10];</samp>&rsquo; this time!
</p>
<p>On the other hand, descriptions (e.g., the last parameter of
<code>AC_DEFINE</code> or <code>AS_HELP_STRING</code>) should not be double quoted.
These arguments are treated as whitespace-separated
lists of text to be reformatted, and are not subject to macro expansion.
Even if these descriptions are short and are not actually broken, double
quoting them yields weird results.
</p>
<div class="header">
<p>
Next: <a href="Quadrigraphs.html#Quadrigraphs" accesskey="n" rel="next">Quadrigraphs</a>, Up: <a href="Autoconf-Language.html#Autoconf-Language" accesskey="u" rel="up">Autoconf Language</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
