<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Libtool Libraries</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Libtool Libraries">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Libtool Libraries">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Shared-libraries.html#Shared-libraries" rel="up" title="Shared libraries">
<link href="Conditional-Libtool-Libraries.html#Conditional-Libtool-Libraries" rel="next" title="Conditional Libtool Libraries">
<link href="Libtool-Concept.html#Libtool-Concept" rel="prev" title="Libtool Concept">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Libtool-Libraries"></a>
<div class="header">
<p>
Next: <a href="Conditional-Libtool-Libraries.html#Conditional-Libtool-Libraries" accesskey="n" rel="next">Conditional Libtool Libraries</a>, Previous: <a href="Libtool-Concept.html#Libtool-Concept" accesskey="p" rel="prev">Libtool Concept</a>, Up: <a href="Shared-libraries.html#Shared-libraries" accesskey="u" rel="up">Shared libraries</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Building-Libtool-Libraries"></a>
<h4 class="subsection">7.3.2 Building Libtool Libraries</h4>

<a name="index-_005fLTLIBRARIES-primary_002c-defined"></a>
<a name="index-LTLIBRARIES-primary_002c-defined"></a>
<a name="index-Primary-variable_002c-LTLIBRARIES"></a>
<a name="index-Example-of-shared-libraries"></a>
<a name="index-lib_005fLTLIBRARIES"></a>
<a name="index-pkglib_005fLTLIBRARIES"></a>
<a name="index-_005fLTLIBRARIES"></a>

<p>Automake uses libtool to build any libraries declared with the
<code>LTLIBRARIES</code> primary.  Each <code>_LTLIBRARIES</code> variable is a
list of libtool libraries to build.  For instance, to create a libtool
library named <samp>libgettext.la</samp>, and install it in <code>libdir</code>,
write:
</p>
<div class="example">
<pre class="example">lib_LTLIBRARIES = libgettext.la
libgettext_la_SOURCES = gettext.c gettext.h &hellip;
</pre></div>

<p>Automake predefines the variable <code>pkglibdir</code>, so you can use
<code>pkglib_LTLIBRARIES</code> to install libraries in
&lsquo;<samp>$(libdir)/@PACKAGE@/</samp>&rsquo;.
</p>
<p>If <samp>gettext.h</samp> is a public header file that needs to be installed
in order for people to use the library, it should be declared using a
<code>_HEADERS</code> variable, not in <code>libgettext_la_SOURCES</code>.
Headers listed in the latter should be internal headers that are not
part of the public interface.
</p>
<div class="example">
<pre class="example">lib_LTLIBRARIES = libgettext.la
libgettext_la_SOURCES = gettext.c &hellip;
include_HEADERS = gettext.h &hellip;
</pre></div>

<p>A package can build and install such a library along with other
programs that use it.  This dependency should be specified using
<code>LDADD</code>.  The following example builds a program named
<samp>hello</samp> that is linked with <samp>libgettext.la</samp>.
</p>
<div class="example">
<pre class="example">lib_LTLIBRARIES = libgettext.la
libgettext_la_SOURCES = gettext.c &hellip;

bin_PROGRAMS = hello
hello_SOURCES = hello.c &hellip;
hello_LDADD = libgettext.la
</pre></div>

<p>Whether <samp>hello</samp> is statically or dynamically linked with
<samp>libgettext.la</samp> is not yet known: this will depend on the
configuration of libtool and the capabilities of the host.
</p>




</body>
</html>
