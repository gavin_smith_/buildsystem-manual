<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Why Not Imake</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Why Not Imake">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Why Not Imake">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Rationales.html#Rationales" rel="up" title="Rationales">
<link href="History-of-Autoconf.html#History-of-Autoconf" rel="next" title="History of Autoconf">
<link href="Bootstrapping.html#Bootstrapping" rel="prev" title="Bootstrapping">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Why-Not-Imake"></a>
<div class="header">
<p>
Next: <a href="History-of-Autoconf.html#History-of-Autoconf" accesskey="n" rel="next">History of Autoconf</a>, Previous: <a href="Bootstrapping.html#Bootstrapping" accesskey="p" rel="prev">Bootstrapping</a>, Up: <a href="Rationales.html#Rationales" accesskey="u" rel="up">Rationales</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Why-Not-Imake_003f"></a>
<h3 class="section">I.4 Why Not Imake?</h3>
<a name="index-Imake"></a>

<div class="display">
<pre class="display">Why not use Imake instead of <code>configure</code> scripts?
</pre></div>

<p>Several people have written addressing this question, so
adaptations of their explanations are included here.
</p>
<p>The following answer is based on one written by Richard Pixley:
</p>
<blockquote>
<p>Autoconf generated scripts frequently work on machines that it has
never been set up to handle before.  That is, it does a good job of
inferring a configuration for a new system.  Imake cannot do this.
</p>
<p>Imake uses a common database of host specific data.  For X11, this makes
sense because the distribution is made as a collection of tools, by one
central authority who has control over the database.
</p>
<p>GNU tools are not released this way.  Each GNU tool has a
maintainer; these maintainers are scattered across the world.  Using a
common database would be a maintenance nightmare.  Autoconf may appear
to be this kind of database, but in fact it is not.  Instead of listing
host dependencies, it lists program requirements.
</p>
<p>If you view the GNU suite as a collection of native tools, then the
problems are similar.  But the GNU development tools can be
configured as cross tools in almost any host+target permutation.  All of
these configurations can be installed concurrently.  They can even be
configured to share host independent files across hosts.  Imake doesn&rsquo;t
address these issues.
</p>
<p>Imake templates are a form of standardization.  The GNU coding
standards address the same issues without necessarily imposing the same
restrictions.
</p></blockquote>


<p>Here is some further explanation, written by Per Bothner:
</p>
<blockquote>
<p>One of the advantages of Imake is that it is easy to generate large
makefiles using the &lsquo;<samp>#include</samp>&rsquo; and macro mechanisms of <code>cpp</code>.
However, <code>cpp</code> is not programmable: it has limited conditional
facilities, and no looping.  And <code>cpp</code> cannot inspect its
environment.
</p>
<p>All of these problems are solved by using <code>sh</code> instead of
<code>cpp</code>.  The shell is fully programmable, has macro substitution,
can execute (or source) other shell scripts, and can inspect its
environment.
</p></blockquote>


<p>Paul Eggert elaborates more:
</p>
<blockquote>
<p>With Autoconf, installers need not assume that Imake itself is already
installed and working well.  This may not seem like much of an advantage
to people who are accustomed to Imake.  But on many hosts Imake is not
installed or the default installation is not working well, and requiring
Imake to install a package hinders the acceptance of that package on
those hosts.  For example, the Imake template and configuration files
might not be installed properly on a host, or the Imake build procedure
might wrongly assume that all source files are in one big directory
tree, or the Imake configuration might assume one compiler whereas the
package or the installer needs to use another, or there might be a
version mismatch between the Imake expected by the package and the Imake
supported by the host.  These problems are much rarer with Autoconf,
where each package comes with its own independent configuration
processor.
</p>
<p>Also, Imake often suffers from unexpected interactions between
<code>make</code> and the installer&rsquo;s C preprocessor.  The fundamental problem
here is that the C preprocessor was designed to preprocess C programs,
not makefiles.  This is much less of a problem with Autoconf,
which uses the general-purpose preprocessor M4, and where the
package&rsquo;s author (rather than the installer) does the preprocessing in a
standard way.
</p></blockquote>


<p>Finally, Mark Eichin notes:
</p>
<blockquote>
<p>Imake isn&rsquo;t all that extensible, either.  In order to add new features to
Imake, you need to provide your own project template, and duplicate most
of the features of the existing one.  This means that for a sophisticated
project, using the vendor-provided Imake templates fails to provide any
leverage&mdash;since they don&rsquo;t cover anything that your own project needs
(unless it is an X11 program).
</p>
<p>On the other side, though:
</p>
<p>The one advantage that Imake has over <code>configure</code>:
<samp>Imakefile</samp> files tend to be much shorter (likewise, less redundant)
than <samp>Makefile.in</samp> files.  There is a fix to this, however&mdash;at least
for the Kerberos V5 tree, we&rsquo;ve modified things to call in common
<samp>post.in</samp> and <samp>pre.in</samp> makefile fragments for the
entire tree.  This means that a lot of common things don&rsquo;t have to be
duplicated, even though they normally are in <code>configure</code> setups.
</p></blockquote>


<div class="header">
<p>
Next: <a href="History-of-Autoconf.html#History-of-Autoconf" accesskey="n" rel="next">History of Autoconf</a>, Previous: <a href="Bootstrapping.html#Bootstrapping" accesskey="p" rel="prev">Bootstrapping</a>, Up: <a href="Rationales.html#Rationales" accesskey="u" rel="up">Rationales</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
