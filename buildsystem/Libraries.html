<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Libraries</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Libraries">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Libraries">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Supported-objects.html#Supported-objects" rel="up" title="Supported objects">
<link href="LIBOBJS.html#LIBOBJS" rel="next" title="LIBOBJS">
<link href="EXEEXT.html#EXEEXT" rel="prev" title="EXEEXT">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Libraries"></a>
<div class="header">
<p>
Next: <a href="Shared-libraries.html#Shared-libraries" accesskey="n" rel="next">Shared libraries</a>, Previous: <a href="Programs.html#Programs" accesskey="p" rel="prev">Programs</a>, Up: <a href="Supported-objects.html#Supported-objects" accesskey="u" rel="up">Supported objects</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Libraries-1"></a>
<h3 class="section">7.2 Libraries</h3>

<a name="index-_005fLIBRARIES-primary_002c-defined"></a>
<a name="index-LIBRARIES-primary_002c-defined"></a>
<a name="index-Primary-variable_002c-LIBRARIES"></a>
<a name="index-_005fLIBRARIES"></a>

<a name="index-lib_005fLIBRARIES"></a>
<a name="index-pkglib_005fLIBRARIES"></a>
<a name="index-noinst_005fLIBRARIES"></a>

<p>Building a library is much like building a program.  In this case, the
name of the primary is <code>LIBRARIES</code>.  Libraries can be installed in
<code>libdir</code> or <code>pkglibdir</code>.
</p>
<p>See <a href="Shared-libraries.html#Shared-libraries">Shared libraries</a>, for information on how to build shared
libraries using libtool and the <code>LTLIBRARIES</code> primary.
</p>
<p>Each <code>_LIBRARIES</code> variable is a list of the libraries to be built.
For instance, to create a library named <samp>libcpio.a</samp>, but not install
it, you would write:
</p>
<div class="example">
<pre class="example">noinst_LIBRARIES = libcpio.a
libcpio_a_SOURCES = &hellip;
</pre></div>

<p>The sources that go into a library are determined exactly as they are
for programs, via the <code>_SOURCES</code> variables.  Note that the library
name is canonicalized (see <a href="Other-Automake-variables.html#Other-Automake-variables">Other Automake variables</a>), so the 
<code>_SOURCES</code>
variable corresponding to <samp>libcpio.a</samp> is &lsquo;<samp>libcpio_a_SOURCES</samp>&rsquo;,
not &lsquo;<samp>libcpio.a_SOURCES</samp>&rsquo;.
</p>
<a name="index-maude_005fLIBADD"></a>
<p>Extra objects can be added to a library using the
<code><var>library</var>_LIBADD</code> variable.  This should be used for objects
determined by <code>configure</code>.  Again from <code>cpio</code>:
</p>
<div class="example">
<pre class="example">libcpio_a_LIBADD = $(LIBOBJS) $(ALLOCA)
</pre></div>

<p>In addition, sources for extra objects that will not exist until
configure-time must be added to the <code>BUILT_SOURCES</code> variable
(see <a href="Sources.html#Sources">Sources</a>).
</p>
<p>Building a static library is done by compiling all object files, then
by invoking &lsquo;<samp>$(AR) $(ARFLAGS)</samp>&rsquo; followed by the name of the
library and the list of objects, and finally by calling
&lsquo;<samp>$(RANLIB)</samp>&rsquo; on that library.  You should call
<code>AC_PROG_RANLIB</code> from your <samp>configure.ac</samp> to define
<code>RANLIB</code> (Automake will complain otherwise).  You should also
call <code>AM_PROG_AR</code> to define <code>AR</code>, in order to support unusual
archivers such as Microsoft lib.  <code>ARFLAGS</code> will default to
<code>cru</code>; you can override this variable by setting it in your
<samp>Makefile.am</samp> or by <code>AC_SUBST</code>ing it from your
<samp>configure.ac</samp>.  You can override the <code>AR</code> variable by
defining a per-library <code>maude_AR</code> variable (see <a href="Program-and-Library-Variables.html#Program-and-Library-Variables">Program and Library Variables</a>).
</p>
<a name="index-Empty-libraries"></a>
<p>Be careful when selecting library components conditionally.  Because
building an empty library is not portable, you should ensure that any
library always contains at least one object.
</p>
<p>To use a static library when building a program, add it to
<code>LDADD</code> for this program.  In the following example, the program
<samp>cpio</samp> is statically linked with the library <samp>libcpio.a</samp>.
</p>
<div class="example">
<pre class="example">noinst_LIBRARIES = libcpio.a
libcpio_a_SOURCES = &hellip;

bin_PROGRAMS = cpio
cpio_SOURCES = cpio.c &hellip;
cpio_LDADD = libcpio.a
</pre></div>

<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="LIBOBJS.html#LIBOBJS" accesskey="1">LIBOBJS</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<div class="header">
<p>
Next: <a href="Shared-libraries.html#Shared-libraries" accesskey="n" rel="next">Shared libraries</a>, Previous: <a href="Programs.html#Programs" accesskey="p" rel="prev">Programs</a>, Up: <a href="Supported-objects.html#Supported-objects" accesskey="u" rel="up">Supported objects</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
