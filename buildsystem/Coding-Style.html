<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Coding Style</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Coding Style">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Coding Style">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Writing-Autoconf-Macros.html#Writing-Autoconf-Macros" rel="up" title="Writing Autoconf Macros">
<link href="Custom-macros.html#Custom-macros" rel="next" title="Custom macros">
<link href="Obsoleting-Macros.html#Obsoleting-Macros" rel="prev" title="Obsoleting Macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Coding-Style"></a>
<div class="header">
<p>
Previous: <a href="Obsoleting-Macros.html#Obsoleting-Macros" accesskey="p" rel="prev">Obsoleting Macros</a>, Up: <a href="Writing-Autoconf-Macros.html#Writing-Autoconf-Macros" accesskey="u" rel="up">Writing Autoconf Macros</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Coding-Style-1"></a>
<h4 class="subsection">8.5.6 Coding Style</h4>
<a name="index-Coding-style"></a>

<p>The Autoconf macros follow a strict coding style.  You are encouraged to
follow this style, especially if you intend to distribute your macro,
either by contributing it to Autoconf itself or the
<a href="http://www.gnu.org/software/autoconf-archive/">Autoconf Macro
Archive</a>, or by other means.
</p>
<p>The first requirement is to pay great attention to the quotation.  For
more details, see <a href="Autoconf-Language.html#Autoconf-Language">Autoconf Language</a>, and <a href="M4-Quotation.html#M4-Quotation">M4 Quotation</a>.
</p>
<p>Do not try to invent new interfaces.  It is likely that there is a macro
in Autoconf that resembles the macro you are defining: try to stick to
this existing interface (order of arguments, default values, etc.).  We
<em>are</em> conscious that some of these interfaces are not perfect;
nevertheless, when harmless, homogeneity should be preferred over
creativity.
</p>
<p>Be careful about clashes both between M4 symbols and between shell
variables.
</p>
<p>If you stick to the suggested M4 naming scheme (see <a href="Macro-Names.html#Macro-Names">Macro Names</a>),
you are unlikely to generate conflicts.  Nevertheless, when you need to
set a special value, <em>avoid using a regular macro name</em>; rather,
use an &ldquo;impossible&rdquo; name.  For instance, up to version 2.13, the macro
<code>AC_SUBST</code> used to remember what <var>symbol</var> macros were already defined
by setting <code>AC_SUBST_<var>symbol</var></code>, which is a regular macro name.
But since there is a macro named <code>AC_SUBST_FILE</code>, it was just
impossible to &lsquo;<samp>AC_SUBST(FILE)</samp>&rsquo;!  In this case,
<code>AC_SUBST(<var>symbol</var>)</code> or <code>_AC_SUBST(<var>symbol</var>)</code> should
have been used (yes, with the parentheses).
</p>
<p>No Autoconf macro should ever enter the user-variable name space; i.e.,
except for the variables that are the actual result of running the
macro, all shell variables should start with <code>ac_</code>.  In
addition, small macros or any macro that is likely to be embedded in
other macros should be careful not to use obvious names.
</p>
<a name="index-dnl-2"></a>
<p>Do not use <code>dnl</code> to introduce comments: most of the comments you
are likely to write are either header comments which are not output
anyway, or comments that should make their way into <samp>configure</samp>.
There are exceptional cases where you do want to comment special M4
constructs, in which case <code>dnl</code> is right, but keep in mind that it
is unlikely.
</p>
<p>M4 ignores the leading blanks and newlines before each argument.
Use this feature to
indent in such a way that arguments are (more or less) aligned with the
opening parenthesis of the macro being called.  For instance, instead of
</p>
<div class="example">
<pre class="example">AC_CACHE_CHECK(for EMX OS/2 environment,
ac_cv_emxos2,
[AC_COMPILE_IFELSE([AC_LANG_PROGRAM(, [return __EMX__;])],
[ac_cv_emxos2=yes], [ac_cv_emxos2=no])])
</pre></div>

<p>write
</p>
<div class="example">
<pre class="example">AC_CACHE_CHECK([for EMX OS/2 environment], [ac_cv_emxos2],
[AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [return __EMX__;])],
                   [ac_cv_emxos2=yes],
                   [ac_cv_emxos2=no])])
</pre></div>

<p>or even
</p>
<div class="example">
<pre class="example">AC_CACHE_CHECK([for EMX OS/2 environment],
               [ac_cv_emxos2],
               [AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],
                                                   [return __EMX__;])],
                                  [ac_cv_emxos2=yes],
                                  [ac_cv_emxos2=no])])
</pre></div>

<p>When using <code>AC_RUN_IFELSE</code> or any macro that cannot work when
cross-compiling, provide a pessimistic value (typically &lsquo;<samp>no</samp>&rsquo;).
</p>
<p>Feel free to use various tricks to prevent auxiliary tools, such as
syntax-highlighting editors, from behaving improperly.  For instance,
instead of:
</p>
<div class="example">
<pre class="example">m4_bpatsubst([$1], [$&quot;])
</pre></div>

<p>use
</p>
<div class="example">
<pre class="example">m4_bpatsubst([$1], [$&quot;&quot;])
</pre></div>

<p>so that Emacsen do not open an endless &ldquo;string&rdquo; at the first quote.
For the same reasons, avoid:
</p>
<div class="example">
<pre class="example">test $[#] != 0
</pre></div>

<p>and use:
</p>
<div class="example">
<pre class="example">test $[@%:@] != 0
</pre></div>

<p>Otherwise, the closing bracket would be hidden inside a &lsquo;<samp>#</samp>&rsquo;-comment,
breaking the bracket-matching highlighting from Emacsen.  Note the
preferred style to escape from M4: &lsquo;<samp>$[1]</samp>&rsquo;, &lsquo;<samp>$[@]</samp>&rsquo;, etc.  Do
not escape when it is unnecessary.  Common examples of useless quotation
are &lsquo;<samp>[$]$1</samp>&rsquo; (write &lsquo;<samp>$$1</samp>&rsquo;), &lsquo;<samp>[$]var</samp>&rsquo; (use &lsquo;<samp>$var</samp>&rsquo;),
etc.  If you add portability issues to the picture, you&rsquo;ll prefer
&lsquo;<samp>${1+&quot;$[@]&quot;}</samp>&rsquo; to &lsquo;<samp>&quot;[$]@&quot;</samp>&rsquo;, and you&rsquo;ll prefer do something
better than hacking Autoconf <code>:-)</code>.
</p>
<p>When using <code>sed</code>, don&rsquo;t use <samp>-e</samp> except for indenting
purposes.  With the <code>s</code> and <code>y</code> commands, the preferred
separator is &lsquo;<samp>/</samp>&rsquo; unless &lsquo;<samp>/</samp>&rsquo; itself might appear in the pattern
or replacement, in which case you should use &lsquo;<samp>|</samp>&rsquo;, or optionally
&lsquo;<samp>,</samp>&rsquo; if you know the pattern and replacement cannot contain a file
name.  If none of these characters will do, choose a printable character
that cannot appear in the pattern or replacement.  Characters from the
set &lsquo;<samp>&quot;#$&amp;'()*;&lt;=&gt;?`|~</samp>&rsquo; are good choices if the pattern or
replacement might contain a file name, since they have special meaning
to the shell and are less likely to occur in file names.
</p>
<p>See <a href="Macro-Definitions.html#Macro-Definitions">Macro Definitions</a>, for details on how to define a macro.  If a
macro doesn&rsquo;t use <code>AC_REQUIRE</code>, is expected to never be the object
of an <code>AC_REQUIRE</code> directive, and macros required by other macros
inside arguments do not need to be expanded before this macro, then
use <code>m4_define</code>.  In case of doubt, use <code>AC_DEFUN</code>.
Also take into account that public third-party macros need to use
<code>AC_DEFUN</code> in order to be found by <code>aclocal</code>
(see <a href="http://www.gnu.org/software/automake/manual/html_node/Extending-aclocal.html#Extending-aclocal">Extending aclocal</a> in <cite>GNU Automake</cite>).
All the <code>AC_REQUIRE</code> statements should be at the beginning of the
macro, and each statement should be followed by <code>dnl</code>.
</p>
<p>You should not rely on the number of arguments: instead of checking
whether an argument is missing, test that it is not empty.  It provides
both a simpler and a more predictable interface to the user, and saves
room for further arguments.
</p>
<p>Unless the macro is short, try to leave the closing &lsquo;<samp>])</samp>&rsquo; at the
beginning of a line, followed by a comment that repeats the name of the
macro being defined.  This introduces an additional newline in
<code>configure</code>; normally, that is not a problem, but if you want to
remove it you can use &lsquo;<samp>[]dnl</samp>&rsquo; on the last line.  You can similarly
use &lsquo;<samp>[]dnl</samp>&rsquo; after a macro call to remove its newline.  &lsquo;<samp>[]dnl</samp>&rsquo;
is recommended instead of &lsquo;<samp>dnl</samp>&rsquo; to ensure that M4 does not
interpret the &lsquo;<samp>dnl</samp>&rsquo; as being attached to the preceding text or
macro output.  For example, instead of:
</p>
<div class="example">
<pre class="example">AC_DEFUN([AC_PATH_X],
[AC_MSG_CHECKING([for X])
AC_REQUIRE_CPP()
<span class="roman"># &hellip;omitted&hellip;</span>
  AC_MSG_RESULT([libraries $x_libraries, headers $x_includes])
fi])
</pre></div>

<p>you would write:
</p>
<div class="example">
<pre class="example">AC_DEFUN([AC_PATH_X],
[AC_REQUIRE_CPP()[]dnl
AC_MSG_CHECKING([for X])
<span class="roman"># &hellip;omitted&hellip;</span>
  AC_MSG_RESULT([libraries $x_libraries, headers $x_includes])
fi[]dnl
])# AC_PATH_X
</pre></div>

<p>If the macro is long, try to split it into logical chunks.  Typically,
macros that check for a bug in a function and prepare its
<code>AC_LIBOBJ</code> replacement should have an auxiliary macro to perform
this setup.  Do not hesitate to introduce auxiliary macros to factor
your code.
</p>
<p>In order to highlight the recommended coding style, here is a macro
written the old way:
</p>
<div class="example">
<pre class="example">dnl Check for EMX on OS/2.
dnl _AC_EMXOS2
AC_DEFUN(_AC_EMXOS2,
[AC_CACHE_CHECK(for EMX OS/2 environment, ac_cv_emxos2,
[AC_COMPILE_IFELSE([AC_LANG_PROGRAM(, return __EMX__;)],
ac_cv_emxos2=yes, ac_cv_emxos2=no)])
test &quot;x$ac_cv_emxos2&quot; = xyes &amp;&amp; EMXOS2=yes])
</pre></div>

<p>and the new way:
</p>
<div class="example">
<pre class="example"># _AC_EMXOS2
# ----------
# Check for EMX on OS/2.
m4_define([_AC_EMXOS2],
[AC_CACHE_CHECK([for EMX OS/2 environment], [ac_cv_emxos2],
[AC_COMPILE_IFELSE([AC_LANG_PROGRAM([], [return __EMX__;])],
                   [ac_cv_emxos2=yes],
                   [ac_cv_emxos2=no])])
test &quot;x$ac_cv_emxos2&quot; = xyes &amp;&amp; EMXOS2=yes[]dnl
])# _AC_EMXOS2
</pre></div>


<div class="header">
<p>
Previous: <a href="Obsoleting-Macros.html#Obsoleting-Macros" accesskey="p" rel="prev">Obsoleting Macros</a>, Up: <a href="Writing-Autoconf-Macros.html#Writing-Autoconf-Macros" accesskey="u" rel="up">Writing Autoconf Macros</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
