<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: aclocal Invocation</title>

<meta name="description" content="Automake and Autoconf Reference Manual: aclocal Invocation">
<meta name="keywords" content="Automake and Autoconf Reference Manual: aclocal Invocation">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Tests-in-configure_002eac.html#Tests-in-configure_002eac" rel="up" title="Tests in configure.ac">
<link href="aclocal-Options.html#aclocal-Options" rel="next" title="aclocal Options">
<link href="Custom-macros.html#Custom-macros" rel="prev" title="Custom macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="aclocal-Invocation"></a>
<div class="header">
<p>
Previous: <a href="Custom-macros.html#Custom-macros" accesskey="p" rel="prev">Custom macros</a>, Up: <a href="Tests-in-configure_002eac.html#Tests-in-configure_002eac" accesskey="u" rel="up">Tests in configure.ac</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="aclocal-Invocation-1"></a>
<h3 class="section">8.7 <code>aclocal</code> Invocation</h3>

<a name="index-Invocation-of-aclocal"></a>
<a name="index-aclocal_002c-Invocation"></a>
<a name="index-Invoking-aclocal"></a>
<a name="index-aclocal_002c-Invoking"></a>

<p>Automake includes a number of Autoconf macros that can be used in
your package (see <a href="Macros-supplied-with-Automake.html#Macros-supplied-with-Automake">Macros supplied with Automake</a>); some of them are 
actually required by
Automake in certain situations.  These macros must be defined in your
<samp>aclocal.m4</samp>; otherwise they will not be seen by
<code>autoconf</code>.
</p>
<p>The <code>aclocal</code> program will automatically generate
<samp>aclocal.m4</samp> files based on the contents of <samp>configure.ac</samp>.
This provides a convenient way to get Automake-provided macros,
without having to search around.  The <code>aclocal</code> mechanism
allows other packages to supply their own macros (see <a href="Extending-aclocal.html#Extending-aclocal">Extending aclocal</a>).  You can also use it to maintain your own set of custom
macros (see <a href="Local-Macros.html#Local-Macros">Local Macros</a>).
</p>
<p>When <code>aclocal</code> is run, it scans your <samp>configure.ac</samp> for
macros that you have used beyond the default ones provided by Autoconf.
These include macros provided with Automake as well as any other custom 
macros.  <code>aclocal</code> makes these macros available to 
<code>autoconf</code> by placing them in a file called <samp>aclocal.m4</samp>.
</p>
<p>However, you should not usually need to run <code>aclocal</code> yourself, 
as it is run automatically by <code>autoreconf</code>.  Using 
<code>autoreconf</code> will protect you against any future changes to the 
<code>aclocal</code> command, which are expected.  (Some people install 
Automake to use <code>aclocal</code> to manage custom M4 macros without 
actually using Automake otherwise, so it&rsquo;s possible that this 
functionality will cease to be offered as part of Automake.)
</p>
<p>We can expand on the flowchart introduced in <a href="System-overview.html#System-overview">System overview</a> to 
show the role of <code>aclocal</code>.
</p>
<div class="example">
<pre class="example">                 configure.ac
[local macros]  ____/|
     |         /     |
     |        |      |
     |        V      |
     `---&gt;(aclocal)  |
              |      |
              V      |
          aclocal.m4 |
               \____ |
                    \|
                     |
                     V
                 (autoconf)
                     |
                     |
                     V
                  configure
</pre></div>

<p>See <a href="Custom-macros.html#Custom-macros">Custom macros</a>.
</p>
<p>At startup, <code>aclocal</code> scans all the <samp>.m4</samp> files it can
find, looking for macro definitions (see <a href="Macro-Search-Path.html#Macro-Search-Path">Macro Search Path</a>).  Then
it scans <samp>configure.ac</samp>.  Any mention of one of the macros found
in the first step causes that macro, and any macros it in turn
requires, to be put into <samp>aclocal.m4</samp>.
</p>
<p><em>Putting</em> the file that contains the macro definition into
<samp>aclocal.m4</samp> is usually done by copying the entire text of this
file, including unused macro definitions as well as both &lsquo;<samp>#</samp>&rsquo; and
&lsquo;<samp>dnl</samp>&rsquo; comments.  If you want to make a comment that will be
completely ignored by <code>aclocal</code>, use &lsquo;<samp>##</samp>&rsquo; as the comment
leader.
</p>
<p>When a file selected by <code>aclocal</code> is located in a subdirectory
specified as a relative search path with <code>aclocal</code>&rsquo;s <samp>-I</samp>
argument, <code>aclocal</code> assumes the file belongs to the package
and uses <code>m4_include</code> instead of copying it into
<samp>aclocal.m4</samp>.  This makes the package smaller, eases dependency
tracking, and cause the file to be distributed automatically.
(See <a href="Local-Macros.html#Local-Macros">Local Macros</a>, for an example.)  Any macro that is found in a
system-wide directory, or via an absolute search path will be copied.
So use &lsquo;<samp>-I `pwd`/reldir</samp>&rsquo; instead of &lsquo;<samp>-I reldir</samp>&rsquo; whenever
some relative directory should be considered outside the package.
</p>
<p>The contents of <samp>acinclude.m4</samp>, if this file exists, are also
automatically included in <samp>aclocal.m4</samp>.  We recommend against
using <samp>acinclude.m4</samp> in new packages (see <a href="Local-Macros.html#Local-Macros">Local Macros</a>).
</p>
<a name="index-AUTOM4TE"></a>
<a name="index-autom4te"></a>
<p>While computing <samp>aclocal.m4</samp>, <code>aclocal</code> runs
<code>autom4te</code> (see <a href="http://www.gnu.org/software/autoconf/manual/html_node/Using-autom4te.html#Using-autom4te">Using <code>Autom4te</code></a> in <cite>The Autoconf Manual</cite>) in order to trace the macros that are
really used, and omit from <samp>aclocal.m4</samp> all macros that are
mentioned but otherwise unexpanded (this can happen when a macro is
called conditionally).  <code>autom4te</code> is expected to be in the
<code>PATH</code>, just as <code>autoconf</code>.  Its location can be
overridden using the <code>AUTOM4TE</code> environment variable.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="aclocal-Options.html#aclocal-Options" accesskey="1">aclocal Options</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Options supported by aclocal
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Macro-Search-Path.html#Macro-Search-Path" accesskey="2">Macro Search Path</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">How aclocal finds .m4 files
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Extending-aclocal.html#Extending-aclocal" accesskey="3">Extending aclocal</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Writing your own aclocal macros
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Local-Macros.html#Local-Macros" accesskey="4">Local Macros</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Organizing local macros
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Serials.html#Serials" accesskey="5">Serials</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">Serial lines in Autoconf macros
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Future-of-aclocal.html#Future-of-aclocal" accesskey="6">Future of aclocal</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">aclocal&rsquo;s scheduled death
</td></tr>
</table>

<div class="header">
<p>
Previous: <a href="Custom-macros.html#Custom-macros" accesskey="p" rel="prev">Custom macros</a>, Up: <a href="Tests-in-configure_002eac.html#Tests-in-configure_002eac" accesskey="u" rel="up">Tests in configure.ac</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
