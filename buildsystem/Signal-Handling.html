<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Signal Handling</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Signal Handling">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Signal Handling">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Portable-Shell.html#Portable-Shell" rel="up" title="Portable Shell">
<link href="File-System-Conventions.html#File-System-Conventions" rel="next" title="File System Conventions">
<link href="File-Descriptors.html#File-Descriptors" rel="prev" title="File Descriptors">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Signal-Handling"></a>
<div class="header">
<p>
Next: <a href="File-System-Conventions.html#File-System-Conventions" accesskey="n" rel="next">File System Conventions</a>, Previous: <a href="File-Descriptors.html#File-Descriptors" accesskey="p" rel="prev">File Descriptors</a>, Up: <a href="Portable-Shell.html#Portable-Shell" accesskey="u" rel="up">Portable Shell</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Signal-Handling-1"></a>
<h4 class="subsection">C.1.5 Signal Handling</h4>
<a name="index-Signal-handling-in-the-shell"></a>
<a name="index-Signals_002c-shells-and"></a>

<p>Portable handling of signals within the shell is another major source of
headaches.  This is worsened by the fact that various different, mutually
incompatible approaches are possible in this area, each with its
distinctive merits and demerits.  A detailed description of these possible
approaches, as well as of their pros and cons, can be found in
<a href="http://www.cons.org/cracauer/sigint.html">this article</a>.
</p>
<p>Solaris 10 <code>/bin/sh</code> automatically traps most signals by default;
the shell still exits with error upon termination by one of those signals,
but in such a case the exit status might be somewhat unexpected (even if
allowed by POSIX, strictly speaking):
</p>
<div class="example">
<pre class="example">$ <kbd>bash -c 'kill -1 $$'; echo $?</kbd> # Will exit 128 + (signal number).
Hangup
129
$ <kbd>/bin/ksh -c 'kill -15 $$'; echo $?</kbd> # Likewise.
Terminated
143
$ <kbd>for sig in 1 2 3 15; do</kbd>
&gt; <kbd>  echo $sig:</kbd>
&gt; <kbd>  /bin/sh -c &quot;kill -$s \$\$&quot;; echo $?</kbd>
&gt; <kbd>done</kbd>
signal 1:
Hangup
129
signal 2:
208
signal 3:
208
signal 15:
208
</pre></div>

<p>This gets even worse if one is using the POSIX &ldquo;wait&rdquo; interface to get
details about the shell process terminations: it will result in the shell
having exited normally, rather than by receiving a signal.
</p>
<div class="example">
<pre class="example">$ <kbd>cat &gt; foo.c &lt;&lt;'END'</kbd>
#include &lt;stdio.h&gt;    /* for printf */
#include &lt;stdlib.h&gt;   /* for system */
#include &lt;sys/wait.h&gt; /* for WIF* macros */
int main(void)
{
  int status = system (&quot;kill -15 $$&quot;);
  printf (&quot;Terminated by signal: %s\n&quot;,
          WIFSIGNALED (status) ? &quot;yes&quot; : &quot;no&quot;);
  printf (&quot;Exited normally: %s\n&quot;,
          WIFEXITED (status) ? &quot;yes&quot; : &quot;no&quot;);
  return 0;
}
END
$ <kbd>cc -o foo foo.c</kbd>
$ <kbd>./a.out</kbd> # On GNU/Linux
Terminated by signal: no
Exited normally: yes
$ <kbd>./a.out</kbd> # On Solaris 10
Terminated by signal: yes
Exited normally: no
</pre></div>

<p>Various shells seem to handle <code>SIGQUIT</code> specially: they ignore it even
if it is not blocked, and even if the shell is not running interactively
(in fact, even if the shell has no attached tty); among these shells
are at least Bash (from version 2 onwards), Zsh 4.3.12, Solaris 10
<code>/bin/ksh</code> and <code>/usr/xpg4/bin/sh</code>, and AT&amp;T <code>ksh93</code> (2011).
Still, <code>SIGQUIT</code> seems to be trappable quite portably within all
these shells.  OTOH, some other shells doesn&rsquo;t special-case the handling
of <code>SIGQUIT</code>; among these shells are at least <code>pdksh</code> 5.2.14,
Solaris 10 and NetBSD 5.1 <code>/bin/sh</code>, and the Almquist Shell 0.5.5.1.
</p>
<p>Some shells (especially Korn shells and derivatives) might try to
propagate to themselves a signal that has killed a child process; this is
not a bug, but a conscious design choice (although its overall value might
be debatable).  The exact details of how this is attained vary from shell
to shell.  For example, upon running <code>perl -e 'kill 2, $$'</code>, after
the perl process has been interrupted AT&amp;T <code>ksh93</code> (2011) will
proceed to send itself a <code>SIGINT</code>, while Solaris 10 <code>/bin/ksh</code>
and <code>/usr/xpg4/bin/sh</code> will proceed to exit with status 130 (i.e.,
128 + 2). In any case, if there is an active trap associated with
<code>SIGINT</code>, those shells will correctly execute it.
</p>
<p>Some Korn shells, when a child process die due receiving a signal with
signal number <var>n</var>, can leave in &lsquo;<samp>$?</samp>&rsquo; an exit status of
256+<var>n</var> instead of the more common 128+<var>n</var>.  Observe the
difference between AT&amp;T <code>ksh93</code> (2011) and <code>bash</code> 4.1.5 on
Debian:
</p>
<div class="example">
<pre class="example">$ <kbd>/bin/ksh -c 'sh -c &quot;kill -1 \$\$&quot;; echo $?'</kbd>
/bin/ksh: line 1: 7837: Hangup
257
$ <kbd>/bin/bash -c 'sh -c &quot;kill -1 \$\$&quot;; echo $?'</kbd>
/bin/bash: line 1:  7861 Hangup        (sh -c &quot;kill -1 \$\$&quot;)
129
</pre></div>

<p>This <code>ksh</code> behavior is allowed by POSIX, if implemented with
due care; see this <a href="http://www.austingroupbugs.net/view.php?id=51">Austin Group discussion</a> for more background.  However, if it is not
implemented with proper care, such a behavior might cause problems
in some corner cases.  To see why, assume we have a &ldquo;wrapper&rdquo; script
like this:
</p>
<div class="example">
<pre class="example">#!/bin/sh
# Ignore some signals in the shell only, not in its child processes.
trap : 1 2 13 15
wrapped_command &quot;$@&quot;
ret=$?
other_command
exit $ret
</pre></div>

<p>If <code>wrapped_command</code> is interrupted by a <code>SIGHUP</code> (which
has signal number 1), <code>ret</code> will be set to 257.  Unless the
<code>exit</code> shell builtin is smart enough to understand that such
a value can only have originated from a signal, and adjust the final
wait status of the shell appropriately, the value 257 will just get
truncated to 1 by the closing <code>exit</code> call, so that a caller of
the script will have no way to determine that termination by a signal
was involved.  Observe the different behavior of AT&amp;T <code>ksh93</code>
(2011) and <code>bash</code> 4.1.5 on Debian:
</p>
<div class="example">
<pre class="example">$ <kbd>cat foo.sh</kbd>
#!/bin/sh
sh -c 'kill -1 $$'
ret=$?
echo $ret
exit $ret
$ <kbd>/bin/ksh foo.sh; echo $?</kbd>
foo.sh: line 2: 12479: Hangup
257
1
$ <kbd>/bin/bash foo.sh; echo $?</kbd>
foo.sh: line 2: 12487 Hangup        (sh -c 'kill -1 $$')
129
129
</pre></div>

<div class="header">
<p>
Next: <a href="File-System-Conventions.html#File-System-Conventions" accesskey="n" rel="next">File System Conventions</a>, Previous: <a href="File-Descriptors.html#File-Descriptors" accesskey="p" rel="prev">File Descriptors</a>, Up: <a href="Portable-Shell.html#Portable-Shell" accesskey="u" rel="up">Portable Shell</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
