<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Macros and Submakes</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Macros and Submakes">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Macros and Submakes">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Portable-Make-Programming.html#Portable-Make-Programming" rel="up" title="Portable Make Programming">
<link href="The-Make-Macro-MAKEFLAGS.html#The-Make-Macro-MAKEFLAGS" rel="next" title="The Make Macro MAKEFLAGS">
<link href="Long-Lines-in-Makefiles.html#Long-Lines-in-Makefiles" rel="prev" title="Long Lines in Makefiles">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Macros-and-Submakes"></a>
<div class="header">
<p>
Next: <a href="The-Make-Macro-MAKEFLAGS.html#The-Make-Macro-MAKEFLAGS" accesskey="n" rel="next">The Make Macro MAKEFLAGS</a>, Previous: <a href="Long-Lines-in-Makefiles.html#Long-Lines-in-Makefiles" accesskey="p" rel="prev">Long Lines in Makefiles</a>, Up: <a href="Portable-Make-Programming.html#Portable-Make-Programming" accesskey="u" rel="up">Portable Make Programming</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="make-macro_003dvalue-and-Submakes"></a>
<h4 class="subsection">C.2.7 <code>make macro=value</code> and Submakes</h4>

<p>A command-line variable definition such as <code>foo=bar</code> overrides any
definition of <code>foo</code> in a makefile.  Some <code>make</code>
implementations (such as GNU <code>make</code>) propagate this
override to subsidiary invocations of <code>make</code>.  Some other
implementations do not pass the substitution along to submakes.
</p>
<div class="example">
<pre class="example">$ <kbd>cat Makefile</kbd>
foo = foo
one:
        @echo $(foo)
        $(MAKE) two
two:
        @echo $(foo)
$ <kbd>make foo=bar</kbd>            # GNU make 3.79.1
bar
make two
make[1]: Entering directory `/home/adl'
bar
make[1]: Leaving directory `/home/adl'
$ <kbd>pmake foo=bar</kbd>           # BSD make
bar
pmake two
foo
</pre></div>

<p>You have a few possibilities if you do want the <code>foo=bar</code> override
to propagate to submakes.  One is to use the <samp>-e</samp>
option, which causes all environment variables to have precedence over
the makefile macro definitions, and declare foo as an environment
variable:
</p>
<div class="example">
<pre class="example">$ <kbd>env foo=bar make -e</kbd>
</pre></div>

<p>The <samp>-e</samp> option is propagated to submakes automatically,
and since the environment is inherited between <code>make</code>
invocations, the <code>foo</code> macro is overridden in
submakes as expected.
</p>
<p>This syntax (<code>foo=bar make -e</code>) is portable only when used
outside of a makefile, for instance from a script or from the
command line.  When run inside a <code>make</code> rule, GNU
<code>make</code> 3.80 and prior versions forget to propagate the
<samp>-e</samp> option to submakes.
</p>
<p>Moreover, using <samp>-e</samp> could have unexpected side effects if your
environment contains some other macros usually defined by the
makefile.  (See also the note about <code>make -e</code> and <code>SHELL</code>
below.)
</p>
<p>If you can foresee all macros that a user might want to override, then
you can propagate them to submakes manually, from your makefile:
</p>
<div class="example">
<pre class="example">foo = foo
one:
        @echo $(foo)
        $(MAKE) foo=$(foo) two
two:
        @echo $(foo)
</pre></div>

<p>Another way to propagate a variable to submakes in a portable way is to
expand an extra variable in every invocation of &lsquo;<samp>$(MAKE)</samp>&rsquo; within
your makefile:
</p>
<div class="example">
<pre class="example">foo = foo
one:
        @echo $(foo)
        $(MAKE) $(SUBMAKEFLAGS) two
two:
        @echo $(foo)
</pre></div>

<p>Users must be aware that this technique is in use to take advantage of
it, e.g. with <code>make foo=bar SUBMAKEFLAGS='foo=bar'</code>, but it
allows any macro to be overridden.  Makefiles generated by
<code>automake</code> use this technique, expanding <code>$(AM_MAKEFLAGS)</code>
on the command lines of submakes (see <a href="http://www.gnu.org/software/automake/manual/html_node/Subdirectories.html#Subdirectories">Automake</a> in <cite>GNU Automake</cite>).
</p>
<div class="header">
<p>
Next: <a href="The-Make-Macro-MAKEFLAGS.html#The-Make-Macro-MAKEFLAGS" accesskey="n" rel="next">The Make Macro MAKEFLAGS</a>, Previous: <a href="Long-Lines-in-Makefiles.html#Long-Lines-in-Makefiles" accesskey="p" rel="prev">Long Lines in Makefiles</a>, Up: <a href="Portable-Make-Programming.html#Portable-Make-Programming" accesskey="u" rel="up">Portable Make Programming</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
