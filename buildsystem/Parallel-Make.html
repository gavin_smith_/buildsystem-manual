<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: Parallel Make</title>

<meta name="description" content="Automake and Autoconf Reference Manual: Parallel Make">
<meta name="keywords" content="Automake and Autoconf Reference Manual: Parallel Make">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Portable-Make-Programming.html#Portable-Make-Programming" rel="up" title="Portable Make Programming">
<link href="Comments-in-Make-Rules.html#Comments-in-Make-Rules" rel="next" title="Comments in Make Rules">
<link href="The-Make-Macro-SHELL.html#The-Make-Macro-SHELL" rel="prev" title="The Make Macro SHELL">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="Parallel-Make"></a>
<div class="header">
<p>
Next: <a href="Comments-in-Make-Rules.html#Comments-in-Make-Rules" accesskey="n" rel="next">Comments in Make Rules</a>, Previous: <a href="The-Make-Macro-SHELL.html#The-Make-Macro-SHELL" accesskey="p" rel="prev">The Make Macro SHELL</a>, Up: <a href="Portable-Make-Programming.html#Portable-Make-Programming" accesskey="u" rel="up">Portable Make Programming</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="Parallel-Make-1"></a>
<h4 class="subsection">C.2.10 Parallel Make</h4>
<a name="index-Parallel-make"></a>

<p>Support for parallel execution in <code>make</code> implementation varies.
Generally, using GNU make is your best bet.
</p>
<p>When NetBSD or FreeBSD <code>make</code> are run in parallel mode, they will
reuse the same shell for multiple commands within one recipe.  This can
have various unexpected consequences.  For example, changes of directories
or variables persist between recipes, so that:
</p>
<div class="example">
<pre class="example">all:
        @var=value; cd /; pwd; echo $$var; echo $$$$
        @pwd; echo $$var; echo $$$$
</pre></div>

<p>may output the following with <code>make -j1</code>, at least on NetBSD up to
5.1 and FreeBSD up to 8.2:
</p>
<div class="example">
<pre class="example">/
value
32235
/
value
32235
</pre></div>

<p>while without <samp>-j1</samp>, or with <samp>-B</samp>, the output looks less
surprising:
</p>
<div class="example">
<pre class="example">/
value
32238
/tmp

32239
</pre></div>

<p>Another consequence is that, if one command in a recipe uses <code>exit 0</code>
to indicate a successful exit, the shell will be gone and the remaining
commands of this recipe will not be executed.
</p>
<p>The BSD <code>make</code> implementations, when run in parallel mode,
will also pass the <code>Makefile</code> recipes to the shell through
its standard input, thus making it unusable from the recipes:
</p>
<div class="example">
<pre class="example">$ <kbd>cat Makefile</kbd>
read:
        @read line; echo LINE: $$line
$ <kbd>echo foo | make read</kbd>
LINE: foo
$ <kbd>echo foo | make -j1 read</kbd> # NetBSD 5.1 and FreeBSD 8.2
LINE:
</pre></div>

<p>Moreover, when FreeBSD <code>make</code> (up at least to 8.2) is run in
parallel mode, it implements the <code>@</code> and <code>-</code> &ldquo;recipe
modifiers&rdquo; by dynamically modifying the active shell flags.  This
behavior has the effects of potentially clobbering the exit status
of recipes silenced with the <code>@</code> modifier if they also unset
the <samp>errexit</samp> shell flag, and of mangling the output in
unexpected ways:
</p>
<div class="example">
<pre class="example">$ <kbd>cat Makefile</kbd>
a:
        @echo $$-; set +e; false
b:
        -echo $$-; false; echo set -
$ <kbd>make a; echo status: $?</kbd>
ehBc
*** Error code 1
status: 1
$ <kbd>make -j1 a; echo status: $?</kbd>
ehB
status: 0
$ <kbd>make b</kbd>
echo $-; echo set -
hBc
set -
$ <kbd>make -j1 b</kbd>
echo $-; echo hvB
</pre></div>

<p>You can avoid all these issues by using the <samp>-B</samp> option to enable
compatibility semantics.  However, that will effectively also disable
all parallelism as that will cause prerequisites to be updated in the
order they are listed in a rule.
</p>
<p>Some make implementations (among them, FreeBSD <code>make</code>, NetBSD
<code>make</code>, and Solaris <code>dmake</code>), when invoked with a
<samp>-j<var>N</var></samp> option, connect the standard output and standard
error of all their child processes to pipes or temporary regular
files.  This can lead to subtly different semantics in the behavior
of the spawned processes.  For example, even if the <code>make</code>
standard output is connected to a tty, the recipe command will not be:
</p>
<div class="example">
<pre class="example">$ <kbd>cat Makefile</kbd>
all:
        @test -t 1 &amp;&amp; echo &quot;Is a tty&quot; || echo &quot;Is not a tty&quot;
$ <kbd>make -j 2</kbd> # FreeBSD 8.2 make
Is not a tty
$ <kbd>make -j 2</kbd> # NetBSD 5.1 make
--- all ---
Is not a tty
$ <kbd>dmake -j 2</kbd> # Solaris 10 dmake
<var>hostname</var> --&gt; 1 job
<var>hostname</var> --&gt; Job output
Is not a tty
</pre></div>

<p>On the other hand:
</p>
<div class="example">
<pre class="example">$ <kbd>make -j 2</kbd> # GNU make, Heirloom make
Is a tty
</pre></div>

<p>The above examples also show additional status output produced in parallel
mode for targets being updated by Solaris <code>dmake</code> and NetBSD
<code>make</code> (but <em>not</em> by FreeBSD <code>make</code>).
</p>
<p>Furthermore, parallel runs of those <code>make</code> implementations will
route standard error from commands that they spawn into their own
standard output, and may remove leading whitespace from output lines.
</p>

<div class="header">
<p>
Next: <a href="Comments-in-Make-Rules.html#Comments-in-Make-Rules" accesskey="n" rel="next">Comments in Make Rules</a>, Previous: <a href="The-Make-Macro-SHELL.html#The-Make-Macro-SHELL" accesskey="p" rel="prev">The Make Macro SHELL</a>, Up: <a href="Portable-Make-Programming.html#Portable-Make-Programming" accesskey="u" rel="up">Portable Make Programming</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
