<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
This manual is for GNU Automake (version VERSION,
UPDATED, a program that creates GNU standards-compliant
Makefiles from template files,
and for GNU Autoconf
(version VERSION),
a package for creating scripts to configure source code packages using
templates and an M4 macro package.
Copyright (C) 1992-2014 Free Software Foundation, Inc. 

Copyright (C) 2015 Gavin Smith

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover texts,
and with no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License." -->
<!-- Created by GNU Texinfo 6.0dev, http://www.gnu.org/software/texinfo/ -->
<head>
<title>Automake and Autoconf Reference Manual: File System Conventions</title>

<meta name="description" content="Automake and Autoconf Reference Manual: File System Conventions">
<meta name="keywords" content="Automake and Autoconf Reference Manual: File System Conventions">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Indices.html#Indices" rel="index" title="Indices">
<link href="buildsystem_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Portable-Shell.html#Portable-Shell" rel="up" title="Portable Shell">
<link href="Shell-Pattern-Matching.html#Shell-Pattern-Matching" rel="next" title="Shell Pattern Matching">
<link href="Signal-Handling.html#Signal-Handling" rel="prev" title="Signal Handling">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nocodebreak {white-space: nowrap}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: serif; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="layout.css">


</head>

<body lang="en">
<a name="File-System-Conventions"></a>
<div class="header">
<p>
Next: <a href="Shell-Pattern-Matching.html#Shell-Pattern-Matching" accesskey="n" rel="next">Shell Pattern Matching</a>, Previous: <a href="Signal-Handling.html#Signal-Handling" accesskey="p" rel="prev">Signal Handling</a>, Up: <a href="Portable-Shell.html#Portable-Shell" accesskey="u" rel="up">Portable Shell</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>

<a name="File-System-Conventions-1"></a>
<h4 class="subsection">C.1.6 File System Conventions</h4>
<a name="index-File-system-conventions"></a>

<p>Autoconf uses shell-script processing extensively, so the file names
that it processes should not contain characters that are special to the
shell.  Special characters include space, tab, newline, NUL, and
the following:
</p>
<div class="example">
<pre class="example">&quot; # $ &amp; ' ( ) * ; &lt; = &gt; ? [ \ ` |
</pre></div>

<p>Also, file names should not begin with &lsquo;<samp>~</samp>&rsquo; or &lsquo;<samp>-</samp>&rsquo;, and should
contain neither &lsquo;<samp>-</samp>&rsquo; immediately after &lsquo;<samp>/</samp>&rsquo; nor &lsquo;<samp>~</samp>&rsquo;
immediately after &lsquo;<samp>:</samp>&rsquo;.  On Posix-like platforms, directory names
should not contain &lsquo;<samp>:</samp>&rsquo;, as this runs afoul of &lsquo;<samp>:</samp>&rsquo; used as the
path separator.
</p>
<p>These restrictions apply not only to the files that you distribute, but
also to the absolute file names of your source, build, and destination
directories.
</p>
<p>On some Posix-like platforms, &lsquo;<samp>!</samp>&rsquo; and &lsquo;<samp>^</samp>&rsquo; are special too, so
they should be avoided.
</p>
<p>Posix lets implementations treat leading <samp>//</samp> specially, but
requires leading <samp>///</samp> and beyond to be equivalent to <samp>/</samp>.
Most Unix variants treat <samp>//</samp> like <samp>/</samp>.  However, some treat
<samp>//</samp> as a &ldquo;super-root&rdquo; that can provide access to files that are
not otherwise reachable from <samp>/</samp>.  The super-root tradition began
with Apollo Domain/OS, which died out long ago, but unfortunately Cygwin
has revived it.
</p>
<p>While <code>autoconf</code> and friends are usually run on some Posix
variety, they can be used on other systems, most notably DOS
variants.  This impacts several assumptions regarding file names.
</p>
<p>For example, the following code:
</p>
<div class="example">
<pre class="example">case $foo_dir in
  /*) # Absolute
     ;;
  *)
     foo_dir=$dots$foo_dir ;;
esac
</pre></div>

<p>fails to properly detect absolute file names on those systems, because
they can use a drivespec, and usually use a backslash as directory
separator.  If you want to be portable to DOS variants (at the
price of rejecting valid but oddball Posix file names like <samp>a:\b</samp>),
you can check for absolute file names like this:
</p>
<a name="index-absolute-file-names_002c-detect"></a>
<div class="example">
<pre class="example">case $foo_dir in
  [\\/]* | ?:[\\/]* ) # Absolute
     ;;
  *)
     foo_dir=$dots$foo_dir ;;
esac
</pre></div>

<p>Make sure you quote the brackets if appropriate and keep the backslash as
first character (see <a href="Limitations-of-Builtins.html#case">Limitations of Shell Builtins</a>).
</p>
<p>Also, because the colon is used as part of a drivespec, these systems don&rsquo;t
use it as path separator.  When creating or accessing paths, you can use the
<code>PATH_SEPARATOR</code> output variable instead.  <code>configure</code> sets this
to the appropriate value for the build system (&lsquo;<samp>:</samp>&rsquo; or &lsquo;<samp>;</samp>&rsquo;) when it
starts up.
</p>
<p>File names need extra care as well.  While DOS variants
that are Posixy enough to run <code>autoconf</code> (such as DJGPP)
are usually able to handle long file names properly, there are still
limitations that can seriously break packages.  Several of these issues
can be easily detected by the
<a href="ftp://ftp.gnu.org/gnu/non-gnu/doschk/doschk-1.1.tar.gz">doschk</a>
package.
</p>
<p>A short overview follows; problems are marked with SFN/LFN to
indicate where they apply: SFN means the issues are only relevant to
plain DOS, not to DOS under Microsoft Windows
variants, while LFN identifies problems that exist even under
Microsoft Windows variants.
</p>
<dl compact="compact">
<dt>No multiple dots (SFN)</dt>
<dd><p>DOS cannot handle multiple dots in file names.  This is an especially
important thing to remember when building a portable configure script,
as <code>autoconf</code> uses a .in suffix for template files.
</p>
<p>This is perfectly OK on Posix variants:
</p>
<div class="example">
<pre class="example">AC_CONFIG_HEADERS([config.h])
AC_CONFIG_FILES([source.c foo.bar])
AC_OUTPUT
</pre></div>

<p>but it causes problems on DOS, as it requires &lsquo;<samp>config.h.in</samp>&rsquo;,
&lsquo;<samp>source.c.in</samp>&rsquo; and &lsquo;<samp>foo.bar.in</samp>&rsquo;.  To make your package more portable
to DOS-based environments, you should use this instead:
</p>
<div class="example">
<pre class="example">AC_CONFIG_HEADERS([config.h:config.hin])
AC_CONFIG_FILES([source.c:source.cin foo.bar:foobar.in])
AC_OUTPUT
</pre></div>

</dd>
<dt>No leading dot (SFN)</dt>
<dd><p>DOS cannot handle file names that start with a dot.  This is usually
not important for <code>autoconf</code>.
</p>
</dd>
<dt>Case insensitivity (LFN)</dt>
<dd><p>DOS is case insensitive, so you cannot, for example, have both a
file called &lsquo;<samp>INSTALL</samp>&rsquo; and a directory called &lsquo;<samp>install</samp>&rsquo;.  This
also affects <code>make</code>; if there&rsquo;s a file called &lsquo;<samp>INSTALL</samp>&rsquo; in
the directory, &lsquo;<samp>make install</samp>&rsquo; does nothing (unless the
&lsquo;<samp>install</samp>&rsquo; target is marked as PHONY).
</p>
</dd>
<dt>The 8+3 limit (SFN)</dt>
<dd><p>Because the DOS file system only stores the first 8 characters of
the file name and the first 3 of the extension, those must be unique.
That means that <samp>foobar-part1.c</samp>, <samp>foobar-part2.c</samp> and
<samp>foobar-prettybird.c</samp> all resolve to the same file name
(<samp>FOOBAR-P.C</samp>).  The same goes for <samp>foo.bar</samp> and
<samp>foo.bartender</samp>.
</p>
<p>The 8+3 limit is not usually a problem under Microsoft Windows, as it
uses numeric
tails in the short version of file names to make them unique.  However, a
registry setting can turn this behavior off.  While this makes it
possible to share file trees containing long file names between SFN
and LFN environments, it also means the above problem applies there
as well.
</p>
</dd>
<dt>Invalid characters (LFN)</dt>
<dd><p>Some characters are invalid in DOS file names, and should therefore
be avoided.  In a LFN environment, these are &lsquo;<samp>/</samp>&rsquo;, &lsquo;<samp>\</samp>&rsquo;,
&lsquo;<samp>?</samp>&rsquo;, &lsquo;<samp>*</samp>&rsquo;, &lsquo;<samp>:</samp>&rsquo;, &lsquo;<samp>&lt;</samp>&rsquo;, &lsquo;<samp>&gt;</samp>&rsquo;, &lsquo;<samp>|</samp>&rsquo; and &lsquo;<samp>&quot;</samp>&rsquo;.
In a SFN environment, other characters are also invalid.  These
include &lsquo;<samp>+</samp>&rsquo;, &lsquo;<samp>,</samp>&rsquo;, &lsquo;<samp>[</samp>&rsquo; and &lsquo;<samp>]</samp>&rsquo;.
</p>
</dd>
<dt>Invalid names (LFN)</dt>
<dd><p>Some DOS file names are reserved, and cause problems if you
try to use files with those names.  These names include <samp>CON</samp>,
<samp>AUX</samp>, <samp>COM1</samp>, <samp>COM2</samp>, <samp>COM3</samp>, <samp>COM4</samp>,
<samp>LPT1</samp>, <samp>LPT2</samp>, <samp>LPT3</samp>, <samp>NUL</samp>, and <samp>PRN</samp>.
File names are case insensitive, so even names like
<samp>aux/config.guess</samp> are disallowed.
</p>
</dd>
</dl>

<div class="header">
<p>
Next: <a href="Shell-Pattern-Matching.html#Shell-Pattern-Matching" accesskey="n" rel="next">Shell Pattern Matching</a>, Previous: <a href="Signal-Handling.html#Signal-Handling" accesskey="p" rel="prev">Signal Handling</a>, Up: <a href="Portable-Shell.html#Portable-Shell" accesskey="u" rel="up">Portable Shell</a> &nbsp; [<a href="buildsystem_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Indices.html#Indices" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
